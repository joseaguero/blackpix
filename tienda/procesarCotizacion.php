<?php
//conf
include('../admin/conf.php');

require_once('../PHPMailer/PHPMailerAutoload.php');
// Include functions
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
$nombre  = (isset($_POST[nombre])) ? mysqli_real_escape_string($conexion, $_POST[nombre]) : 0;
$apellido  = (isset($_POST[apellido])) ? mysqli_real_escape_string($conexion, $_POST[apellido]) : 0;
$telefono  = (isset($_POST[telefono])) ? mysqli_real_escape_string($conexion, $_POST[telefono]) : 0;
$email = (isset($_POST[email])) ? mysqli_real_escape_string($conexion, $_POST[email]) : 0;
$rut  = (isset($_POST[rut])) ? mysqli_real_escape_string($conexion, $_POST[rut]) : 0;
$comentarios  = (isset($_POST[comentarios])) ? mysqli_real_escape_string($conexion, $_POST[comentarios]) : 0;

if(isset($_COOKIE[usuario_id])){
	$current_user = $_COOKIE[usuario_id];
	$idClienteInsert = $_COOKIE[usuario_id];
}



$date = date('Ymdhis');
$oc = "COT_$date";


$subtotal = round(get_total_price($comuna_id));
$total = $subtotal;

//CART EXPLODE AND COUNT ITEMS
if(!isset($_COOKIE[cotizacion])){
	setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
	}  
$cart = $_COOKIE[cotizacion];
$items = explode(',',$cart);
foreach ($items as $item) {
	$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
}


if ($_COOKIE['cotizacion']!="") {
	
	$estado_pendiente = 1;
	$campos = "oc, 
	cliente_id, 
	fecha, 
	estado_id, 
	total, 
	nombre, 
	apellido,
	telefono, 
	email, 
	rut, 
	comentarios";
	$values = "'$oc', $current_user, NOW(), $estado_pendiente, $total, '$nombre', '$apellido', '$telefono', '$email', '$rut', '$comentarios'";
	
    $generar_pedido = insert_bd("cotizaciones","$campos","$values");
			
	$cotizacion_id = mysqli_insert_id($conexion);
	$cantTotal = 0;
		
		foreach ($contents as $id=>$qty) {
			//consulto el precio segun la lista de precios
			$prod = consulta_bd("pd.precio, pd.descuento, p.pack, p.codigos, pd.sku","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
			////Veo si el producto es un pack, lo manejo de cierta forma///////////////////////
			if($prod[0][2] == 1){
				
				$precio_final = $prod[0][0];
				$codigo_pack = $prod[0][4];
				$codigosProductos = trim($prod[0][3]);
				$productosPack = explode(",", $codigosProductos);
				
				
				//saco la suma de los productos reales
				$totalReal = 0;
				foreach($productosPack as $skuPack) {
					$valorProdDetalle = consulta_bd("precio","productos_detalles","sku='$skuPack'","");
					$totalReal += $valorProdDetalle[0][0];
				}
				//obtengo el porcentaje de descuento
				$descPorcentajeProducto = ($totalReal - $precio_final)/$totalReal;
				
				
				$retorno = "";
				foreach($productosPack as $skuPack) {
					$valorProdDetalle2 = consulta_bd("precio, id","productos_detalles","sku='$skuPack'","");
					$idProductoDetalle = $valorProdDetalle2[0][1];
					$precioUnitario = $valorProdDetalle2[0][0] * (1 - $descPorcentajeProducto);
					$precioTotal = round($precioUnitario * $qty);
					
					$sku = $skuPack;
					/* $precio = $all_sku[0][1]; */
		
					$campos = "cotizacion_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion, codigo_pack";
					$values = "$cotizacion_id,'$idProductoDetalle','$qty','$precioUnitario','$sku', $precioTotal, NOW(), '$codigo_pack'";
					$detalle = insert_bd("productos_cotizaciones","$campos","$values");
					$cantTotal = $cantTotal + $qty;
				}//fin foreach
				
				
				
			} else {
				
				if($prod[0][1] > 0){
					$precio_final = $prod[0][1];
					$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
				}else{
					$precio_final = $prod[0][0];
					$descuento = 0;
				}
				
				$precioUnitario = $precio_final;
				$precioTotal = $precioUnitario * $qty;
				$porcentaje_descuento = $descuento;
				
				$all_sku = consulta_bd("sku","productos_detalles","id=$id","");
				$sku = $all_sku[0][0];
				/* $precio = $all_sku[0][1]; */
	
				$campos = "cotizacion_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion, descuento";
				$values = "$cotizacion_id, $id, $qty, $precioUnitario,'$sku', $precioTotal, NOW(), $porcentaje_descuento";
				$detalle = insert_bd("productos_cotizaciones","$campos","$values");
				$cantTotal = $cantTotal + $qty;
			}//fin del ciclo
		}

		$qtyFinal = update_bd("cotizaciones","cant_productos = $cantTotal, fecha_creacion = NOW()","id=$cotizacion_id");
		
		//elimino cookie
		setcookie('cotizacion', null, -1, '/');
		
		//envio notificacion
		enviarComprobanteClienteCotizacion($oc);
        enviarComprobanteAdminCotizacion($oc);
		
        header("location: ../exito-cotizacion");
		
	
} else {
	header("location:404");
}
?>
