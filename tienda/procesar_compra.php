<?php
include('../admin/conf.php');
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
require_once "mercadopago/lib/mercadopago.php";
include('mercadopago/claves.php');

date_default_timezone_set('America/Santiago');

/* CLIENTE LOGEADO */
$sesionIniciada 	= (isset($_COOKIE['usuario_id']) and $_COOKIE['usuario_id'] != NULL) ? true : false;

/* DATOS USUARIO */
$nombre 			= mysqli_real_escape_string($conexion, $_POST['nombre']);
$email 				= strtolower(mysqli_real_escape_string($conexion, $_POST['email']));
$telefono 			= mysqli_real_escape_string($conexion, $_POST['telefono']);

/* FACTURACIÓN */
$tipoDocumento 		= mysqli_real_escape_string($conexion, $_POST['tipoDocumento']);
$rutFactura 		= mysqli_real_escape_string($conexion, $_POST['rut_factura']);
$razonSocial 		= mysqli_real_escape_string($conexion, $_POST['razon_social']);
$regionFactura 		= mysqli_real_escape_string($conexion, $_POST['region_factura']);
$comunaFactura 		= mysqli_real_escape_string($conexion, $_POST['comuna_factura']);
$direccionFactura 	= mysqli_real_escape_string($conexion, $_POST['direccion_factura']);
$giro 				= mysqli_real_escape_string($conexion, $_POST['giro']);

/* DATOS ENVÍO O RETIRO */
$tipoEnvio 			= mysqli_real_escape_string($conexion, $_POST['tipoEnvio']);
$sucursal 			= mysqli_real_escape_string($conexion, $_POST['sucursal']);
$nombreRetiro 		= mysqli_real_escape_string($conexion, $_POST['nombre_retiro']);
$rutRetiro 			= mysqli_real_escape_string($conexion, $_POST['rut_retiro']);
$pais				= mysqli_real_escape_string($conexion, $_POST['pais']);
$region 			= mysqli_real_escape_string($conexion, $_POST['region']);
$comuna 			= mysqli_real_escape_string($conexion, $_POST['comuna']);
$direccion 			= mysqli_real_escape_string($conexion, $_POST['direccion']);
$comentarios 		= mysqli_real_escape_string($conexion, $_POST['comentarios']);

/* MÉTODO DE PAGO */
$medioPago 			= mysqli_real_escape_string($conexion, $_POST['metodoPago']);

/* FECHA MODIFICACIÓN Y/O CREACIÓN (AÑO-MES-DIA HORA:MINUTO:SEGUNDO) */
$fechaHoy 			= date("Y-m-d H:i:s", time());

/* ////////////////////////////////////////////////////////////////////////////////// */
/* ////////////////////////////////////////////////////////////////////////////////// */
/* ////////////////////////////////////////////////////////////////////////////////// */

/* CONDICIONES USUARIO */
if ($sesionIniciada) :

	$clienteId = (int)$_COOKIE['usuario_id'];
	$updateDatosUsuario = update_bd('clientes', "nombre = '$nombre', email = '$email', telefono = '$telefono', fecha_modificacion = '$fechaHoy'", "id = '$clienteId'");

else :

	$existeEmail = consulta_bd('id', 'clientes', "email = '$email'", '');
	if (is_array($existeEmail)) :

		$clienteId = (int)$existeEmail[0][0];
		$updateDatosUsuario = update_bd('clientes', "nombre = '$nombre', email = '$email', telefono = '$telefono', fecha_modificacion = '$fechaHoy'", "id = '$clienteId'");

	else :

		$insertUsuario = insert_bd('clientes', 'nombre, email, telefono, fecha_creacion, activo', "'$nombre', '$email', '$telefono', '$fechaHoy', 0");
		$clienteId = mysqli_insert_id($conexion);

	endif;
endif;

/* CONDICIONES DIRECCIONES */
if ($tipoEnvio == 2) :
	// Despacho
	$datosDespacho = consulta_bd('r.nombre, c.nombre', 'comunas c JOIN regiones r ON c.region_id = r.id', "r.id = $region AND c.id = $comuna", '');

	$nombreRegion = $datosDespacho[0][0];
	$nombreComuna = $datosDespacho[0][1];

	$nombreRetiro = '';
	$rutRetiro = '';
	$sucursal = 0;

	$retiroEnTienda = 0;

elseif ($tipoEnvio == 1) :
	// Retiro
	$datosSucursal = consulta_bd('s.direccion, r.nombre, c.nombre', 'sucursales s JOIN comunas c ON c.id = s.comuna_id JOIN regiones r ON r.id = c.region_id', "s.id = $sucursal", '');

	$nombreRegion = $datosSucursal[0][1];
	$nombreComuna = $datosSucursal[0][2];
	$direccion = $datosSucursal[0][0];

	$region = 0;
	$comuna = 0;

	$comentarios = '';

	$retiroEnTienda = 1;
else :
	header("Location: envio-y-pago?a=0&msje=Tipo de envio incorrecto");
endif;

/* CONDICIONES FACTURA */
if ($tipoDocumento == 1) :

	$esFactura 				= 0;

	$rutFactura 			= '';
	$razonSocial 			= '';
	$giro 					= '';
	$direccionFactura 		= '';

	$nombreRegionFactura 	= '';
	$nombreComunaFactura 	= '';

else :

	$esFactura 				= 1;

	$datosDirFactura 		= consulta_bd('r.nombre, c.nombre', 'comunas c JOIN regiones r ON r.id = c.region_id', "r.id = $regionFactura AND c.id = $comunaFactura", '');

	$nombreRegionFactura 	= $datosDirFactura[0][0];
	$nombreComunaFactura 	= $datosDirFactura[0][1];

endif;

/* ////////////////////////////////////////////////////////////////////////////////// */
/* ////////////////////////////////////////////////////////////////////////////////// */
/* ////////////////////////////////////////////////////////////////////////////////// */

$totalesCarro = valorTotalCarro($comuna);

$subtotal 	= $totalesCarro['subtotal'] + $totalesCarro['iva'];
$despacho 	= (is_numeric($totalesCarro['despacho'])) ? $totalesCarro['despacho'] : 0;

$date 		= date('Ymdhis');
$oc 		= "OC_$date";

// DESCUENTO
$cod = 'null';
if ($_SESSION["descuento"]) :
	$valor_descuento = $_SESSION['val_descuento'];
	$cod = $_SESSION['descuento'];
else :
	$valor_descuento = 0;
	$cod = '';
endif;

//TOTAL
$total = $subtotal + $despacho - $valor_descuento;

$carro = cartPrepare();

////////////////// ----------- REVISA STOCK TEMPORAL ----------- //////////////////

foreach ($carro as $item) :

	$qty = $item['cantidad'];
	$id = $item['id'];

	$isPack = consulta_bd("p.pack, p.codigos", "productos p, productos_detalles pd", "p.id = pd.producto_id AND pd.id = $id", "");

	if ($isPack[0][0]) :

		$codes_pack = explode(',', $isPack[0][1]);
		foreach ($codes_pack as $value) :
			$value 			= trim($value);
			$stock 			= consulta_bd("id, stock, stock_reserva, sku", "productos_detalles", "sku='$value'", "");
			$stock_temporal = consulta_bd("stock", "stock_temporal", "sku='" . $stock[0][3] . "'", "");
			if ($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])) {
				header("location: mi-carro?stock=$id");
				die();
			}
		endforeach;

	else :
		$stock 			= consulta_bd("id, stock, stock_reserva, sku", "productos_detalles", "id=$id", "");
		$stock_temporal = consulta_bd("stock", "stock_temporal", "sku='" . $stock[0][3] . "'", "");
		if ($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])) :
			header("location: mi-carro?stock=$id");
			die();
		endif;
	endif;
endforeach;

////////////////// ----------- ////////////////// ----------- //////////////////

if ($_COOKIE['cart_alfa_cm'] != "") :

	$estado_pendiente = 1;

	$campos = "oc, 
	cliente_id, 
	fecha, 
	estado_id, 
	codigo_descuento, 
	descuento, 
	total,
	valor_despacho,
	total_pagado,
	nombre,
	telefono,
	email,
	direccion,
	comuna,
	region,
	giro,  
	retiro_en_tienda, 
	retiro_en_tienda_id,
	factura, razon_social, 
	direccion_factura, 
	rut_factura, 
	comentarios_envio, 
	medio_de_pago, 
	id_notificacion, 
	nombre_retiro,
	rut_retiro,
	region_factura,
	comuna_factura";
	$values = "NULL, '$clienteId', NOW(), $estado_pendiente, '$cod', '$valor_descuento', $subtotal, $despacho, $total, '$nombre', '$telefono', '$email', '$direccion', '$nombreComuna', '$nombreRegion', '$giro', $retiroEnTienda, $sucursal, $esFactura, '$razonSocial', '$direccionFactura', '$rutFactura', '$comentarios', '$medioPago', 0, '$nombreRetiro','$rutRetiro','$nombreRegionFactura','$nombreComunaFactura'";

	$generar_pedido = insert_bd("pedidos", "$campos", "$values");

	$pedido_id = mysqli_insert_id($conexion);

	$new_oc = $oc . $pedido_id;
	update_bd('pedidos', "oc = '$new_oc'", "id = $pedido_id");

	/* CARRO ABANDONADO */
	/*if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
	$pdca = $_COOKIE[cart_alfa_cm];
	update_bd("carros_abandonados","oc='$new_oc'","productos = '$pdca' and correo = '$info_email'");
	$notificacion = (isset($_SESSION[notificacion])) ? mysqli_real_escape_string($conexion, $_SESSION[notificacion]) : 0;*/

	$cantTotal = 0;

	foreach ($carro as $item) :

		$qty = $item['cantidad'];
		$prd = $item['id'];
		$marco = $item['marco'];
		//$lista = $item['lista'];

		///$producto = consulta_bd('pd.precio, pd.descuento', 'productos p JOIN productos_detalles pd ON pd.producto_id = p.id JOIN lista_precio_producto lpp ON lpp.productos_detalle_id = pd.id', "pd.id = $item[id] AND lpp.lista_precio_id = $item[lista]", '');

		$producto = consulta_bd('pd.precio, pd.descuento', 'productos p, productos_detalles pd', "pd.producto_id = p.id and pd.id = $item[id]", '');

		$precioUnitario = $producto[0][0];
		$precioOferta = $producto[0][1];
		$precioFinal += ($producto[0][1] > 0) ? $producto[0][1] * $qty : $producto[0][0] * $qty;

		$cantTotal += $qty;

		$insertProductoPedido = insert_bd('productos_pedidos', "pedido_id, productos_detalle_id, cantidad, precio_unitario, descuento, precio_total, marco", "$pedido_id, $prd, $qty, $precioUnitario, $precioOferta, $precioFinal, '$marco'");

	endforeach;

	$qtyFinal = update_bd("pedidos", "cant_productos = $cantTotal, fecha_creacion = '$fechaHoy'", "id = $pedido_id");

	unset($_SESSION["notificacion"]);

	$token = generateToken($new_oc, $total);

	if ($medioPago == 'mercadopago') :
		// mpago 
		$mp = new MP($keys['client_id'], $keys['client_secret']);

		$preference_data = array(
			"items" => array(
				array(
					"id" => $new_oc,
					"title" => 'Compra de productos ' . opciones('nombre_cliente') . ' OC: ' . $new_oc,
					"currency_id" => "CLP",
					"picture_url" => "https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
					"quantity" => 1,
					"unit_price" => $total
				)
			),
			"payment_method_id" => array('visa', 'master', 'webpay', 'servipag'),
			"payer" => array(
				"first_name" => $nombre,
				"email" => $email
			),
			"back_urls" => array(
				"success" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=success",
				"failure" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=failure",
				"pending" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=pending"
			),
			"auto_return" => "approved",
			"notification_url" => $url_base . "tienda/mercadopago/notificacion.php?oc={$new_oc}&cod={$cod}",
			"expires" => false,
			"expiration_date_from" => null,
			"expiration_date_to" => null
		);

		$preference = $mp->create_preference($preference_data);

	elseif ($medioPago == 'transferencia') :

		include('../PHPMailer/PHPMailerAutoload.php');
		include('../tienda/mailchimp.class.php');

		enviarComprobanteCliente($new_oc);
		enviarComprobanteAdmin($new_oc);
		header("Location: exito-transferencia?oc=$new_oc");
		die();

	endif;

else :

	header("location:404");

endif;
?>

<div style="position:absolute; top:0; left:0; width:100%; height:100%; z-index:999; background-image: url(<?= $url_base; ?>tienda/trama.png);">
	<div style="margin: 0 auto; width: 100%;text-align:center;font-family:verdana;font-size: 12px;padding-top:200px;">
		<img src="<?php echo $url_base; ?>tienda/cargador.gif" border="0" /><br /><br />
		Estamos procesando su compra, espere por favor...<br />
		cant productos = <?php echo $cantTotal; ?><br />
		total = <?php echo $total ?>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<?php if ($medioPago == 'webpay') { ?>
	<form id="form" action="tienda/webpay/tbk-normal.php" method="post">
		<input type="hidden" name="TBK_MONTO" value="<?php echo $token; ?>" />
		<input type="hidden" name="TBK_ORDEN_COMPRA" value="<?php echo $new_oc; ?>" />
		<input type="hidden" name="TBK_ID_SESION" value="<?php echo $new_oc; ?>" />
		<input type="hidden" name="URL_RETURN" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal.php?action=result" />
		<input type="hidden" name="URL_FINAL" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal.php?action=end" />
	</form>
<?php } elseif ($medioPago == 'mercadopago') { ?>
	<script type="text/javascript">
		setTimeout(function() {
			window.location.href = "<?= $preference["response"]["init_point"] ?>";
		}, 1000); // 2Segs
	</script>
<?php } ?>

<?php mysqli_close($conexion); ?>

<script type="text/javascript">
	$(function() {
		$('#form').submit();
	});
</script>