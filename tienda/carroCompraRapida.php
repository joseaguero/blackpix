<?php 
include("../admin/conf.php");
$comuna_id = (isset($_GET[comuna_id])) ? mysqli_real_escape_string($conexion, $_GET[comuna_id]) : 0;
$retiro = (isset($_GET[retiro])) ? mysqli_real_escape_string($conexion, $_GET[retiro]) : 0;

// Include functions
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

echo resumenCompraShort($comuna_id, $retiro); 
mysqli_close($conexion);
?>