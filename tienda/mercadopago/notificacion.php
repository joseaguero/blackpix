<?php
require_once "lib/mercadopago.php";
include('../../admin/conf.php');
include('../../PHPMailer/PHPMailerAutoload.php');
include('../../admin/includes/tienda/cart/inc/functions.inc.php');
include('claves.php');

$mp = new MP($keys['client_id'], $keys['client_secret']);

$oc = $_GET['oc'];
$cod_descuento = $_GET['cod'];
// Get the payment and the corresponding merchant_order reported by the IPN.
if (!isset($_GET["id"]) || !ctype_digit($_GET["id"])) {
	http_response_code(400);
	return;
}

$payment_info = $mp->get_payment_info($_GET["id"]);
$payment = $payment_info["response"]["collection"];

$id_compra = $payment['id'];
$total = (int)$payment["total_paid_amount"];
$tipo_pago = $payment["payment_type"];
$metodo_pago = $payment["payment_method_id"];
$cantidad_cuotas = (int)$payment["installments"];
$valor_cuotas = (int)$payment["installment_amount"];
$cod_auth = $payment['authorization_code'];
$card_number = $payment['last_four_digits'];
$fecha = $payment["date_created"];
$estado_id = 0;

if ($payment_info["status"] == 200) {
	if ($payment["status"] == 'approved') {
		$estado_id = 2;	
	}elseif ($payment["status"] == 'pending' || $payment["status"] == 'in_process') {
		$estado_id = 1;
	}elseif ($payment["status"] == 'cancelled' || $payment["status"] == 'rejected') {
		$estado_id = 3;
	}

	$id_pedido = consulta_bd("id, retiro_en_tienda, estado_id","pedidos","oc='$oc'","");

	if ($id_pedido[0][2] != 2) {

		update_bd("pedidos","estado_id = $estado_id, mp_payment_type = '$tipo_pago', mp_payment_method = '$metodo_pago', mp_auth_code = '$cod_auth', mp_paid_amount = $total, mp_card_number = '$card_number', mp_id_paid = '$id_compra', mp_cuotas = $cantidad_cuotas, mp_valor_cuotas = $valor_cuotas, mp_transaction_date = '$fecha'","oc='$oc'");

		if ($estado_id == 2) {
			enviarComprobanteCliente($oc);
			enviarComprobanteAdmin($oc);
			
			$reducirStock = consulta_bd("pp.productos_detalle_id, pp.cantidad, pd.subproducto_id","productos_pedidos pp join pedidos p on p.id = pp.pedido_id join productos_detalles pd on pp.productos_detalle_id = pd.id","p.oc = '$oc'","");
			for($i=0; $i<sizeof($reducirStock); $i++){
	          $cantActual = update_bd("productos_detalles", "comprados = comprados + {$reducirStock[$i][1]}","subproducto_id = {$reducirStock[$i][2]}");
			}
		}

	}else{
		$mensaje_cron = "Pasó por notificación pero el pedido ya está aprobado $oc";
	}

	
	echo $payment["status"];
}
?>