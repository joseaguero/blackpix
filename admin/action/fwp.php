<?php
include ("../conf.php");

if ($_POST[fwp])
{
	if ($_POST[mail])
	{
		
		$url_q = consulta_bd("valor","opciones","nombre = 'url_sitio'","");
		$url_sitio = $url_q[0][0];
		
		$email = $_POST[mail];
		$ok = comprobar_mail($email);
		if ($ok)
		{
			$hash = md5(date("Ymds"));
			$update = update_bd("administradores","password_hash = '$hash'","email = '$email'");
			$cant = mysqli_affected_rows($conexion);
			if ($cant == 1)
			{
				$para = $email;
				$asunto = "Olvido de contraseña";
				
                $nombre_sitio = opciones("nombre_cliente");
				$nombre_corto = opciones("dominio");
				$noreply = "no-reply@".$nombre_corto;
				$url_sitio = opciones("url_sitio");
				$logo = opciones("url_sitio")."/admin/pics/logo-moldeable-cms.png";
				$color_logo = "#1777B6";
				
				
				$msg2 = '
					<html>
						<head>
						<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
						<title>'.$nombre_sitio.'</title>
						</head>
						<body style="background:#f9f9f9;">
							<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">

										<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
											<tr>
												<th align="center" width="100%">
													<p style="margin:20px 0 30px 0;">
														<a href="'.$url_sitio.'" style="color:#8CC63F;">
															<img src="'.$logo.'" alt="'.$logo.'" border="0" width="276"/>
														</a>
													</p>
												</th>
											</tr>
											
										</table>
										<br/><br/>
										<table cellspacing="0" cellpadding="0" border="0" width="100%">
											<tr>
												<td valign="top">
												   ';

											$msg2.='<p style="color: #333;">Estimado Usuario:</p>';

											$msg2 .= "<p>Se ha solicitado un cambio de contraseña desde el administrador de contenidos de su sitio web, si Ud. no ha solicitado este cambio no debe realizar ninguna acción.</p>";
											$msg2 .= "<p>Para poder realizar el cambio de contraseña haga clic en el siguiente link o cópielo en su navegador:</p>";
											$msg2 .= "<p>$url_sitio/admin/adlogin.php?op=fgp&hash=$hash&e=$email</p>";
											
				
											$msg2.='<p align="center" style="margin:0;color:#999;">Gracias,</p>
													<p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo de Moldeable S.A.</strong></p>
												</td>
											</tr>
										</table>
							</div>
						</body>
					</html>';
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
				
				
				$mensaje = $msg2;
				$to = $email;
				$cc = "";
				$website_key = "T29sMDh2VWhTbE9iYzFWVUt3MXdVRU5FSTY5UWVjMXU5cXM0ek5FbEFFMD0tLUNYVW9xTnoxR3hKUHhRYjRlT0FhbEE9PQ==--118b7d8cc23278dab8352451965c0445fb0afdd4"; // la obtengo en el paso anterior

				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => "http://142.93.196.220/api/messages",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"asunto\"\r\n\r\n$asunto\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mensaje\"\r\n\r\n$mensaje\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to\"\r\n\r\n$to\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cc\"\r\n\r\n$cc\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"website_key\"\r\n\r\n$website_key\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
				  CURLOPT_HTTPHEADER => array(
					"Cache-Control: no-cache",
					"Postman-Token: 98658c76-cbfd-41dc-9a8d-80c83ed3571f",
					"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
				  //echo "cURL Error #:" . $err;
					$error = "Error al generar el correo, por favor inténtelo nuevamente.&tipo=error";
				} else {
				  //echo $response;
				  $error = "Se le ha enviado un correo con las instrucciones para el cambio de contraseña, dfsdfsdf.&tipo=ventana";	
				}
				
				
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
				
				
			    /*Log*/
			    $log = insert_bd("system_logs","tabla, accion, fila, administrador_id,date","'administradores','Solicitud de cambio de contraseña enviada', '','$row', NOW()");
			    /*Fin Log*/
				
			}
			else
			{
				$error = "No se ha encontrado el correo ingresado en nuestros registros, por favor regístrese nuevamente.&tipo=ventana";
			}
		}
		else
		{
			$error = "Correo inválido&tipo=error";
		}
	}
	else
	{
		$error = "Debe ingresar su correo.&tipo=notificacion";
	}
}

header("location:../adlogin.php?error=$error");


?>