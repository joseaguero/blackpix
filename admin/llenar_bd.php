<?php 

include('conf.php');

for ($i=0; $i < 25; $i++) { 
	insert_bd('productos', 'publicado, cyber, recomendado, pack, codigos, nombre, marca_id, video, ficha_tecnica, thumbs, descripcion, fecha_creacion', "1, 0, 0, 0, NULL, 'Producto prueba {$i}', 1, NULL, NULL, 'sinFoto.jpg', NULL, NULL");
}

for ($i=0; $i < 25; $i++) { 
	$rand = rand(100000, 999999999);
	insert_bd('productos_detalles', 'producto_id, publicado, nombre, sku, ancho, alto, largo, precio, descuento, stock, stock_reserva, venta_minima, fecha_creacion, fecha_modificacion, creado_por, modificado_por, oferta_tiempo_activa', "$i, 1, 'Producto prueba {$i}', '$rand', NULL, NULL, NULL, 20000, 0, 13, 2, 1, NULL, NULL, NULL, NULL, NULL");
}

insert_bd('lineas', 'nombre, publicado, posicion', "'Linea 1', 1, 1");
insert_bd('lineas', 'nombre, publicado, posicion', "'Linea 2', 1, 2");
insert_bd('lineas', 'nombre, publicado, posicion', "'Linea 3', 1, 3");

insert_bd('categorias', 'publicada, linea_id, nombre, posicion', "1, 1, 'Categoria 1', 1");
insert_bd('categorias', 'publicada, linea_id, nombre, posicion', "1, 1, 'Categoria 2', 2");
insert_bd('categorias', 'publicada, linea_id, nombre, posicion', "1, 1, 'Categoria 3', 3");
insert_bd('categorias', 'publicada, linea_id, nombre, posicion', "1, 1, 'Categoria 4', 4");
insert_bd('categorias', 'publicada, linea_id, nombre, posicion', "1, 1, 'Categoria 5', 5");
insert_bd('categorias', 'publicada, linea_id, nombre, posicion', "1, 1, 'Categoria 6', 6");
insert_bd('categorias', 'publicada, linea_id, nombre, posicion', "1, 1, 'Categoria 7', 7");

insert_bd('subcategorias', 'nombre, posicion, categoria_id', "'SubCategoria 1', 1, 1");
insert_bd('subcategorias', 'nombre, posicion, categoria_id', "'SubCategoria 2', 2, 1");
insert_bd('subcategorias', 'nombre, posicion, categoria_id', "'SubCategoria 3', 3, 1");
insert_bd('subcategorias', 'nombre, posicion, categoria_id', "'SubCategoria 4', 4, 1");
insert_bd('subcategorias', 'nombre, posicion, categoria_id', "'SubCategoria 5', 5, 1");
insert_bd('subcategorias', 'nombre, posicion, categoria_id', "'SubCategoria 6', 6, 1");

for ($i=0; $i < 25; $i++) { 
	insert_bd('lineas_productos', 'producto_id, linea_id, categoria_id, subcategoria_id', "$i, 1, 1, 1");
}

insert_bd('marcas', 'nombre', "'Marca de prueba'");
insert_bd('marcas', 'nombre', "'Marca de prueba 2'");

insert_bd('clientes', 'activo, id_cliente_hbt, nombre, apellido, rut, telefono, email, giro, fecha_creacion, fecha_modificacion, pass, password_salt, clave ,descuento_segunda_compra, vendedor', "1, NULL, 'Jose', 'Aguero', '19.545.087-0', NULL, 'jaguero@moldeable.com', NULL, '2019-01-03 10:49:00', '2019-01-03 10:49:00', NULL, '76330c26dea62332de2ca7b4a9ef51ec', '274bbef7e64e91d0e355df8ed9927e44', 0, 0");
?>