<?php
require('conf.php');
function sanitize_output($buffer) {

    $search = array(
        '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
        '/[^\S ]+\</s',     // strip whitespaces before tags, except space
        '/(\s)+/s',         // shorten multiple whitespace sequences
        '/<!--(.|\s)*?-->/' // Remove HTML comments
    );

    $replace = array(
        '>',
        '<',
        '\\1',
        ''
    );

    $buffer = preg_replace($search, $replace, $buffer);

    return $buffer;
}

if(isset($_GET['id'])) {

  $id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
  $id = htmlentities( $id);
  $id = intval($id);


  $tabla_newsletter = opciones('tabla_newsletter');
  $consulta = consulta_bd('id,mes',$tabla_newsletter, 'id = '.$id,"");

  if($consulta == null) {
    echo 'No se encuentra el elemento.';
  }
  else {
    
	  	
	ob_start();
	$idInforme = $id;
	include("newsletters/".$_GET[template].".php");
	$contenido = ob_get_contents();
	ob_end_clean();
	
	//$contenido = htmlentities($contenido);
	$contenido = sanitize_output($contenido);
	echo '<div style="box-sizing:border-box;padding:30px;width:calc(100% - 6px);height:calc(100% - 6px);"><textarea class="focustextarea" style="width:calc(100% - 30px);height:calc(100% - 30px);padding:15px;margin:0 auto;">'.str_replace('í', '&iacute;',$contenido).'</textarea></div>';
  }
}
else {
  header('Location: index.php');
}






?>
