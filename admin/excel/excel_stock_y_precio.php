<?php

/********************************************************\
|  Moldeable CMS - Excel generator.			             |
|  Fecha Modificación: 16/06/2011		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

date_default_timezone_set('America/Santiago');
include('../conf.php');
mysqli_query ($conexion, "SET NAMES 'latin1'");


header("Content-type: application/octet-stream");
header("Content: charset=UTF-8");
header("Content-Disposition: attachment; filename=excel_stock_y_precio.xls"); 
header("Pragma: no-cache" );
header("Expires: 0"); 

// $hoy = date("d/m/Y");

$productos = consulta_bd("sku, precio, descuento, stock","productos_detalles", "","id desc");
?>
<table width="700" border="1">
        <thead>
            <tr>
            	<th><h3>sku</h3></th>
                <th><h3>precio</h3></th>
                <th><h3>descuento</h3></th>
                <th><h3>stock</h3></th>			
            </tr>
        </thead>
        <tbody>
        	<?php for($i=0; $i<sizeof($productos); $i++) {?>
			<tr>
            	<td><?= trim($productos[$i][0]); ?></td>
                <td><?= (trim($productos[$i][1]) != '') ? $productos[$i][1] : 0 ?></td>
                <td><?= (trim($productos[$i][2]) != '') ? $productos[$i][2] : 0 ?></td>
                <td><?= $productos[$i][3]; ?></td>
            </tr>
            <?php } ?>
        </tbody>

</table>