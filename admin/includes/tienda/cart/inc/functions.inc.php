<?php
//total de items agregados al carro de compra
function totalCart()
{
	global $db;
	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',', $cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id => $qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto_madre = consulta_bd('p.id', "productos p join productos_detalles pd on pd.producto_id = p.id", "pd.id = $prd_id", '');
			if ($is_cyber and is_cyber_product($producto_madre[0][0])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber'] * $qty;
			} else {
				$total += getPrecio($prd_id) * $qty;
			}
		}
	}
	return round($total);
}
//cantidad de productos en el carro
function qty_pro()
{
	$cart = cartPrepare();

	$total = 0;

	foreach ($cart as $item) {
		$qty = $item['cantidad'];

		$total += $qty;
	}

	return $total;
}

function ultimasUnidades($pid)
{
	$ultimas = get_option('ultimas_unidades');
	if ($ultimas) {
		$cant = get_option('cant_ultimas_unidades');
		$prd = consulta_bd("stock", "productos_detalles", "id = $pid", "");
		if ((int)$prd[0][0] <= (int)$cant) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function ofertaTiempo($id)
{
	$oferta = consulta_bd("oferta_tiempo_activa", "productos_detalles", "id=$id", "");
	if ($oferta) {
		if ($oferta[0][0]) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function ofertaTiempoHasta($id)
{
	$oferta = consulta_bd("oferta_tiempo_hasta", "productos_detalles", "id=$id", "");
	return $oferta[0][0];
}

function ofertaTiempoDescuento($id)
{
	$oferta = consulta_bd("oferta_tiempo_descuento", "productos_detalles", "id=$id", "");
	return $oferta[0][0];
}

function getPrecio($pd)
{
	$is_cyber 	= (opciones('cyber') == 1) ? true : false;
	$detalles 	= consulta_bd("pd.precio, pd.descuento, p.id, pd.precio_cyber", "productos_detalles pd join productos p on p.id = pd.producto_id", "pd.id = $pd", "");
	$precio 	= $detalles[0][0];

	if ($is_cyber and is_cyber_product($detalles[0][2])) {
		$descuento 	= $detalles[0][3];
	} else {
		$descuento 	= $detalles[0][1];
	}

	if (ofertaTiempo($pd)) {
		if (ofertaTiempoDescuento($pd) > 0) {
			$descuento = ofertaTiempoDescuento($pd);
		}
	}

	if ($descuento and $precio > $descuento) {
		$precio_final = $descuento;
	} else {
		$precio_final = $precio;
	}

	return $precio_final;
}

function getPrecioNormal($pd)
{
	$detalles 	= consulta_bd("precio", "productos_detalles", "id = $pd", "");
	return $detalles[0][0];
}

function tieneDescuento($pd)
{
	$detalles 	= consulta_bd("precio, descuento", "productos_detalles", "id = $pd", "");
	$precio 	= $detalles[0][0];
	$descuento 	= $detalles[0][1];
	if ($descuento and $precio > $descuento) return true;
	else return false;
}

function get_cyber_price($pd)
{
	$sql = consulta_bd("precio, precio_cyber", "productos_detalles", "id = $pd", "");
	$out['precio'] = $sql[0][0];
	$out['precio_cyber'] = $sql[0][1];
	return $out;
}

function ShowCart()
{
	global $db;

	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',', $cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id => $qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.venta_minima", "productos p, productos_detalles pd", "p.id=pd.producto_id and pd.id=$prd_id", "");
			if ($producto[0][0] != '') {
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}

			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if (!tieneDescuento($prd_id)) {
				$pd = consulta_bd("precio_cantidad", "productos_detalles", "id = $prd_id", "");
				if ($pd[0][0]) {
					$precios_cantidad = consulta_bd("nombre, rango, descuento", "precios_cantidades", "rango <= $qty AND productos_detall_id = $prd_id", "rango DESC");
					if ($precios_cantidad) {
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}

			$no_disponible = ($_GET['stock'] == $prd_id) ? 1 : 0;



			$output[] .= '<div class="filaProductos" id="fila_carro_' . $prd_id . '">
                	<div class="imgFila ancho10">
						<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
							<img src="imagenes/productos/' . $thumbs . '" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCart">
						<div class="nombreFila">
							<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
								<span>' . $producto[0][1] . '</span>
							</a>
						</div>
						<div class="skuShowCart">SKU: ' . $producto[0][5] . '</div>
						<div class="botoneraShowCart">
							<a href="javascript:void(0)" onclick="eliminaItemCarro(' . $prd_id . ')">Eliminar</a>
							<span> | </span>
							<a href="javascript:void(0)" onclick="guardarParaDespues(' . $prd_id . ')">Guardar para despues</a>
						</div>
						
					</div><!-- fin contInfoShowCart-->
                    
                    
					<div class="precioFila ancho20"><span>$' . number_format($valorUnitario, 0, ",", ".") . '</span> <span class="unidadMovil">c/u</span></div>
                    <div class="cantFila ancho10">';

			if ($no_disponible) {
				$output[] .= '<div class="sin-stock">Sin stock</div>';
			}

			$output[] .= '<div class="pull-left spinnerCarro" id="spinnerCarro_' . $prd_id . '">
                        	<input type="text" name="cant" class="campoCantCarroResumen" value="' . $qty . '" />
                            <div class="contFlechas">
                                <span class="mas" onclick="agregarElementoCarro(' . $prd_id . ', ' . $producto[0][6] . ')"  rel="' . $prd_id . '">▲</span>
                                <span class="menos" onclick="quitarElementoCarro(' . $prd_id . ', ' . $producto[0][6] . ')" rel="' . $prd_id . '">▼</span>
                            </div>
                    	</div>
                    </div>
                    <div class="totalFila ancho20"><span>$' . number_format($valor, 0, ",", ".") . '</span></div>
                    
					<div class="botoneraShowCart botoneraMovil">
						<a href="javascript:void(0)" onclick="eliminaItemCarro(' . $prd_id . ')">Eliminar</a>
						<span> | </span>
						<a href="javascript:void(0)" onclick="guardarParaDespues(' . $prd_id . ')">Guardar para despues</a>
					</div>
                </div><!-- fin filaProductos-->';
		}
	} else {
		$output[] = '<div class="carroVacio">
						<p class="carroVacio">Su carro está vacío.</p><br />
					</div>';
	}
	return join('', $output);
}

function saveForLater()
{
	global $db;
	//$listaDeseos = $_SESSION['listaDeseos'];
	if (!isset($_COOKIE[listaDeseos])) {
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$listaDeseos = json_decode($_COOKIE[listaDeseos], true);

	if ($listaDeseos) {
		$itemDiferente = 0;
		for ($i = 0; $i < sizeof($listaDeseos); $i++) {
			$itemDiferente = $itemDiferente + 1;
		}
		$output[] = '<div class="mensajeGuardados">Tienes artículos guardados para comprar más tarde. Para comprar uno, o más, ahora, haga clic en Mover al carrito junto al artículo.</div>
		<div class="tituloGuardados">Articulos guardados para despues <span>(' . $itemDiferente . ')</span></div>';


		for ($i = 0; $i < sizeof($listaDeseos); $i++) {
			$prd_id = $listaDeseos[$i]['id'];

			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, p.descripcion", "productos p, productos_detalles pd", "p.id=pd.producto_id and pd.id=$prd_id", "");
			if ($producto[0][0] != '') {
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}

			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if (!tieneDescuento($prd_id)) {
				$pd = consulta_bd("precio_cantidad", "productos_detalles", "id = $prd_id", "");
				if ($pd[0][0]) {
					$precios_cantidad = consulta_bd("nombre, rango, descuento", "precios_cantidades", "rango <= $qty AND productos_detall_id = $prd_id", "rango DESC");
					if ($precios_cantidad) {
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}



			$output[] .= '<div class="filaProductos" id="fila_carro_guardado' . $prd_id . '">
                	<div class="imgFila ancho20">
						<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
							<img src="imagenes/productos/' . $thumbs . '" width="100%" />
						</a>
					</div>
					<div class="contCarroGuardado">
						<div class="nombreFila">
							<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
								<span>' . $producto[0][1] . '</span>
							</a>
							<div class="breveDescripcion">
								' . preview($producto[0][5], 170) . '
							</div>
						</div>
						<div class="botonesFilaGuardado">
							<a class="" href="javascript:void(0)" onclick="eliminaFavorito(' . $prd_id . ')" rel="' . $prd_id . '">Eliminar</a>
							<span> | </span>
							<a class="" href="javascript:void(0)" onclick="moverAlCarro(' . $prd_id . ')" rel="' . $prd_id . '">Mover al carro</a>
						</div>
					</div><!-- fin contCarroGuardado -->
                    <div class="precioFilaGuardado ancho20">
						<span>$' . number_format($valorUnitario, 0, ",", ".") . '</span>
					</div>
					
                </div><!-- fin filaProductos-->';
		} //fin for








	}
	return join('', $output);
}

function guardadoParaDespues($id)
{
	if (!isset($_COOKIE[listaDeseos])) {
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$listaDeseos = $_COOKIE[listaDeseos];

	if ($listaDeseos) {
		$items = explode(',', $listaDeseos);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$estado = 0;
		foreach ($contents as $prd_id => $qty) {
			if ($id == $prd_id) {
				$estado = 1;
			}
		}
	}
	return $estado;
}

function resumenCompra()
{
	global $db;
	$carro = cartPrepare();
	$total = 0;

	foreach ($carro as $item) {
		$qty = $item['cantidad'];
		//$producto = consulta_bd('lpp.precio, lpp.descuento', 'productos p JOIN productos_detalles pd ON pd.producto_id = p.id JOIN lista_precio_producto lpp ON lpp.productos_detalle_id = pd.id', "pd.id = $item[id] AND lpp.lista_precio_id = $item[lista]", '');

		$producto = consulta_bd('pd.precio, pd.descuento', 'productos p, productos_detalles pd', "pd.producto_id = p.id and pd.id = $item[id]", '');

		if ($producto[0][1] > 0) {
			$total += $producto[0][1] * $qty;
		} else {
			$total += $producto[0][0] * $qty;
		}
	}

	$neto = $total / 1.19;
	$iva = $total - $neto;
	$resumen = '<div class="contentRes"><div class="ancho100">
					<div class="fl-resumen">
						<h3>Resumen del pedido: <span>' . cambiarMoneda(round($total)) . '</span></h3>
						<div class="ancho100 filatoolTip">*El valor del despacho se definira en el tercer paso del carro</div>
					</div>
					
					<a href="identificate" class="btnCompletarCompra" id="btnCompletarCompra">ir a pagar</a>
				</div>';
	$resumen .= '<div class="cont100carro1 fixedCarro1">
					<div class="cont100Centro centroFixedCarro1">
						<a href="identificacion" class="btnCompletarCompra">ir a pagar</a>
						<div class="valoresCarro1Fixed"><span>Subtotal(' . $cantArticulos . ' articulos):</span> <span class="montoAPagarFixed"><strong>' . cambiarMoneda(round($total)) . '</strong></span></div>
					</div>
				</div></div>';
	if (qty_pro() > 0) {
		return $resumen;
	} else {
		// return $resumen;
	}
}


function vistosRecientemente($vista, $cantidad)
{
	global $db;
	$recientes = $_COOKIE['productosRecientes'];
	if ($recientes) {
		$items = explode(',', $recientes);
		$items = array_reverse($items);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '<h4>Vistos recientemente</h4>';
		$i = 0;
		foreach ($contents as $id => $qty) {

			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.id, a.nombre", "productos p, productos_detalles pd, artistas a", "p.id=$id and p.id = pd.producto_id and p.artista_id = a.id", "");
			$cant = count($producto);

			if ($cant > 0) {
				$j = $j + 1;
				if ($producto[0][0] != null and $producto[0][0] != '' and is_file("imagenes/productos/{$producto[0][0]}")) {
					$thumbs = imagen("imagenes/productos/", $producto[0][0]);
				} else {
					$thumbs = imagen("img/", "sinFotoGrid.jpg");
				}

				$valorUnitario 	= getPrecio($producto[0][6]);


				//solo muestro los ultimos 3
				if ($cantidad > 0) {
					$cantidad = $cantidad;
				} else {
					$cantidad = 99999999999;
				}

				if ($vista == "grilla") {
					$vista = "grilla";
				} else {
					$vista = "filaProductosVistos";
				}

				if ($i < $cantidad) {

					$output[] .= '<div class="' . $vista . '" >
								<div class="imgFila ">
									<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
										<img src="' . $thumbs . '" width="100%" />
									</a>
								</div>
								<div class="contInfoShowCart">
									<div class="nombreFila">
										<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
											<span>' . $producto[0][1] . '</span>
										</a>
									</div>
									<div class="precioFila"><span>' . $producto[0][7] . '</span></div>
									<a class="btn-ver-ficha" href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">Ver ficha</a>
								</div><!-- fin contInfoShowCart-->
								
							</div><!-- fin filaProductos-->';
					$i = $i + 1;
				}
			} //fin condicion de cantidad
		}
	}

	return join('', $output);
}

function resumenCompraShort($comuna_id, $retiro)
{
	global $db;
	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',', $cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';


		$i = 1;
		foreach ($contents as $prd_id => $qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs", "productos p, productos_detalles pd", "p.id = pd.producto_id and pd.id = $prd_id", "");

			/*
$valoresFinales = precioFinalConDescuentoGeneral($prd_id);
			if($valoresFinales[descuento] > 0){
				$precio_final = $valoresFinales[valorConDescuento]*$qty;
			} else {
				$precio_final = $valoresFinales[valorOriginal]*$qty;
			}
*/



			$precio_final = getPrecio($prd_id) * $qty;

			if (!tieneDescuento($prd_id)) {
				$pd = consulta_bd("precio_cantidad", "productos_detalles", "id = $prd_id", "");
				if ($pd[0][0]) {
					$precios_cantidad = consulta_bd("nombre, rango, descuento", "precios_cantidades", "rango <= $qty AND productos_detall_id = $prd_id", "rango DESC");
					if ($precios_cantidad) {
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}



			$output[] .= '<div class="filaResumen">
                       <a href="ficha/' . $productos[0][0] . '/' . url_amigables($productos[0][1]) . '" class="imgThumbsCartHeader">
                              <img src="imagenes/productos/' . $productos[0][4] . '" width="100%">
                       </a>
                       <div class="datosProductoCart3">
					   	   <div class="tituloProductoResumen">
							   <a href="ficha/' . $productos[0][0] . '/' . url_amigables($productos[0][1]) . '">' . $productos[0][1] . '</a>
						   </div>
						   <div class="cantResumen">' . $qty . '</div>
						   <a href="ficha/' . $productos[0][0] . '/' . url_amigables($productos[0][1]) . '" class="valorFilaHeader">$' . number_format($precio_final, 0, ",", ".") . '</a>
						</div>
					   
                    </div><!--filaResumen -->
					
					
			';
			$total += round($precio_final);
			$i++;
		}
		$total_neto = $total / 1.19;
		$iva = $total_neto * 0.19;
		$despacho = valorDespacho($comuna_id); //3000;//$address[0][5];
		$output[] = '
					<div class="valores">
                        <div class="filaValor">
							<div class="montoValor">$' . number_format($total_neto, 0, ",", ".") . '</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						
    					<div class="filaValor">
							<div class="montoValor">$' . number_format($iva, 0, ",", ".") . '</div>
							<div class="nombreValor">IVA:</div>
                        </div>
						<div class="filaValor">
							<div class="montoValor">$' . number_format($despacho, 0, ",", ".") . '</div>
							<div class="nombreValor">Envío:</div>
                        </div>
						';
		$tiene_descuento = (isset($_SESSION['descuento'])) ? descuentoBy($_SESSION['descuento']) : 0;
		if ($tiene_descuento == 1) {

			$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-' . number_format($_SESSION['val_descuento'], 0, ",", ".") . '</div>
									<div class="nombreValor">Descuento:</div> 
								</div>
										 ';
		}

		$output[] = '	
					<div class="filaValor">
						<div class="montoValor">$' . number_format(($total_neto + $iva + $despacho) - $_SESSION['val_descuento'], 0, ",", ".") . '</div>
						<div class="nombreValor">Total:</div>
                    </div>
						
               </div>';
		if ($retiro == 0) {
			$output[] = '<div class="mensajesDespacho">Su tiempo estimado de envio es de 5 días hábiles.</div>';
		}
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('', $output);
}

//resumen de productos en paso identificacion y envio
function resumenCompraShort2($comuna_id)
{
	global $db;
	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',', $cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';


		$i = 1;
		foreach ($contents as $prd_id => $qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs", "productos p, productos_detalles pd", "p.id = pd.producto_id and pd.id = $prd_id", "");

			/*
$valoresFinales = precioFinalConDescuentoGeneral($prd_id);
	
			if($valoresFinales[descuento] > 0){
				$precio_final = $valoresFinales[valorConDescuento]*$qty;
			} else {
				$precio_final = $valoresFinales[valorOriginal]*$qty;
			}
*/

			$precio_final = getPrecio($prd_id) * $qty;

			if (!tieneDescuento($prd_id)) {
				$pd = consulta_bd("precio_cantidad", "productos_detalles", "id = $prd_id", "");
				if ($pd[0][0]) {
					$precios_cantidad = consulta_bd("nombre, rango, descuento", "precios_cantidades", "rango <= $qty AND productos_detall_id = $prd_id", "rango DESC");
					if ($precios_cantidad) {
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}


			$output[] .= '<div class="filaResumen">
                       <a href="ficha/' . $productos[0][0] . '/' . url_amigables($productos[0][1]) . '" class="imgThumbsCartHeader">
                              <img src="imagenes/productos/' . $productos[0][4] . '" width="100%" />
                       </a>
					   <div class="datosProductoCart3">
						   <div class="tituloProductoResumen">
							   <a href="ficha/' . $productos[0][0] . '/' . url_amigables($productos[0][1]) . '">' . $productos[0][1] . '</a>
						   </div>
						   <div class="cantResumen">' . $qty . '</div>
						   <a href="ficha/' . $productos[0][0] . '/' . url_amigables($productos[0][1]) . '" class="valorFilaHeader text-right">$' . number_format($precio_final, 0, ",", ".") . '</a>
					   </div>
                    </div><!--filaResumen -->
					
					
			';
			$total += round($precio_final);
			$i++;
		}
		$total_neto = $total / 1.19;
		$iva = $total_neto * 0.19;
		$despacho = valorDespacho(0);

		$output[] = '
					<div class="valores">
						<div class="filaValor">
							<div class="montoValor">$' . number_format($total_neto, 0, ",", ".") . '</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						<div class="filaValor">
							<div class="montoValor">$' . number_format($iva, 0, ",", ".") . '</div>
							<div class="nombreValor">IVA:</div>
						</div>
						<div class="filaValor">
							<div class="montoValor">$' . number_format($despacho, 0, ",", ".") . '</div>
							<div class="nombreValor">Envío:</div>
						</div>
					';

		if (isset($_SESSION["descuento"])) {
			$codigo = $_SESSION["descuento"];
			$descuento = consulta_bd("id, codigo, valor, porcentaje", "codigo_descuento", "codigo = '$codigo' and activo = 1", "");
			if ($descuento[0][3] > 0) {
				$nPorcentaje = $descuento[0][3] / 100;
				$descuentoProducto = round($nPorcentaje * $total);
			} else if ($descuento[0][2] > 0) {
				$descuentoProducto = $descuento[0][2];
			} else {
				$descuentoProducto = 0;
			}

			$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-' . number_format($descuentoProducto, 0, ",", ".") . '</div>
									<div class="nombreValor">Descuento:</div> 
								</div>';
		}

		$output[] = '<div class="filaValor">
						<div class="montoValor">$' . number_format(($total_neto + $iva + $despacho) - $descuentoProducto, 0, ",", ".") . '</div>
						<div class="nombreValor">Total:</div>
                        
                    </div>
				</div>';
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('', $output);
}

//valor despacho, REGLAS POR DEFINIR SEGUN CADA CASO
function valorDespacho($comuna_id)
{
	global $db;
	$carro = cartPrepare();
	$total = 0;
	$envio = 0;

	if ($comuna_id > 0) {
		$region = consulta_bd('region_id', "comunas", "id = $comuna_id", "");

		if ($region[0][0] == 13) {
			foreach ($carro as $item) {
				$productos = consulta_bd("subproducto_id", "productos_detalles", "id = {$item['id']}", "");

				if ($productos[0][0] < 3) {
					$envio += 30000 * $item['cantidad'];
				} else {
					$envio += 14000 * $item['cantidad'];
				}
			}
		} else {
			$envio = 'x';
		}
	} else {
		$envio = 0;
	}

	return $envio;
}

function showCartExito($oc)
{
	$pedido = consulta_bd("id, oc, total, valor_despacho, total_pagado, descuento", "pedidos", "oc='$oc'", "");
	$productos_pedidos = consulta_bd("pp.cantidad, pp.precio_unitario, pp.precio_total, p.nombre, p.thumbs, p.id, pd.sku", "productos_pedidos pp, productos p, productos_detalles pd", "pp.productos_detalle_id = pd.id and pd.producto_id=p.id and pp.pedido_id=" . $pedido[0][0], "pp.id");

	$carro_exito = '<div class="contCarro">
						<h2>Productos Asociados</h2>
						<div class="cont100 filaTitulos">
							<div class="ancho50"><span style="float:left; margin-left:10px;">Producto</span></div>
							<div class="ancho20">Precio unitario</div>
							<div class="ancho10">Cantidad</div>
							<div class="ancho20">Total Item</div>
						</div>';
	for ($i = 0; $i < sizeof($productos_pedidos); $i++) {
		$carro_exito .= '<div class="filaProductos">
								<div class="imgFila ancho10">
									<a href="ficha/' . $productos_pedidos[$i][5] . '/' . url_amigables($productos_pedidos[$i][3]) . '">
										<img src="imagenes/productos/' . $productos_pedidos[$i][4] . '" width="100%">
									</a>
								</div>
								<div class="contInfoShowCart">
									<div class="nombreFila">
										<a href="ficha/' . $productos_pedidos[$i][5] . '/' . url_amigables($productos_pedidos[$i][3]) . '">
											<span>' . $productos_pedidos[$i][3] . '</span>
										</a>
									</div>
									<div class="skuShowCart">SKU: ' . $productos_pedidos[$i][6] . '</div>
								</div>
								
								<div class="precioFila ancho20">
									<span>$' . number_format($productos_pedidos[$i][1], 0, ",", ".") . '</span>
									<span class="unidadMovil">c/u</span>
								</div>
								<div class="cantFila ancho10">
									<span>' . $productos_pedidos[$i][0] . '</span>
									<span class="unidadMovil">Unidades</span>
								</div>
								<div class="totalFila ancho20">
									<span>$' . number_format($productos_pedidos[$i][2], 0, ",", ".") . '</span>
								</div>
							</div>';
	}
	$carro_exito .= '</div>';
	//Fin ciclo
	$carro_exito .= '<div class="contTotalesExito">
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$' . number_format($pedido[0][2], 0, ",", ".") . '</span>
							<span class="nomValor">Subtotal</span>
						</div>
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$' . number_format($pedido[0][3], 0, ",", ".") . '</span>
							<span class="nomValor">Envío</span>
						</div>
                        <div class="cont100 filaValoresExito">
							<span class="valorValor">$' . number_format($pedido[0][5], 0, ",", ".") . '</span>
							<span class="nomValor">Descuento</span>
						</div>
						<div class="cont100 filaValoresExito filaTotal">
							<span class="valorValor">$' . number_format($pedido[0][4], 0, ",", ".") . '</span>
							<span class="nomValor">Total</span>
						</div>
					</div>';
	return $carro_exito;
}

//FUNCION PARA SABER TIPO DE PAGO DE WEB PAY Y NUMERO DE CUOTAS
function tipo_pago($tipo_pago, $num_cuotas, $method)
{
	if ($method == 'tbk') {
		switch ($tipo_pago) {
			case 'VN':
				$tipo_pago = "Crédito";
				$tipo_cuota = "Sin cuotas";
				$cuota = "00";
				break;

			case 'VC':
				$tipo_pago = "Crédito";
				$tipo_cuota = "Cuotas Normales";
				$cuota_valores = strlen($num_cuotas);
				if ($cuota_valores == 1) {
					$cuota = "0" . $num_cuotas;
				} else {
					$cuota = $num_cuotas;
				}
				break;

			case 'SI':
				$tipo_pago = "Crédito";
				$tipo_cuota = "Sin interés";
				$cuota_valores = strlen($num_cuotas);
				if ($cuota_valores == 1) {
					$cuota = "0" . $num_cuotas;
				} else {
					$cuota = $num_cuotas;
				}
				break;

			case 'CI':
				$tipo_pago = "Crédito";
				$tipo_cuota = "Cuotas Comercio";
				$cuota_valores = strlen($num_cuotas);
				$cuota_valores = $num_cuotas . " cuotas";

				if ($cuota_valores == 1) {
					$cuota = "0" . $num_cuotas . " cuotas";
				} else {
					$cuota = $num_cuotas . " cuotas";
				}
				break;

			case 'VD':
				$tipo_pago = "Débito";
				$tipo_cuota = "Venta Débito";
				$cuota = "00";
				break;
		}
	} else {
		switch ($tipo_pago) {
			case 'credit_card':
				$tipo_pago = "Crédito";
				$tipo_cuota = ($num_cuotas > 6) ? 'Cuotas normales' : 'Sin interés';
				$cuota_valores = strlen($num_cuotas);
				if ($cuota_valores == 1) {
					$cuota = "0" . $num_cuotas;
				} else {
					$cuota = $num_cuotas;
				}
				break;

			case 'debit_card':
				$tipo_pago = "Débito";
				$tipo_cuota = 'Venta Débito';
				$cuota = "00";
				break;
		}
	}


	return array("tipo_pago" => $tipo_pago, "tipo_cuota" => $tipo_cuota, "cuota" => $cuota);
}

function cambioDePrecio()
{
	$cambioPrecios = json_decode($_COOKIE[listaDeseos], true);
	$cantidad = 0;
	$cadena = "";
	/*
return var_dump($cambioPrecios);
	die();
*/
	//if(!$cambioPrecios){
	for ($i = 0; $i < sizeof($cambioPrecios); $i++) {
		$id = $cambioPrecios[$i]['id'];
		$valor = $cambioPrecios[$i]['valor'];

		$precios = consulta_bd("pd.precio, pd.descuento, p.nombre", "productos_detalles pd, productos p", "p.id = pd.producto_id and pd.id = $id", "");
		if ($precios[0][1] > 0 and $precios[0][1] < $precios[0][0]) {
			$precioAplicable = $precios[0][1];
		} else {
			$precioAplicable = $precios[0][0];
		}
		//imprimo valores que cambian
		if ($valor > $precioAplicable) {
			//bajo de precio
			$cantidad = $cantidad + 1;
			$cadena .= '<div class="filaProductoPrecio">' . $precios[0][2] . ' ha bajado su precio de: <strong class="rojo">$' . number_format($valor, 0, ",", ".") . ' a $' . number_format($precioAplicable, 0, ",", ".") . '</strong></div>';
			$cambioPrecios[$i]['valor'] = "$precioAplicable";
		} else if ($valor < $precioAplicable) {
			//subio de precio
			$cantidad = $cantidad + 1;
			$cadena .= '<div class="filaProductoPrecio">' . $precios[0][2] . ' ha subido su precio de: <strong class="rojo">$' . number_format($precioAplicable, 0, ",", ".") . ' a $' . number_format($valor, 0, ",", ".") . '</strong></div>';
			$cambioPrecios[$i]['valor'] = "$valor";
		} else {
			//se mantiene el precio
		}

		//
	} //Fin for



	//}

	$resultado = '<div class="cont100">
						<div class="cont100Centro">
							
							<div class="contCambioPrecios">
								<h2><strong>' . $cantidad . '</strong> productos han cambiado de precio</h2>
								' . $cadena . '
							</div>
						</div>
					</div>';

	setcookie("listaDeseos", json_encode($cambioPrecios), time() + (365 * 24 * 60 * 60), "/");



	if ($cantidad > 0) {
		//solo si un producto cambio su precio respondo
		return $resultado;
	} else {
	}
} //fin function


/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/

function enviarComprobanteCliente($oc)
{

	$nombre_sitio = opciones("nombre_cliente");
	$nombre_corto = opciones("dominio");
	$noreply = "no-reply@" . $nombre_corto;
	$url_sitio = "https://" . $nombre_corto;
	$logo = opciones("logo_mail");
	$correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	$msje_despacho = 'Su pedido fue recibido y será procesado para despacho.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion,medio_de_pago", "pedidos", "oc='$oc'", "");

	$nombre_cliente = $datos_cliente[0][0];
	$email_cliente = $datos_cliente[0][1];

	$transferencia = ($datos_cliente[0][4] == 'transferencia');

	$id_pedido = $datos_cliente[0][2];

	$id_pedidoAdminitrador = $datos_cliente[0][4];

	$detalle_pedido = consulta_bd("pp.productos_detalle_id,pp.cantidad,pp.precio_unitario,pd.sku, pp.codigo_pack, pp.marco, pd.nombre, t.nombre, pp.descuento", "productos_pedidos pp join productos_detalles pd on pd.id = pp.productos_detalle_id join tamaños t on t.id = pd.tamaño_id", "pp.pedido_id=$id_pedido and pp.codigo_pack is NULL", "");

	$detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos", "productos_pedidos", "pedido_id=$id_pedido and codigo_pack <> '' group by codigo_pack", "");

	$despacho = $datos_cliente[0][3];

	$asunto = "Comprobante de compra,  OC N°" . $oc . "";
	$tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';

	for ($i = 0; $i < sizeof($detalle_pedido); $i++) {
		$pD = consulta_bd("producto_id, nombre", "productos_detalles", "id=" . $detalle_pedido[$i][0], "");
		$id_prod = $pD[0][0];
		$campos = "id, nombre,thumbs";
		$tabla  = "productos";
		$where  = "id=$id_prod";
		$productos = consulta_bd($campos, $tabla, $where, "");
		$precio_unitario = $detalle_pedido[$i][2];
		$cantidad = $detalle_pedido[$i][1];
		$subtotal = $precio_unitario * $cantidad;

		$tabla_compra .= '<tr>';
		$tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="' . $url_sitio . '/imagenes/productos/' . $productos[0][2] . ' " width="90%"/>
												</p>
												
											</td>';
		$tabla_compra .= '  <td valign="top" align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">' . $productos[0][1] . ' ' . $detalle_pedido[$i][3] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">Cantidad: ' . $detalle_pedido[$i][1] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $detalle_pedido[$i][7] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $detalle_pedido[$i][6] . '</p>';
		if ($detalle_pedido[$i][5] != '') {
			$tabla_compra .= '<p style="float:left; width:100%; margin:0 0 5px 0;">Marco: ' . $detalle_pedido[$i][5] . '</p>';
		}
		if ($detalle_pedido[$i][8] > 0) {
			$tabla_compra .= '<p style="float:left; width:100%; margin:0 0 5px 0;">$' . number_format($detalle_pedido[$i][8], 0, ",", ".") . '</p>';
		} else {
			$tabla_compra .= '<p style="float:left; width:100%; margin:0 0 5px 0;">$' . number_format($detalle_pedido[$i][2], 0, ",", ".") . '</p>';
		}

		$tabla_compra .= '</td>'; //nombre producto

		$tabla_compra .= '</tr>';
	}


	for ($i = 0; $i < sizeof($detalle_pack); $i++) {
		$skuPack = $detalle_pack[$i][4];

		$cantProdPack = $detalle_pack[$i][5];
		$pDP = consulta_bd("p.codigos, pd.precio, pd.descuento", "productos_detalles pd, productos p", "p.id= pd. producto_id and pd.sku='$skuPack'", "");
		$productosPorPack = explode(",", $pDP[0][0]);
		$cantArreglo = count($productosPorPack);


		$pD = consulta_bd("producto_id, nombre", "productos_detalles", "sku='$skuPack'", "");
		$id_prod = $pD[0][0];
		$campos = "id, nombre,thumbs";
		$tabla  = "productos";
		$where  = "id=$id_prod";
		$productos = consulta_bd($campos, $tabla, $where, "");

		if ($pDP[0][2] > 0) {
			$precio_unitario = $pDP[0][2];
		} else {
			$precio_unitario = $pDP[0][1];
		}
		//die("$cantProdPack");
		$cantidad = $detalle_pack[$i][1];
		$subtotalFila = $precio_unitario;


		$tabla_compra .= '<tr>';
		$tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="' . $url_sitio . '/imagenes/productos/' . $productos[0][2] . ' " width="90%"/>
												</p>
												
											</td>';
		$tabla_compra .= '  <td  align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">' . $productos[0][1] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $skuPack . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $cantidad . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$' . number_format($subtotalFila, 0, ",", ".") . '</p>
											</td>'; //nombre producto
		$tabla_compra .= '</tr>';
	}




	$tabla_compra .= '</table>';

	$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado", "pedidos", "oc='$oc'", "");

	$tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
	$tabla_compra .= '   <tfoot>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999; text-align:right;">$' . number_format($totales[0][4], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	if ($totales[0][1] != '' || $totales[0][1] != 0) {
		$tabla_compra .= '<tr class="cart_total_price">';
		$tabla_compra .= '   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
		$tabla_compra .= '   <td align="right" width="10%"><span style="color:#999">$' . number_format($totales[0][1], 0, ",", ".") . '</span></td>';
		$tabla_compra .= '</tr>';
	}



	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999; text-align:right;">$' . number_format($totales[0][3], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:' . $color_logo . '; float:right;">Total Pedido:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:' . $color_logo . '; text-align:right;">$' . number_format($totales[0][5], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '   </tfoot>';
	$tabla_compra .= '</table>';

	$tabla_compra .= '<br /><br />';



	$msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>' . $nombre_sitio . '</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px ' . $color_logo . '; border-bottom:solid 4px ' . $color_logo . '; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="center" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="' . $url_sitio . '" style="color:#8CC63F;">
											<img src="' . $logo . '" alt="' . $logo . '" border="0" />
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="center" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado ' . $nombre_cliente . '</p>
                                    <p style="float:right;width:100%;margin: 0px;">Gracias por su compra</p>
                                    <p style="float:right;width:100%;margin: 0px;">Su número de compra es: <br />
<strong style="color:' . $color_logo . ';">' . $oc . '</strong></p>
                                    
                                    <p style="float:right;width:100%;margin: 15px 0px;">
                                        <a style="display:inline-block; color:#fff;padding:10px 20px; margin: 5px 10px; text-decoration:none; background-color:' . $color_logo . ';" href="' . $url_sitio . '/tienda/boucher/boucherCompra.php?oc=' . $oc . '">
                                            VER BOUCHER
                                        </a>
										
										<a style="margin:5px 10px; display:inline-block; color:#fff;padding:10px 20px;text-decoration:none; background-color:' . $color_logo . ';" href="' . $url_sitio . '/tienda/boucher/boucherCompra.php?oc=' . $oc . '">
                                            SEGUIMIENTO
                                        </a>
										
                                    </p>
                                </th>
							</tr>
                        </table>
						<br/>';

	if ($transferencia) {
		$msg2 .= '<div>A continuación encontrarás los datos para que realices la transferencia antes de 48 horas, después de ese periodo se anulara el pedido. <br> <br>
							
		Media & Pix Spa<br>
		Banco Security<br>
							Tipo de cuenta: Cuenta Corriente <br>
							Cuenta: 919138002 <br>
							RUT: 76.699.358-3 <br>
							Correo: mediaypix@gmail.com <br></div>';
	}

	$msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';

	$msg2 .= '<p style="color: #333;">PRODUCTOS COMPRADOS</p>

                                    <p>' . $tabla_compra . '</p>';

	$msg2 .= '<p align="center" style="margin:0;color:#000;">' . $msje_despacho . '</p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo ' . $nombre_sitio . '</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;


	save_in_mailchimp($oc, 'exito');
	save_in_mailchimp($oc, 'todas_compras');

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");

	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	//Set an alternative reply-to address

	$mail->addAddress($email_cliente, $nombre_cliente);
	$mail->Subject = $asunto;
	$mail->msgHTML($msg2);
	$mail->AltBody = $msg2;
	$mail->CharSet = 'UTF-8';
	//send the message, check for errors
	if (!$mail->send()) {
		return "Mailer Error: " . $mail->ErrorInfo;
	} else {
		return 'envio exitoso';
	}
}

function enviarComprobanteAdmin($oc, $imprimir = NULL, $correoForzarNotificacion = NULL)
{

	$nombre_sitio = opciones("nombre_cliente");
	$nombre_corto = opciones("dominio");
	$noreply = "no-reply@" . $nombre_corto;
	$url_sitio = "https://" . $nombre_corto;
	$logo = opciones("logo_mail");
	$correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");


	$fontFamily = ($imprimir == 1) ? "font-family: Trebuchet MS, sans-serif;" : "font-family: Open Sans, sans-serif;";

	/*
$email_admin = 'ventas@moldeable.com';
	$email_admin = 'htorres@moldeable.com';
*/


	$msje_despacho = 'Su pedido sera procesado y despachado dentro de 24 horas.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion", "pedidos", "oc='$oc'", "");
	$nombre_cliente = $datos_cliente[0][0];
	$email_cliente = $datos_cliente[0][1];
	$id_pedido = $datos_cliente[0][2];

	$datos_cliente = consulta_bd("nombre,
    email,
    id,
    direccion, 
    region,
    ciudad,
    comuna,
    direccion,
    telefono,
    rut,
    factura,
    direccion_factura,
    giro,
    email_factura,
    rut_factura,
    fecha_creacion,
    telefono,
    regalo,
	razon_social,
	payment_type_code,
	shares_number, 
	transaction_date, 
    comentarios_envio, 
    nombre_retiro,
    apellidos_retiro, 
    rut_retiro, 
    nombre, 
	retiro_en_tienda, 
	medio_de_pago,
	mp_cuotas, 
	mp_payment_type, 
	mp_transaction_date", "pedidos", "oc='$oc'", "");
	$nombre_cliente = $datos_cliente[0][0];

	$payment_type = ($datos_cliente[0][28] == 'webpay') ? $datos_cliente[0][19] : $datos_cliente[0][30];
	$method = ($datos_cliente[0][28] == 'webpay') ? 'tbk' : 'mpago';
	$cuotas = ($datos_cliente[0][28] == 'webpay') ? $datos_cliente[0][20] : $datos_cliente[0][29];
	$fecha = ($datos_cliente[0][28] == 'webpay') ? $datos_cliente[21] : $datos_cliente[0][31];

	$transferencia = ($datos_cliente[0][28] == 'transferencia');

	$fecha = date('d/m/Y H:i:s', strtotime($fecha));
	$texto = ($datos_cliente[0][28] == 'webpay') ? 'Datos transbank' : 'Datos MercadoPago';

	$tipo_pago = tipo_pago($payment_type, $cuotas, $method);

	$detalle_pedido = consulta_bd("pp.productos_detalle_id,pp.cantidad,pp.precio_unitario,pd.sku, pp.codigo_pack, pp.descuento, pd.nombre, pp.marco, t.nombre", "productos_pedidos pp join productos_detalles pd on pd.id = pp.productos_detalle_id join tamaños t on t.id = pd.tamaño_id", "pp.pedido_id=$id_pedido", "");
	$despacho = $datos_cliente[0][3];




	$tabla_compra = '
                <table width="100%"  border="0" cellspacing="0" cellpadding="5" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc; ' . $fontFamily . ' color:#666; float:left;">
                    <thead>
					<tr>
                        <th align="left" width="10%" style="border-bottom:2px solid #ccc;"></th>
                        <th align="left" width="30%" style="border-bottom:2px solid #ccc;">Producto</th>
                        <th align="center" width="10%" style="border-bottom:2px solid #ccc;">SKU</th>
                        <th align="center" width="5%" style="border-bottom:2px solid #ccc;">Cantidad</th>
                        <th align="right" width="12%" style="border-bottom:2px solid #ccc;">Precio <br>Unitario</th>
						<th align="right" width="12%" style="border-bottom:2px solid #ccc;">Total item</th>
                    </tr>
					</thead>
                <tbody>';


	for ($i = 0; $i < sizeof($detalle_pedido); $i++) {
		$pD = consulta_bd("producto_id, nombre", "productos_detalles", "id=" . $detalle_pedido[$i][0], "");
		$id_prod = $pD[0][0];
		$campos = "id, nombre,thumbs";
		$tabla  = "productos";
		$where  = "id=$id_prod";
		$productos = consulta_bd($campos, $tabla, $where, "");
		$precio_unitario = $detalle_pedido[$i][2];
		$cantidad = $detalle_pedido[$i][1];
		$subtotal = $precio_unitario * $cantidad;

		if ($detalle_pedido[$i][4] != "") {
			$tabla_compra .= '<tr bgcolor="#efefef">';
		} else {
			$tabla_compra .= '<tr>';
		}


		$tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;">
												<img src="' . $url_sitio . '/imagenes/productos/' . $productos[0][2] . '" width="100%"/>
											</td>';
		$tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;color:#797979; ' . $fontFamily . '">' . $productos[0][1] . ' <br> ' . $detalle_pedido[$i][8] . ' <br> ' . $detalle_pedido[$i][6] . ' <br>';
		if ($detalle_pedido[$i][7] != '') {
			$tabla_compra .= 'Marco: ' . $detalle_pedido[$i][7] . '</td>'; //nombre producto
		}
		$tabla_compra .= '  <td align="center" style="border-bottom: 2px solid #ccc;color:#797979;' . $fontFamily . '">' . $detalle_pedido[$i][3] . '</td>'; //codigo producto SKU

		$tabla_compra .= '  <td align="center" style="border-bottom: 2px solid #ccc;color:#797979; ' . $fontFamily . '">' . $detalle_pedido[$i][1] . '</td>'; //cantidad

		if ($detalle_pedido[$i][5] > 0) {
			$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:' . $color_logo . '; ' . $fontFamily . '">$' . number_format($detalle_pedido[$i][5], 0, ",", ".") . '</td>'; //precio producto
			$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:' . $color_logo . '; ' . $fontFamily . '">$' . number_format(($detalle_pedido[$i][5] * $detalle_pedido[$i][1]), 0, ",", ".") . '</td>'; //precio producto
			$tabla_compra .= '</tr>';
		} else {
			$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:' . $color_logo . '; ' . $fontFamily . '">$' . number_format($detalle_pedido[$i][2], 0, ",", ".") . '</td>';
			$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:' . $color_logo . '; ' . $fontFamily . '">$' . number_format(($detalle_pedido[$i][2] * $detalle_pedido[$i][1]), 0, ",", ".") . '</td>'; //precio producto
			$tabla_compra .= '</tr>';
		}
	}


	$tabla_compra .= '</tbody>';
	$tabla_compra .= '</table>';

	$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado", "pedidos", "oc='$oc'", "");

	$tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
	$tabla_compra .= '   <tfoot>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999; text-align:right;">$' . number_format($totales[0][4] / 1.19, 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999; float:right;">IVA:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999; text-align:right;">$' . number_format(($totales[0][4] / 1.19) * 0.19, 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	if ($totales[0][1] != '' || $totales[0][1] != 0) {
		$tabla_compra .= '<tr class="cart_total_price">';
		$tabla_compra .= '   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
		$tabla_compra .= '   <td align="right" width="10%"><span style="color:#999">$' . number_format($totales[0][1], 0, ",", ".") . '</span></td>';
		$tabla_compra .= '</tr>';
	}



	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999">Valor envío:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999">$' . number_format($totales[0][3], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:' . $color_logo . '">Total Pedido:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:' . $color_logo . '">$' . number_format($totales[0][5], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '   </tfoot>';
	$tabla_compra .= '</table>';

	$tabla_compra .= '<br /><br />';



	$asunto = "Comprobante de compra,  OC N°" . $oc . "";
	$msg2 = "<html>";

	if ($imprimir != 1) {
		$msg2 .= '
			<head>
				<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
				<title>' . $nombre_sitio . '</title>
			</head>';
	}

	if ($imprimir != 1) {
		$msg2 .= '<body style="background:#f9f9f9; float:left;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px ' . $color_logo . '; border-bottom:solid 4px ' . $color_logo . '; ' . $fontFamily . ' float:left;">';
	} else {
		$msg2 .= '<body style="background:#fff; float:left;">
			<div style="background:#fff; width:96%; margin-left:0; padding-left:2%; padding-right:2%; border-top:solid 4px ' . $color_logo . '; border-bottom:solid 4px ' . $color_logo . '; float:left; ' . $fontFamily . '">';
	}

	$msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                	<p>
										<a href="' . $url_sitio . '" style="color:#8CC63F;">
											<img src="' . $logo . '" alt="' . $logo . '" border="0"/>
										</a>
									</p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; ' . $fontFamily . '">' . $nombre_cliente . '</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px; ' . $fontFamily . '">Ha generado una compra web.</p>
                                    <p style="color:#797979; float:right; width:100%; margin: 0px; ' . $fontFamily . '">Su número de compra es: <strong style="color:' . $color_logo . '; ' . $fontFamily . '">' . $oc . '</strong></p>';

	if ($imprimir != 1) {
		$msg2 .= '<p style="float:right;width:100%;margin: 10px 0px 0 0;">
                                        <a style="text-decoration: none; float:right; background-color:' . $color_logo . '; padding:10px 20px; color:#fff;" href="' . $url_sitio . '/tienda/voucher/voucherCompra.php?oc=' . $oc . '">
                                            Ver voucher
                                        </a>
                                    </p>';
	}

	$msg2 .= '</th>
                            </tr>
                        </table>
                        <br/><br/>';

	if ($transferencia) {
		$msg2 .= '<div style="margin-bottom: 20px; color: red; font-family: 20px;">Esta compra ha sido realizada por Transferencia</div>';
	}

	$msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">
                            <tr>
                                <td valign="top" width="50%">';
	if ($datos_cliente[0][27] == 0) {

		$msg2 .= '<h3 style="' . $fontFamily . '">Dirección de entrega</h3>
                                        <ul>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Direccion: </strong>' . $datos_cliente[0][3] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Region: </strong>' . $datos_cliente[0][4] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Ciudad: </strong>' . $datos_cliente[0][5] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Comuna: </strong>' . $datos_cliente[0][6] . '</li>
                                        </ul>';
	} else {
		$msg2 .= '<h3 style="' . $fontFamily . '">Datos retiro</h3>
                                        <ul>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Nombre: </strong>' . $datos_cliente[0][23] . ' ' . $datos_cliente[0][24] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Rut: </strong>' . $datos_cliente[0][25] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Patente: </strong>' . $datos_cliente[0][26] . '</li>
                                        </ul>';
	}



	$msg2 .= '</td>
                                <td valign="top" width="50%">
                                    <h3 style="' . $fontFamily . '">Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Nombre: </strong>' . $datos_cliente[0][0] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Correo: </strong>' . $datos_cliente[0][1] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Teléfono: </strong>' . $datos_cliente[0][8] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Rut: </strong>' . $datos_cliente[0][9] . '</li>
                                            <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Comenatrios Cliente: </strong>' . $datos_cliente[0][22] . '</li>
                                            
                                        </ul>
                                    </p>
                                    
    
                                </td>
                            </tr>
                        </table>
                        <br/>
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">';
	$msg2 .= '<tr>';
	if (!$transferencia) {
		$msg2 .= '
							<td valign="top" width="50%" height="100">
								<h3 style="' . $fontFamily . '">' . $texto . '</h3>
								<p>
									<ul>
										<li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Orden de compra: </strong>' . $oc . '</li>
										<li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Tipo de pago: </strong>' . $tipo_pago[tipo_pago] . '</li>
										<li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Tipo de cuota: </strong>' . $tipo_pago[tipo_cuota] . '</li>
										<li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Nº Cuotas: </strong>' . $tipo_pago[cuota] . '</li>
										<li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Fecha: </strong>' . $datos_cliente[0][21] . '</li>
									</ul>
								</p>
							</td>';
	}

	$msg2 .= '<td valign="top" width="50%">';
	if ($datos_cliente[0][14] != '') {
		$msg2 .= '<h3 style="' . $fontFamily . '">Datos empresa</h3>
                                    <ul>
                                        <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Nombre: </strong>' . $datos_cliente[0][18] . '</li>
                                        <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Direccion: </strong>' . $datos_cliente[0][11] . '</li>
                                        <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Giro: </strong>' . $datos_cliente[0][12] . '</li>
                                        <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Rut: </strong>' . $datos_cliente[0][14] . '</li>
                                        <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Email: </strong>' . $datos_cliente[0][13] . '</li>
                                        <li style="' . $fontFamily . '"><strong style="color:' . $color_logo . ';">Telefono: </strong>' . $datos_cliente[0][16] . '</li>
                                    </ul>';
	}
	$msg2 .= '</td>';





	$msg2 .= '
                            </tr>
                        </table>
                        
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="float:left; margin-top:50px;">
                            <tr>
                                <td valign="top">';
	$msg2 .= '<h3 style="' . $fontFamily . ' margin-bottom:20px;">PRODUCTOS COMPRADOS</h3>
                                    <p>' . $tabla_compra . '</p>';

	//Muestro los datos solo si es para enviar por correo
	if ($imprimir != 1) {
		$msg2 .= '<p align="center" style="margin:0;color:#000; ' . $fontFamily . '">Para ver el detalle de la compra y datos del cliente puede pinchar el siguiente <a href="' . $url_sitio . 'admin/index.php?op=35c&id=' . $id_pedido . '">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999; ' . $fontFamily . '">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;' . $fontFamily . '">Saludos cordiales, <strong>Equipo ' . $nombre_sitio . '</strong></p>';
	}
	//Muestro los datos solo si es para enviar por correo
	$msg2 .= '</td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';

	if ($imprimir == 1) {
		//Muestro el html para transformarlo en un pdf
		return $msg2;
	} else if ($correoForzarNotificacion != "") {
		//envio la notificacion al correo que me indican en la variable
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = opciones("Host");
		$mail->Port = opciones("Port");
		$mail->SMTPSecure = opciones("SMTPSecure");
		$mail->SMTPAuth = true;
		$mail->Username = opciones("Username");
		$mail->Password = opciones("Password");
		$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
		//envio el correo al mail que envio por variable
		$mail->addAddress($correoForzarNotificacion, "Notificacion Venta");
		$mail->Subject = $asunto;
		$mail->msgHTML($msg2);
		$mail->AltBody = $msg2;
		$mail->CharSet = 'UTF-8';
		if (!$mail->send()) {
			//return "Mailer Error: " . $mail->ErrorInfo;
			return 2;
		} else {
			return 1;
		}
		//envio la notificacion al correo que me indican en la variable

	} else {
		//si envio las otras 2 variables vacias ejecuto la funcion con normalidad
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = opciones("Host");
		$mail->Port = opciones("Port");
		$mail->SMTPSecure = opciones("SMTPSecure");
		$mail->SMTPAuth = true;
		$mail->Username = opciones("Username");
		$mail->Password = opciones("Password");
		$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));

		if (opciones("correo_admin1") != "") {
			$mail->addAddress(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
		}

		if (opciones("correo_admin2") != "") {
			$mail->addAddress(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
		}

		if (opciones("correo_admin3") != "") {
			$mail->addAddress(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
		}

		$mail->addAddress('ventas@moldeable.com', 'Moldeable');

		$mail->Subject = $asunto;
		$mail->msgHTML($msg2);
		$mail->AltBody = $msg2;
		$mail->CharSet = 'UTF-8';
		if (!$mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return 'envio exitoso';
		}
		//si envio las otras 2 variables vacias ejecuto la funcion con normalidad
	}
}

/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// 
FUNCTIONES COTIZACION*/


function totalCartCotizacion($cotizacion)
{
	global $db;
	if (!isset($_COOKIE[cotizacion])) {
		setcookie("cotizacion", "$cotizacion", time() + (365 * 24 * 60 * 60), "/");
	}
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',', $cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id => $qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto = consulta_bd("pd.id, pd.precio, pd.descuento, p.id", "productos_detalles pd join productos p on p.id = pd.producto_id", "pd.id=$prd_id", "");
			if ($is_cyber and is_cyber_product($producto[0][3])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber'] * $qty;
			} else {
				if ($producto[0][2] > 0) {
					$total += $producto[0][2] * $qty;
				} else {
					$total += $producto[0][1] * $qty;
				}
			}
		}
	}
	return round($total);
}

//cantidad de productos en el carro
function qty_proCotizacion()
{
	if (!isset($_COOKIE[cotizacion])) {
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',', $cotizacion);
		return count($items);
	} else {
		return 0;
	}
}

function ShowCartCotizacion()
{
	global $db;
	if (!isset($_COOKIE[cotizacion])) {
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',', $cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id => $qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku", "productos p, productos_detalles pd", "p.id=pd.producto_id and pd.id=$prd_id", "");
			if ($producto[0][0] != '') {
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}

			$valor = getPrecio($prd_id) * $qty;
			$valorUnitario = getPrecio($prd_id);



			$output[] .= '<div class="filaProductos" id="fila_carro_' . $prd_id . '">
                	<div class="imgFila ancho10">
						<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
							<img src="imagenes/productos/' . $thumbs . '" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCart">
						<div class="nombreFila">
							<a href="ficha/' . $producto[0][4] . '/' . url_amigables($producto[0][1]) . '">
								<span>' . $producto[0][1] . '</span>
							</a>
						</div>
						<div class="skuShowCart">SKU: ' . $producto[0][5] . '</div>
						<div class="botoneraShowCart">
							<a href="javascript:void(0)" onclick="eliminaItemCarroCotizacion(' . $prd_id . ')">Eliminar</a>
						</div>
						
					</div><!-- fin contInfoShowCart-->
                    
                    
					<div class="precioFila ancho20"><span>$' . number_format($valorUnitario, 0, ",", ".") . '</span></div>
                    <div class="cantFila ancho10">
                    	<div class="pull-left spinnerCarro">
                        	<input type="text" name="cant" class="campoCantCarroResumen" value="' . $qty . '" />
                            <div class="contFlechas">
                                <span class="mas" onclick="agregarElementoCarroCotizacion(' . $prd_id . ')"  rel="' . $prd_id . '">▲</span>
                                <span class="menos" onclick="quitarElementoCarroCotizacion(' . $prd_id . ')" rel="' . $prd_id . '">▼</span>
                            </div>
                    	</div>
                    </div>
                    <div class="totalFila ancho20"><span>$' . number_format($valor, 0, ",", ".") . '</span></div>
                    
                </div><!-- fin filaProductos-->';
		}
	} else {
		$output[] = '<div class="carroVacio">
						<p class="carroVacio">Su carro está vacío.</p><br />
					</div>';
	}
	return join('', $output);
}

/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/

function enviarComprobanteClienteCotizacion($oc)
{

	$nombre_sitio = opciones("nombre_cliente");
	$nombre_corto = opciones("dominio");
	$noreply = "no-reply@" . $nombre_corto;
	$url_sitio = "https://" . $nombre_corto;
	$logo = opciones("logo_mail");
	$correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");

	$datos_cliente = consulta_bd("nombre,email,id, apellido", "cotizaciones", "oc='$oc'", "");

	$nombre_cliente = $datos_cliente[0][0] . " " . $datos_cliente[0][3];
	$email_cliente = $datos_cliente[0][1];

	$id_cotizacion = $datos_cliente[0][2];

	$id_pedidoAdminitrador = $datos_cliente[0][4];

	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack", "productos_cotizaciones", "cotizacion_id=$id_cotizacion and codigo_pack is NULL", "");

	$detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos", "productos_cotizaciones", "cotizacion_id=$id_cotizacion and codigo_pack <> '' group by codigo_pack", "");

	$despacho = $datos_cliente[0][3];

	$asunto = "Cotización N°" . $oc . "";
	$tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';

	for ($i = 0; $i < sizeof($detalle_pedido); $i++) {
		$pD = consulta_bd("producto_id, nombre", "productos_detalles", "id=" . $detalle_pedido[$i][0], "");
		$id_prod = $pD[0][0];
		$campos = "id, nombre,thumbs";
		$tabla  = "productos";
		$where  = "id=$id_prod";
		$productos = consulta_bd($campos, $tabla, $where, "");
		$precio_unitario = $detalle_pedido[$i][2];
		$cantidad = $detalle_pedido[$i][1];
		$subtotal = $precio_unitario * $cantidad;

		$tabla_compra .= '<tr>';
		$tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="' . $url_sitio . '/imagenes/productos/' . $productos[0][2] . ' " width="90%"/>
												</p>
												
											</td>';
		$tabla_compra .= '  <td valign="top" align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">' . $productos[0][1] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $detalle_pedido[$i][3] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $detalle_pedido[$i][1] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$' . number_format($detalle_pedido[$i][2], 0, ",", ".") . '</p>
											</td>'; //nombre producto

		$tabla_compra .= '</tr>';
	}


	for ($i = 0; $i < sizeof($detalle_pack); $i++) {
		$skuPack = $detalle_pack[$i][4];

		$cantProdPack = $detalle_pack[$i][5];
		$pDP = consulta_bd("p.codigos, pd.precio, pd.descuento", "productos_detalles pd, productos p", "p.id= pd. producto_id and pd.sku='$skuPack'", "");
		$productosPorPack = explode(",", $pDP[0][0]);
		$cantArreglo = count($productosPorPack);


		$pD = consulta_bd("producto_id, nombre", "productos_detalles", "sku='$skuPack'", "");
		$id_prod = $pD[0][0];
		$campos = "id, nombre,thumbs";
		$tabla  = "productos";
		$where  = "id=$id_prod";
		$productos = consulta_bd($campos, $tabla, $where, "");

		if ($pDP[0][2] > 0) {
			$precio_unitario = $pDP[0][2];
		} else {
			$precio_unitario = $pDP[0][1];
		}
		//die("$cantProdPack");
		$cantidad = $detalle_pack[$i][1];
		$subtotalFila = $precio_unitario;


		$tabla_compra .= '<tr>';
		$tabla_compra .= '  <td valign="top" align="center" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="' . $url_sitio . '/imagenes/productos/' . $productos[0][2] . ' " width="90%"/>
												</p>
												
											</td>';
		$tabla_compra .= '  <td  align="center" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">' . $productos[0][1] . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $skuPack . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">' . $cantidad . '</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$' . number_format($subtotalFila, 0, ",", ".") . '</p>
											</td>'; //nombre producto
		$tabla_compra .= '</tr>';
	}




	$tabla_compra .= '</table>';

	$totales = consulta_bd("id, fecha_creacion, total", "cotizaciones", "oc='$oc'", "");

	$tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
	$tabla_compra .= '   <tfoot>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999; text-align:right;">$' . number_format($totales[0][2], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';


	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999; text-align:right;">por acordar</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:' . $color_logo . '; float:right;">Total Pedido:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:' . $color_logo . '; text-align:right;">$' . number_format($totales[0][2], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '   </tfoot>';
	$tabla_compra .= '</table>';

	$tabla_compra .= '<br /><br />';

	$msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>' . $nombre_sitio . '</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px ' . $color_logo . '; border-bottom:solid 4px ' . $color_logo . '; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="center" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="' . $url_sitio . '" style="color:#8CC63F;">
											<img src="' . $logo . '" alt="' . $logo . '" border="0" width="200"/>
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="center" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado ' . $nombre_cliente . '</p>
                                    <p style="float:right;width:100%;margin: 0px;">Gracias por cotizar</p>
                                    <p style="float:right;width:100%;margin: 0px;">Su número de cotización es: <br />
<strong style="color:' . $color_logo . ';">' . $oc . '</strong></p>
                                </th>
							</tr>
                        </table>
                        <br/><br/>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';

	$msg2 .= '<p style="color: #333;">PRODUCTOS COTIZADOS</p>

                                    <p>' . $tabla_compra . '</p>';

	$msg2 .= '<p align="center" style="margin:0;color:#000;">' . $msje_despacho . '</p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo ' . $nombre_sitio . '</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	//return $msg2;



	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	$mail->addAddress($email_cliente, $nombre_cliente);
	$mail->Subject = $asunto;
	$mail->msgHTML($msg2);
	$mail->AltBody = $msg2;
	$mail->CharSet = 'UTF-8';
	//send the message, check for errors
	if (!$mail->send()) {
		return "Mailer Error: " . $mail->ErrorInfo;
	} else {
		return 'envio exitoso';
	}
}

function enviarComprobanteAdminCotizacion($oc)
{

	$nombre_sitio = opciones("nombre_cliente");
	$nombre_corto = opciones("dominio");
	$noreply = "no-reply@" . $nombre_corto;
	$url_sitio = "https://" . $nombre_corto;
	$logo = opciones("logo_mail");
	$correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");

	$email_admin = 'ventas@moldeable.com';


	$datos_cliente = consulta_bd("nombre,email,id,apellido", "cotizaciones", "oc='$oc'", "");
	$nombre_cliente = $datos_cliente[0][0] . " " . $datos_cliente[0][3];
	$email_cliente = $datos_cliente[0][1];
	$id_cotizacion = $datos_cliente[0][2];

	$datos_cliente = consulta_bd("nombre, apellido,
    email,
    id,
    telefono,
    rut,
    fecha_creacion,
    comentarios", "cotizaciones", "oc='$oc'", "");
	$nombre_cliente = $datos_cliente[0][0] . " " . $datos_cliente[0][1];
	$nombre = $nombre_cliente;
	$email = $datos_cliente[0][2];
	$telefono = $datos_cliente[0][4];
	$rut = $datos_cliente[0][5];
	$comentarios = $datos_cliente[0][7];

	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack", "productos_cotizaciones", "cotizacion_id=$id_cotizacion", "");





	$tabla_compra = '
                <table width="100%" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;font-family: Trebuchet MS, sans-serif; color:#666;">
                    <tr>
                        <th align="center" width="16%">Imagen</th>
                        <th align="center" width="16%">Producto</th>
                        <th align="center" width="16%">SKU</th>
						<th align="center" width="16%">CÓDIGO PACK</th>
                        <th align="center" width="16%">Cantidad</th>
                        <th align="center" width="16%">Precio Unitario</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';

	for ($i = 0; $i < sizeof($detalle_pedido); $i++) {
		$pD = consulta_bd("producto_id, nombre", "productos_detalles", "id=" . $detalle_pedido[$i][0], "");
		$id_prod = $pD[0][0];
		$campos = "id, nombre,thumbs";
		$tabla  = "productos";
		$where  = "id=$id_prod";
		$productos = consulta_bd($campos, $tabla, $where, "");
		$precio_unitario = $detalle_pedido[$i][2];
		$cantidad = $detalle_pedido[$i][1];
		$subtotal = $precio_unitario * $cantidad;

		$tabla_compra .= '<tr>';
		$tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;">
												<img src="' . $url_sitio . '/imagenes/productos/' . $productos[0][2] . '" width="100"/>
											</td>';
		$tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">' . $productos[0][1] . '</td>'; //nombre producto
		$tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">' . $detalle_pedido[$i][3] . '</td>'; //codigo producto
		$tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">' . $detalle_pedido[$i][4] . '</td>'; //codigo pack

		$tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:#797979;">' . $detalle_pedido[$i][1] . '</td>'; //cantidad
		$tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:' . $color_logo . ';">$' . number_format($detalle_pedido[$i][2], 0, ",", ".") . '</td>'; //precio producto
		$tabla_compra .= '</tr>';
	}



	$tabla_compra .= '</table>';

	$totales = consulta_bd("id, fecha_creacion,total", "cotizaciones", "oc='$oc'", "");

	$tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
	$tabla_compra .= '   <tfoot>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999; text-align:right;">$' . number_format($totales[0][2], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';


	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:#999">Valor envío:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:#999">Según acuerdo</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '     <tr class="cart_total_price">';
	$tabla_compra .= '       <td align="right" width="85%"><span style="color:' . $color_logo . '">Total Pedido:</span></td>';
	$tabla_compra .= '       <td align="right" width="15%"><span style="color:' . $color_logo . '">$' . number_format($totales[0][2], 0, ",", ".") . '</span></td>';
	$tabla_compra .= '     </tr>';

	$tabla_compra .= '   </tfoot>';
	$tabla_compra .= '</table>';

	$tabla_compra .= '<br /><br />';


	$asunto = "Cotización N°" . $oc . "";
	$msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>' . $nombre_sitio . '</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px ' . $color_logo . '; border-bottom:solid 4px ' . $color_logo . '; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                	<p>
										<a href="' . $url_sitio . '" style="color:#8CC63F;">
											<img src="' . $logo . '" alt="' . $logo . '" border="0" width="200"/>
										</a>
									</p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px;line-height:20px;">' . $nombre_cliente . '</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">ha solicitado una cotización.</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">Su número de cotización es: <strong style="color:' . $color_logo . ';">' . $oc . '</strong></p>
                                    
                                </th>
                            </tr>
                        </table>
                        <br/><br/>
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">
                            <tr>';
	$msg2 .= '
                                <td valign="top" width="50%">
                                    <h3>Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:' . $color_logo . ';">Nombre: </strong>' . $nombre . '</li>
                                            <li><strong style="color:' . $color_logo . ';">Correo: </strong>' . $email . '</li>
                                            <li><strong style="color:' . $color_logo . ';">Teléfono: </strong>' . $telefono . '</li>
                                            <li><strong style="color:' . $color_logo . ';">Rut: </strong>' . $rut . '</li>
                                            <li><strong style="color:' . $color_logo . ';">Comenatrios Cliente: </strong>' . $comentarios . '</li>
                                            
                                        </ul>
                                    </p>
                                    
    
                                </td>
                            </tr>
                        </table>
                        <br/>';



	$msg2 .= '
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';

	$msg2 .= '<p style="color: #797979;">PRODUCTOS COTIZADOS</p>

                                    <p>' . $tabla_compra . '</p>';

	$msg2 .= '<p align="center" style="margin:0;color:#000;">Para ver el detalle de la cotización y datos del cliente puede pinchar el siguiente <a href="' . $url_sitio . 'admin/index.php?op=250c&id=' . $id_cotizacion . '">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;">Saludos cordiales, <strong>Equipo ' . $nombre_sitio . '</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	//return $msg2;

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));

	if (opciones("correo_admin1") != "") {
		$mail->addAddress(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
	}

	if (opciones("correo_admin2") != "") {
		$mail->addAddress(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
	}

	if (opciones("correo_admin3") != "") {
		$mail->addAddress(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
	}

	$mail->Subject = $asunto;
	$mail->msgHTML($msg2);
	$mail->AltBody = $msg2;
	$mail->CharSet = 'UTF-8';
	if (!$mail->send()) {
		return "Mailer Error: " . $mail->ErrorInfo;
	} else {
		return 'envio exitoso';
	}
}

/* FIN FUNCIONES COTIZACION  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

function get_precio($id, $tipo)
{
	$filas = consulta_bd("precio", "productos", "id = $id", "");
	$precio = $filas[0][0];
	$descuento = $filas[0][1];
	if ($tipo == 'normal') {
		$valor = $precio;
	} else if ($tipo == 'oferta') {
		if ($descuento != 0 and $descuento > 0) {
			$valor = round($precio * (1 - $descuento / 100));
		} else {
			$valor = $precio;
		}
	}
	return $valor;
}

function writeMiniCart()
{
	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		// Parse the cart session variable
		$items = explode(',', $cart);
		$s = (count($items) > 1) ? 's' : '';
		return count($items) . ' producto' . $s;
	} else {
		return ' Mis compras';
	}
}

function writeShoppingCart()
{
	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		// Parse the cart session variable
		$items = explode(',', $cart);
		$s = (count($items) > 1) ? 's' : '';
		return '<p>Ud tiene <strong>' . count($items) . ' producto' . $s . '</strong> en su carro de compras:</p>';
	}
}

function get_total_price()
{
	$total = 0;
	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];

	$items = explode(',', $cart);
	$contents = array();
	foreach ($items as $item) {
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}

	$i = 1;
	foreach ($contents as $prd_id => $qty) {

		$precio_final = getPrecio($prd_id) * $qty;

		if (!tieneDescuento($prd_id)) {
			$pd = consulta_bd("precio_cantidad", "productos_detalles", "id = $prd_id", "");
			if ($pd[0][0]) {
				$precios_cantidad = consulta_bd("nombre, rango, descuento", "precios_cantidades", "rango <= $qty AND productos_detall_id = $prd_id", "rango DESC");
				if ($precios_cantidad) {
					$pc = $precios_cantidad[0];
					$rango 			= $pc[1];
					$descuento 		= $pc[2];
					$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
					$precio_final 	= $valorUnitario * $qty;
				}
			}
		}



		$total += round($precio_final);
		$i++;
	}

	return $total;
}

function generateToken($oc, $monto)
{
	// generar token de forma aleatoria (sin ninguna relación al monto, la relación solo está en la bd)
	$token = md5(uniqid(microtime(), true));
	$insert = insert_bd('checkout_token', 'token, monto, oc', "'$token', $monto, '$oc'");

	if ($insert == true) {
		return $token;
	} else {
		return false;
	}
}

function save_in_mailchimp($oc, $donde)
{
	$mailchimp_module = opciones('mailchimp');
	if ($mailchimp_module == '1') {
		$list = consulta_bd('id_lista', 'mailchimp', "short_name = '$donde'", '');
		$cliente = consulta_bd('nombre, email', 'pedidos', "oc = '$oc'", '');

		$apikey = opciones('key_mailchimp');

		$n_cliente = explode(' ', $cliente[0][0]);
		$nombre = $n_cliente[0];
		$apellido = $n_cliente[1];

		$data['email'] = $cliente[0][1];
		$data['listId'] = $list[0][0];
		$data['nombre'] = $nombre;
		$data['apellido'] = $apellido;

		$mcc = new MailChimpClient($apikey);

		$mcc->subscribe($data);

		save_all_mailchimp($cliente[0][1], $nombre, $apellido);
	}
}

function save_all_mailchimp($email, $nombre, $apellido)
{
	$list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');
	$apikey = opciones('key_mailchimp');

	$data['email'] = $email;
	$data['listId'] = $exito_list[0][0];
	$data['nombre'] = $nombre;
	$data['apellido'] = $apellido;

	$mcc = new MailChimpClient($apikey);

	$mcc->subscribe($data);
}

function descuentoBy($cod)
{
	global $db;

	$session_correo = $_SESSION['correo'];

	/* Consultamos si el codigo de descuento es válido */
	$fecha = date('Ymd');
	$consulta = consulta_bd('id, valor, porcentaje, codigo, descuento_opcion_id,donde_el_descuento, cliente_id', 'codigo_descuento', "codigo = '{$cod}' COLLATE utf8_bin and activo = 1 and $fecha >= fecha_desde and $fecha <= fecha_hasta and (cantidad - usados > 0)", "");

	/* Si es válido, seguimos con el proceso de cálculo */
	if (is_array($consulta)) {

		if ($consulta[0][6] == 0) {
			$whatfor = $consulta[0][4]; // A qué se le asigna el descuento (Marca[1], Categorías[2], Subcategorías[3]).
			$id = $consulta[0][5]; // Nombre de la marca, categoría, subcategoría.
			$condition;
			if ($whatfor == 1) {
				$dev = consulta_bd('id, nombre', 'marcas', "", "");
				$tbl = 'marcas';
				$condition = 'and p.marca_id = ';
			} elseif ($whatfor == 2) {
				$dev = consulta_bd('id, nombre', 'categorias', "", "");
				$tbl = 'categorias';
				$condition = 'and cp.categoria_id = ';
			} else if ($whatfor == 3) {
				$dev = consulta_bd('id, nombre', 'subcategorias', "", "");
				$tbl = 'subcategorias';
				$condition = 'and cp.subcategoria_id = ';
			} else {
				$condition = '';
			}

			$id_dev = 0;
			for ($i = 0; $i < sizeof($dev); $i++) {
				$nom_compare = url_amigables($dev[$i][1]);
				if ($id == $dev[$i][0]) {
					$id_dev = $dev[$i][0];
				}
			}

			if ($condition != '') {
				$condition .= $id_dev;
			}

			/* Si el descuento es por porcentaje o por valor */
			$porcentaje = ($consulta[0][2] > 0) ? true : false;

			$cart = $_COOKIE['cart_alfa_cm'];
			if ($cart) {
				$items = explode(',', $cart);
				$contents = array();
				foreach ($items as $item) {
					$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
				}
				$output[] = '';

				$valorProducto = 0;
				$encontrados = 0;
				$no_descuento = 0;

				/* Recorremos los productos que tiene el carro */
				foreach ($contents as $prd_id => $qty) {
					$productos = consulta_bd("p.id, pd.precio, pd.descuento", "productos p, productos_detalles pd", "p.id = pd.producto_id and pd.id = $prd_id", "");

					$valorProducto = (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
					$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;

					/* Consulta que devuelve el id del producto si es que cumple la condición  */
					$productosDescuento = consulta_bd('p.id', 'productos p join lineas_productos cp on cp.producto_id = p.id', "p.id = {$productos[0][0]} {$condition} group by p.id", "");

					if (is_array($productosDescuento)) { // Si el producto pertenece a la condición del descuento
						$encontrados += $qty;
						$valProductosEncontrados += $valorProducto;

						if ($porcentaje) {
							if ($consulta[0][2] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							} else {
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - ' . $consulta[0][2] . '%';
								$calculoFinalValor = ($valProductosEncontrados * $consulta[0][2] / 100);
							}
						} else {
							if ($consulta[0][1] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							} else {
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - $' . $consulta[0][1];
								$calculoFinalValor = $consulta[0][1];
							}
						}
					} else {
						$no_descuento += $valorProducto;
					}
				} // End foreach

				$total = $calculoFinalValor + $no_descuento;
			}
			if ($encontrados > 0) {
				$resultado = true;
			} else {
				$resultado = false;
			}
		} else {
			// Si el código existe pero tiene a un usuario asociado
			$tieneAssoc = consulta_bd('cd.cliente_id, cd.codigo, c.email', 'codigo_descuento cd join clientes c on c.id = cd.cliente_id', "c.email = '{$_SESSION['correo']}'", '');

			if (is_array($tieneAssoc) > 0) {

				// Recorremos los descuentos que tiene asociado el cliente
				foreach ($tieneAssoc as $codigo) {

					if ($codigo[1] == $cod) {
						/* Si el descuento es por porcentaje o por valor */
						$porcentaje = ($consulta[0][2] > 0) ? true : false;

						// Trabajamos el carro activo
						$cart = $_COOKIE['cart_alfa_cm'];
						if ($cart) {
							$items = explode(',', $cart);
							$contents = array();
							foreach ($items as $item) {
								$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
							}
							$output[] = '';

							$valorProducto = 0;
							$encontrados = 0;
							$no_descuento = 0;

							$contador_ofertas = 0;
							$cont_general = 0;

							/* Recorremos los productos que tiene el carro */
							foreach ($contents as $prd_id => $qty) {
								$productos = consulta_bd("p.id, pd.precio, pd.descuento", "productos p, productos_detalles pd", "p.id = pd.producto_id and pd.id = $prd_id", "");

								if ($productos[0][2] > 0) {
									$valorTotal += 0;
									$contador_ofertas++;
								} else {
									$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
								}
								$cont_general++;
							} // End foreach carro

							if ($porcentaje) {
								$calculoFinalValor = ($valorTotal * $consulta[0][2] / 100);
							} else {
								$calculoFinalValor = $consulta[0][1];
							} // End if porcentaje

							if ($cont_general == 1) {
								if ($contador_ofertas > 0) {
									$resultado = false;
								} else {
									$resultado = true;
								}
							} else {
								$resultado = true;
							}
							// Si encuentra el código rompo el ciclo.
							break;
						} else {
							$resultado = false;
						}
					} // End if codigo == cod

				} // End foreach tieneAssoc

			} else { // Else tieneAssoc

				$resultado = false;
			}
		}
	} else { /* Else ---> if cont > 0 */
		$resultado = false;
	}/* End if cont > 0 */

	if ($resultado == true) {
		if (validarMontoDescuento($calculoFinalValor)) {
			$_SESSION['val_descuento'] = $calculoFinalValor;
			$_SESSION['descuento'] = $cod;
			return 1;
		} else {
			unset($_SESSION['descuento']);
			unset($_SESSION['val_descuento']);
			return 0;
		}
	} else {
		unset($_SESSION['descuento']);
		unset($_SESSION['val_descuento']);
		return 0;
	}
}

function validarMontoDescuento($valor)
{
	$precio_carro = (int)get_total_price();

	if ($precio_carro < (int)$valor || $precio_carro == (int)$valor) {
		return false;
	} else {
		return true;
	}
}

/* Retorna si un producto pertenece al cyberday
Parámetro => id producto madre */
function is_cyber_product($id)
{
	$sql = consulta_bd('p.cyber, pd.precio_cyber', 'productos p join productos_detalles pd on pd.producto_id = p.id', "p.id = $id", '');

	if ($sql[0][0] == 1 and $sql[0][1] > 0) {
		return true;
	} else {
		return false;
	}
}

// Retorna true si no encuentra al usuario en la tabla primera_compra.
// True: Enviar codigo, False: No enviar codigo
function enviarCodigo($oc)
{
	$usuario = consulta_bd('cliente_id', 'pedidos', "oc = '{$oc}'", '');
	$id_usuario = $usuario[0][0];

	$tieneDescuento = consulta_bd('cliente_id', 'first_buy', "cliente_id = {$id_usuario}");

	if (!is_array($tieneDescuento)) {
		return true;
	} else {
		return false;
	}

	if ($cont < 1) {
		return true;
	} else {
		return false;
	}
}

function enviarCodigoDescuento($oc, $notification)
{

	// Consulto si ya tiene un codigo de primera compra
	$consulta_pc = consulta_bd("codigo", "first_buy", "oc = '{$oc}'", "");
	if ($consulta_pc[0][0] == '' or $consulta_pc[0][0] == NULL) {
		// Si no tiene un código de descuento, se le crea uno (Notificación 1)
		$codigo_desc = explode("_", $oc);
		$codigo = "COD_" . $codigo_desc[1];

		$hoy = date('Y-m-d'); // Fecha desde que se envía el código (HOY)

		update_bd("first_buy", "codigo = '{$codigo}', fecha = '{$hoy}'", "oc = '{$oc}'");

		$id_cliente = consulta_bd('c.id', 'clientes c join pedidos p on c.email = p.email', "p.oc = '{$oc}'");

		// +3 meses de duración
		$nuevaFecha = strtotime('+3 month', strtotime($hoy));

		$hasta = date('Y-m-d', $nuevaFecha); // Fecha hasta

		// y le asignamos un codigo de descuento 
		insert_bd('codigo_descuento', 'cliente_id, codigo, porcentaje, activo, oc, fecha_desde, fecha_hasta, cantidad, descuento_opcion_id, donde_el_descuento', "{$id_cliente[0][0]},'$codigo', 10, 1, '$oc', '$hoy', '$hasta', 1, 4, 'Primera compra'");
	} else {
		$codigo = $consulta_pc[0][0];
	}

	$nombre_sitio = opciones("nombre_cliente");
	$nombre_corto = opciones("dominio");
	$noreply = "no-reply@" . $nombre_corto;
	$url_sitio = "https://" . $nombre_corto;
	$logo = opciones("logo_mail");
	$correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");

	$datos_cliente = consulta_bd("nombre,email,id,direccion, cliente_id, cliente_id", "pedidos", "oc='$oc'", "");
	$nombre_cliente = $datos_cliente[0][0];
	$email_cliente = $datos_cliente[0][1];
	$id_cliente = $datos_cliente[0][5];

	/*$header = "From: ".$nombre_sitio." <".$noreply.">\nReply-To:".$noreply."\n";
    $header .= "X-Mailer:PHP/".phpversion()."\n";
    $header .= "Mime-Version: 1.0\n";
    $header .= "Content-Type: text/html";*/

	if ($notification == 1) {
		$asunto = "Código descuento para tu próxima compra";
		$body_message = '<p>Muchas gracias por su preferencia. Adjuntamos un código por un 10% de descuento. Con él su próxima compra en nuestro sitio web será más conveniente. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="' . $nombre_corto . '">' . $nombre_sitio . '</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />
									
			<br />Recuerda que el código tiene una vigencia de 3 meses a partir de hoy.</p>
			<h2><strong>Código de descuento: ' . $codigo . '</strong></h2>';
	} else {
		$asunto = "Recuerda que tienes un descuento para tu próxima compra";
		$body_message = '<p>Recuerda que tienes un código de descuento por un 10% en tú próxima compra. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="https://www.mercadojardin.cl">mercadojardin.cl</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />

			<h2><strong>Código de descuento: ' . $codigo . '</strong></h2>';
	}

	$msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
        <title>' . $nombre_sitio . '</title>
		<style type="text/css">
				p, ul, a { 
					color:#666; 
					font-family: "Open Sans", sans-serif;
					background-color: #ffffff; 
					font-weight:300;
				}
				strong{
					font-weight:600;
				}
				a {
					color:#666;
				}
			</style>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px ' . $color_logo . '; border-bottom:solid 4px ' . $color_logo . '; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
            
					<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
						<tr>
							<th align="left" width="50%">
								<p>
									<a href="' . $url_sitio . '" style="color:#8CC63F;">
										<img src="' . $logo . '" alt="' . $logo . '" border="0" width="200"/>
									</a>
								</p>
							</th>
						</tr>
					</table>
					<br/><br/>
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   <p><strong>Estimado ' . $nombre_cliente . ':</strong></p>' .
		$body_message
		. '<p></p>
									<p>Muchas gracias<br /> Atte,</p>
									<p><strong>Equipo de ' . $nombre_sitio . '</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>
	';

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");

	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	//Set an alternative reply-to address

	$mail->addAddress($email_cliente, $nombre_cliente);
	$mail->Subject = $asunto;
	$mail->msgHTML($msg2);
	$mail->AltBody = $msg2;
	$mail->CharSet = 'UTF-8';
	//send the message, check for errors
	if (!$mail->send()) {
		return "Mailer Error: " . $mail->ErrorInfo;
	} else {
		return true;
	}
}

function get_products($filtro)
{

	if (isset($filtro['max']) and $filtro['max'] != false) {
		$campos = 'MAX(IF(pd.descuento > 0, pd.descuento, pd.precio))';
	} else {

		if (!isset($filtro['busqueda'])) {
			$campos = "DISTINCT(p.id), p.nombre, p.thumbs, pd.precio, pd.descuento, pd.stock, pd.id, p.descripcion, a.nombre,min(IF(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor,(case when pd.descuento > '0' then pd.descuento else pd.precio end) as precio_calculado, IFNULL(pp.posicion, 999999) as posicion, pd.id, p.thumb_cuadrado";
		} else {
			$campos = "DISTINCT(p.id), p.nombre, p.thumbs, pd.precio, pd.descuento, pd.stock, pd.id, p.descripcion, a.nombre,min(IF(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor,(case when pd.descuento > '0' then pd.descuento else pd.precio end) as precio_calculado, p.id, pd.id, p.thumb_cuadrado";
		}


		/*$campos = "DISTINCT(p.id), p.nombre, p.thumbs, li.precio, li.descuento, pd.stock, pd.id, p.descripcion, a.nombre,min(IF(li.descuento > 0, li.descuento, li.precio)) as valorMenor,(case when li.descuento > '0' then li.descuento else li.precio end) as precio_calculado, IFNULL(pp.posicion, 999999) as posicion, li.lista_precio_id, p.thumb_cuadrado";*/
	}

	$tablas = 'productos p 
	INNER JOIN (
		SELECT producto_id, MIN(if(descuento > 0, descuento, precio)) as valorMenor
		FROM productos_detalles
		GROUP BY producto_id
	) sub ON p.id = sub.producto_id
	JOIN productos_detalles pd ON pd.producto_id = sub.producto_id AND if(pd.descuento > 0, pd.descuento, pd.precio) = sub.valorMenor
	LEFT JOIN lineas_productos lp ON p.id = lp.producto_id 
	LEFT JOIN lineas l ON l.id = lp.linea_id 
	LEFT JOIN categorias c ON c.id = lp.categoria_id ';

	if (!isset($filtro['busqueda'])) {
		$tablas .= 'LEFT JOIN posicion_productos pp ON p.id = pp.producto_id ';
	}


	$tablas .= 'JOIN artistas a ON a.id = p.artista_id
	JOIN colores_productos color ON color.producto_id = p.id
	JOIN tamaños tam ON tam.id = pd.tamaño_id
	LEFT JOIN grupo_tamanios gt ON gt.id = tam.grupo_tamanio_id';

	/*$tablas = 'productos p 
	LEFT JOIN lineas_productos lp ON p.id = lp.producto_id 
	JOIN productos_detalles pd ON pd.producto_id = p.id 
	LEFT JOIN lineas l ON l.id = lp.linea_id 
	LEFT JOIN categorias c ON c.id = lp.categoria_id
	LEFT JOIN posicion_productos pp ON p.id = pp.producto_id 
	JOIN artistas a ON a.id = p.artista_id
	JOIN lista_precio_producto li ON li.productos_detalle_id = pd.id
	JOIN colores_productos color ON color.producto_id = p.id';*/
	$where = 'p.publicado = 1 and pd.publicado = 1';


	if (isset($filtro['orden']) and $filtro['orden'] != null and $filtro['orden'] != '') {
		$last .= $filtro['orden'];
	} else if (isset($filtro['random']) and $filtro['random'] != false) {
		$last .= '';
	} else {
		$last .= 'p.id desc';
	}

	if (isset($filtro['ficha']) and $filtro['ficha'] != null and $filtro['ficha'] != '') {
		$where .= ' AND p.id = ' . $filtro['ficha'];
	}

	if (isset($filtro['random']) and $filtro['random'] != false and $filtro['random'] != '') {
		$last .= ' RAND()';
	}

	// Limite normal
	if (isset($filtro['limite']) and $filtro['limite'] != null and $filtro['limite'] != '') {
		$last .= ' LIMIT ' . $filtro['limite'];
	}

	// Límite para paginado
	if (isset($filtro['limit']) and $filtro['limit'] != null and $filtro['limit'] != '') {
		$last .= ' ' . $filtro['limit'];
	}

	if (isset($filtro['linea']) and $filtro['linea'] != null and $filtro['linea'] != '') {
		$where .= ' AND pp.linea_id = ' . $filtro['linea'];
	}

	if (isset($filtro['categoria']) and $filtro['categoria'] != null and $filtro['categoria'] != '') {
		$where .= ' AND pp.categoria_id = ' . $filtro['categoria'];
	}

	if (isset($filtro['subcategoria']) and $filtro['subcategoria'] != null and $filtro['subcategoria'] != '') {
		$where .= " AND pp.subcategoria_id = {$filtro['subcategoria']}";
	}

	if (isset($filtro['destacados']) and $filtro['destacados'] != null and $filtro['destacados'] != false) {
		$where .= " AND p.recomendado = 1";
	}

	if (isset($filtro['home']) and $filtro['home'] != null and $filtro['home'] != false) {
		$where .= " AND p.home = 1";
	}

	if (isset($filtro['id_ficha']) and $filtro['id_ficha'] != null and $filtro['id_ficha'] != '') {
		$where .= " AND p.id != {$filtro['id_ficha']}";
	}

	if (isset($filtro['rel']) and $filtro['rel'] != null and $filtro['rel'] != '') {
		$where .= " AND p.id != " . $filtro['rel'];
	}

	if (isset($filtro['cantidad']) and $filtro['cantidad'] != false and $filtro['cantidad'] != '') {
		$where .= " AND pd.precio_cantidad = 1";
	}

	if (isset($filtro['artista']) and $filtro['artista'] != false and $filtro['artista'] != '') {
		$where .= " AND p.artista_id = " . $filtro['artista'];
	}

	if (isset($filtro['ofertas']) and $filtro['ofertas'] != false and $filtro['ofertas'] != '') {
		$where .= " AND pd.descuento > 0";
	}


	$whereLineas = '';
	if (isset($filtro['lineas']) and $filtro['lineas'] != NULL and $filtro['lineas'] != '') {
		$lineas_seleccionadas = $filtro['lineas'];
		$explode_lineas = explode('-', $lineas_seleccionadas);
		$contador_lineas = 0;

		$whereLineas = ' AND (';
		foreach ($explode_lineas as $item_line) {
			if ($contador_lineas == 0) {
				$whereLineas .= "l.id = $item_line";
			} else {
				$whereLineas .= " OR l.id = $item_line";
			}
			$contador_lineas++;
		}
		$whereLineas .= ')';
	}

	$whereCategorias = '';
	if (isset($filtro['categorias']) and $filtro['categorias'] != NULL and $filtro['categorias'] != '') {
		$categorias_seleccionadas = $filtro['categorias'];
		$explode_categorias = explode('-', $categorias_seleccionadas);

		$contador_categorias = 0;

		$whereCategorias = ' AND (';
		foreach ($explode_categorias as $item_cat) {
			if ($contador_categorias == 0) {
				$whereCategorias .= "c.id = $item_cat";
			} else {
				$whereCategorias .= " OR c.id = $item_cat";
			}
			$contador_categorias++;
		}
		$whereCategorias .= ')';
	}

	$whereArtistas = '';
	if (isset($filtro['artistas']) and $filtro['artistas'] != NULL and $filtro['artistas'] != '') {
		$categorias_seleccionadas = $filtro['artistas'];
		$explode_artistas = explode('-', $categorias_seleccionadas);

		$contador_artistas = 0;

		$whereArtistas = ' AND (';
		foreach ($explode_artistas as $item_cat) {
			if ($contador_artistas == 0) {
				$whereArtistas .= "a.id = $item_cat";
			} else {
				$whereArtistas .= " OR a.id = $item_cat";
			}
			$contador_artistas++;
		}
		$whereArtistas .= ')';
	}

	$whereColores = '';
	if (isset($filtro['colores']) and $filtro['colores'] != NULL and $filtro['colores'] != '') {
		$colores_seleccionadas = $filtro['colores'];
		$explode_colores = explode('-', $colores_seleccionadas);

		$contador_colores = 0;

		$whereColores = ' AND (';
		foreach ($explode_colores as $item_cat) {
			if ($contador_colores == 0) {
				$whereColores .= "color.color_id = $item_cat";
			} else {
				$whereColores .= " OR color.color_id = $item_cat";
			}
			$contador_colores++;
		}
		$whereColores .= ')';
	}

	$whereTamanio = '';
	if (isset($filtro['tamaños']) and $filtro['tamaños'] != NULL and $filtro['tamaños'] != '') {

		$tamanio_seleccionados = $filtro['tamaños'];
		$explode_tamanio = explode('-', $tamanio_seleccionados);

		$contador_tamanio = 0;

		$whereTamanio = ' AND (';
		foreach ($explode_tamanio as $item_fil) {
			if ($contador_tamanio == 0) {
				$whereTamanio .= "gt.id = $item_fil";
			} else {
				$whereTamanio .= " OR gt.id = $item_fil";
			}
			$contador_tamanio++;
		}
		$whereTamanio .= ')';
	}

	if (isset($filtro['desde']) and $filtro['desde'] != NULL and $filtro['desde'] != '') {
		$having .= 'precio_calculado >= ' . $filtro['desde'];
	}

	if (isset($filtro['hasta']) and $filtro['hasta'] != NULL and $filtro['hasta'] != '') {
		$having .= ' and precio_calculado <= ' . $filtro['hasta'];
	}

	if (isset($filtro['busqueda']) and $filtro['busqueda'] != null and trim($filtro['busqueda']) != '') {
		$busqueda = $filtro['busqueda'];
		$where .= " AND (p.nombre LIKE '%$busqueda%' OR pd.sku LIKE '%$busqueda%' OR a.nombre LIKE '%$busqueda%' OR c.nombre LIKE '%$busqueda%' OR l.nombre LIKE '%$busqueda%')";
	}

	// return "SELECT $campos FROM $tablas WHERE $where $whereArtistas $whereLineas $whereCategorias $whereColores GROUP BY p.id HAVING MIN(IF(pd.descuento > 0, pd.descuento, pd.precio)) $having ORDER BY $last";

	/* FIN FILTROS */

	if (isset($filtro['max']) and $filtro['max'] != false) {
		$productos = consulta_bd($campos, $tablas, $where . $whereArtistas . $whereLineas . $whereCategorias . $whereColores . $whereTamanio, $last);
	} else {
		if ($having != '') {
			// return 1;
			$productos = consulta_bd($campos, $tablas, $where . $where_posicion . $whereArtistas . $whereLineas . $whereCategorias . $whereColores . $whereTamanio . " GROUP BY p.id HAVING $having", $last);
		} else {
			// return 2;
			$productos = consulta_bd($campos, $tablas, $where . $where_posicion . $whereArtistas . $whereLineas . $whereCategorias . $whereColores . $whereTamanio . " GROUP BY p.id", $last);
		}
	}

	// Retorno Max
	//  return "SELECT $campos FROM $tablas WHERE $where$whereArtistas$whereLineas$whereCategorias$whereColores ORDER BY $last";

	// return "SELECT $campos FROM $tablas WHERE $where$where_posicion$whereArtistas$whereLineas$whereCategorias$whereColores GROUP BY p.id ORDER BY $last";

	$index = 0;
	$count = 0;

	if (is_array($productos)) {
		if (isset($filtro['max']) and $filtro['max'] != false) {
			$out['valor_max'] = $productos[0][0];
		} else {


			foreach ($productos as $aux) {

				$lista_id = $aux[12];

				$out['productos']['producto'][$index]['id_producto'] = $aux[0]; // Madre
				$out['productos']['producto'][$index]['id_hijo'] = $aux[6]; // Hijo
				$out['productos']['producto'][$index]['nombre'] = $aux[1];
				$out['productos']['producto'][$index]['imagen_grilla'] = ($aux[2] != NULL and $aux[2] != '' and is_file("imagenes/productos/{$aux[2]}")) ? imagen('imagenes/productos/', $aux[2]) : imagen("img/", "sinFotoGrid.jpg");
				$out['productos']['producto'][$index]['precio'] = $aux[3];
				$out['productos']['producto'][$index]['descuento'] = $aux[4];
				// $out['productos']['producto'][$index]['precio_cyber'] = $aux[12];
				// $out['productos']['producto'][$index]['porcentaje'] = (($aux[3] - $aux[4]) / $aux[3]) * 100;
				$out['productos']['producto'][$index]['stock'] = $aux[5];
				$out['productos']['producto'][$index]['descripcion'] = $aux[7];
				$out['productos']['producto'][$index]['nombre_seteado'] = url_amigables($aux[1]);
				$out['productos']['producto'][$index]['artista'] = $aux[8];
				$out['productos']['producto'][$index]['lista_id'] = $lista_id;
				$out['productos']['producto'][$index]['thumb_cuadrado'] = ($aux[13] != NULL and $aux[13] != '' and is_file("imagenes/productos/{$aux[13]}")) ? imagen('imagenes/productos/', $aux[13]) : imagen("img/", "sinFotoGridBox.jpg");

				// $out['productos']['producto'][$index]['oferta_tiempo_hasta'] = $aux[19];
				// $out['productos']['producto'][$index]['oferta_tiempo_descuento'] = $aux[20];

				$index++;
				$count++;
			}
			$out['productos']['total'][] = $count;
		}
	} else {
		return false;
	}

	return $out;
}


function cambiarMoneda($valor)
{
	$moneda = (!isset($_SESSION['usd'])) ? 'pesos' : (int)$_SESSION['usd'];

	if ($moneda != 'pesos') {
		$final = 'US $' . round($valor / $moneda, 2);
	} else {
		$final = '$' . number_format($valor, 0, ',', '.');
	}

	return $final;
}

function productoGuardado($producto_id)
{
	// producto id detalle
	if (!isset($_COOKIE['usuario_id'])) {
		return false;
	}

	$cliente = (int)$_COOKIE['usuario_id'];
	$query = consulta_bd('id', 'productos_guardados', "productos_detalle_id = $producto_id and cliente_id = $cliente", '');

	return (is_array($query)) ? true : false;
}

function filtros_aplicados($artistas, $filtro_desde, $filtro_hasta, $categorias, $colores, $tamaños)
{

	if ($artistas != '') {
		$explode_art = explode('-', $artistas);

		$index = 0;
		foreach ($explode_art as $item) {
			$artista = consulta_bd('id, nombre', 'artistas', "id = $item", '');

			if (is_array($artista)) {
				$out['filtro_artistas'][$index]['id'] = $artista[0][0];
				$out['filtro_artistas'][$index]['nombre'] = $artista[0][1];

				$index++;
			}
		}
	}

	if (!is_null($categorias) and $categorias != '') {
		$explode_cat = explode('-', $categorias);
		$index = 0;
		foreach ($explode_cat as $item) {
			$categoria = consulta_bd('id, nombre', 'categorias', "id = $item", '');

			if (is_array($categoria)) {
				$out['filtro_categorias'][$index]['id'] = $categoria[0][0];
				$out['filtro_categorias'][$index]['nombre'] = $categoria[0][1];

				$index++;
			}
		}
	}

	if ($colores != '') {
		$explode_colores = explode('-', $colores);
		$index = 0;
		foreach ($explode_colores as $item) {

			$color = consulta_bd('id, nombre', 'colores', "id = $item", '');

			if (is_array($color)) {
				$out['filtro_colores'][$index]['id'] = $color[0][0];
				$out['filtro_colores'][$index]['nombre'] = $color[0][1];

				$index++;
			}
		}
	}

	if ($tamaños != '') {
		$explode_tamaños = explode('-', $tamaños);
		$index = 0;
		foreach ($explode_tamaños as $item) {

			$tamaño = consulta_bd('id, nombre', 'tamaños', "id = $item", '');

			if (is_array($tamaño)) {
				$out['filtro_tamaños'][$index]['id'] = $tamaño[0][0];
				$out['filtro_tamaños'][$index]['nombre'] = $tamaño[0][1];

				$index++;
			}
		}
	}

	if ($filtro_hasta != 0) {
		$out['filtro_precios'][0] = 'Desde ' . number_format($filtro_desde, 0, ',', '.') . ' hasta ' . number_format($filtro_hasta, 0, ',', '.');
	}

	return $out;
}

function ShowCart2()
{
	global $db;
	$carro = cartPrepare();

	if (is_array($carro)) :
		foreach ($carro as $item) :
			//$producto = consulta_bd('p.nombre, a.nombre, pd.sku, t.nombre, s.nombre, pd.precio, pd.descuento, p.thumbs, p.id, pd.stock', 'productos p JOIN artistas a ON a.id = p.artista_id JOIN productos_detalles pd ON pd.producto_id = p.id JOIN tamaños t ON t.id = pd.tamaño_id JOIN lista_precio_producto lpp ON lpp.productos_detalle_id = pd.id JOIN lista_precios l ON l.id = lpp.lista_precio_id', "pd.id = $item[id] AND l.id = $item[lista]", '');


			$producto = consulta_bd('p.nombre, a.nombre, pd.sku, t.nombre, pd.bajada, pd.precio, pd.descuento, p.thumbs, p.id, pd.stock', 'productos p, artistas a, productos_detalles pd, tamaños t', "pd.producto_id = p.id and pd.id = $item[id] AND t.id = pd.tamaño_id and a.id = p.artista_id", '');


			$linkProducto = 'ficha/' . $producto[0][8] . '/' . url_amigables($producto[0][0]);

			$thumProducto = ($producto[0][7] != null and $producto[0][7] != '' and is_file("imagenes/productos/{$producto[0][7]}")) ? imagen("imagenes/productos/", $producto[0][7]) : imagen("img/", "sinFotoGrid.jpg");

			$output[] .= '<div class="grid-fila_productos">
			<div class="col_cart">
				<a href="' . $linkProducto . '">
					<img src="' . $thumProducto . '" width="100%">
				</a>
			</div>
			<div class="col_cart">
				<span class="nombre_carro">' . $producto[0][0] . '</span>
				<span class="artista_carro">' . $producto[0][1] . '</span>
				<span class="cod_carro">COD. ' . $producto[0][2] . '</span>

				<div class="detalle_prd">
					<span>' . $producto[0][3] . '</span>
					<span>' . $producto[0][4] . '</span>';
			if ($item['marco'] != '') {
				$output[] .= '<span>Marco: ' . $item['marco'] . '</span>';
			}
			$output[] .= '</div>
			</div>
			<div class="col_cart">';
			if ($producto[0][6] > 0) {
				$output[] .= '<span class="descuento_carro">' . cambiarMoneda($producto[0][5]) . '</span>
				<span class="precio_carro">' . cambiarMoneda($producto[0][6]) . '</span>';
			} else {
				$output[] .= '<span class="precio_carro">' . cambiarMoneda($producto[0][5]) . '</span>';
			}
			$output[] .= '</div>
			<div class="col_cart">
				<div class="box_main_qty_cart">
	                <div class="box_qty" data-id="' . $item['id'] . '">
	                    
	                    <input type="text" class="cantidad_ficha" value="' . $item[cantidad] . '" onclick="openListQty(this)" readonly>

	                </div>

	                <div class="box_select_stock" data-id="' . $item['id'] . '" data-list="' . $item['lista'] . '" data-visible="0">';
			if ($producto[0][9] > 10) {
				for ($x = 0; $x < 10; $x++) {
					$pos = $x + 1;
					$output[] .= '<div class="row_stock_ficha" onclick="changeQtyCart(this)" data-val="' . $pos . '">' . $pos . '</div>';
				}
				$output[] .= '<div class="row_stock_ficha" onclick="changeQtyCart(this)" data-val="add">+ más</div>';
			} else {
				for ($x = 0; $x < $producto[0][9]; $x++) {
					$pos = $x + 1;
					$output[] .= '<div class="row_stock_ficha" onclick="changeQtyCart(this)" data-val="' . $pos . '">' . $pos . '</div>';
				}
			}
			$output[] .= '</div>
	            </div>
			</div>
			<div class="col_cart">';
			if ($producto[0][6] > 0) {
				$output[] .= '<span class="descuento_carro">' . cambiarMoneda($producto[0][5] * $item['cantidad']) . '</span>
				<span class="precio_carro blue_text">' . cambiarMoneda($producto[0][6] * $item['cantidad']) . '</span>';
			} else {
				$output[] .= '<span class="precio_carro blue_text">' . cambiarMoneda($producto[0][5] * $item['cantidad']) . '</span>';
			}
			$output[] .= '</div></div>
			<div class="foot_carro">
				<a href="javascript:void(0)" class="eliminarProducto" onclick="delCart(this)" data-rel="' . $item['id'] . '" data-list="' . $item['lista'] . '">Eliminar del carro</a>
				<a href="javascript:void(0)" class="moverProducto" onclick="moveProdFav(this)" data-id="' . $item['id'] . '" data-list="' . $item['id'] . '">Mover a productos guardados</a>
			</div>';

		endforeach;
	endif;

	return join('', $output);
}

function cartPrepare()
{

	$carro = json_decode($_COOKIE['cart_alfa_cm'], true);

	return $carro;
}

function valorTotalCarro($comuna_id)
{
	$carro = cartPrepare();
	$total = 0;

	foreach ($carro as $item) {
		$qty = $item['cantidad'];
		//$producto = consulta_bd('lpp.precio, lpp.descuento', 'productos p JOIN productos_detalles pd ON pd.producto_id = p.id JOIN lista_precio_producto lpp ON lpp.productos_detalle_id = pd.id', "pd.id = $item[id] AND lpp.lista_precio_id = $item[lista]", '');

		$producto = consulta_bd('pd.precio, pd.descuento', 'productos p, productos_detalles pd', "pd.producto_id = p.id and pd.id = $item[id]", '');


		if ($producto[0][1] > 0) {
			$total += $producto[0][1] * $qty;
		} else {
			$total += $producto[0][0] * $qty;
		}
	}

	$neto = round($total / 1.19);
	$iva = $total - $neto;
	$despacho = (is_numeric(valorDespacho($comuna_id))) ? valorDespacho($comuna_id) : 'x';
	$total = $total + $despacho;

	return array('subtotal' => $neto, 'iva' => $iva, 'despacho' => $despacho, 'total' => $total);
}

function setFechaPedido($fecha)
{
	$fecha 	= explode('-', $fecha);
	$anio 	= $fecha[0];
	$mes 	= $fecha[1];
	$dia 	= $fecha[2];

	switch ($mes) {
		case '01':
			$mes = 'Enero';
			break;
		case '02':
			$mes = 'Febrero';
			break;
		case '03':
			$mes = 'Marzo';
			break;
		case '04':
			$mes = 'Abril';
			break;
		case '05':
			$mes = 'Mayo';
			break;
		case '06':
			$mes = 'Junio';
			break;
		case '07':
			$mes = 'Julio';
			break;
		case '08':
			$mes = 'Agosto';
			break;
		case '09':
			$mes = 'Septiembre';
			break;
		case '10':
			$mes = 'Octubre';
			break;
		case '11':
			$mes = 'Noviembre';
			break;
		case '12':
			$mes = 'Diciembre';
			break;
	}

	return $dia . ' de ' . $mes . ' de ' . $anio;
}

function infoPedido($id)
{

	$query = consulta_bd('retiro_en_tienda, medio_de_pago, total, valor_despacho, total_pagado, descuento, direccion, comuna, region,transaction_date, mp_transaction_date, shares_number, mp_cuotas, card_number, mp_card_number, payment_type_code, mp_payment_type, authorization_code, mp_auth_code', 'pedidos', "id = $id", '');

	$medio_pago = $query[0][1];

	$out['retiro_en_tienda'] 	= $query[0][0];
	$out['medio_de_pago']		= $query[0][1];
	$out['subtotal']			= $query[0][2];
	$out['despacho']			= $query[0][3];
	$out['total_pagado']		= $query[0][4];
	$out['descuento']			= $query[0][5];
	$out['direccion']			= $query[0][6];
	$out['comuna']				= $query[0][7];
	$out['region']				= $query[0][8];
	$out['fecha_transaccion']	= ($medio_pago == 'webpay') ? $query[0][9] : $query[0][10];
	$out['cuotas']				= ($medio_pago == 'webpay') ? $query[0][11] : $query[0][12];
	$out['card_number']			= ($medio_pago == 'webpay') ? $query[0][13] : $query[0][14];
	$out['tipo_pago']			= ($medio_pago == 'webpay') ? $query[0][15] : $query[0][16];
	$out['auth_code']			= ($medio_pago == 'webpay') ? $query[0][17] : $query[0][18];

	return $out;
}

function tiene_posicion($columna, $id, $producto_id)
{
	// return "SELECT posicion FROM posicion_productos WHERE $columna = $id AND producto_id = $producto_id AND posicion IS NOT NULL";
	$query = consulta_bd('posicion', "posicion_productos", "$columna = $id AND producto_id = $producto_id AND posicion IS NOT NULL", '');
	// Si no es arreglo lo elimino en el actions.php
	return is_array($query);
}
