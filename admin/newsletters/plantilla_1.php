<?php include('../conf.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400|Ubuntu:300,400" rel="stylesheet">
	<?php 
	$id = $_GET['id'];
	$nombre_sitio = opciones('nombre_cliente');
	$color_principal = opciones('color_logo');
	$logo = opciones('logo_mail');

	$banners = consulta_bd('banner_principal,banner_secundario', 'creador_newsletteres', "id = $id", '');
	$lineas = consulta_bd('id, nombre', 'lineas', '', 'id asc LIMIT 5');

	$banner1 = $banners[0][0];
	$banner2 = $banners[0][1];

	// Productos que se mostrarán en lista
	$many_to_many = consulta_bd('producto_id', 'productos_creador_newsletteres', "creador_newsletter_id = $id", '');
	// Productos que se mostrarán en grilla
	$has_many = consulta_bd('producto_relacionado', 'productos_grilla', "creador_newsletter_id = $id", '');

	// Si tiene many to many creamos un arreglo.
	if (is_array($many_to_many)) {
		$index = 0;
		foreach ($many_to_many as $item) {
			$productos_asociados = consulta_bd('p.nombre, pd.precio, pd.descuento, p.thumbs, m.imagen,p.id', 'productos p join productos_detalles pd on p.id = pd.producto_id join marcas m on m.id = p.marca_id', "p.id = {$item[0]}", '');

			$prod_rows['producto'][$index]['id'] = $productos_asociados[0][5];
			$prod_rows['producto'][$index]['nombre'] = $productos_asociados[0][0];
			$prod_rows['producto'][$index]['precio'] = $productos_asociados[0][1];
			$prod_rows['producto'][$index]['descuento'] = $productos_asociados[0][2];
			$prod_rows['producto'][$index]['imagen'] = $productos_asociados[0][3];
			$prod_rows['producto'][$index]['imagen_marca'] = $productos_asociados[0][4];
			$index++;
		}	
	}else{
		$prod_rows = "No hay productos asociados.";
	}

	// Si tiene has many creamos un arreglo.
	if (is_array($has_many)) {
		$index = 0;
		foreach ($has_many as $item) {
			$productos_has_many = consulta_bd('p.nombre, IF(pd.descuento > 0, pd.descuento, pd.precio), p.thumbs, m.imagen,p.id', 'productos p join productos_detalles pd on p.id = pd.producto_id join marcas m on m.id = p.marca_id', "p.id = $item[0] limit 4", '');

			$prod_bottom['producto'][$index]['id'] = $productos_has_many[0][4];
			$prod_bottom['producto'][$index]['nombre'] = $productos_has_many[0][0];
			$prod_bottom['producto'][$index]['precio'] = $productos_has_many[0][1];
			$prod_bottom['producto'][$index]['imagen'] = $productos_has_many[0][2];
			$prod_bottom['producto'][$index]['imagen_marca'] = $productos_has_many[0][3];
			$index++;
		}
	}else{
		$prod_bottom = "No hay productos";
	}
	?>
	<style>
		*{
			font-family: 'Ubuntu', sans-serif; 
			color: #666666;
		}
		.button-prod{
			font-family: 'Lato', sans-serif;
			font-weight: 300;
			font-size: 14px;
		}
		.box-prd-bottom{
			background-size:100%!important;
			background-position-y:20px!important;
			vertical-align:top!important;
			padding:10px;
			position: relative;
		}
		.nombre-prd-bottom{
			position: absolute;left: 5px;font-size: 12px;bottom: 11px;
		}
		.precio-prd-bottom{
			color: #fa6027!important;
		}

		.name-who{
		}
	</style>
	<div style="width: 100%; background: #f8f8f8;">
		<table style="background:#fff;width:100%;max-width:600px;margin: auto; border-top: 2px solid <?=$color_principal?>" cellspacing="0">
			<tr><!-- Header -->
				<td style="padding: 20px;"><img src="<?= $logo ?>" alt="<?= $nombre_sitio ?>" width="160"></td>
				
				<td style="padding: 20px 20px 20px 6px;float: right;">
					<span style="font-size: 12px;">Servicio al cliente</span><br>
					<span style="color:<?= $color_principal ?>;font-size: 12px;">+569 2 2414 2563</span>
				</td>
				<td style="text-align: right;float: right;padding-top: 27px;">
					<img src="<?=$url_base?>admin/newsletters/img/whatsapp.jpg" alt="WhatsaApp" style="vertical-align: middle;">
				</td>
			</tr>
		</table>
		<table style="background:#fff;width:100%;max-width: 600px;margin: auto; border-top: 1px solid #dddddd;border-bottom:1px solid #dddddd;padding: 10px" cellspacing="0">
			<tr>
				<?php foreach ($lineas as $menu): 
					$url = $url_base.'lineas/'.$menu[0].'/'.url_amigables($menu[1]); ?>
					<td style="text-align: center;">
						<a href="<?= $url ?>" style="text-decoration:none;color: #8a8a8a;font-weight: 300;font-size: 12px;"><?=$menu[1]?></a>
					</td>
				<?php endforeach ?>
			</tr>
		</table>
	
		<table style="width:100%;max-width: 600px;margin: auto;" cellspacing="0">
			<tr>
				<td>
				<?php if ($banner1 != null || $banner1 != '') {
					echo '<img src="'.$url_base.'imagenes/creador_newsletteres/'.$banner1.'" alt="">';
				}else{
					echo '<img src="'.$url_base.'admin/newsletters/img/no-fotob1.jpg" alt="">';
				} ?>
				</td>	
			</tr>
		</table>
		<table style="width:100%;max-width: 600px;margin: auto;background: #fff;padding:20px;">
			<?php 
			if (is_array($prod_rows)) {
				foreach ($prod_rows['producto'] as $item) {
					$nombre_set = url_amigables($item['nombre']);
					echo '<tr style="margin-bottom:20px;">
					<td style="border:1px solid #f1f1f1;padding:20px;background:url('.$url_base.'imagenes/productos/'.$item['imagen'].') no-repeat;background-size: 40%;background-position:100%;">
						<img src="'.$url_base.'imagenes/marcas/'.$item['imagen_marca'].'" width="82" /><br>
						<span style="color=#666">'.strtoupper($item['nombre']).'</span><br>';
						if ($item['descuento'] > 0) {
							echo '<span style="font-size:12px;color:#999;">Normal: $'.number_format($item['precio'], 0, ',','.').'</span><br>
						<span style="color:'.$color_principal.';font-size:14px;">$'.number_format($item['descuento'], 0, ',','.').'</span>';
						}else{
							echo '<span style="color:'.$color_principal.';font-size:14px;">$'.number_format($item['precio'], 0, ',','.').'</span>';
						}
						echo '<br><br><br><a href="'.$url_base.'/ficha/'.$item['id'].'/'.$nombre_set.'" class="button-prod" style="background:'.$color_principal.';padding:12px 28px;color:#fff;text-decoration:none;">Ver producto</a>
					</td>
					</tr>
					<tr style="height:18px;"></tr>';
				}
			}else{
				echo '<tr><td>No hay productos asociados</td></tr>';
			}
			?>
		</table>
		<?php 

		// Urls amigables
		$prod1 = url_amigables($prod_bottom['producto'][0]['nombre']);
		$prod2 = url_amigables($prod_bottom['producto'][1]['nombre']);
		$prod3 = url_amigables($prod_bottom['producto'][2]['nombre']);
		$prod4 = url_amigables($prod_bottom['producto'][3]['nombre']);

		echo '<table style="width:100%;max-width: 600px;margin: auto;background: #f4f4f4;" cellspacing="20">
			<tr>
				<td><span style="font-weight:300;color:#89cbca;">Descubre más</span></td>
			</tr>
			<tr>
				<td colspan="4" rowspan="2">';
				if ($banner2 != null || $banner2 != '') {
					echo '<img src="'.$url_base.'imagenes/creador_newsletteres/'.$banner2.'" alt="">';
				}else{
					echo '<img src="'.$url_base.'admin/newsletters/img/no-fotob2.jpg" alt="">';
				}
					
				echo '</td>
				<td width="50%" style="background:#fff url('.$url_base.'imagenes/productos/'.$prod_bottom['producto'][0]['imagen'].') no-repeat;background-size:100%!important;background-position: 3px 22px;vertical-align:top!important;padding:10px;position: relative;">
					<img src="'.$url_base.'imagenes/marcas/'.$prod_bottom['producto'][0]['imagen_marca'].'" alt="" width="78"><br>
					<div style="margin-top:78px;">
					<a href="'.$url_base.'/ficha/'.$prod_bottom['producto'][0]['id'].'/'.$prod1.'" style="text-decoration:none;">
					<span style="font-size: 12px;">'.substr($prod_bottom['producto'][0]['nombre'], 0, 20).'...<br>
					<span style="color: #fa6027!important;">$'.number_format($prod_bottom['producto'][0]['precio'], 0, ',','.').'</span></span></div>
					</a>
				</td>
				<td width="50%" style="background:#fff url('.$url_base.'imagenes/productos/'.$prod_bottom['producto'][1]['imagen'].') no-repeat;background-size:100%!important;background-position: 3px 22px;vertical-align:top!important;padding:10px;position: relative;">
					<img src="'.$url_base.'imagenes/marcas/'.$prod_bottom['producto'][1]['imagen_marca'].'" alt="" width="78"><br>
					<div style="margin-top:78px;">
					<a href="'.$url_base.'/ficha/'.$prod_bottom['producto'][1]['id'].'/'.$prod1.'" style="text-decoration:none;">
					<span style="font-size: 12px;">'.substr($prod_bottom['producto'][1]['nombre'], 0, 20).'...<br>
					<span style="color: #fa6027!important;">$'.number_format($prod_bottom['producto'][1]['precio'], 0, ',','.').'</span></span></div>
					</a>
				</td>
			</tr>
			<tr>
				<td width="50%" style="background:#fff url('.$url_base.'imagenes/productos/'.$prod_bottom['producto'][2]['imagen'].') no-repeat;background-size:100%!important;background-position: 3px 22px;vertical-align:top!important;padding:10px;position: relative;">
					<img src="'.$url_base.'imagenes/marcas/'.$prod_bottom['producto'][2]['imagen_marca'].'" alt="" width="78"><br>
					<div style="margin-top:78px;">
					<a href="'.$url_base.'/ficha/'.$prod_bottom['producto'][2]['id'].'/'.$prod1.'" style="text-decoration:none;">
					<span style="font-size: 12px;">'.substr($prod_bottom['producto'][2]['nombre'], 0, 20).'...<br>
					<span style="color: #fa6027!important;">$'.number_format($prod_bottom['producto'][2]['precio'], 0, ',','.').'</span></span></div>
					</a>
				</td>
				<td width="50%" style="background:#fff url('.$url_base.'imagenes/productos/'.$prod_bottom['producto'][3]['imagen'].') no-repeat;background-size:100%!important;background-position: 3px 22px;vertical-align:top!important;padding:10px;position: relative;">
					<img src="'.$url_base.'imagenes/marcas/'.$prod_bottom['producto'][3]['imagen_marca'].'" alt="" width="78"><br>
					<div style="margin-top:78px;">
					<a href="'.$url_base.'/ficha/'.$prod_bottom['producto'][3]['id'].'/'.$prod1.'" style="text-decoration:none;">
					<span style="font-size: 12px;">'.substr($prod_bottom['producto'][3]['nombre'], 0, 20).'...<br>
					<span style="color: #fa6027!important;">$'.number_format($prod_bottom['producto'][3]['precio'], 0, ',','.').'</span></span></div>
					</a>
				</td>
			</tr>
		</table>';
		?>

		<table style="width:100%;max-width: 600px;margin: auto;background: #fff;padding:20px;">
			<tr>	
				<td style="text-align: center;vertical-align:top;">
					<img src="<?=$url_base?>admin/newsletters/img/whoshop1.jpg" alt=""><br>
					<span style="font-weight: 300;font-size: 12px;color: #666666;">Envío a regiones</span>
				</td>
				<td style="text-align: center;vertical-align:top;">
					<img src="<?=$url_base?>admin/newsletters/img/whoshop2.jpg" alt=""><br>
					<span style="font-weight: 300;font-size: 12px;color: #666666;">Pago seguro</span>
				</td>
				<td style="text-align: center;vertical-align:top;">
					<img src="<?=$url_base?>admin/newsletters/img/whoshop3.jpg" alt=""><br>
					<span style="font-weight: 300;font-size: 12px;color: #666666;">Todo medio de pago</span>
				</td>
				<td style="text-align: center;vertical-align:top;">
					<img src="<?=$url_base?>admin/newsletters/img/whoshop4.jpg" alt=""><br>
					<span style="font-weight: 300;font-size: 12px;color: #666666;">Servicio técnico <br>en todo chile</span>
				</td>
			</tr>
		</table>
		
		<table style="width:100%;max-width: 600px;margin: auto;" cellspacing="0">
			<tr>
				<td style="background:url(<?=$url_base?>admin/newsletters/img/texto-fondo.jpg);height:253px;text-align:center;color:#fff;font-weight:300;font-size:10px;padding: 0px 12%">
					Este e-mail es un anuncio de <a href="<?=$url_base?>" style="color:#fff;text-decoration:none;">mercadojardin.cl</a>. Te enviamos este e-mail ya que inscribiste tu dirección de correo en nuestros registros para recibir novedades, ofertas, promociones y más. Por favor, no respondas este correo, si necesitas más información llámanos al +56 2 2414 2563.
					<br><br>
					¡PROTEGERNOS DE FRAUDES ES RESPONSABILIDAD DE TODOS!
					Con estos simples consejos, podrás evitar inconvenientes:
					<br><br>
					1.-Nunca te pediremos por e-mail contraseñas o claves de acceso <br>
					2.-Ingresa a <a href="<?=$url_base?>" style="color:#fff;text-decoration:none;">mercadojardin.cl</a> directamente desde tu navegador de Internet
					<br><br>
					Siguenos <br><br>
					<a href="https://www.facebook.com/MercadoJardin/"><img src="<?=$url_base?>admin/newsletters/img/logo-fb.png" alt=""></a>     
					<a href=""><img src="<?=$url_base?>admin/newsletters/img/logo-yt.png" alt=""></a>
				</td>
			</tr>
			<tr>
				<td style="background:url(<?=$url_base?>admin/newsletters/img/fondofoot.jpg);text-align:center;">
					<div style="color:#69d3d4;max-height:169px;padding-top:3%;padding-bottom:10%;">
						<a href="https://mercadojardin.cl/contacto" style="color: #69d3d4;font-weight: 300;font-size: 12px;text-decoration: none;padding: 10px 15px;">CONTÁCTANOS</a> | <a href="https://mercadojardin.cl/preguntas-frecuentes" style="color: #69d3d4;font-weight: 300;font-size: 12px;text-decoration: none;padding: 10px 15px;">PREGUNTAS FRECUENTES</a> | <a href="https://mercadojardin.cl/sobre-mercado-jardin" style="color: #69d3d4;font-weight: 300;font-size: 12px;text-decoration: none;padding: 10px 15px;">QUIÉNES SOMOS</a><br><br><br><br>

						<a href="<?=$url_base?>" style="background: #fa4f01;color: #fff;padding: 10px 15px;text-decoration: none;font-weight: 300;font-size: 12px">Ir a <?= $nombre_sitio ?></a>
					</div>
				</td>
			</tr>
			<tr>
				<td style="color:#fff;background:#0d3229;font-size:12px;height:64px;text-align:center;">
					Todos los derechos reservados. <?= $nombre_sitio ?>. <?= date('Y', time()) ?>
				</td>
			</tr>
			<tr>
				<td style="font-size:12px;font-weight:300;text-align:center;padding: 10px;">
					Si no desea seguir recibiendo este correo, favor haga clic en <a href="" style="color:#6e8cff;">cancelar suscripción</a>
				</td>
			</tr>
		</table>
	</div>
	
</body>
</html>