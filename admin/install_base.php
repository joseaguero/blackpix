<?php

/********************************************************\
|  Moldeable CMS - Instalador.		            		 |
|  Fecha Modificación: 24/09/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/
	
include('conf.php');
require_once('includes/bcrypt_class.php');

$die_start = "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Error de instalaci&oacute;n:</span> <br /><br />";
$die_end = "</p>";

function create_dir($dir) {
	if (!file_exists("../".$dir."/"))
	{
 		$mk = mkdir("../".$dir); 
 		if (!$mk) 
		return false;
	}
	return true;
}

//Creación de directorios
if (fileperms('../') == 16895)
{
	if (!create_dir("imagenes")) {
		die ("Error al crear directorio de im&aacute;genes");
	}
	if (!create_dir("docs")) {
		die ("Error al crear directorio de documentos");
	}
}
else
{
	echo fileperms('../');
	die($die_start."Directorio ra&iacute;z no dispone de permisos de escritura. Debe dar permisos de escritura al directorio ra&iacute;z para que el sistema cree los directorios necesarios. Una vez instalado el sistema debe cambiar los permisos de la carpeta nuevamente.".mysqli_error($conexion)."$die_end");
}


//Creación de tabla Admin
$administradores = "CREATE TABLE IF NOT EXISTS `administradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id` int(11) DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido_paterno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido_materno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `username` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `last_log_in` datetime DEFAULT NULL,
  `session_hash` varchar(60) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `password_hash` varchar(60) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `attempt` int(11) NOT NULL DEFAULT 0,
  `active` int(1) NOT NULL DEFAULT 1,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
)";
//ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci
$run_administradores = mysqli_query($conexion, $administradores);

if (!$run_administradores)
{
	die($die_start."Error al crear tabla administradores: ".mysqli_error($conexion)."$die_end");
}

for ($i=0; $i<8; $i++) { 
    $d=rand(1,30)%2; 
    $password .= $d ? chr(rand(65,90)) : chr(rand(48,57)); 
} 
$bcrypt = new Bcrypt(15);
$pass_md5 = $bcrypt->hash($password);



$insert_administradores = "INSERT INTO `administradores` (`id`, `perfil_id`, `nombre`, `apellido_paterno`, `apellido_materno`, `email`, `username`, `password`, `last_log_in`, `session_hash`, `password_hash`, `attempt`, `active`, `fecha_creacion`, `fecha_modificacion`)
VALUES
	(1, 1, 'José Pedro', 'Galecio', 'Carrasco', 'jpgalecio@moldeable.com', 'jpgalecio', '$2a$15$gUO1RBxkVUQHnqOMJfucwuV6XR66pYUdl7baPEIo3ShSJByv.KqBa', NOW(), '0', '0', 0, 1, NOW(), NOW()),
	
	(2, 1, 'Hernan', 'Torres', 'Gangas', 'htorres@moldeable.com', 'hernan', '$2a$15$ePAIKiob.zl3AReXHe0KgedfylEOgsMC3wJ1NFwsQPweB6FIklnLS', NOW(), 'b8ffa41d4e492f0fad2f13e29e1762eb', '7e40b07b4c5af55461f158722efe8bbf', 0, 1, NOW(), NOW()),

	(3, 1, 'Administrador', 'Moldeable', '', 'dmaturana@moldeable.com', 'admin', '$2a$15$hrUULzGFBDkTFtIkKU7rxeFJnhYFtF/95shQDn9HzUN6zeQ56j7wC', NOW(), '894b77f805bd94d292574c38c5d628d5', '0', 0, 1, NOW(), NOW()),
	
	(4, 1, 'José', 'Aguero', 'NULL', 'jaguero@moldeable.com', 'jose', '$2a$15$EBPAiT1zV9J5Ah39OPIHveG6kC58jlY2dwcOT/8a.jieYDtBAgFD2', NOW(), '0', '073042fb3621f60a46577f405d20f846', 0, 1, NOW(), NOW()),
	
	(5, 1, 'Orlando', 'barrera', 'farfan', 'obarrera@moldeable.com', 'orlando', '$2a$15$n6wbfQEko27tiBV46Fgia./w//ah7lxedVuYStUVVnHRtN9mInlsq', NOW(), '0', '0', 0, 1, NOW(), NOW()),
	
	(6, 1, 'Elias', 'Grayde', 'NULL', 'egrayde@moldeable.com', 'elias', '$2a$15$gUO1RBxkVUQHnqOMJfucwuV6XR66pYUdl7baPEIo3ShSJByv.KqBa', NOW(), '0', '0', 0, 1, NOW(), NOW())";
	
$run_insert_administradores = mysqli_query($conexion, $insert_administradores);

if (!$run_insert_administradores)
{
	die($die_start."Error al ingresar datos a tabla administradores: ".mysqli_error($conexion)."$die_end");
}


//Tabla opciones
$sql3 = "CREATE TABLE IF NOT EXISTS `opciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `valor` text DEFAULT NULL,
  PRIMARY KEY (`id`)
)";
$run3 = mysqli_query($conexion, $sql3);

if (!$run3)
{
	die($die_start."Error al crear tabla opciones: ".mysqli_error($conexion)."$die_end");
}

$sql4 = "INSERT INTO `opciones` (`id`,`nombre`,`valor`)
VALUES
	(1, 'tiny_mce', 'comentario, contenido, keywords, descripcion_seo, descripcion_breve, descripcion'),
	(2, 'url_sitio', 'https://www.dominio_cliente.cl'),
    (3, 'usar_webp', '1'),
    (4, 'path_lib_webp', '/usr/local/Cellar/webp/1.0.3/bin/cwebp'),
	(5, 'datepicker', '#fecha, #fecha_noticia, #fecha_desde, #fecha_hasta'),
	(7, 'checkboxes', 'publicada'),
	(8, 'first_table_id', '1'),
	(9, 'nombre_cliente', ''),
	(10, 'env', 'produccion'),
	(11, 'rut', '#run, #rut'),
	(12, 'numericos', '#numeric'),
	(13, 'upload_galerias', '1')";

$run4 = mysqli_query($conexion, $sql4);

if (!$run4)
{
	die($die_start."Error al ingresar datos a tabla opciones: ".mysqli_error($conexion)."$die_end");
}
	
//Tabla CMS
$sql9 = "CREATE TABLE IF NOT EXISTS `cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publicada` int(1) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `contenido` text,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
$run9 = mysqli_query($conexion, $sql9);
if (!$run9)
{
	die($die_start."Error al crear tabla CMS".mysqli_error($conexion)."$die_end");
}


//Tabla tablas 
$sql5 = "CREATE TABLE `tablas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `display` varchar(60) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `is_sub_menu` int(1) NOT NULL DEFAULT '0',
  `op` varchar(50) DEFAULT NULL,
  `posicion` int(3) DEFAULT NULL,
  `filtro` varchar(200) DEFAULT NULL,
  `show_in_menu` int(1) DEFAULT '1',
  `submenu_table` varchar(20) DEFAULT NULL,
  `save_in` int(11) DEFAULT NULL,
  `icono` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run5 = mysqli_query($conexion, $sql5);

if (!$run5)
{
	die($die_start."Error al crear tabla Tablas".mysqli_error($conexion)."$die_end");
}

$sql6 = "INSERT INTO `tablas` (`id`, `nombre`, `display`, `parent`, `is_sub_menu`, `op`, `posicion`, `filtro`, `show_in_menu`, `submenu_table`, `save_in`, `icono`)
VALUES
	(1, 'administradores', 'Administradores', 15, 0, NULL, 1, NULL, 1, NULL, NULL, NULL),
	(2, 'perfiles', 'Perfiles de usuario', 15, 0, NULL, 2, NULL, 1, NULL, NULL, NULL),
	(3, 'cms', 'Páginas', 0, 0, NULL, 1, NULL, 1, NULL, NULL, '<i class=\"far fa-file\"></i>'),
	(15, 'configuracion', 'Configuración', 0, 1, NULL, 9, NULL, 1, NULL, NULL, '<i class=\"fas fa-user-cog\"></i>'),
	(16, 'soporte', 'Soporte', 15, 0, 's', 4, NULL, 1, NULL, NULL, NULL),
	(17, 'system_logs', 'Logs del sistema', 15, 0, NULL, 3, NULL, 1, NULL, NULL, NULL);
";
$run6 = mysqli_query($conexion, $sql6);

if (!$run6)
{
	die($die_start."Error al ingresar datos a tabla tablas: ".mysqli_error($conexion)."$die_end");
}

//Tabla Opciones Tablas
$sql7 = "CREATE TABLE IF NOT EXISTS `opciones_tablas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabla_id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `valor` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1";
$run7 = mysqli_query($conexion, $sql7);

if (!$run7)
{
	die($die_start."Error al crear tabla opciones_tablas: ".mysqli_error($conexion)."$die_end");
}


$sql8 = "INSERT INTO `opciones_tablas` (`id`, `tabla_id`, `nombre`, `valor`)
VALUES
	(1, 1, 'exclusiones_list', 'password, fecha_creacion, fecha_modificacion, session_hash, password_hash'),
	(4, 1, 'exclusiones', 'id, last_log_in, fecha_creacion, fecha_modificacion, session_hash, password_hash, attempt'),
	(5, 1, 'obligatorios_edit', 'perfil_id, nombre, apellido_paterno, email, username'),
	(7, 1, 'obligatorios', 'nombre, apellido_paterno, email'),
	(6, 3, 'exclusiones_list', 'contenido'),
	(9, 3, 'exclusiones', 'id, fecha_creacion, fecha_modificacion'),
	(10, 2, 'many_to_many_perfiles', 'tablas_perfiles'),
	(11, 17, 'create', '0'),
	(12, 17, 'save', '0'),
	(13, 17, 'edit', '0');";
$run8 = mysqli_query($conexion, $sql8);

if (!$run8)
{
	die($die_start."Error al ingresar datos a tabla opciones_tablas: ".mysqli_error($conexion)."$die_end");
}

//tabla permisos
$sql12 = "CREATE TABLE `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;";
$run12 = mysqli_query($conexion, $sql12);

$sql13 = "INSERT INTO `permisos` (`id`,`nombre`)
VALUES
	(1, 'Sin acceso'),
	(2, 'Listar'),
	(3, 'Editar');
";
$run13 = mysqli_query($conexion, $sql13);

//Tabla perfiles
$sql10 = "CREATE TABLE `perfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;";
$run10 = mysqli_query($conexion, $sql10);

if (!$run10)
{
	die($die_start."Error al crear tabla perfiles: ".mysqli_error($conexion)."$die_end");
}

$sql11 = "INSERT INTO `perfiles` (`id`,`nombre`,`fecha_creacion`,`creado_por`,`fecha_modificacion`,`modificado_por`)
VALUES
	(1, 'Administradores', '2011-06-29 12:13:56', 2, NULL, NULL);
";
$run11 = mysqli_query($conexion, $sql11);

//Tabla tablas_perfiles
$sql11 = "CREATE TABLE `tablas_perfiles` (
  `tabla_id` int(11) DEFAULT NULL,
  `perfil_id` int(11) DEFAULT NULL,
  `permiso_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run11 = mysqli_query($conexion, $sql11);

//Poblamiento de tabla_perfiles
$sql14 = "INSERT INTO `tablas_perfiles` (`tabla_id`, `perfil_id`, `permiso_id`)
VALUES
	(1, 1, 3),
	(2, 1, 3),
	(3, 1, 3),
	(15, 1, 3),
	(16, 1, 3),
	(17, 1, 3);";
$run14 = mysqli_query($conexion, $sql14);


//Tabla Ortografía
$sql15 = "CREATE TABLE IF NOT EXISTS `ortografia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `busqueda` varchar(40) NOT NULL,
  `mostrar` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `busqueda` (`busqueda`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
$run15 = mysqli_query($conexion, $sql15);


//Tabla Logs
$sql16 = "CREATE TABLE `system_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabla` varchar(45) NOT NULL DEFAULT '',
  `accion` varchar(45) NOT NULL DEFAULT '',
  `fila` varchar(45) NOT NULL DEFAULT '',
  `administrador_id` varchar(45) DEFAULT NULL,
  `date` datetime NOT NULL,
  `remote_ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";
$run16 = mysqli_query($conexion, $sql16);

$sql17 = "INSERT INTO `system_logs` (`id`, `tabla`, `accion`, `fila`, `administrador_id`, `date`) VALUES (1, 'Sistema', 'init', 0, 3, NOW());";
$run17 = mysqli_query($conexion,$sql17);


if (!$run_administradores OR !$run_insert_administradores OR !$run3 OR !$run4 OR !$run5 OR !$run6 OR !$run7 OR !$run8 OR !$run9 OR !$run10 OR !$run11 OR !$run12 OR !$run13 OR !$run14 OR !$run15) 
{
	die($die_start."Por favor revise que la base de datos est&eacute; bien configurada".$die_end);
}
else
{
	echo "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Success!!</span> <br /><br />Instalaci&oacute;n correcta, por favor elimine este archivo y cambie los permisos de escritura en el directorio ra&iacute;z.<br /><p style='font-family: verdana;'>Haga clic <a href='adlogin.php'>aqu&iacute;</a> para ir al panel de administraci&oacute;n.</p>";
}

?>