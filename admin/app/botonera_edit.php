<?php
include('../conf.php');
/********************************************************\
|  Moldeable CMS - Botonera de vista edición.            |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

$real_val_op = $val_op;
$val_op = (is_numeric($_GET[last])) ? mysqli_real_escape_string($conexion, $_GET[last]): $val_op;

?>
<div id="botonera">
	<div id="titulo">
		<h3>
			<?php 
			echo (isset($id)) ? "<i class='fas fa-edit'></i> ": "<i class='far fa-plus-square'></i> ";
			echo get_real_name(singular(get_table_name($real_val_op)));
			?>
		</h3>
	</div>
	<div id="botones">
                <?php
                    if ($sm_table[0][0] != '')
                    {
                        $rid = $_GET[rid];
                        $rel="&r=$submenu_table&id=$rid";
                    } 
$obligatorio = consulta_bd("valor","opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t","nombre = 'obligatorios' AND tabla_id = t.id","");
			$obligatorios = $obligatorio[0][0];
			if ($obligatorios == '')
			{
				$next = 'a';
			} else {
				$next = 'b';
			}
			    ?>

		<?php if($tabla != 'productos_detalles'){ ?>
			<button type="button" class="boton3 volver-edit" onclick="javascript:volver('index.php?op=<?php echo (is_numeric($val_op)) ? $val_op."a".$rel:$val_op;?>')">Volver</button>
		<?php }else{ ?>

			<?php 
				$tabla_parent = get_table_id('productos');
				$parent_prod = consulta_bd("producto_id","productos_detalles","id = $id","");
			?>
		
			<button type="button" class="boton3 volver-edit" onclick="javascript:volver('index.php?op=<?php echo (is_numeric($tabla_parent)) ? $tabla_parent."c&id=".$parent_prod[0][0]:$tabla_parent;?>')">Volver</button>
		<?php } ?>


		<?php if ($duplicable) { ?>
		<button type="button" class="boton3" onclick="javascript:volver('app/duplicate_entry.php?val_op=<?php echo "$val_op&id=$id";?>')">Duplicar</button>
		<?php } if ($create OR !isset($create)) { ?>
		<button type="button" class="boton3 nuevo-edit" onclick="javascript:volver('index.php?op=<?php echo $val_op.$next ?>')"  style="margin-right:20px;color:red">Nuevo</button>	

		<?php 
		$real_op = explode('c', $_GET[op]);

		$table_name = consulta_bd('nombre', 'tablas', "id = $real_op[0]", '');
 
		if($table_name[0][0] == 'creador_newsletteres') {
			echo '<a target="blank" data-id="'.$id.'" href="#" class="preview boton3 nuevo-edit" style="text-decoration:none;background-image:url(pics/misitio-icono.png);background-size:20px;background-position:10px ">Vista previa</a>';
			echo '<a target="blank" href="#" data-id="'.$id.'" class="getcode boton3 nuevo-edit" style="text-decoration:none;background-image:url(pics/code.png);background-size:20px;background-position:10px ">Obtener código</a>';
		}

		?>
		
		<?php } if ($galerias) { 
		$upGal = consulta_bd("valor","opciones","nombre='upload_galerias'","");
		if($upGal[0][0] == 1){
		?>
        <a href="js/fancyupload/index.php?id=<?php echo $id;?>&tabla=<?php echo $tabla;?>" class="boton4 color_box imagenes-edit" >Agregar imágenes</a>
        <?php } else { ?>
			<a href="jQuery-File-Upload/index.php?id=<?php echo $id;?>&tabla=<?php echo $tabla;?>" class="boton4 color_box imagenes-edit" >Agregar imágenes jquery</a>
		<?php }	?>		
			
            
		<?php } if ($create OR !isset($create)) { ?>
		<a onclick="javascript:confirmar('<?php echo $id;?>', 'del', '', '<?php echo $tabla;?>', '<?php echo $rid;?>', '')" class="boton2 eliminar-edit">Eliminar</a>
		<?php } ?>
		<?php if (!isset($save) or $save == 1) { ?>	
		<input type="submit" name="<?php echo (isset($id)) ? "update_$tabla": "add_$tabla";?>" id="<?php echo (isset($id)) ? "update_$tabla": "add_$tabla";?>" value="Guardar" class="guardar-edit"/>
		<?php } ?>
		<input type="hidden" name="tabla" value="<?php echo $tabla; ?>" />
		<input type="hidden" name="op" value="<?php echo $op; ?>" />
		<?php if (isset($id)) { ?>
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?php } ?>
		<input type="hidden" name="exception" value="<?php echo $exception_update; ?>" />
		
		
		
		<!--MUESTRO LOS BOTONES DEL NEWSLETTER SOLO CUANDO TENGO LA OPCION ACTIVADA Y ESTOY EN LA PAGINA CORRECTA-->
		<?php if(opciones("newsletter") > 1 and opciones("newsletter") == $val_op){ ?>
		<a target="blank" href="#" data-id="<?= $_GET[id];?>" data-tpl="" class="getcode boton3 nuevo-edit" style="text-decoration:none;background-image:url(pics/code.png);background-size:20px;background-position:10px ">Obtener código</a>
		<a target="blank" data-id="<?= $_GET[id];?>" data-tpl="" href="#" class="preview boton3 nuevo-edit" style="text-decoration:none;background-image:url(pics/misitio-icono.png);background-size:20px;background-position:10px ">Vista previa</a>
		<?php } ?>
		<!--FIN BOTONERA NWSLETTER-->
		
		
		
		
		<?php if(opciones("preview") == 1 and $val_op == 219){ ?>
		<a href="../index.php?op=preview&id=<?= $id; ?>" target="_blank" class="preview-edit" style="margin-right:10px;">Preview</a><?php } ?>
		
		
		
		<?php 
			if($val_op == 231){
				$idOC = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
				$ocMenu = consulta_bd("oc","pedidos","id=$idOC","");
				$ocEntrada = $ocMenu[0][0];
		?>
		<a href="documentoVenta.php?oc=<?= $ocEntrada; ?>" target="_blank" class="btnBotonera" title="Imprimir documento">
			<i class="fas fa-print"></i> <span>Imprimir documento</span>
		</a>
		<a href="formNotificacion.php?oc=<?= $ocEntrada; ?>" target="_blank" class="btnBotonera notificacionCompra" title="Enviar por correo">
			<i class="fas fa-envelope"></i><span>Reenviar notificación</span>
		</a>
		<?php } ?>
		
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(document).on('focus', '.focustextarea', function(event) {
			$(this).select();
		});
	});
</script>
<?php
	$val_op = (is_numeric($_GET[last])) ? mysqli_real_escape_string($conexion, $_GET[last]): 0;
?>