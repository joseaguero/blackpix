<?php
	include('../../conf.php');
	require_once('../../includes/tienda/cart/inc/functions.inc.php');

	$cliente 		= $_POST[cliente];
	$envio 			= $_POST[envio];
	$tipo_compra 	= $_POST[tipo_compra];
	$venta 			= $_POST[venta];


	$info_email = (isset($cliente[email])) ? mysqli_real_escape_string($conexion, $cliente[email]) : 0;
	$info_tele  = (isset($cliente[telefono])) ? mysqli_real_escape_string($conexion, $cliente[telefono]) : 0;
	$info_name  = (isset($cliente[nombre])) ? mysqli_real_escape_string($conexion, $cliente[nombre]) : 0;
	$rut  		= (isset($cliente[rut])) ? mysqli_real_escape_string($conexion, $cliente[rut]) : 0;


	if(isset($_COOKIE[usuario_id])){
		$current_user = $_COOKIE[usuario_id];
		$idClienteInsert = $_COOKIE[usuario_id];
	} else {
		//usuario no logueado
			$info_email = trim(strtolower($info_email));		
			$clientes = consulta_bd("id","clientes","LOWER(email)='$info_email'","");
			$cantClientes = mysqli_affected_rows($conexion);
			if($cantClientes > 0){
				$current_user = $clientes[0][0];
			} else {
				$inserCliente = insert_bd("clientes","nombre,rut,email,telefono","'$info_name','$rut','$info_email','$info_tele'");
				$idClienteInsert = mysqli_insert_id($conexion);
				$current_user = $idClienteInsert;
			}
		//usuario no logueado
	}


	$date = date('Ymdhis');
	$oc = "OC_$date";


	if($envio[tipo] == 'retiro'){
		$tiendaRetiro = $envio[sucursal];
		$sucursales = consulta_bd("s.id, s.nombre, s.direccion, s.comuna_id, c.id, ciu.nombre, c.nombre, r.nombre","sucursales s, comunas c, ciudades ciu, regiones r","s.comuna_id = c.id and c.ciudad_id = ciu.id and ciu.region_id = r.id and s.id = $tiendaRetiro","s.posicion asc");
			$ciudad 	= $sucursales[0][5];//'Santiago';
			$comuna     = $sucursales[0][6];
			$localidad  = $sucursales[0][6];
			$region    	= $sucursales[0][7]; //'Metropolitana';
			$direccion  = $sucursales[0][2];
			//DESPACHO
			$despacho = 0;
			$despacho_id = 1;
			$retiroEnTienda = 1;
			$comuna_id = $sucursales[0][4];//(is_numeric($_POST[comuna])) ? mysqli_real_escape_string($conexion, $_POST[comuna]) : 0;
	} else {
		//DESPACHO A LA DIRECCION INDICADA POR EL CLIENTE
		$comuna_id = (is_numeric($envio[comuna])) ? mysqli_real_escape_string($conexion, $envio[comuna]) : 0;
		$campos = "r.nombre, ciu.nombre, c.nombre, c.id, r.id, ciu.id";
		$tablas = "ciudades ciu, comunas c, regiones r";
		$where = "r.id = ciu.region_id and ciu.id = c.ciudade_id and c.id = $comuna_id";
		$address = consulta_bd($campos,$tablas,$where,"");
		
		$calle 		= (isset($envio[direccion])) ? mysqli_real_escape_string($conexion, $envio[direccion]) : 0;
		$numero 	= '';//$address[0][1];
		$region    	= $address[0][0];
		$ciudad 	= $address[0][1];
		$comuna     = $address[0][2];
		
		$direccion = $calle." #".$numero;
		
		$ciudad_id = $address[0][5];
		$region_id = (is_numeric($envio[region])) ? mysqli_real_escape_string($conexion, $envio[region]) : 0;
		
		//ingreso la direccion del cliente para futuras compras
		if($cantClientes == 0){
			if(!isset($_COOKIE[usuario_id])){
	            $inserCliente = insert_bd("clientes_direcciones","cliente_id,region_id,ciudad_id,comuna_id, calle","$idClienteInsert, $region_id, $ciudad_id,$comuna_id, '$calle'");
	        }
	    }
		
		$despacho = valorDespacho($comuna_id);
		$retiroEnTienda = 0;
	    $despacho_id = 0;
	}


//datos retiro en tienda
$nombre_retiro 		= "";//(isset($_POST[nombre_retiro])) ? mysqli_real_escape_string($conexion, $_POST[nombre_retiro]) : 0;
$apellidos_retiro 	= "";//(isset($_POST[apellidos_retiro])) ? mysqli_real_escape_string($conexion, $_POST[apellidos_retiro]) : 0;
$rut_retiro  		= "";//(isset($_POST[rut_retiro])) ? mysqli_real_escape_string($conexion, $_POST[rut_retiro]) : 0;
//$patente_retiro = (isset($_POST[patente_retiro])) ? mysqli_real_escape_string($conexion, $_POST[patente_retiro]) : 0;


//Comentarios envios
$comentarios_envio = "";//(isset($_POST[comentarios_envio])) ? mysqli_real_escape_string($conexion, $_POST[comentarios_envio]) : 0;

//REGALO
$regalo = "";(isset($_POST[regalo])) ? mysqli_real_escape_string($conexion, $_POST[regalo]) : 0;//mysql_real_escape_string($_POST[regalo]);
$name_regalo = 0;//mysql_real_escape_string($_POST[name_regalo]);
$tel_regalo = 0;//mysql_real_escape_string($_POST[tel_regalo]);


//facturacion
$factura  = (isset($tipo_compra[tipo])) ? mysqli_real_escape_string($conexion, $tipo_compra[tipo]) : 0;
$datosFacturaHBT = consulta_bd("giro, rut","clientes","email='$info_email'","");
if($factura == 'boleta'){
	$giro = 'Particular';
	$factura = 0;
}else{
	$giro = (isset($tipo_compra[giro])) ? mysqli_real_escape_string($conexion, $tipo_compra[giro]) : 0;
	$factura = 1;
}
$razon_social = (isset($tipo_compra[razon_social])) ? mysqli_real_escape_string($conexion, $tipo_compra[razon_social]) : 0;
$rut_factura = (isset($tipo_compra[rut])) ? mysqli_real_escape_string($conexion, $tipo_compra[rut]) : 0;
$email_factura = "";//(isset($tipo_compra[email_factura])) ? mysqli_real_escape_string($conexion, $tipo_compra[email_factura]) : 0;
$direccion_factura = (isset($tipo_compra[direccion])) ? mysqli_real_escape_string($conexion, $tipo_compra[direccion_factura]) : 0;

$id_cliente_hbt = $datosFacturaHBT[0][2];		
	
$subtotal = round(get_total_price($comuna_id));

$cod = 'null';
//descuento
if($_SESSION["descuento"]){
	$valor_descuento = $_SESSION['val_descuento'];
	// Condición session descuento
	$cod = $_SESSION['descuento'];
} else {
	$valor_descuento = 0;
	$cod = '';
}
							
$medioPago  = "Manual";//(isset($_POST[medioPago])) ? mysqli_real_escape_string($conexion, $_POST[medioPago]) : 0;

//TOTAL
$total = $subtotal+$despacho-$valor_descuento;

//CART EXPLODE AND COUNT ITEMS
if(!isset($_COOKIE[cart_alfa_cm])){
	setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
$cart = $_COOKIE[cart_alfa_cm];
	

$items = explode(',',$cart);
foreach ($items as $item) {
	$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
}

////////////////// ----------- REVISA STOCK TEMPORAL ----------- //////////////////

	foreach ($contents as $id=>$qty){

		$isPack = consulta_bd("p.pack, p.codigos","productos p, productos_detalles pd","p.id = pd.producto_id AND pd.id = $id","");

		if($isPack[0][0]){

			$codes_pack = explode(',', $isPack[0][1]);
			foreach ($codes_pack as $value){
				$value 			= trim($value);
				$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","sku='$value'","");
				$stock_temporal = consulta_bd("stock","stock_temporal","sku='".$stock[0][3]."'","");
				if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
					die("Producto sin stock ".$stock[0][3]);
				}
			}

		}else{
			$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","id=$id","");
			$stock_temporal = consulta_bd("stock","stock_temporal","sku='".$stock[0][3]."'","");
			if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
				die("Producto sin stock ".$stock[0][3]);
			}	
		}

		
	}

////////////////// ----------- ////////////////// ----------- //////////////////


$nombre_retiro  	= "";//(isset($_POST[nombre_retiro])) ? mysqli_real_escape_string($conexion, $_POST[nombre_retiro]) : 0;
$apellidos_retiro 	= "";//(isset($_POST[apellidos_retiro])) ? mysqli_real_escape_string($conexion, $_POST[apellidos_retiro]) : 0;
$rut_retiro  		= "";//(isset($_POST[rut_retiro])) ? mysqli_real_escape_string($conexion, $_POST[rut_retiro]) : 0;


if ($_COOKIE['cart_alfa_cm']!="") {
	
	if($venta == 'G'){
		$estado_pendiente = 1;
	}else{
		$estado_pendiente = 2;
	}
	



	$campos = "oc, 
	cliente_id, 
	fecha, 
	estado_id, 
	codigo_descuento, 
	descuento, 
	total, 
	valor_despacho, 
	total_pagado, 
	regalo, 
	nombre, 
	telefono, 
	email, 
	direccion, 
	comuna, 
	region, 
	ciudad, 
	localidad, 
	giro, 
	rut, 
	retiro_en_tienda, 
	retiro_en_tienda_id,
	factura, razon_social, 
	direccion_factura, 
	rut_factura, 
	email_factura, comentarios_envio, medio_de_pago, id_notificacion, nombre_retiro, apellidos_retiro, rut_retiro";
	$values = "NULL, '$current_user', NOW(), $estado_pendiente, '$cod', '$valor_descuento', $subtotal, $despacho, $total, 1, '$info_name', '$info_tele', '$info_email', '$direccion', '$comuna', '$region', '$ciudad', '$localidad', '$giro', '$rut', $retiroEnTienda, $despacho_id, $factura, '$razon_social', '$direccion_factura', '$rut_factura', '$email_factura', '$comentarios_envio', '$medioPago', 0, '$nombre_retiro', '$apellidos_retiro','$rut_retiro'";
	
    $generar_pedido = insert_bd("pedidos","$campos","$values");
			
	$pedido_id = mysqli_insert_id($conexion);

	$new_oc = $oc.$pedido_id;
	update_bd('pedidos', "oc = '$new_oc'", "id = $pedido_id");

	/* CARRO ABANDONADO */
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
	$pdca = $_COOKIE[cart_alfa_cm];
	update_bd("carros_abandonados","oc='$new_oc'","productos = '$pdca' and correo = '$info_email'");
	$notificacion = (isset($_SESSION[notificacion])) ? mysqli_real_escape_string($conexion, $_SESSION[notificacion]) : 0;

	$cantTotal = 0;

		foreach ($contents as $id=>$qty) {
			//consulto el precio segun la lista de precios
			$prod = consulta_bd("pd.precio, pd.descuento, p.pack, p.codigos, pd.sku","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
			////Veo si el producto es un pack, lo manejo de cierta forma///////////////////////
			if($prod[0][2] == 1){
				
				$precio_final = $prod[0][0];
				$codigo_pack = $prod[0][4];
				$codigosProductos = trim($prod[0][3]);
				$productosPack = explode(",", $codigosProductos);
				
				
				//saco la suma de los productos reales
				$totalReal = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle = consulta_bd("precio","productos_detalles","sku='$skuPack'","");
					$totalReal = $totalReal + $valorProdDetalle[0][0];
				}
				//obtengo el porcentaje de descuento
				$descPorcentajeProducto = ($totalReal - $precio_final)/$totalReal;
				//$descPorcentajeProducto2 = (($precio_final - $totalReal)/$totalReal) * -1;
				//die("$descPorcentajeProducto /////-///// $descPorcentajeProducto2");
				
				$retorno = "";
				$idPd = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle2 = consulta_bd("precio, id","productos_detalles","sku='$skuPack'","");
					
					$precioUnitario = $valorProdDetalle2[0][0];
					$idProductoDetalle = $valorProdDetalle2[0][1];
					
					$precioDescuento = $valorProdDetalle2[0][0] * (1 - $descPorcentajeProducto);
					$precioTotal = round($precioDescuento * $qty);
					
					$desc = 1 - $descPorcentajeProducto;
					//die("$precioUnitario  --  $precioTotal");
					$sku = $skuPack;
					
					
					$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, descuento, codigo, precio_total, fecha_creacion, codigo_pack";
					$values = "$pedido_id,$idProductoDetalle,'$qty',$precioUnitario, $precioDescuento, '$sku', $precioTotal, NOW(), '$codigo_pack'";
					$detalle = insert_bd("productos_pedidos","$campos","$values");
					$cantTotal = $cantTotal + $qty;
				}//fin foreach
				
			} else {
			///////////////////////////
			
				if($prod[0][1] > 0){
					$precio_final = $prod[0][1];
					$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
				}else{
					$precio_final = $prod[0][0];
					$descuento = 0;
				}
				
				$precioUnitario = $precio_final;
				$precioTotal = $precioUnitario * $qty;
				$porcentaje_descuento = $prod[0][1];//$valoresFinales[descuento];
				
				$all_sku = consulta_bd("sku","productos_detalles","id=$id","");
				$sku = $all_sku[0][0];
				/* $precio = $all_sku[0][1]; */
	
				$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion, descuento";
				$values = "$pedido_id, $id, $qty, $precioUnitario,'$sku', $precioTotal, NOW(), $porcentaje_descuento";
				$detalle = insert_bd("productos_pedidos","$campos","$values");
				$cantTotal = $cantTotal + $qty;
			}//fin del ciclo

		}

		$qtyFinal = update_bd("pedidos","cant_productos = $cantTotal, fecha_creacion = NOW()","id=$pedido_id");

        unset($_SESSION["notificacion"]);
        $token = generateToken($oc, $total);

        setcookie('cart_alfa_cm', null, -1, '/');
        die("SUCCESS");

} else {
	die("ERROR");
}
?>