<?php
include('../../conf.php');
/********************************************************\
|  Alfastudio CMS - Edición de contenido - General       |
|  Fecha Modificación: 16/04/2012                        |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total                    |
|  http://www.alfastudio.cl/                             |
\********************************************************/

?>
<div id="tab-<?php echo $t;?>">
    <table width="100%" border="0">
            <?php     
            //Obtengo los valores segun el id
            $columnas = explode(',', $campos);
            $lista_columnas = $campos;
            $filas = consulta_bd($lista_columnas,$tabla,"id = '$id'","");
    
            $i = 0;
            while($i <= (sizeof($columnas)-1)) {    
				
			
                $column = get_real_name($columnas[$i]);
                
                //Ponemos los * en los campos obligatorios.
                if ($obligatorios_edit != '')
                {
                    if (substr_count($opcion['obligatorios_edit'],",")>0)
                    {
                        $obligatorios_edit = explode(',', $opcion['obligatorios_edit']);
                        foreach ($obligatorios_edit as $obl)
                        {
                            $obl = trim($obl);
                            if ($columnas[$i] == $obl)
                            {
                                $column = "$column *";
                            }
                        }
                    }
                    else
                    {
                        if ($columnas[$i] == trim($obligatorios_edit))
                        {
                            $column = "$column *";
                        }
                    }
                }
                
				
					
                $val = ($filas[0][$i]!='') ? $filas[0][$i]: $_GET[$columnas[$i]];
                $column_id = $columnas[$i];
                
				
				//////////campos para dejar bloqueados////////
				
				$vaciar = $campos_disabled;
				$arregloVacios = explode(",", $vaciar);
				$arregloVacios = array_map('trim', $arregloVacios);
				if(in_array(trim($column_id), $arregloVacios)){
					$bloqueado = "disabled";
				} else {
					$bloqueado = "";
				}
				//////////campos para dejar bloqueados////////
				
				
				
                $parent = column_is_related($columnas[$i]);
    
                //RELACIONES
                if ($parent)
                {
                    //Si el padre tiene una relaciÃ³n, el select debe ser dependiente (belongs_to) o se debe establecer una relaciÃ³n many_to_many
                    //Consulto por la opciÃ³n many_to_many del padre
                    $parent_plural = plural($parent);
                    $many_to_many_q = consulta_bd("ot.nombre, ot.valor","opciones_tablas ot, tablas t","t.nombre = '$parent_plural' AND ot.tabla_id = t.id AND (ot.nombre = 'many_to_many' OR ot.nombre = 'belongs_to')","");
                    $j = 0;
                    while($j <= (sizeof($many_to_many_q)-1))
                    {    
                        $nombre_p = $many_to_many_q[$j][0];
                        $opcion_p[$nombre_p."_parent"] = $many_to_many_q[$j][1];
                        $j++;
                    }
                    
                    if (isset($belongs_to_parent))
                    $belongs_to_parent = NULL;
                    
                    if ($opcion_p != NULL)
                    extract($opcion_p);
                    
                    $opcion_p = NULL;
    
                    
                    if ($many_to_many_parent != NULL)
                    {
                        //En caso de que sea una entrada nueva muestro al padre lleno y al hijo vacÃ­o
                        $sel2 = $columnas[$i];
                        if (!isset($id))
                        {
                            echo '<tr>';
                            echo '<td>'.getrealname(singular($many_to_many_parent)).'</td>';
                            echo '<td>';
                                select($many_to_many_parent, 'nombre', 'id', $many_to_many_parent, '', '', '', '', '', "");
                            echo '</td>';
                            echo '</tr>';
                            
                            echo '<tr>';
                            echo '<td>'.getrealname($parent).'</td>';
                            echo '<td>';
                                select(plural($parent), 'nombre', 'id', $sel2, '', "WHERE nombre = NULL", '', '', '', "");
                            echo '</td>';
                            echo '</tr>';
                        }
                        else
                        {
                            //Obtengo el id del padre de la relaciÃ³n
                            $rel_table = $many_to_many_parent."_".plural($parent);
                            $rel_id_q = consulta_bd(singular($many_to_many_parent)."_id",$rel_table,$parent."_id = $val","");
                            $rel_id = $rel_id_q[0][0];
                        
                            echo '<tr>';
                            echo '<td>'.ucwords(singular($many_to_many_parent)).'</td>';
                            echo '<td>';
                                select($many_to_many_parent, 'nombre', 'id', $many_to_many_parent, '', '', '', $rel_id, '', "");
                            echo '</td>';
                            echo '</tr>';
                            
                            echo '<tr>';
                            echo '<td>'.ucwords($parent).'</td>';
                            echo '<td>';
                                select(plural($parent), 'nombre', 'id', $sel2, '', "", '', $val, '', "");
                            echo '</td>';
                            echo '</tr>';
                        }
                        $related_selects_mtmp[] = $many_to_many_parent;
                        $related_selects_sel2[] = $sel2;
                    }
                    elseif ($belongs_to_parent != '')
                    {
                        //En caso de que sea una entrada nueva muestro al padre lleno y al hijo vacÃ­o
                        $sel2 = $columnas[$i];
                        if (!isset($id) OR $val == '')
                        {
                            echo '<tr>';
                            echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
                            echo '<td>';
                                select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', '', '', "");
                            echo '</td>';
                            echo '</tr>';
                            
                            echo '<tr>';
                            echo '<td>'.get_real_name($parent).'</td>';
                            echo '<td>';
                                select(plural($parent), 'nombre', 'id', $sel2, '', "WHERE nombre = NULL", '', '', '', "");
                            echo '</td>';
                            echo '</tr>';                        
                        }
                        else
                        {
                            //Obtengo el id del padre de la relaciÃ³n    
                            $column_parent = singular($belongs_to_parent)."_id";
                            $rel_id_q = consulta_bd($column_parent,plural($parent),"id = $val","");
                            $rel_id = $rel_id_q[0][0];
                            
                            echo '<tr>';
                            echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
                            echo '<td>';
                                select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', $rel_id, '', "");
                            echo '</td>';
                            echo '</tr>';
                            
                            echo '<tr>';
                            echo '<td>'.get_real_name($parent).'</td>';
                            echo '<td>';
                                select(plural($parent), 'nombre', 'id', $sel2, '', "", '', $val, '', "");
                            echo '</td>';
                            echo '</tr>';
                        }
                        $btp = $belongs_to_parent;
                        $related_selects_btp[] = $btp;
                        $sel2_btp[] = $sel2;
                    }
                    else
                    {
                        echo '<tr>';
                        echo '<td>'.ucwords($parent).'</td>';
                        echo '<td>';
                        
                        //show tables like 'juicios';
                        $t_parent = plural(trim($parent)); 
                        $sql = "show tables like '$t_parent'";
                        $run = mysqli_query($conexion, $sql) OR die($sql);
                        $existe_cant = mysqli_affected_rows($conexion);
                        if ($existe_cant>0)
                        {
                            select(plural($parent), 'nombre', 'id', $columnas[$i], '', "", '', $val, '', "");
                        }
                        else
                        {
                            if ($t_parent == 'apoderados')
                            $t_parent = "abogados";
                            
                            $perfil_id = consulta_bd("id","perfiles","nombre = '$t_parent'","");
                            $perfil_id = $perfil_id[0][0];
                            $where_p = "WHERE perfil_id = $perfil_id";
                            select('administradores', 'nombre', 'id', $columnas[$i], '', $where_p, '', $val, '', "");
                        }
                        echo '</td>';
                        echo '</tr>';
                    }    
                }
                elseif (stristr($archivos,trim($columnas[$i])))
                {
                    $file_ext = strtolower(substr(strrchr($val, '.'), 1));
                    
                    if ($file_ext == 'jpg' or $file_ext == 'gif' or $file_ext == 'png' or $file_ext == 'jpeg' or $file_ext == 'webp')
					{
						$f = $dir.'/'.$val;
						$size=getimagesize($f); 
						$width=$size[0]; 
						$height=$size[1];
						
						if ($width>300)
						{
							$tam = "width='320'";
							$width = 300;
						}
						elseif($height>300)
						{	
							$tam = "height='300'";
						}
						else
						{
							$tam = '';
							$width = $width-40;
						}
						
						echo '<tr class="fila_imagen_'.$columnas[$i].'">';
						echo '<td valign="top">'.$column.' actual:</td>';
						echo '<td>';
						echo	  '<a class="img_admin" href="javascript:void(0)" alt="Borrar" title="Borrar">
										<img class="btn_borrar_imagen" onclick="delete_img(';
										echo "'$id', '$tabla', '$columnas[$i]'";
										echo ')"
											src="pics/delete.png" border="0"/>
										
										<img src="'.$f.'" border="0" '.$tam.' id="cont_img_actual_'.$columnas[$i].'"/>
									</a>';
						echo 	'</td>';
						echo '</tr>';
						
						
					}
					elseif ($file_ext != '')
					{
						echo '<tr>';
					    echo '<td valign="bottom">'.$column.' actual:</td>';
						echo '<td>
								<div id="file_name_for">
									<a href="'.$dir_docs.'/'.$val.'">'.$val.'</a>
									<a href="#" onclick="javascript:delete_file(';
									echo "'$id','$tabla','$columnas[$i]','$val'";
									echo ')" >
									<img src="pics/delete.jpg" border="0" alt="Eliminar Archivo" title="Eliminar Archivo">
								</div>
								</a>
							  </td>';	
						echo '</tr>';
						
					}
					
					
					$paso1 = consulta_bd("id","tablas","nombre='$tabla'","id desc");
					$paso4 = consulta_bd("valor","opciones_tablas","tabla_id=".$paso1[0][0]." and nombre ='crop'","id desc");
					$campos_separados = explode(",", $paso4[0][0]);
					
					if(has_array($campos_separados, $columnas[$i])){
							
							//die("1");
						} else {
							//die("2");
							echo '<tr class="'.$columnas[$i].' '.$oculto.'">';
								echo '<td valign="top">'.$column.'</td>';
								echo '<td><input type="file" id="'.$column_id.'" name="'.$column_id.'" /></td>';
							echo '</tr>';
						}
					
					
					for($k=0; $k<sizeof($campos_separados); $k++){
						if($campos_separados[$k] == $columnas[$i]){
							$medidas = consulta_bd("valor","opciones_tablas","nombre='$campos_separados[$k]' and tabla_id=".$paso1[0][0],"");
							//echo $medidas[0][0];
							if($val != ''){
								$oculto = 'oculto';
							} else {
								$oculto = '';
							}
							$medidas_separadas = explode(",", $medidas[0][0]);
							$ancho_crop = trim($medidas_separadas[0]);
							$alto_crop = trim($medidas_separadas[1]);
							echo '<tr class="'.$columnas[$i].' '.$oculto.'" >
									<td valign="top">'.$column.'</td>
									<td>
										<div id="malla" class="cont_crop" style="float:left; width:'.$ancho_crop.'px; height: '.$alto_crop.'px" >
											<div class="cropContainerModal" id="'.$column_id.'_cropContainerModal" rel="'.$f.'"></div>
										</div>
									</td>
								</tr>';
						}
					}//fin for
					
				}
                elseif (is_boolean(trim($columnas[$i]), $tabla))
                {
                        $publicado = ($val == 1) ? "checked='checked'": "";
                        echo '<tr>';
                        echo '<td>'.$column.'</td>';
                        echo '<td><input type="checkbox" id="'.$column_id.'" name="'.$column_id.'" '.$publicado.' '.$bloqueado.'/></td>';
                        echo '</tr>';
                }
                elseif (is_enum(trim($columnas[$i]), $tabla))
                {
                        $enum_vals =  enum_vals(trim($columnas[$i]), $tabla);
                        echo '<tr>';
                        echo '<td>'.$column.'</td>';
                        echo '<td><select name="'.$column_id.'" id="'.$column_id.'" '.$bloqueado.'><option>Seleccione</option>';
                        foreach ($enum_vals as $e)
                        {
                                echo ($val == $e) ? "<option value='$e' selected>".ucwords(str_replace("_", " ", $e))."</option>":"<option value='$e'>".ucwords(str_replace("_", " ", $e))."</option>";
                        }
                        echo '</select>';
                        echo '</td>';
                        echo '</tr>';
                }
                elseif ($columnas[$i] == 'password')
                {
                    echo '<tr>';
                    echo '<td>'.$column.'</td>';
                    echo '<td><input class="campo_texto" type="password" id="'.$column_id.'" name="'.$column_id.'" '.$bloqueado.' /></td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>'.$column.' Check</td>';
                    echo '<td><input class="campo_texto" type="password" id="'.$column_id.'_check" name="'.$column_id.'_check" '.$bloqueado.'/></td>';
                    echo '</tr>';
                }
                elseif (stristr($tiny_mce,$columnas[$i]))
                {
                                        $column_id = trim($column_id);
                    echo '<tr>';
                    echo '<td valign="top">'.$column.'</td>';
                    echo '<td><textarea id="'.$column_id.'" name="'.$column_id.'" '.$bloqueado.'>'.$val.'</textarea></td>';
                    echo '</tr>';
                }
                else
                {
                    echo '<tr>';
                    echo '<td>'.$column.'</td>';
                    echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$column_id.'" value="'.$val.'" '.$bloqueado.' /></td>';
                    echo '</tr>';
                }
                $i++;
            }
            $t++;        
        ?>
    </table>
</div>