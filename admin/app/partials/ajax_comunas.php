<?php
include_once("../../conf.php");
header('Content-Type: application/json');

$action = $_POST['action'];

switch ($action) {	
	case 'reg':
		$regiones = consulta_bd("id, nombre","regiones","","");
		echo json_encode($regiones);
		break;
	case 'com':
		$reg_id = $_POST['value'];
		$comunas = consulta_bd("c.id, c.nombre","comunas c, ciudades ci","c.ciudade_id = ci.id AND ci.region_id = $reg_id");
		echo json_encode($comunas);
		break;
	case 'suc':
		$sucursales = consulta_bd("id, direccion, region_id","sucursales","publicado = 1");
		echo json_encode($sucursales);
		break;
}

?>