
<!--<a href="javascript:void(0)" class="price_list_button"><i class="fas fa-dollar-sign"></i> Asignar precio</a>-->
<?php $lista_precios = consulta_bd('id, nombre', 'lista_precios', '', 'id asc'); ?>

<div id="tab-lista-de-precio">
	<div class="form_precio">
		<table>
			<thead>
				<tr>
					<th>Lista</th>
					<th>Precio</th>
					<th>Descuento</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lista_precios as $lp): 
					$precios_producto = consulta_bd('precio, descuento, id', 'lista_precio_producto', "lista_precio_id = $lp[0] and producto_id = {$_GET['id']}", ''); ?>
					<tr>
						<td><?= $lp[1] ?></td>
						<td><input type="text" name="precioLista" data-id="<?= $precios_producto[0][2] ?>" data-lista-id="<?= $lp[0] ?>" value="<?= $precios_producto[0][0] ?>"></td>
						<td><input type="text" name="descuentoLista" data-id="<?= $precios_producto[0][2] ?>" data-lista-id="<?= $lp[0] ?>" value="<?= $precios_producto[0][1] ?>"></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<input type="hidden" name="productoDetalleId" value="<?=$_GET['id']?>">
		<a href="javascript:void(0)" class="savePrices">Guardar precios</a>
	</div>
	
</div>
		