<?php
	include('../../conf.php');
	include('../../includes/tienda/cart/inc/functions.inc.php');

	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
	}

	$total = 0;

?>

<table class="productos">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>SKU</th>
			<th>Cantidad</th>
			<th>Precio</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

	<?php if($cart){ ?>
		<?php 
			foreach ($contents as $prd_id=>$qty) {
				$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.venta_minima","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
				if($producto[0][0] != ''){
					$thumbs = $producto[0][0];
				} else {
					$thumbs = "img/sinImagenGrilla.jpg";
				}
				
				$valor 			= getPrecio($prd_id) * $qty;
				$valorUnitario 	= getPrecio($prd_id);

				if(!tieneDescuento($prd_id)){
					$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
					if($pd[0][0]){
						$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
						if($precios_cantidad){
							$pc = $precios_cantidad[0];
							$rango 			= $pc[1];
							$descuento 		= $pc[2];
							$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
							$valor 			= $valorUnitario * $qty;
						}
					}
				} 

				$total += $valor;

			?>
			<tr>
				<td><?= $producto[0][1]; ?></td>
				<td><?= $producto[0][5]; ?></td>
				<td><?= $qty; ?></td>
				<td>$<?= number_format($valorUnitario,0,',','.'); ?></td>
				<td><a href="#" rel="<?= $prd_id; ?>" class="del-prod"><i class="fas fa-trash-alt"></i></a></td>
			</tr>
		<?php } ?>
	<?php }else{ ?>
		<tr>
			<td align="center" colspan="5">No tienes productos en el carro</td>
		</tr>
	<?php } ?>

	</tbody>
</table>


<?php
	
	if($_GET['d'] == 'true'){
		$despacho = valorDespacho($_GET['i']);
	}else{
		$despacho = 0;
	}


	$subtotal 	= $total / 1.19;
	$iva 		= $total - $subtotal;
	$total 		+= $despacho;


?>

<?php if($cart){ ?>
	<table class="totales">
		<tr>
			<td>Subtotal</td>
			<td align="right">$<?= number_format($subtotal,0,',','.'); ?></td>
		</tr>
		<tr>
			<td>IVA</td>
			<td align="right">$<?= number_format($iva,0,',','.'); ?></td>
		</tr>
		<tr>
			<td>Despacho</td>
			<td align="right">$<?= number_format($despacho,0,',','.'); ?></td>
		</tr>
		<tr>
			<td>Total</td>
			<td align="right"> $<?= number_format($total,0,',','.'); ?></td>
		</tr>
	</table>
<?php } ?>


<script type="text/javascript">
	
	$('.del-prod').on('click', function(e){
		e.preventDefault();
		var sku 		= "";
		var cantidad 	= 1;
		var id 			= $(this).attr('rel');

		$.ajax({
			type: 'GET',
			url: 'app/partials/ajax_cart_manual.php',
			data: {id:id, sku:sku, action:"remove", qty:cantidad},
			cache: false,
			success:function(resp) {
				$('.table-prod').load('app/partials/_cart-manual.php');
			}
		});
	});

</script>