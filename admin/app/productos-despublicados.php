<div class="filaDashboard">
	<h2 class="tituloReporte">Productos despublicados</h2>
	<?php include('includes/menuLateralInterior.php');?>
	
	<?php 
	$resultado = consulta_bd("pd.producto_id, p.nombre, pd.sku, p.publicado, p.fecha_modificacion, pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and p.publicado = 0",""); 
	?>
	
																 
	<div class="columnaDerecha">
		<div class="contTabsDashboard">
			<div class="tabActivoDashboard">
				<div class="filaTitulosInterior2">
					<div class="col1">Nombre</div>
					<div class="col2" style="width: 10%;">SKU</div>
					<div class="col3" style="width: 10%;">Estado</div>
					<div class="col4" style="width: 13%;">Modificacion</div>
					<div class="col5" style="width: 10%;">precio</div>
					<div class="col5" style="width: 10%;">precio oferta</div>
					<div class="col5"></div>
					
				</div>
				<?php for($i=0; $i<sizeof($resultado); $i++){ 
						
					if($resultado[$i][4] != ""){
						$fechaModificacion = substr($resultado[$i][4], 0, 10);
					} else {
						$fechaModificacion = "----";
					}
					
					$publicado = "";
					if($resultado[$i][3]){
						$publicado = '<i class="fas fa-check verde"></i>';
					} else {
						$publicado = '<i class="fas fa-times rojo"></i>';
					}
				
					
					?>
					<div class="filaDatos2">
						<div class="col1"><?= preview($resultado[$i][1], 35); ?></div>
						<div class="col2" style="width: 10%;"><?= $resultado[$i][2]; ?></div>
						<div class="col3" style="width: 10%;"><?= $publicado; ?></div>
						<div class="col4" style="width: 13%; min-height: 10px;"><?= $fechaModificacion; ?></div>
						<div class="col5" style="width: 10%;"><?= $resultado[$i][5]; ?></div>
						<div class="col5" style="width: 10%;"><?= $resultado[$i][6]; ?></div>
						<div class="col5" style="float: right;"><a class="previewOjo" href="index.php?op=219c&id=<?= $resultado[$i][0]; ?>"><i class="far fa-eye"></i></a></div>
					</div>
					
				
				<?php } ?>
				
				<!--<div class="fila">
					<a href="" target="_blank" class="descargarXLS">Descargar XLS</a>
				</div>-->
				
			</div>
		</div>
		
		
		
	
	</div>
	
</div><!--fin filaDashboard-->

<div style="clear: both"></div>