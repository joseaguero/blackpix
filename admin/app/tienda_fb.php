<?php
	include('../conf.php');
	$file = fopen('https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt','r');
	$aux = array();
	$i = 0;
	while ($linea = fgets($file)) {
	    $cod 	= trim(explode("-",$linea)[0]);
	    $cat 	= trim(explode(">",explode("-",$linea)[1])[0]);
	    $sub 	= $cat ." > ".trim(explode(">",explode("-",$linea)[1])[1]);
	   	if($i > 0){
		   	if(!in_array($sub, $aux)){ $aux[$cod] = $sub; }
	   	}
	    $i++;
	}	
	fclose($file);

	if($_GET['cod_fb']){
		$tienda_facebook 	= ($_GET['activo_fb']) ? 1:0;
		$categoria_google 	= ($_GET['cod_fb']) ? $_GET['cod_fb']:0;
		$update 			= update_bd("opciones","valor = $tienda_facebook","nombre = 'tienda_facebook'");
		$update 			= update_bd("opciones","valor = $categoria_google","nombre = 'categoria_google'");
	}

	$tienda_facebook 	= get_option('tienda_facebook');
	$categoria_google 	= get_option('categoria_google');
	$url_sitio 			= get_option('url_sitio');

?>

<style type="text/css">
	ul.lista-cat{
		list-style: none;
		margin: 0;
		padding: 20px;
		max-height: 300px;
		overflow-y: scroll; 
	}
</style>


<h2>Opciones Facebook</h2>

<form class="tienda">
	<table width="100%">
		<tr>
			<td width="50%">
				<table>
					<input type="hidden" name="op" value="tienda_fb">
					<tr>
						<td>Active</td>
						<td><input type="checkbox" id="activo_fb" name="activo_fb" <?= ($tienda_facebook) ? 'checked' : '';?>/></td>
					</tr>
					<tr>
						<td>Codigo categoria</td>
						<td><input type="text" class="campo_texto" id="cod_fb" name="cod_fb" value="<?= $categoria_google; ?>" /></td>
					</tr>
					<tr>
						<td colspan="2">
							<div style="border: 1px solid #ccc;">
								<ul class="lista-cat">
									<?php foreach ($aux as $key => $value) { ?>				
										<li> 
											<input type="radio" name="val_cat" class="val_cat campo_texto" value="<?= $key; ?>" id="val_<?= $key; ?>" <?php if($key == $categoria_google) echo 'checked'; ?>> 
											<label for="val_<?= $key; ?>">[<?= $key; ?>] - <?= $value; ?></label>
										</li>
									<?php } ?>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><button class="save">Guardar</button></td>
					</tr>
				</table>
			</td>
			<?php if($categoria_google){ ?>
				<td width="50%">
					<label for="url_prd">Url productos</label>
					<input type="text" value="<?= $url_sitio; ?>/admin/xml/productos_fb.php" class="campo_texto" id="url_prd">
				</td>
			<?php } ?>
		</tr>
	</table>
</form>

<script type="text/javascript">
	
$(document).ready(function(){
	$('.val_cat').on('click', function(){
		$('#cod_fb').val($(this).val());
	});
});

</script>