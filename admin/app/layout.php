<?php
setcookie("cms",1,time()+5*24*60*60);

/********************************************************\
|  Moldeable CMS - Listado de entradas.		             |
|  Fecha Modificación: 22/08/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php 
include('includes/header.php');
if (is_numeric($val_op))
$sm_table = consulta_bd("submenu_table","tablas","id = $val_op","");

?>


<link rel="stylesheet" href="css/aristo/css/uniform.default.css" media="screen" />
<script src="js/jquery.uniform.js"></script>
<script>
	$(function() {
		$(":checkbox").uniform({checkboxClass: 'checker'});
		$(".select").uniform();
	});
</script>


<script type="text/javascript" src="js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#entero').numeric();
})
</script>



</head>

<body>
<!--<div id="loader_consulta" style="display:none">Cargando...</div> -->
	
    
<div id="header">
    <?php 
		
		$val_uid = $_SESSION[$nombre_sitio."_admin"];
		$user_modificado_por = consulta_bd_por_id("nombre, apellido_paterno","administradores","",$val_uid);
		$user_name = $user_modificado_por['nombre']." ".$user_modificado_por['apellido_paterno'];
		
		$cliente = consulta_bd("valor","opciones","nombre='nombre_cliente'","");
		if($cliente[0][0] == ''){
			$nombre_cliente = 'Cliente Anonimo';
		} else {
			$nombre_cliente = $cliente[0][0];
		}
		
	?>
    <div class="center">
        <div class="logo">
        	<img src="pics/logo-moldeable-cms.png" width="" height="18" alt="Moldeable CMS" />
        </div>
         
			<div class="cerrarSesion">
				<p class="salir"><a href="action/logout.php" title="Cerrar Sesión"><i class="fas fa-user-times"></i></a></p>
			</div>
            <div class="perfil">
				<div class="foto">
                	<?php
                    $nombre_fichero = '../favicons/favicon.png';

                    if (file_exists($nombre_fichero)) {
                        //echo "El fichero $nombre_fichero existe";
						$iconoCliente = $nombre_fichero;
                    } else {
                        //echo "El fichero $nombre_fichero no existe";
						$iconoCliente = "pics/perfil.jpg";
                    }
					?>
                	<img src="<?= $iconoCliente; ?>" height="27" />
                </div>
                <div class="box">
                    <p class="nombre"><?php echo $user_name; ?></p>
                    <p class="empresa"><?php echo $nombre_cliente; ?></p>
                </div>
                
            </div><!--Fin perfil -->
			<?php if (opciones("informe_tienda") == 1){ ?>
				<a href="app/informes.php" class="informes btnHeader2">Informe de ventas</a>
			 <?php } ?>

			 <?php if (opciones("configuracion_cliente") == 1){ ?>
				<a href="index.php?op=conf_admin" class="btnHeader2">Configuración</a>
			 <?php } ?>
		
            <?php 
            $popup = consulta_bd('activo', 'active_popup', '', '');
            $cyber = opciones('cyber');
            ?>
            <?php if (opciones('popup') == '1'): ?>
            	<a href="javascript:void(0)" class="activar_popup btn-active <?= ($popup[0][0] == 1) ? 'activado' : '' ?>"><?= ($popup[0][0] == 0) ? 'Activar PopUp' : 'Desactivar PopUp' ?></a>
            <?php endif ?>
			<?php if (opciones('modulo_cyber') == '1'): ?>
				<a href="javascript:void(0)" class="activar_cyber btn-active <?= ($cyber == 1) ? 'activado' : '' ?>"><?= ($cyber == 0) ? 'Activar Cyber' : 'Desactivar Cyber' ?></a>
			<?php endif ?>
            
        </div><!--Fin center -->
    </div><!--fin header -->
    
    
    
<div id="menu">
	<?php include('includes/menu.php'); ?>
    <div style="clear:both"></div>
</div>



<div id="back" <?php if($op == "dashboard" || $op == "sin-imagen" || $op == "mas-vendidos" || $op == "productos-despublicados" || $op == "conf_admin" || $op == "sin-stock" || $op == "cantidades-disponibles" || $op == "sin-precio" || $op == "sin-descripcion"){ ?>style="margin-top: 53px;" <?php } ?>>
	<div id="principal"><?php include("app/$page.php"); ?></div>
</div>




<!--cierre faltante -->
</div>



<!--<div id="footer">
	Powered By Moldeable S.A. - 2019<br />
	Todos los derechos reservados. <?php echo $_SESSION['env'];?>
</div>-->







<?php  

	$idEntrada = $_GET[id];
	
?>
<script src="js/croppic.js"></script>
<script type="text/javascript">
var croppicHeaderOptions = {
			uploadUrl:'img_save_to_file.php',
			cropData:{
				"dummyData":1,
				"tabla":"nombre_tabla"
			},
			cropUrl:'img_crop_to_file.php',
			customUploadButtonId:'cropContainerHeaderButton',
			modal:false,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
			onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
			onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
			onImgDrag: function(){ console.log('onImgDrag') },
			onImgZoom: function(){ console.log('onImgZoom') },
			onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
			onAfterImgCrop:function(){ console.log('onAfterImgCrop') }
	}	
	var croppic = new Croppic('croppic', croppicHeaderOptions);
	
	
	
	
<?php 		
	$idTablaActual = substr($_GET[op], -1);
	if(is_numeric($real_op)){
		$idTablaActual = $_GET[op];
	} else {
		$idTablaActual = substr($_GET[op], 0, -1);
		if($idTablaActual > 0){
			$idTablaActual = substr($_GET[op], 0, -1);
			
			} else {
			$idTabla2 = opciones("first_table_id");
			if($idTabla2 > 0){
				$idTablaActual = $idTabla2;
				} else {
				$idTablaActual = 1;
				}
			
			}
		
		
		}
		
	//$paso1 = consulta_bd("id","tablas","nombre='$tabla'","id desc");
	$paso4 = consulta_bd("valor","opciones_tablas","tabla_id= $idTablaActual"." and nombre ='crop'","id desc");
	$campos_separados2 = explode(",", $paso4[0][0]);
	for($k=0; $k<sizeof($campos_separados2); $k++){ ?>
		var croppicContainerModalOptions_<?php echo $campos_separados2[$k] ?> = {
			uploadUrl:'img_save_to_file.php',
			cropUrl:'img_crop_to_file.php?tabla=<?php echo $tabla; ?>&campo=<?php echo $campos_separados2[$k] ?>&id=<?php echo $idEntrada; ?>',
			modal:true,
			imgEyecandyOpacity:0.4,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
			onAfterImgCrop:function(){
				var imagen_nueva = $("#<?php echo $campos_separados2[$k] ?>_cropContainerModal").find("img.croppedImg").attr("src");
				$("img#cont_img_actual_<?php echo $campos_separados2[$k] ?>").attr("src", imagen_nueva)
			}
			};
	<?php
		echo "var ".$campos_separados2[$k]."_cropContainerModal = new Croppic('".$campos_separados2[$k]."_cropContainerModal', croppicContainerModalOptions_".$campos_separados2[$k].");";
		
	}//fin for
	?>
</script>
    
    

<!--alertas -->

<script type="text/javascript">
$(function(){
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'ventana'){ ?>
		alertify.alert("<?php echo $_GET[error]; ?>");
	<?php } ?>	
	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'notificacion'){ ?>
	alertify.log("<?php echo $_GET[error];?>");
	<?php } ?>
	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'exito'){ ?>
	alertify.success("<?php echo $_GET[error];?>");
	<?php } ?>
	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'error'){ ?>
	alertify.error("<?php echo $_GET[error];?>");
	<?php } ?>

	$(function(){
		$('.activar_popup').click(function(){
			console.log('a');
			$.ajax({
				url: 'action/activar_popup.php',
				type: 'post',
				success: function(res){
					console.log(res);
					location.reload();
				}
			}).fail(function(res){
				console.log(res);
			});
		});

		$('.activar_cyber').click(function(){
			console.log('a');
			$.ajax({
				url: 'action/activar_cyber.php',
				type: 'post',
				success: function(res){
					console.log(res);
					location.reload();
				}
			}).fail(function(res){
				console.log(res);
			});
		});
	})
});	
</script>

<?php 

$real_op = substr($_GET[op], -1);
if(is_numeric($real_op)){
	$idTabla = $_GET[op];
	$table_name = consulta_bd('nombre', 'tablas', "id = $idTabla", '');	
	} else {
		$idTabla = substr($_GET[op], 0, -1);
		if($idTabla > 0){
			$table_name = consulta_bd('nombre', 'tablas', "id = $idTabla", '');	
			} else {
			$idTabla = opciones("first_table_id");
			if($idTabla > 0){
				$table_name = consulta_bd('nombre', 'tablas', "id = $idTabla", '');
				} else {
				$table_name = consulta_bd('nombre', 'tablas', "id = 1", '');	
				}
			
			}
		
		
		}


if ($table_name[0][0] == 'codigo_descuento') { 
	if (opciones('descuento_premium') == 0) { ?>
		<script>
			$(function(){
				$("#descuento_opcion_id").parent().parent().parent().css('display', 'none');
				$("#donde_el_descuento").parent().parent().css('display', 'none');
				$("#cliente_id").parent().parent().parent().css('display', 'none');
				$("#cliente_id").val(0);
				$("#descuento_opcion_id").val(4);
				$("#donde_el_descuento").val(0);
			})
		</script>
	<?php }else{ ?>
		<script>
			$(function(){
				if ($('#descuento_opcion_id').val() == 0 || $('#descuento_opcion_id').val() == 4) {
					$("#donde_el_descuento").parent().parent().css('display', 'none');
				}else{
					$("#donde_el_descuento").parent().append('<select class="select" name="'+$("#donde_el_descuento").attr('name')+'" id="donde_el_descuento_select"></select>');
					$.ajax({
						url: 'action/buscar_tipo.php',
						type: 'post',
						dataType: 'json',
						data: {id_opcion: $('#descuento_opcion_id').val()}
					}).done(function(res){
						var input = $("#donde_el_descuento");
						console.log(res);
						// $('#donde_el_descuento_select').remove();

						for (var i = 0; i < res.length; i++) {
							if ($('#donde_el_descuento').val() == res[i][0]) {
								$('#donde_el_descuento_select').append('<option value="'+res[i][0]+'" selected>'+res[i][1]+'</option>');
							}else{
								$('#donde_el_descuento_select').append('<option value="'+res[i][0]+'">'+res[i][1]+'</option>');
							}
						}
					}).fail(function(res){
						console.log(res);
					})
				}
				$("#donde_el_descuento").css('display', 'none');
			})
		</script>

		<!-- Ecommerce con descuentos por Categorías, Marcas o Subcategorias -->
		<script type="text/javascript">
			// var nombre_tabla = $('#descuento_opcion_id option:selected').text();
			$(function(){
				$('#descuento_opcion_id').change(function(){
					var id_opcion = $(this).val();
					var nombre_tabla = $('#descuento_opcion_id option:selected').text();
					$("#donde_el_descuento").parent().parent().css('display', 'table-row');

					if (id_opcion != 4 && id_opcion != 0) {
						$.ajax({
							url: 'action/buscar_tipo.php',
							type: 'post',
							dataType: 'json',
							data: {id_opcion: id_opcion}
						}).done(function(res){
							var input = $("#donde_el_descuento");
							$('#donde_el_descuento_select').remove();

							input.parent().append('<select class="select" name="'+input.attr('name')+'" id="donde_el_descuento_select"><option value="0">Seleccione '+nombre_tabla+'</option></select>');

							for (var i = 0; i < res.length; i++) {
								$('#donde_el_descuento_select').append('<option value="'+res[i][0]+'">'+res[i][1]+'</option>');
							}

							reset_select();
						}).fail(function(res){
							console.log(res);
						})
					}else{
						$("#donde_el_descuento").parent().parent().css('display', 'none');
						$('#donde_el_descuento_select').val(0);
						$('#donde_el_descuento').val(0);
					}
				});

				function reset_select(){
					$('#donde_el_descuento_select').on('change', function(){
						$("#donde_el_descuento").val($(this).val());
					});
				}
				
			});
		</script>
<?php } // End if descuento premium
} // End if codigo descuento ?>

<?php if ($_GET['op'] == 'carga'): ?>
	<script>
		$(function(){
			$('.tipo_excel').on('change', function(){
				var value = $(this).val();
				var url = $('.link_plantilla').attr('data-base');

				if (value == 1) {
					$('.link_plantilla').attr('href', url + "admin/excel/excel_stock_y_precio.php");
					$('.link_plantilla').css('display', 'block');
				}else if(value == 2){
					$('.link_plantilla').attr('href', url + "admin/excel/excel_productos.xlsx");
					$('.link_plantilla').css('display', 'block');
				}
			})
		})
	</script>
<?php endif ?>



<?php //if($newsletter){ ?>
<script type="text/javascript" src="js/newsletter.js"></script>
<?php //} ?>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<script src="js/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>