<?php

/********************************************************\
|  Moldeable CMS - Acciones            				     |
|  Fecha Modificación: 25/07/2013		                 |
|  Todos los derechos reservados © Moldeable Ltda 2011   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

include('../conf.php');
include('../includes/tienda/cart/inc/functions.inc.php');
$nombre_sitio = get_nombre_sitio($base);

$remote_ip = $_SERVER['REMOTE_ADDR'];
if ($_SESSION[$nombre_sitio . "_admin"] != '' or $_SESSION['env'] == 'desarrollo') {
	//die("actions");

	$user = $_SESSION[$nombre_sitio . "_admin"];
	$user_id = (int)$user;
	$hash = $_SESSION[$nombre_sitio . "_admin_sh"];
	$s_error = "Error de sesión.Otro usuario puede estar usando su nombre de usuario.";

	if (!check_admin_loged($hash, $user) and $_SESSION['env'] == 'produccion')
		die(header("location:adlogin.php?error=$s_error"));

	header("Content-Type: text/html;charset=utf-8");


	//Acciones generales de contenido 
	if ($_POST[tabla]) {
		$tabla = $_POST[tabla];
		$id = (is_numeric($_POST[id])) ? $_POST[id] : 0;
		$val_op = get_val_op($_POST[op]); //Valor de la opción o id de la tabla, para devolverse a la misma página
		$sec_op = get_sec_op($_POST[op]);

		$productoInterior = (isset($_POST[productoInterior])) ? $_POST[productoInterior] : 0;

		//Editar
		if ($_POST[update_ . "$tabla"]) {

			//Llama a un archivo before_update de la tabla.
			$before_update_file = "callbacks/" . $tabla . "/before_update.php";
			if (file_exists($before_update_file))
				include_once($before_update_file);

			$accion_de_sistema = "Editar";
			$values = "fecha_modificacion = NOW()";

			if (column_exists($tabla, 'modificado_por')) {
				$values .= ", modificado_por = $user_id";
			}

			$exception .= 'password_check, posicion_img, ';
			$exception .= $_POST['exception'] . ", ";


			//Obtengo las opciones de la tabla
			$opciones = consulta_bd("nombre, valor", "opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t", "tabla_id = t.id", "");
			$i = 0;
			while ($i <= (sizeof($opciones) - 1)) {
				$nombre = $opciones[$i][0];
				$opcion[$nombre] = $opciones[$i][1];
				$i++;
			}
			$archivos = $opcion['archivos'];

			//Verifico que los obligatorios estén ok.
			if ($opcion['obligatorios_edit']) {
				$obligatorios_edit = explode(',', $opcion['obligatorios_edit']);
				foreach ($obligatorios_edit as $obl) {
					$obl = trim($obl);
					$column_is_related = column_is_related($obl);
					if ($column_is_related) {
						if ($_POST["$obl"] == '' or $_POST["$obl"] == 0) {
							$obl = str_replace('_id', '', $obl);
							$campos_error .= ($campos_error) ? ", " . get_real_name($obl) : get_real_name($obl);
						}
					} else {
						if ($_POST["$obl"] == '') {
							$obl = str_replace('_id', '', $obl);
							$campos_error .= ($campos_error) ? ", " . get_real_name($obl) : get_real_name($obl);
						}
					}

					if ($campos_error) {
						$campos_error = str_replace(',', '<br />', $campos_error);
						$error = "Debe completar los siguientes campos:<br /><br /> $campos_error&tipo=notificacion";
					}
				}
			}

			if ($archivos != '') {

				if (strrpos($archivos, ",")) {
					$col_archivos = explode(',', $archivos);
					$exception .= "$col_archivos,";
				} else {
					$col_archivos =  $archivos;
					$exception .= "$col_archivos,";
				}
			}

			//Se manejan los archivos en caso de existir (Sólo en caso de update)
			if ($col_archivos != '') {

				$cant_files = 0;
				if (sizeof($col_archivos) > 1) {
					foreach ($col_archivos as $f) {
						if ($_FILES[trim($f)]['name'] != '')
							$cant_files++;
					}
				} else {
					if ($_FILES[$col_archivos]['name'] != '')
						$cant_files++;
				}
				if ($cant_files > 0) {
					//var_dump($cant_files);
					$manage_files = manage_files($col_archivos, $opcion, $tabla, $id);
					$error .= $manage_files['error'];
					$values .= $manage_files['values'];
					//Arreglo con los nombres de las imagenes procesadas
					//var_dump($values);
				}
			}

			//Se manejan los valores de una relación muchos a muchos en caso de existir.
			$many_to_many = $opcion['many_to_many'];
			if (substr_count($many_to_many, ",") > 0) {
				$many_to_many = explode(',', $many_to_many);
			}

			if (is_array($many_to_many)) {
				foreach ($many_to_many as $many) {
					var_dump($many);
					if ($many != '') {
						$rel_column1 = singular($tabla) . "_id"; //Subcategoria_id
						$rel_column2 = singular($many) . "_id"; //Categoria_id
						$rel_table = $many . "_" . $tabla;
						//Elimino todos los asociados de la tabla relacionada
						$del = del_bd_generic($rel_table, $rel_column1, $id);
						if ($del) {
							//Obtengo el listado de Ids de la relación
							$filas = consulta_bd("id, nombre", "$many", "", "id DESC");
							$i = 0;
							while ($i <= (sizeof($filas) - 1)) {
								$id_mtm = $filas[$i][0];
								$nombre_mtm = str_replace(" ", "_", $filas[$i][1]);
								$col = $nombre_mtm . "_" . $id_mtm;
								if ($_POST["$col"] == 'on') {
									//Ingreso los valores normales
									$campos_mtm = "$rel_column1, $rel_column2";
									$valores_mtm = "$id, $id_mtm";
									$insert = insert_bd($rel_table, $campos_mtm, $valores_mtm);

									if ($many == 'lineas') {
										if (!tiene_posicion($rel_column2, $id_mtm, $id)) {
											$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $rel_column2 = $id_mtm");
											$insert_posicion = insert_bd("posicion_productos", $campos_mtm, $valores_mtm);
										}
									}

									if (!$insert)
										$error .= "Error al ingresar las" . get_real_name($many) . "&tipo=error";

									//Veo si hay una relacion belongs_to
									$belongs_to_q = consulta_bd("tabla_id", "opciones_tablas", "nombre = 'belongs_to' AND valor = '$many'", "");
									$belongs_to_id = $belongs_to_q[0][0];
									if ($belongs_to_id) {
										$bt_table = obtener_tabla($belongs_to_id);
										$bt_rel = singular($bt_table) . '_id';
										//Recorro la tabla bt
										$filas_bt = consulta_bd("id, nombre", "$bt_table", "$rel_column2 = $id_mtm", "");
										$j = 0;
										while ($j <= sizeof($filas_bt) - 1) {
											$id_bt = $filas_bt[$j][0];
											$col_bt = $col . '_' . $id_bt;
											if ($_POST["$col_bt"] == 'on') {
												$campos_mtm = "$rel_column1, $rel_column2, $bt_rel";
												$valores_mtm = "$id, $id_mtm, $id_bt";
												$insert = insert_bd($rel_table, $campos_mtm, $valores_mtm);

												if ($many == 'lineas') {
													if (!tiene_posicion($bt_rel, $id_bt, $id)) {
														$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $bt_rel = $id_bt");
														$insert_posicion = insert_bd("posicion_productos", $campos_mtm, $valores_mtm);
													}
												}

												if (!$insert)
													$error .= "Error al ingresar las" . get_real_name($many) . "&tipo=error";
											} else {
												if ($many == 'lineas') {
													$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $bt_rel = $id_bt");
												}
											}
											$j++;
										}
									}
								} else {
									if ($many == 'lineas') {
										$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $rel_column2 = $id_mtm");
									}
								}
								$i++;
							}
						} else {
							$error .= "Error al actualizar las" . get_real_name($many_to_many) . "&tipo=error";
						}
					}
				}
			} else {
				if ($many_to_many != '') {
					$rel_column1 = singular($tabla) . "_id"; //Subcategoria_id
					$rel_column2 = singular($many_to_many) . "_id"; //Categoria_id
					$rel_table = $many_to_many . "_" . $tabla;
					//Elimino todos los asociados de la tabla relacionada
					$del = del_bd_generic($rel_table, $rel_column1, $id);
					if ($del) {
						//Obtengo el listado de Ids de la relación
						$filas = consulta_bd("id, nombre", "$many_to_many", "", "id DESC");
						$i = 0;
						while ($i <= (sizeof($filas) - 1)) {
							$id_mtm = $filas[$i][0];
							$nombre_mtm = str_replace(" ", "_", $filas[$i][1]);
							$col = $nombre_mtm . "_" . $id_mtm;
							if ($_POST["$col"] == 'on') {
								//Ingreso los valores normales
								$campos_mtm = "$rel_column1, $rel_column2";
								$valores_mtm = "$id, $id_mtm";
								$insert = insert_bd($rel_table, $campos_mtm, $valores_mtm);

								if (!tiene_posicion($rel_column2, $id_mtm, $id)) {
									$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $rel_column2 = $id_mtm");
									$insert_posicion = insert_bd("posicion_productos", $campos_mtm, $valores_mtm);
								}

								if (!$insert)
									$error .= "Error al ingresar las" . get_real_name($many_to_many) . "&tipo=error";

								//Veo si hay una relacion belongs_to
								$belongs_to_q = consulta_bd("tabla_id", "opciones_tablas", "nombre = 'belongs_to' AND valor = '$many_to_many'", "");
								$belongs_to_id = $belongs_to_q[0][0];
								if ($belongs_to_id) {
									$bt_table = obtener_tabla($belongs_to_id);
									$bt_rel = singular($bt_table) . '_id';
									//Recorro la tabla bt
									$filas_bt = consulta_bd("id, nombre", "$bt_table", "$rel_column2 = $id_mtm", "");
									$j = 0;
									while ($j <= sizeof($filas_bt) - 1) {
										$id_bt = $filas_bt[$j][0];
										$col_bt = $col . '_' . $id_bt;
										if ($_POST["$col_bt"] == 'on') {
											$campos_mtm = "$rel_column1, $rel_column2, $bt_rel";
											$valores_mtm = "$id, $id_mtm, $id_bt";
											$insert = insert_bd($rel_table, $campos_mtm, $valores_mtm);

											if (!tiene_posicion($bt_rel, $id_bt, $id)) {
												$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $bt_rel = $id_bt");
												$insert_posicion = insert_bd("posicion_productos", $campos_mtm, $valores_mtm);
											}

											if (!$insert)
												$error .= "Error al ingresar las" . get_real_name($many_to_many) . "&tipo=error";

											// TERCERA CATEGORIA
											$belongs_to_q2 = consulta_bd("tabla_id", "opciones_tablas", "nombre = 'belongs_to' AND valor = '$bt_table'", "");
											$belongs_to_id2 = $belongs_to_q2[0][0];
											if ($belongs_to_id2) {
												$bt_table2 = obtener_tabla($belongs_to_id2);
												$bt_rel2 = singular($bt_table2) . '_id';
												//Recorro la tabla bt
												$filas_bt2 = consulta_bd("id, nombre", "$bt_table2", "$bt_rel = $id_bt", "");
												$k = 0;
												while ($k <= sizeof($filas_bt2) - 1) {
													$id_bt2 = $filas_bt2[$k][0];
													$col_bt2 = $col_bt . '_' . $id_bt2;
													//die($_POST["$col_bt"]);
													if ($_POST["$col_bt2"] == 'on') {
														$campos_mtm = "$rel_column1, $rel_column2, $bt_rel, $bt_rel2";
														$valores_mtm = "$id, $id_mtm, $id_bt, $id_bt2";
														//die($rel_table." ".$campos_mtm." ".$valores_mtm);
														$insert = insert_bd($rel_table, $campos_mtm, $valores_mtm);

														if (!tiene_posicion($bt_rel2, $id_bt2, $id)) {
															$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $bt_rel2 = $id_bt2");
															$insert_posicion = insert_bd("posicion_productos", $campos_mtm, $valores_mtm);
														}

														if (!$insert)
															$error .= "Error al ingresar las" . get_real_name($many_to_many) . "&tipo=error";


														// CUARTA CATEGORIA
														$belongs_to_q3 = consulta_bd("tabla_id", "opciones_tablas", "nombre = 'belongs_to' AND valor = '$bt_table2'", "");
														$belongs_to_id3 = $belongs_to_q3[0][0];
														if ($belongs_to_id3) {
															$bt_table3 = obtener_tabla($belongs_to_id3);
															$bt_rel3 = singular($bt_table3) . '_id';
															//Recorro la tabla bt

															$filas_bt3 = consulta_bd("id, nombre", "$bt_table3", "$bt_rel2 = $id_bt2", "");

															$l = 0;
															while ($l <= sizeof($filas_bt3) - 1) {
																$id_bt3 = $filas_bt3[$l][0];
																$col_bt3 = $col_bt2 . '_' . $id_bt3;

																if ($_POST["$col_bt3"] == 'on') {
																	$campos_mtm = "$rel_column1, $rel_column2, $bt_rel, $bt_rel2, $bt_rel3";
																	$valores_mtm = "$id, $id_mtm, $id_bt, $id_bt2, $id_bt3";
																	//die($rel_table." ".$campos_mtm." ".$valores_mtm);


																	$insert = insert_bd($rel_table, $campos_mtm, $valores_mtm);
																	if (!$insert)
																		$error .= "Error al ingresar las" . get_real_name($many_to_many) . "&tipo=error";
																}
																$l++;
															}
														}
														// CUARTA CATEGORIA




													}
													$k++;
												}
											}
											// TERCERA CATEGORIA


										} else {
											$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $bt_rel = $id_bt");
										}
										$j++;
									}
								}
							} else {
								$del_posicion = del_bd_generic_columns("posicion_productos", "$rel_column1 = $id AND $rel_column2 = $id_mtm");
							}
							$i++;
						}
					} else {
						$error .= "Error al actualizar las" . get_real_name($many_to_many) . "&tipo=error";
					}
				}
			}
			//Fin many to many


			//Manejo de Has Many
			$option_has_many = $opcion['has_many'];
			$lista_has_many = explode(",", $option_has_many);
			foreach ($lista_has_many as $has_many) {
				$thm = trim($has_many);
				if ($_POST["nested_fields_$thm"]) {
					$c = 1;
					foreach ($_POST[trim($thm)] as $k => $v) {
						$size_hm = sizeof($_POST[trim($thm)]);
						if ($c == $size_hm) {
							if ($v == 'on') {
								$v = 1;
							}
							$campos_hm .= "$k";
							$values_hm .= ($v != '') ? "'$v'" : 'NULL';
						} else {
							if ($v == 'on') {
								$v = 1;
							}
							$campos_hm .= "$k, ";
							$values_hm .= ($v != '') ? "'$v'," : "NULL,";
						}

						$c++;
					}

					//Se manejan los archivos de la tabla has_many
					$tabla_hm_id = obtener_tabla_id($thm);
					$opciones_hm = consulta_bd("nombre, valor", "opciones_tablas", "tabla_id = '$tabla_hm_id'", "");
					$i = 0;
					while ($i <= (sizeof($opciones_hm) - 1)) {
						$nombre_ohm = $opciones_hm[$i][0];
						$opcion_hm[$nombre_ohm] = $opciones_hm[$i][1];
						$i++;
					}

					if ($opcion_hm['archivos'] != '') {
						if (strrpos($opcion_hm['archivos'], ",")) {
							$col_archivos_hm = explode(',', $opcion_hm['archivos']);
						} else {
							$col_archivos_hm =  $opcion_hm['archivos'];
						}
					}

					if ($col_archivos_hm != '') {
						$archivos2 = array();
						$nombreArchivos;
						foreach ($col_archivos_hm as $campoFinal) {
							$archivos2[] .= $thm . '_' . $campoFinal;
							$nombreArchivos .= ',' . $campoFinal;
						}
						//
						$manage_files = manage_files($archivos2, $opcion_hm, $thm, '');
						$error .= $manage_files['error'];
						$file_hm = $manage_files['values'];
						if ($file_hm != '') {
							$valorArchivos;
							$archivosInsert = explode(',', $file_hm);

							foreach ($archivosInsert as $archivosHS) {
								if ($archivosHS != '') {
									$val3 = explode('=', $archivosHS);
									$valorArchivos .= ',' . trim($val3[1]);
								} else {
									//echo 'malo';
								}
							}
							$campos_hm .= $nombreArchivos;
							$values_hm .= $valorArchivos;
							$campos_hm .= ",fecha_creacion, creado_por, " . singular($tabla) . "_id";
							$values_hm .= ",NOW(), " . $user_id . ", " . $id;
							$insert = insert_bd("$thm", "$campos_hm", "$values_hm");

							//die($valorArchivos);
							if (!$insert) {
								$error = "Archivos son obligatorios.&tipo=notificacion";
								break;
							}
						}
					} else {
						$campos_hm .= ",fecha_creacion, creado_por, " . singular($tabla) . "_id";
						$values_hm .= ",NOW(), " . $user_id . ", " . $id;
						$insert = insert_bd("$thm", "$campos_hm", "$values_hm");
					}


					if (!$insert and !$error) {
						$error .= "Error al ingresar los campos de $thm&tipo=error";
						break;
					}
				}
			}
			//End Has Many


			//Cambio las posiciones de las galerías
			if ($opcion['galerias']) {
				$size_post_posicion = sizeof($_POST['posicion_img']);
				$id_tabla = singular($tabla) . "_id";
				$cant_img = consulta_bd("*", "img_$tabla", "$id_tabla = $id", "");
				$cant_img = mysqli_affected_rows($conexion);
				if ($size_post_posicion == $cant_img) {
					if ($_POST['posicion_img']) {
						foreach ($_POST['posicion_img'] as $cod_img => $val) {
							$update = update_bd("img_$tabla", "posicion = $val", "id = $cod_img");
						}
					}
				}
			}
			//Fin Cambio posiciones

			//Cambio lde los textos de las galerias
			if ($opcion['texto_galerias']) {
				$size_post_textos = sizeof($_POST['texto_img']);
				$id_tabla = singular($tabla) . "_id";
				$cant_img = consulta_bd("*", "img_$tabla", "$id_tabla = $id", "");
				$cant_img = mysqli_affected_rows($conexion);
				if ($size_post_textos == $cant_img) {
					if ($_POST['texto_img']) {
						foreach ($_POST['texto_img'] as $cod_img => $val) {
							$update = update_bd("img_$tabla", "texto = '$val'", "id = $cod_img");
						}
					}
				}
			}
			//Fin textos galerias

			//Cambio la galería en caso de ser multiple
			if ($opcion['multiple_galerias']) {
				if ($_POST['galeria_img']) {
					foreach ($_POST['galeria_img'] as $cod_img => $val) {
						$update = update_bd("img_$tabla", "galeria = '$val'", "id = $cod_img");
					}
				}
			}
			//Fin multiple galerias

			$columnas = show_columns($tabla, $exception);

			/*echo "<pre>";
		var_dump($columnas);
		echo "</pre>";
		die("$columnas");*/
			$i = 0;
			while ($i <= (sizeof($columnas) - 1)) {
				if (!$error) {
					$fila = $columnas[$i];
					$conditions = "id = '$id'";
					if (is_boolean($fila, $tabla)) {
						$checked = ($_POST[$fila]) ? 1 : 0;
						$values .= ", $fila = '$checked'";
					} elseif ($fila == 'password') {
						if ($_POST['password'] != '') {
							if ($_POST['password'] == $_POST['password_check']) {
								if (strlen($_POST['password']) >= 6) {
									require_once('../includes/bcrypt_class.php');
									$bcrypt = new Bcrypt(15);
									$pass = $bcrypt->hash($_POST['password']);
									$values .= ", $fila = '$pass'";
								} else {
									$error = "La contraseña debe ser de un largo mínimo de 6 caracteres.&tipo=notificacion";
								}
							} else {
								$error .= "Error, las password no coinciden.&tipo=error";
							}
						}
					} elseif (!is_inside($exception, $fila)) {
						$values2 = '';
						$val = eng_sql($_POST[$fila]);
						if ($val == '') {
							//evitar que los campos que se vacien se mantengan asi
							$vaciar = opciones("tiny_mce");
							$arregloVacios = explode(",", $vaciar);
							$arregloVacios = array_map('trim', $arregloVacios);
							if (in_array("$fila", $arregloVacios)) {
								$values .= ", $fila = NULL";
							} else {
							}
						} else {
							$values .= ", $fila = '$val'";
						}
					}
				}
				$i++;
			}
			//var_dump($values);








			if (!$error) {
				$update = update_bd($tabla, $values, $conditions);
				if ($update) {
					$error .= "Se ha editado la entrada.&tipo=exito";
				} else {
					$error .= "Error al editado la entrada.&tipo=error";
				}
			}

			/*Log*/
			$log = insert_bd("system_logs", "tabla, accion, fila, administrador_id,date, remote_ip", "'$tabla','$accion_de_sistema', '$id','$user_id', NOW(), '$remote_ip'");
			/*Fin Log*/

			//Llama a un archivo after_update de la tabla.
			$after_update_file = "callbacks/" . $tabla . "/after_update.php";
			if (file_exists($after_update_file))
				include_once($after_update_file);

			$vars = get_post('');
			if (isset($_POST[productoInterior])) {
				//die("$error");
				header("location:../index.php?op=" . $_POST[opRetorno] . "&id=" . $_POST[idPadre] . "&error=$error");
			} else {
				//die("$error");
				header("location:../index.php?op=" . $val_op . "c&id=$id&error=$error");
			}
		}


		//Agregar
		elseif ($_POST[add_ . "$tabla"]) {
			//Llama a un archivo before_create de la tabla.
			$before_create_file = "callbacks/" . $tabla . "/before_create.php";
			if (file_exists($before_create_file))
				include_once($before_create_file);

			$accion_de_sistema = "Agregar";
			//Obtengo las opciones de la tabla
			$opciones = consulta_bd("nombre, valor", "opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t", "tabla_id = t.id", "");
			$i = 0;
			while ($i <= (sizeof($opciones) - 1)) {
				$nombre = $opciones[$i][0];
				$opcion[$nombre] = $opciones[$i][1];
				$i++;
			}

			//Verifico que estén completos los campos obligatorios	
			$obligatorios = ($opcion['obligatorios'] != '') ? $opcion['obligatorios'] : $opcion['obligatorios_edit'];
			if ($obligatorios != '') {
				if (strrpos($obligatorios, ",")) {
					$col_ob = explode(',', $obligatorios);
					foreach ($col_ob as $columna) {
						$col = trim($columna);
						if ($_POST["$col"] == '') {
							$error = "Debes completar todos los campos&tipo=error";
							break;
						}
					}
				} else {
					$col_ob =  trim($obligatorios);
					if ($_POST["$col_ob"] == '') {
						$error = "Debes completar todos los campos&tipo=error";
					}
				}
			}

			if (!isset($error)) {
				$exception = "password_check, add_$tabla, tabla, op, fecha_modificacion, publicada, publicado, password, exception, creado_por, rid";
				if ($opcion['exclusiones']) {
					$exception .= ", " . $opcion['exclusiones'];
				}
				if ($_POST['exception']) {
					$exception .= ", " . $_POST['exception'];
				}

				$campos = "fecha_creacion";
				$values = "NOW()";
				if (column_exists($tabla, 'creado_por')) {
					$campos .= ", creado_por";
					$values .= ", $user_id";
				}

				//Verifico que el correo sea válido, en caso de existir
				if ($_POST['email']) {
					$ok = comprobar_mail($_POST['email']);
					if (!$ok)
						$error = "E-mail no válido&tipo=error";
				}

				//Verifico la contraseña en caso de que exista
				if ($_POST['password']) {
					if ($_POST['password'] === $_POST['password_check']) {
						if (strlen($_POST['password']) >= 6) {
							require_once('../includes/bcrypt_class.php');
							$bcrypt = new Bcrypt(15);
							$pass = $bcrypt->hash($_POST['password']);
							$campos .= ", password";
							$values .= ", '$pass'";
						} else {
							$error .= "La contraseña debe ser de un largo mínimo de 6 caracteres.&tipo=notificacion";
						}
					} else {
						$error .= "Error, las password no coinciden.&tipo=error";
					}
				}

				if (!$error) {
					foreach ($_POST as $key => $val) {
						if (is_boolean($key, $tabla)) {
							$checked = ($_POST[$key]) ? 1 : 0;
							$campos .= ", $key";
							$values .= ", '$checked'";
						} elseif (!is_inside($exception, $key) and !is_boolean($key, $tabla)) {
							$val = eng_sql($val);
							$campos .= ", $key";
							$values .= ", '$val'";
						}
					}


					$insert = insert_bd($tabla, $campos, $values);
					if ($insert) {
						$ok = 1;
						$error = "Se ha ingresado la entrada.&tipo=exito";
						$id = get_last("$tabla");
					} else {
						$ok = 0;
						$error = "Error al ingresar la entrada.&tipo=error";
					}
				}


				//Llama a un archivo after_create de la tabla.
				$after_create_file = "callbacks/" . $tabla . "/after_create.php";
				if (file_exists($after_create_file))
					include_once($after_create_file);

				if ($ok == 1) {
					/*Log*/
					$log = insert_bd("system_logs", "tabla, accion, fila, administrador_id,date, remote_ip", "'$tabla','$accion_de_sistema', '$id','$user_id', NOW(), '$remote_ip'");
					/*Fin Log*/

					if ($sec_op == 'c') {
						//Tabla configurada para un solo paso
						$vars = get_post('');
						header("location:../index.php?op=" . $val_op . "c&id=$id&error=$error$vars");
					} else {
						$rid = ($_POST[rid]) ? "&rid=" . $_POST[rid] : '';
						header("location:../index.php?op=" . $val_op . "c&id=$id$rid");
					}
				} else {
					$vars = get_post('');
					header("location:../index.php?op=" . $val_op . "c&id=$id&error=$error$vars");
				}
			} else {
				$vars = get_post('');
				header("location:../index.php?op=" . $val_op . "b&id=$id&error=$error$vars");
			}
		}
	}
	//Eliminar
	elseif ($_GET[del]) {
		$id = $_GET[del];
		$tabla = $_GET[tabla];
		$tabla_id = consulta_bd("id", "tablas", "nombre = '$tabla'", "");
		$val_op = $tabla_id[0][0];

		//Llama a un archivo before_destroy de la tabla.
		$before_destroy_file = "callbacks/" . $tabla . "/before_destroy.php";
		if (file_exists($before_destroy_file))
			include_once($before_destroy_file);

		$accion_de_sistema = "Eliminar";
		if (is_numeric($_GET[del])) {
			//Veo si tiene galería asociada
			$gal_q = consulta_bd("valor", "opciones_tablas", "tabla_id = '$val_op' AND nombre = 'galerias'", "");
			$gal = $gal_q[0][0];
			if ($gal) {
				//Consulto por todas las fotos que tiene asiciada la entrada y las borro
				$columna_id = singular("$tabla") . "_id";
				$img_q = consulta_bd("archivo, id", "img_$tabla", "$columna_id = '$id'", "");
				$i = 0;
				while ($i <= (sizeof($img_q) - 1)) {
					$file = "../../imagenes/$tabla/" . $img_q[$i][0];
					$file_thumb = "../../imagenes/$tabla/thumbs/" . $img_q[$i][0];
					if (file_exists($file)) {
						unlink($file);
						if (file_exists($file_thumb))
							unlink($file_thumb);
					}
					del_bd("img_$tabla", $img_q[$i][1]);
					$i++;
				}
			}

			//Veo si tiene algún archivo
			$ar = consulta_bd("valor", "opciones_tablas", "nombre= 'archivos' AND tabla_id = '$val_op'", "");
			if (mysqli_affected_rows($conexion) > 0) {
				$archivos = explode(',', $ar[0][0]);
				if (sizeof($archivos) > 1) {
					foreach ($archivos as $archivo) {
						$file_q = consulta_bd_por_id("$archivo", "$tabla", "", $id);
						$file = $file_q[$archivo];

						$img = "../../imagenes/$tabla/$file";
						if (file_exists($img) and $file != '')
							unlink($img);

						$img_thumb = "../../imagenes/$tabla/thumbs/$file";
						if (file_exists($img_thumb) and $file != '')
							unlink($img_thumb);

						$doc = "../../docs/$tabla/$file";
						if (file_exists($doc) and $file != '')
							unlink($doc);
					}
				} elseif ($archivos != '') {
					$archivo = $archivos[0];
					$file_q = consulta_bd_por_id("$archivo", "$tabla", "", $id);
					$file = $file_q[$archivo];

					$img = "../../imagenes/$tabla/$file";
					if (file_exists($img) and $file != '')
						unlink($img);

					$img_thumb = "../../imagenes/$tabla/thumbs/$file";
					if (file_exists($img_thumb) and $file != '')
						unlink($img_thumb);

					$doc = "../../docs/$tabla/$file";
					if (file_exists($doc) and $file != '')
						unlink($doc);
				}
			}

			$delete = del_bd($tabla, $id);
			if ($delete) {
				$error = "Se ha eliminado la entrada&tipo=exito";
			} else {
				$error = "Error al borrar la entrada&tipo=error";
			}
		}

		$submenu_table_q = consulta_bd("submenu_table", "tablas", "nombre = '$tabla'", "");
		$submenu_table = $submenu_table_q[0][0];
		if (substr_count($submenu_table, ",") != 0)
			$rid = "&id=" . $_GET[rid] . "&r=" . $submenu_table;

		if ($_GET[op]) {
			$op = $_GET[op];
			$id = $_GET[id];
			$last = $_GET[last];
		}

		/*Log*/
		$log = insert_bd("system_logs", "tabla, accion, fila, administrador_id,date, remote_ip", "'$tabla','$accion_de_sistema', '$id','$user_id', NOW(), '$remote_ip'");
		/*Fin Log*/

		//Llama a un archivo after_destroy de la tabla.
		$after_destroy_file = "callbacks/" . $tabla . "/after_destroy.php";
		if (file_exists($after_destroy_file))
			include_once($after_destroy_file);

		$link = (isset($op)) ? "../index.php?error=$error&op=$op&id=$id&last=$last" : "../index.php?op=" . $val_op . "a&error=$error$rid";
		header("location:$link");
	}

	//Fin IF Session

} else {
	header("location:../adlogin.php");
}
