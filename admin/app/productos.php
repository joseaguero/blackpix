<?php
	include('../conf.php');
	$servicio = consulta_bd("valor","opciones","nombre = 'servicio'","");
	$tienda = "Ejemplo moldeable";//$_GET['tienda'];
		
	$pedidos = countPedidos($servicio[0][0]);
	$productos = countProductos($servicio[0][0]);
	$tiendaName = "Ejemplo moldeable";//$result["nombre"];

	ksort($pedidos['total_meses']);
	ksort($productos['total_meses']);

	ksort($productos['totales']);
	
?>


	<script type="text/javascript">
	$(function () {
	    var pedidos = new Highcharts.chart({
			chart: {
		    zoomType: 'xy',
		    backgroundColor: '#FAFAFA',
		    renderTo: 'grafico_pedidos'
		    },
		    title: {
		        text: 'Cantidad Productos de Pedidos'
		    },
		    xAxis: [{
		        categories:  ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		        crosshair: true
		    }],
			colors: ['#101DC2', '#10C2AA', '#1050CC', '#1777B6', '#10B2CC'],
		    yAxis: [{ // Primary yAxis
		        title: {
		            text: 'Cantidad promedio por pedido',
		            style: {
		                color: Highcharts.getOptions().colors[1]
		            }
		        }
		    }, { // Secondary yAxis
		        title: {
		        	text: 'Cantidad Productos por mes Según Año',
		            style: {
		                color: Highcharts.getOptions().colors[0]
		            }
		        },
		        opposite: true
		    }],
		    tooltip: {
		        shared: true
		    },
		    series: [
		    	<?php foreach ($pedidos['total_meses'] as $item => $value): ?>
	        		{ name: <?=$item?>,
	        		type: 'column',
	        		yAxis: 1,
	        		data: [
	        			<?php 
				        	for($i = 0; $i < 12; $i++):
				        		if ($value[$i] != null) {
				        			echo $value[$i] . ',';
				        		}else{
				        			echo 0 .',';
				        		}
				        		
				        	endfor;
				        ?>
	        		]
			    },
	        	<?php endforeach ?>
	        	<?php foreach ($productos['total_meses'] as $key => $value): ?>
        		{
        			name: 'Cantidad Promedio Productos '+ <?= $key ?>,
        			type: 'spline',
        			data: [
        				<?php 
        				for($i = 0; $i < 12; $i++):
			        		if ($value[$i] == 0):
			        			echo 0 . ',';
			        		 else:
			        			echo round($value[$i] / $pedidos['total_meses'][$key][$i]) . ',';
			        		endif;
		        		endfor;
        				?>
        			]
        		},
	        	<?php endforeach ?>
		    ]
		});
	});
</script>

<h2 class="tituloReporte" style="margin-top: -50px;">Pedidos por Mes/Año</h2>
	<div class="contenedor">
        
        <div class="total-por-anio">
        	<h1>Total Productos</h1>
        	<h2>POR AÑO</h2>
        	<div class="box">
        		<p id="anio">Año</p> <p id="porc">Promedio por pedido</p>
                <div class="clearfix"></div>
        		
                
                <table width="100%" border="0" cellpadding="0">
                  <tr style="background-color:#1777B6; color:#fff;">
                    <td width="40%"><p id="anio">Año</p></td>
                    <td width="60%"><p id="porc">Promedio por pedido</p></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php foreach ($productos['totales'] as $key => $value): ?>
					<tr>
						<td><?= substr($key, 1) ?></td>
						<td>&nbsp;</td>
	                </tr>
					<tr>
					<td>
						<p class="number"><?= number_format($value[0]); ?></p>
					</td>
					<td>
						<p class="number porcentaje"><?php 
							if ($value[0] > 0):
								echo number_format(round($value[0] / $pedidos['totales'][$key][0]));
							else:
								echo 0;
							endif;	        			 
							?>
						</p>
					</td>
					</tr>
					<tr style="border-top: solid 1px #ccc;">
                    <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                    <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                  </tr>
                  	
                  <?php endforeach ?>
                 </table>
        	</div>
        </div>
        <div class="div-g">
        	<div id="grafico_pedidos" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
        </div><!--fin selectores-->
        <div style="clear:both"></div>
    </div>
