<?php
	include('../conex.php');
	require_once "../functions.php";
	$tienda = $_GET['tienda'];
	$service;
	$serv1; $serv2; $serv3; $serv4;
	
	$select = "SELECT nombre, servicio FROM tienda WHERE url='$tienda'";
	$query = mysqli_query($conexion, $select);

	$result = mysqli_fetch_assoc($query);
	
	if ($tienda == 'todas'):
		$prod2016 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$prod2017 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$prod2018 = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$ped2016 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$ped2017 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$ped2018 = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$productos;
		$pedidos;
		$totales = array(0,0,0,0,0,0);
		$queryT = "SELECT nombre, servicio FROM tienda";
		$resultT = mysqli_query($conexion, $queryT);

		if (mysqli_num_rows($resultT) > 0):
			while ($row = mysqli_fetch_assoc($resultT)) {
				$pedidos = countPedidos($row["servicio"]);
				$productos = countProductos($row["servicio"]);

				for($i = 0; $i < 12; $i++):
					$ped2016[$i] += $pedidos[2016][$i];
					$prod2016[$i] += $productos[2016][$i];
				endfor;

				for($i = 0; $i < 12; $i++):
					$ped2017[$i] += $pedidos[2017][$i];
					$prod2017[$i] += $productos[2017][$i];
				endfor;

				for($i = 0; $i < 12; $i++):
					$ped2018[$i] += $pedidos[2018][$i];
					$prod2018[$i] += $productos[2018][$i];
				endfor;

				$totales[0] += $pedidos['t2016'][0];
				$totales[1] += $pedidos['t2017'][0];
				$totales[2] += $pedidos['t2018'][0];
				$totales[3] += $productos['t2016'][0];
				$totales[4] += $productos['t2017'][0];
				$totales[5] += $productos['t2018'][0];
			}		
		$tiendaName = 'Todas';	
		endif;
	else:
		$pedidos = countPedidos($result["servicio"]);
		$productos = countProductos($result["servicio"]);
		$tiendaName = $result["nombre"];
	endif;
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tienda <?php echo $tiendaName; ?> - Gráfico Productos</title>
	<script src="../../js/jquery-1.8.0.js"></script>
	<script src="../../js/highcharts.js"></script>
	<script src="../../js/exporting.js"></script>

	<script type="text/javascript" src="../../js/jquery.uniform.js"></script>

	<link rel="stylesheet" href="../../css/themes/aristo/css/uniform.aristo.css" type="text/css" media="screen" charset="utf-8" />

	<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">

	<script type="text/javascript">
	$(function () {
	    var pedidos = new Highcharts.chart({
			chart: {
		    zoomType: 'xy',
		    backgroundColor: '#FAFAFA',
		    renderTo: 'grafico_pedidos'
		    },
		    title: {
		        text: 'Cantidad Productos de Pedidos'
		    },
		    xAxis: [{
		        categories:  ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		        crosshair: true
		    }],
		    yAxis: [{ // Primary yAxis
		        title: {
		            text: 'Cantidad promedio por pedido',
		            style: {
		                color: Highcharts.getOptions().colors[1]
		            }
		        }
		    }, { // Secondary yAxis
		        title: {
		        	text: 'Cantidad Productos por mes Según Año',
		            style: {
		                color: Highcharts.getOptions().colors[0]
		            }
		        },
		        opposite: true
		    }],
		    tooltip: {
		        shared: true
		    },
		    series: [{
		        name: '2016',
		        type: 'column',
		        yAxis: 1,
		        data: [

		        	<?php 
		        	if ($tienda == 'todas') :
		        		for($i = 0; $i < 12; $i++):
					echo $prod2016[$i] . ',';
		        		endfor;
		        	else:
		        		for($i = 0; $i < 12; $i++):
		        			echo $productos['2016'][$i] . ',';
		        		endfor; 
		        	endif;
		        	?>
		        ]
		    	}, {
		    	name: '2017',
		    	type: 'column',
		    	yAxis: 1,
			    data: [
		        	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
					echo $prod2017[$i] . ',';
		        		endfor;
		       	else:
		        		for($i = 0; $i < 12; $i++):
		        			echo $productos['2017'][$i] . ',';
		        		endfor;
		        	endif;
		        	?>
		        ]
		    	},{

			   name: '2018',
			   type: 'column',
			   yAxis: 1,
			    data: [
		        	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
						echo $prod2018[$i] . ',';
		        		endfor;
		        	else:
		        		for($i = 0; $i < 12; $i++):
		        			echo $productos['2018'][$i] . ',';
		        		endfor;
		        	endif;
		        	?>
		        ]}, {
		       	name: 'Cantidad Promedio Productos 2016',
		        type: 'spline',
		        data: [
		        	<?php 
		        	if ($tienda == 'todas'):
			        	for($i = 0; $i < 12; $i++):
			        		$sumaPedidos = $ped2016[$i];
			        		if ($sumaPedidos > 0):
			        			echo round($prod2016[$i]  / $sumaPedidos) . ',';
			        		else:
			        			echo 0 . ',';
			        		endif;
		        		endfor;
		        	else:
		        	 	for($i = 0; $i < 12; $i++):
			        		if ($productos['2016'][$i] == 0):
			        			echo 0 . ',';
			        		else:
			        			echo round($productos['2016'][$i] / $pedidos['2016'][$i]) . ',';
			        		endif; 
		        		endfor;
		        	endif; 
		        	?> ],
		        },{
		      	name: 'Cantidad Promedio Productos 2017',
		      type: 'spline',
		      data: [
		        	<?php 
		        	if ($tienda == 'todas') :
		        		for($i = 0; $i < 12; $i++):
			        		$sumaPedidos = $ped2017[$i];
			        		if ($sumaPedidos > 0):
			        			echo round($prod2017[$i]  / $sumaPedidos) . ',';
			        		else:
			        			echo 0 . ',';
			        		endif; 
		        		endfor; 
		        	else:
		        	 	for($i = 0; $i < 12; $i++):
			        		if ($productos['2017'][$i] == 0):
			        			echo 0 . ',';
			        		else:
			        			echo round($productos['2017'][$i] / $pedidos['2017'][$i]) . ',';
			        		endif;
		        		endfor; 
		        	endif;
		        	?> ],
		        },{
		      name: 'Cantidad Promedio Productos 2018',
		      type: 'spline',
		      data: [
		        	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
		        			$sumaPedidos = $ped2018[$i];
			        		if ($sumaPedidos > 0):
			        			echo round($prod2018[$i]  / $sumaPedidos) . ',';
			        		else:
			        			echo 0 . ',';
			        		endif;
		        		endfor; 
		        	else:
		        	 	for($i = 0; $i < 12; $i++):
			        		if ($productos['2018'][$i] == 0):
			        			echo 0 . ',';
			        		else:
			        			echo round($productos['2018'][$i] / $pedidos['2018'][$i]) . ',';
			        		endif; 
		        		endfor;
		        	endif; 
		        	?> ],
		    }]
		});
	});
</script>
</head>
<body>
	<div class="flecha"><a href="home"><i class="fa fa-reply fa-3x" aria-hidden="true"></i></a></div>
	<div class="contenedor" style="margin-top:20px;">
        <?php include('../index.php'); ?>
        
        <div class="total-por-anio">
        	<h1>Total Productos</h1>
        	<h2>POR AÑO</h2>
        	<div class="box">
        		<p id="anio">Año</p> <p id="porc">Promedio por pedido</p>
                <div class="clearfix"></div>
        		<div class="colum">
	        		<p>2016</p>
	        		<p class="number"><?php 
	        		if ($tienda == 'todas'):
	        			echo number_format($totales[3]);
	        		else:
	        			echo number_format($productos['t2016'][0]); 
	        		endif;
	        		?>
	        		</p>
        		</div>
        		<div class="colum">
	        		<p class="number porcentaje"><?php 
	        		if ($tienda == 'todas'):
	        			if ($totales[0] > 0):
	        				echo number_format(round($totales[3] / $totales[0]));
	        			else:
	        				echo 0;
	        			endif;        			
	        		else:
	        			if ($pedidos['t2016'][0] > 0):
	        				echo number_format(round($productos['t2016'][0] / $pedidos['t2016'][0]));
	        			else:
	        				echo 0;
	        			endif;	        			 
	        		endif;?>
	        		</p>
        		</div>

        	</div>

        	<div class="box">
        		<div class="colum">
        			<p>2017</p>
	        		<p class="number"><?php 
	        		if ($tienda == 'todas'):
	        			echo number_format($totales[4]);
	        		else:
	        			echo number_format($productos['t2017'][0]); 
	        		endif;
	        		?>
	        		</p>
        		</div>

				<div class="colum">
	        		<p class="number porcentaje"><?php 
	        		if ($tienda == 'todas'):
	        			if ($totales[1] > 0):
	        				echo number_format(round($totales[4] / $totales[1]));
	        			else:
	        				echo 0;
	        			endif;
	        		else:
	        			if ($pedidos['t2017'][0] > 0):
	        				echo number_format(round($productos['t2017'][0] / $pedidos['t2017'][0]));
	        			else:
	        				echo 0;
	        			endif;
	        		endif;?>
	        		</p>
        		</div>
        	</div>

        	<div class="box">
				<div class="colum">
        			<p>2018</p>
	        		<p class="number"><?php 
	        		if ($tienda == 'todas'):
	        			echo number_format($totales[5]);
	        		else:
	        			echo number_format($productos['t2018'][0]); 
	        		endif;
	        		?>
	        		</p>
        		</div>

				<div class="colum">
	        		<p class="number porcentaje"><?php 
	        		if ($tienda == 'todas'):
	        			if ($totales[2] > 0):
	        				echo number_format(round($totales[5] / $totales[2]));
	        			else:
	        				echo 0;
	        			endif;	
	        		else:
	        			if ($pedidos['t2018'][0] > 0):
	        				echo number_format(round($productos['t2018'][0] / $pedidos['t2018'][0]));
	        			else:
	        				echo 0;
	        			endif;    			 
	        		endif;
	        		?>
	        		</p>
        		</div>
        	</div>
        </div>
        <div class="div-g">
        	<div id="grafico_pedidos" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
            <!--<div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>-->
        </div><!--fin selectores-->
    </div>
</body>
</html>