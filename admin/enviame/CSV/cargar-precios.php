<?php
    include("../../conf.php");
?>


<!DOCTYPE html>
<html>
<head>
	<title>ENVIAME.IO CSV</title>
</head>
<body>

	<?php
		$leidos 		= 0;
		$actualizados 	= 0;
		$insertados 	= 0;
		$tipo_despacho 	= 2;

		if(($gestor = fopen("cch.csv", "r")) !== FALSE) {
	    	while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE){
	    		$leidos++;

				$region 	= $datos[0];
				$comuna 	= utf8_encode(str_replace("'", "", $datos[1]));
				$dias 		= $datos[2];
				$a 			= str_replace(".","",$datos[3]);
				$b 			= str_replace(".","",$datos[4]);
				$c 			= str_replace(".","",$datos[5]);
				$d 			= str_replace(".","",$datos[6]);
				$e 			= str_replace(".","",$datos[7]);
				$f 			= str_replace(".","",$datos[8]);
				$g 			= str_replace(".","",$datos[9]);
				$h 			= str_replace(".","",$datos[10]);
				$i 			= str_replace(".","",$datos[11]);
				$j 			= str_replace(".","",$datos[12]);
				$k 			= str_replace(".","",$datos[13]);
				$l 			= str_replace(".","",$datos[14]);
				$m 			= str_replace(".","",$datos[15]);
				$n 			= str_replace(".","",$datos[16]);
				$o 			= str_replace(".","",$datos[17]);
				$p 	 		= str_replace(".","",$datos[18]);
				$q  		= str_replace(".","",$datos[19]);
				$r 	 		= str_replace(".","",$datos[20]);
				$s 	 		= str_replace(".","",$datos[21]);
				$t 	 		= str_replace(".","",$datos[22]);

				
				$regiones = consulta_bd("id","regiones","codigo = '$region'","");
				$r_id = $regiones[0][0];

				$comunas = consulta_bd("id","comunas","nombre = '$comuna' AND region_id = $r_id","");
				if($comunas){
					$c_id = $comunas[0][0];
				}else{
					$insert = insert_bd("comunas","nombre, region_id","'$comuna', $r_id");
					$c_id 	= mysqli_insert_id($conexion);
				}

				$despachos = consulta_bd("id","despachos","comuna_id = $c_id AND despachador_id = $tipo_despacho","");
				if($despacho){
					$update = update_bd("despachos",
						"dias = '$dias',
						a = $a,
						b = $b,
						c = $c,
						d = $d,
						e = $c,
						f = $f,
						g = $g,
						h = $h,
						i = $i,
						j = $j,
						k = $k,
						l = $l,
						m = $m,
						n = $n,
						o = $o,
						p = $p,
						q = $q,
						r = $r,
						s = $s,
						t = $t",
						"comuna_id = $c_id AND despachador_id = $tipo_despacho");
					$actualizados++;
				}else{
					$insert = insert_bd("despachos","comuna_id,despachador_id,dias,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t","$c_id,$tipo_despacho,$dias,$a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l,$m,$n,$o,$p,$q,$r,$s,$t");
					$insertados++;
				}
	    	}
	    }
	?>

	</table>

	<h1> Leidos: <?= $leidos; ?> </h1>
	<h1> Insertados: <?= $insertados; ?> </h1>
	<h1> Actualizados: <?= $actualizados; ?> </h1>

</body>
</html>