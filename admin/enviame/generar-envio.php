<?php
    include("../conf.php");

    /*********** CODIGOS DE CARRIERS ************/
    /*                                          */
    /* BLX -> BLUEXPRESS                        */
    /* SKN -> STARKEN                           */
    /* CHX -> CHILEXPRESS                       */
    /* DHL -> DHL                               */
    /* CCH -> CORREOSCHILE                      */
    /*                                          */
    /********************************************/

    //GenerarEnvio(17);
    //EstadoEnvio(17);

    function GenerarEnvio($pedido_id){

        $pedido     = consulta_bd("oc, nombre, rut, direccion, comuna, email, telefono, total","pedidos","id = $pedido_id","");
        $pr_pedidos = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id = $pedido_id","");
        $nombre_pr  = "";
        $peso_total = 0;
        $peso_total = 0;
        $largo      = 0;
        $altura     = 0;
        $ancho      = 0;

        foreach($pr_pedidos as $pr){
            $producto = consulta_bd("nombre, ancho, alto, largo, peso","productos_detalles","id = $pr[0]","");
            if($nombre_pr){ $nombre_pr .= ", ".$producto[0][0]; }
            else{ $nombre_pr = $producto[0][0]; }
            $peso_total += $producto[0][4];
            $largo      += $producto[0][3];
            $altura     += $producto[0][2];
            $ancho      += $producto[0][1];
        }


        $volumen = (($ancho * $largo) * $altura) / 3;


        //DATOS ENVIO           
        $imported_id            = str_replace("OC_", "", $pedido[0][0]);    // ID DEL ENVIO
        $order_price            = $pedido[0][7];                            // PRECIO
        $n_packages             = 1;                                        // NUMERO DE PAQUETES
        $content_description    = $nombre_pr;                               // DESCRIPCION DEL CONTENIDO
        $type                   = 'delivery';
        $weight                 = $peso_total;               // PESO
        $volume                 = $volumen;             // VOLUMEN

        //DATOS RECEPTOR
        $name                   = $pedido[0][1];
        $phone                  = $pedido[0][6];
        $email                  = $pedido[0][5];

        //DIRECCIÖN DESPACHO
        $place                  = $pedido[0][4];
        $full_address           = $pedido[0][3];


        //BODEGA
        $warehouse_code         = 'bod_mol';

        //CARRIER DE ENVIO
        $carrier_code           = 'CHX';    // CODIGO DE CARRIER
        $tracking_number        = str_replace("OC_", "", $pedido[0][0]);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://stage.api.enviame.io/api/s2/v2/companies/620/deliveries",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n        \"shipping_order\"       : {\n        \t\t\"imported_id\"\t\t: \"$imported_id\",\n                \"order_price\"         : \"$order_price\",\n                \"n_packages\"          : \"$n_packages\",\n                \"content_description\" : \"$content_description\",\n                \"type\"                : \"$type\",\n                \"weight\"              : \"$weight\",\n                \"volume\"              : \"$volume\"\n        },\n            \"shipping_destination\" : {\n                \"customer\"         : {\n                    \"name\"  : \"$name\",\n                    \"phone\" : \"$phone\",\n                    \"email\" :\"$email\"\n                },\n                \"delivery_address\" : {\n                    \"home_address\" : {\n                        \"place\"        : \"$place\",\n                        \"full_address\" : \"$full_address\"\n                    }\n                }\n            },\n            \"shipping_origin\" : {\n            \t\"warehouse_code\" : \"$warehouse_code\"\n            },\n            \"carrier\" : {\n            \t\"carrier_code\" : \"$carrier_code\",\n            \t\"tracking_number\": \"$tracking_number\"\n            }\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 2f43d24c-4001-46c8-a020-be5541732528",
                "api-key: 3654df0a84ccc0f4b55bc7ab4d6cb0a1"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err){
            echo "cURL Error #:" . $err;
        }
        
        echo $response;

        $resp           = json_decode($response);
        $identificador  = $resp->data->identifier;


        $update = update_bd("pedidos","id_envio = '$identificador'","id = $pedido_id");
        //die(print_r($resp));

    }

    function EstadoEnvio($pedido_id){
        $pedido     = consulta_bd("id_envio","pedidos","id = $pedido_id","");
        $id_envio   = $pedido[0][0];

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://stage.api.enviame.io/api/s2/v2/deliveries/$id_envio/tracking",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: d3016a71-d294-48c9-a608-2bd25bd1d8b2",
            "api-key: 3654df0a84ccc0f4b55bc7ab4d6cb0a1"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err){
            echo "cURL Error #:" . $err;
        }
        
        $resp = json_decode($response);
        $return = $resp->data->status;

        return $return;

        /*
            stdClass Object
            (
                [id] => 5
                [name] => Listo para despacho
                [code] => CREATED
                [info] => 
                [created_at] => 
            )
        */
    }   

?>

