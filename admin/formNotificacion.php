<?php 
	include("conf.php");
	require_once('includes/tienda/cart/inc/functions.inc.php');
	
	$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
	$eoc = consulta_bd("oc","pedidos","oc='$oc'","");
	$existeOC = mysqli_affected_rows($conexion);

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Enviar notificación</title>
	
<link href="css/style.css?v=3" rel="stylesheet" type="text/css" />
<link href="css/layout.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.btnEnvioNotificacion').click(function(){
				var oc = "<?= $oc; ?>";
				var mailReenvio = $('#campoNotificacion').val();
				
				
				
				if(mailReenvio.indexOf('@', 0) == -1 || mailReenvio.indexOf('.', 0) == -1) {
					$("#mensajeRespuesta").html("El correo electrónico introducido no es correcto.");
					$("#mensajeRespuesta").removeClass('rechazado');
					$("#mensajeRespuesta").removeClass('pagado');
					$("#mensajeRespuesta").removeClass('abandonado');
					$("#mensajeRespuesta").addClass('rechazado');
				} else {
					$("#mensajeRespuesta").removeClass('rechazado');
					$("#mensajeRespuesta").removeClass('pagado');
					$("#mensajeRespuesta").removeClass('abandonado');
					$("#mensajeRespuesta").addClass('abandonado');
					$("#mensajeRespuesta").html("Espere un momento, estamos generando el envio.");
					$.ajax({
						url      : "envioDocumentoVenta.php",
						type     : "post",
						async    : true,
						data 	 : {mailReenvio : mailReenvio, oc:oc},
						success  : function(resp){
							console.log(resp);
							if(resp == 1){
								$("#mensajeRespuesta").html("Notificacion enviada exitosamente al correo indicado.");
								$("#mensajeRespuesta").removeClass('rechazado');
								$("#mensajeRespuesta").removeClass('pagado');
								$("#mensajeRespuesta").removeClass('abandonado');
								$("#mensajeRespuesta").addClass('pagado');
							}
							else if(resp == 2){
								$("#mensajeRespuesta").html("No fue posible enviar la notificacion en este momento al mail  "+mailReenvio+", intenta mas tarde.")
								$("#mensajeRespuesta").removeClass('rechazado');
								$("#mensajeRespuesta").removeClass('pagado');
								$("#mensajeRespuesta").removeClass('abandonado');
								$("#mensajeRespuesta").addClass('rechazado');
							};
						}
					});
				};
				
			});
		});
	</script>
</head>

<body>
	
<div class="contEnvioNotificacion">
	<form method="post" action="reenvioNotificacion.php" id="reenvioNotificacion">
		<h2>Reenvio Notificación</h2>
		<h4>Escriba el correo al que desea reenviar la notificación</h4>
		<input type="text" name="correo_notificacion" class="campoNotificacion" id="campoNotificacion">
		<input type="hidden" name="ordenCompra" id="ordenCompra" value="<?= $oc; ?>">
		<a href="javascript:void(0)" class="btnEnvioNotificacion">Enviar notificación</a>
		<div id="mensajeRespuesta" class=""></div>
	</form>
</div>
</body>
</html>