$(function(){
	var time = 3;
	var $bar,
		$slick,
		isPause,
		tick,
		percentTime;

	$slick = $('.slider-home');

	$('.slider-home').slick({
		'arrows': false,
		'dots': true,
		'pauseOnDotsHover': true,
	});

	$bar = $('.slider-progress .progress');
  
	$('.slider-home').on({
		mouseenter: function() {
		  isPause = true;
		},
		mouseleave: function() {
		  isPause = false;
		}
	})

	$('.slick-next, .slick-dots li').on('click', function(){
		resetProgressbar();
		percentTime = 0;
		isPause = false;
		tick = setInterval(interval, 10);
	})

	function startProgressbar() {
		resetProgressbar();
		percentTime = 0;
		isPause = false;
		tick = setInterval(interval, 10);
	}

	function interval() {
		if(isPause === false) {
		  percentTime += 1 / (time+0.1);
		  $bar.css({
		    width: percentTime+"%"
		  });
		  if(percentTime >= 100)
		    {
		      $slick.slick('slickNext');
		      startProgressbar();
		    }
		}
	}


	function resetProgressbar() {
		$bar.css({
		 width: 0+'%' 
		});
		clearTimeout(tick);
	}

	startProgressbar();
})