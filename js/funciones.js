$(function () {

    //Oculto el buscador al hacer click en cualquier parte
    $('.i-search').click(function () {
        $('.contBuscador').fadeIn(200);
    });

    $('.contBuscador').click(function () {
        $('.contBuscador').fadeOut(200);
    });
    $('#buscador').click(function (event) {
        event.stopPropagation();
    });

    $('.slider-ficha').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.nav-slider-ficha',
        infinite: false,
        responsive: [{
            breakpoint: 860,
            settings: {
                //       arrows: true,
                //       'prevArrow': '<button type="button" class="slick-prev"><img src="img/arrow-ficha-prev.jpg" /></button>',
                // 'nextArrow': '<button type="button" class="slick-next"><img src="img/arrow-ficha-next.jpg" /></button>',
            }
        }, ]
    });

    $('.slider-ficha-zoom').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        prevArrow: '<button type="button" class="slick-prev"><img src="img/prevzoom.png" /></button>',
        nextArrow: '<button type="button" class="slick-next"><img src="img/nextzoom.png" /></button>'
    })

    $('.nav-slider-ficha').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.slider-ficha',
        dots: false,
        focusOnSelect: true,
        arrows: false,
        infinite: false,
    });

    /*Menu responsive */
    $('.toggle-btn').click(function () {
        $('.filter-btn').toggleClass('open');

    });
    $('.filter-btn a').click(function () {
        $('.filter-btn').removeClass('open');
    });

    $('.hamburger').click(function () {
        $('.hamburger').toggleClass('open');
        $('.main_menu').slideToggle(100);
    });

    $('.rutInput').Rut({
        format_on: 'keyup',
        on_error: function () {
            swal('', 'Rut ingresado incorrecto', 'error');
            $('.rutInput').val('');
        },
    })

    $(".movil").click(function () {
        $("ul.sub").hide();
        console.log("sdfsdfsfd");
        $(this).parent().find("ul.sub").show(100);
    });

    $(".abrirFooter").click(function () {
        $(".contrespFooter").hide(100);
        $(this).parent().parent().find(".contrespFooter").show(1000);
    });
    /*Menu responsive */

    /*Buscador responsive*/
    $('.iconoBuscador').click(function () {
        $('.buscador').slideToggle();
    });

    $('.content_display').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.nav-slider-home'
    });

    $('.slider-artistas').slick({
        arrows: false,
        fade: true,
        dots: true
    });

    $('.nav-slider-home').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.content_display',
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });

    $('.owl-carousel').owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        navText: ['<img src="img/prev-owl.png" />', '<img src="img/next-owl.png" />'],
        responsive: {
            0: {
                center: false,
                items: 1,
                margin: 45
            },
            840: {
                center: true,
                items: 2,
                margin: 45
            }
        }
    });

    /*Botonera responsive de identificacion*/
    $(".contBotonesIdentificacionResponsive a").click(function () {
        $(".contBotonesIdentificacionResponsive a").removeClass("active");
        $(this).addClass("active");
        $(".cont50Identificacion").fadeOut(100);
        var tabs = $(this).attr("rel");
        $("#" + tabs).fadeIn(100);
    });


    /*BOTONERAS ENVIO Y PAGO RESPONSIVE*/
    $(".contBotoneraEnvioYPago a").click(function () {
        $(".contBotoneraEnvioYPago a").removeClass("activo");
        $(this).addClass("activo");
        var tab = $(this).attr("rel");
        $(".contPaso3Movil").fadeOut(100);
        $("#" + tab).fadeIn(100);
        if (tab == "misDatos") {
            $("#facturaMovil").fadeIn(100);
        }

    });
    /*BOTONERAS ENVIO Y PAGO RESPONSIVE*/


    $('#btnNewsletter').click(function () {
        var correo = $('#newsletter').val();
        if (correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
            swal({
                type: 'error',
                text: 'El correo electrónico introducido no es correcto'
            });
            return false;
        } else {
            $.ajax({
                url: "ajax/newsletter.php",
                type: "post",
                async: true,
                data: {
                    correo: correo
                },
                success: function (resp) {
                    console.log(resp);
                    if (resp == 1) {
                        swal({
                            type: 'success',
                            text: 'Correo ingresado exitosamente'
                        });
                    } else if (resp == 2) {
                        swal({
                            type: 'error',
                            text: 'tu correo ya se encuentra en nuestros registros'
                        });
                    };
                }
            });
        };
    });
    /*
    Agregar correo al newsletter -------------------------	----------------------	----------------------	
    */


    /*
    GUARDAR NUEVA DIRECCION
    */
    $("#guardarDireccion").click(function () {
        var nombre = $("#nombre").val();
        var region = $("#region").val();
        var comuna = $("#comuna").val();
        var localidad = $("#localidad").val();
        var calle = $("#calle").val();
        var numero = $("#numero").val();

        console.log(region);
        console.log(comuna);
        console.log(localidad);
        if (nombre != "" && region != "" && comuna != "" && localidad != "" && calle != "" && numero != "") {
            $("#formNuevaDireccion").submit();
        } else {
            console.log("Faltan campos por completar.");
            swal({
                type: 'error',
                text: 'Faltan campos por completar.'
            });
        }

    });


    /*Eliminar direcciones guardadas*/
    $(".deleteDireccion").click(function () {
        var id = $(this).attr("rel");
        console.log(id);
        $.ajax({
            url: "ajax/eliminaDireccion.php",
            type: "post",
            async: true,
            data: {
                id: id
            },
            success: function (resp) {
                console.log(resp);
                if (resp == 1) {
                    console.log("dirección eliminada");
                    swal({
                        type: 'success',
                        text: 'Dirección eliminada.'
                    });
                    $("#dir_" + id).hide(300);

                } else {
                    console.log("No fue posible eliminar dirección.");
                    swal({
                        type: 'error',
                        text: '"No fue posible eliminar dirección.'
                    });
                }
            }
        });

    });









    /*
Registro
*/
    $("#btnRegistro").click(function () {
        var nombre = $("#nombre").val();
        var apellido = $('#apellido').val();
        var rut = $("#Rut").val();
        var email = $("#email").val();
        var clave = $("#clave").val();
        var clave2 = $("#clave2").val();



        if (nombre != '' && email != '' && clave != '' && clave2 != '' && apellido != '') {
            if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                /* Correo invalido */
                $("#email").addClass("campo_vacio");
                //alertify.error("Email invalido");
                swal('', 'Email invalido', 'error');
            } else {
                /* Correo valido */
                if (clave === clave2) {
                    /* Envio el formulario */
                    $("#formularioRegistro").submit();
                } else {
                    /* Claves no coinciden */
                    //alertify.error("Claves no coinciden");
                    swal('', 'Claves no coinciden', 'error');
                    $("#clave").addClass("campo_vacio");
                    $("#clave2").addClass("campo_vacio");
                };
            }; /*Fin validacion email */
        } else {
            /* indico que todos los campos deben estar llenos */
            //alertify.error("Faltan campos por completar");
            swal('', 'Faltan campos por completar', 'error');
            if (nombre == '') {
                $("#nombre").addClass("campo_vacio");
            } else {
                $("#nombre").removeClass("campo_vacio");
            };

            if (apellido == '') {
                $("#apellido").addClass("campo_vacio");
            } else {
                $("#apellido").removeClass("campo_vacio");
            };

            if (email == '') {
                $("#email").addClass("campo_vacio");
            } else {
                $("#email").removeClass("campo_vacio");
            };

            if (rut == '') {
                $("#Rut").addClass("campo_vacio");
            } else {
                $("#Rut").removeClass("campo_vacio");
            };

            if (clave == '') {
                $("#clave").addClass("campo_vacio");
            } else {
                $("#clave").removeClass("campo_vacio");
            };

            if (clave2 == '') {
                $("#clave2").addClass("campo_vacio");
            } else {
                $("#clave2").removeClass("campo_vacio");
            };



        };

    }); /*fin registro paso 1*/

    $('#formularioActualizar').on('submit', function () {
        var nombre = $(this).find('input[name="nombre"]').val(),
            email = $(this).find('input[name="email"]').val(),
            telefono = $(this).find('input[name="telefono"]').val();

        if (nombre.trim() != '' && email.trim() != '' && telefono.trim() != '') {
            if (validarEmail(email)) {
                var re = /^([0-9]){8,12}$/;

                if (re.test(telefono)) {

                    return true;

                } else {
                    swal('', 'Teléfono incorrecto', 'error');
                }

            } else {
                swal('', 'Email incorrecto', 'error');
            }
        } else {
            swal('', 'Todos los campos son obligatorios', 'error');
        }

        return false;

    })


    /*Actualizar */
    $("#btnActualizar").click(function () {
        var nombre = $("#nombre").val();
        var apellido = $('#apellido').val();
        var Rut = $("#Rut").val();
        var email = $("#email").val();
        var telefono = $("#telefono").val();
        var clave = $("#clave").val();
        var clave2 = $("#clave2").val();


        if (clave == clave2) {
            if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                swal('', 'Email incorrecto', 'error');
            } else {
                $("#formularioActualizar").submit();
            }

        } else if (clave == "" && clave2 == "") {
            if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                swal('', 'Email incorrecto', 'error');
            } else {
                $("#formularioActualizar").submit();
            }
        } else {
            swal('', 'Claves no coinciden', 'error');
        }

    }); /*fin actualizar1*/



    $('#formularioLogin').on('submit', function () {
        var email = $(this).find('input[name="email"]').val(),
            password = $(this).find('input[name="clave"]').val();

        if (email.trim() != '' && password.trim() != '') {
            if (validarEmail(email.trim())) {
                return true;
            } else {
                swal('', 'Email incorrecto', 'error');
            }
        } else {
            swal('', 'Faltan campos por completar', 'error');
        }

        return false;
    })



    /*Inicio de session carro de compras*/
    $("#inicioSesionIdentificacion").click(function () {

        var email = $("#email").val();
        var password = $("#clave").val();

        console.log(password);
        if (email != '' && password != '') {
            if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                /* Correo invalido */
                $("#email").addClass("campo_vacio");
                swal('', 'Email invalido', 'error');
            } else {
                /*envio formulario para log*/
                $(".formInicioSesion").submit();
            }; /*Fin validacion email */
        } else {
            /* indico que todos los campos deben estar llenos */
            swal('', 'Faltan campos por completar', 'error');
            if (email == '') {
                $("#email").addClass("campo_vacio");
            } else {
                $("#email").removeClass("campo_vacio");
            };

            if (password == '') {
                $("#clave").addClass("campo_vacio");
            } else {
                $("#clave").removeClass("campo_vacio");
            };
        };
    }); /*fin login*/




    /*Recuperar clave ----------------------------------------------------------------------------------------------------*/
    $('#recuperarClave').click(function () {
        //alert("sdfdsf");
        var correo = $('#email').val();
        if (correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
            swal('', 'El correo electrónico introducido no es correcto.', 'error');
            return false;
        } else {
            console.log(correo);
            $("#formRecuperarClave").submit();
        };
    }); /*fin funcion*/
    /*Recuperar clave ----------------------------------------------------------------------------------------------------*/


    /* Encvio formulario para crear cuenta a partir de los datos obtenidos de una compra exitosa*/
    $(".btnCuentaExito").click(function () {
        $("#crearCuentaDesdeExito").submit();
    });

});









/*
Validar rut
*/

$("#Rut").Rut({
    on_error: function () {
        //swal(type:'error', text:'');
        swal('Error', 'Rut incorrecto', 'error');
        $("#Rut").addClass("campo_vacio");
        $("#Rut").val("");
    },
    format_on: 'keyup',
    on_success: function () {
        $("#Rut").removeClass("campo_vacio");
    }
});
$("#rut_retiro").Rut({
    on_error: function () {
        //swal(type:'error', text:'');
        swal('Error', 'Rut incorrecto', 'error');
        $("#Rut").addClass("campo_vacio");
        $("#Rut").val("");
    },
    format_on: 'keyup',
    on_success: function () {
        $("#Rut").removeClass("campo_vacio");
    }
});
$("#RutFactura").Rut({
    on_error: function () {
        alertify.error('El rut ingresado es incorrecto');
        $("#RutFactura").addClass("incompleto");
        $("#RutFactura").val("");
    },
    format_on: 'keyup',
    on_success: function () {
        $("#RutFactura").removeClass("incompleto");
    }

});

$('.countdown').each(function () {
    var $this = $(this);
    var date = $this.attr('rel');
    var countDownDate = new Date(date).getTime();

    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        $this.empty();

        if (days == 1) {
            $this.append(days + " día " + hours + " Horas ");
        } else if (days > 0) {
            $this.append(days + " días " + hours + " Horas ");
        } else if (hours == 1) {
            $this.append(hours + " Hora " + minutes + " minutos");
        } else if (hours > 0) {
            $this.append(hours + " Horas " + minutes + " minutos");
        } else {
            $this.append(minutes + " Minutos " + seconds + "s");
        }

        if (distance < 0) {
            $this.empty();
            $this.append("TERMINADA");
        }
    }, 1000);
});

$('#spinner').on("spinstop", function (e) {
    var cant = $(this).val();
    var id = $(this).parent().parent().attr('rel');

    $('.precio-ficha').load('ajax/precio_ficha.php?cant=' + cant + '&id=' + id);

    //console.log('cant: ' + cant + ' id: ' + id);
});

var popup = setInterval(IsVisible, 3000);

function IsVisible() {
    if ($('.popup-news').is(':visible')) {
        $(".bg-popup").click(function () {
            var nombre_popup = $('.bg-popup').attr('data-cliente');
            $('.bg-popup').fadeOut();
            $('.popup-news').fadeOut();
            Cookies.set(nombre_popup, 'Newsletter (No Suscrito)', {
                expires: 14
            });
        });
    }
    clearInterval(popup);
}

$('.popup-news').click(function (e) {
    e.stopPropagation();
});
$(".popup-news .contenido > .close").on('click', function (e) {
    var nombre_popup = $('.bg-popup').attr('data-cliente');
    Cookies.set(nombre_popup, 'Newsletter (No Suscrito)', {
        expires: 14
    });
    $('.bg-popup').fadeOut();
    $('.popup-news').fadeOut();
})

$('#suscribe-newsletter').on('click', function (e) {

    e.preventDefault();
    var nombre = $('#nombre-suscribe').val();
    var email = $('#email-suscribe').val();
    var nombre_popup = $('.bg-popup').attr('data-cliente');

    if (nombre.trim() != '' && email.trim() != '') {
        $.ajax({
            url: 'ajax/popup.php',
            type: 'post',
            data: {
                nombre: nombre,
                email: email
            },
            beforeSend: function () {
                $('#suscribe-newsletter').html('ESPERE POR FAVOR...');
            },
            success: function (res) {
                Cookies.set(nombre_popup, 'Newsletter Suscrito', {
                    expires: 90
                });
                $('#suscribe-newsletter').html('SUSCRIBIRME');
                $('.bg-popup').fadeOut();
                $('.popup-news').fadeOut();
                swal('', 'Gracias por suscribirte.', 'success');
            }
        })
    } else {
        swal('', 'Debe completar todos los campos', 'error');
    }
})

$('.select_filtros .selected').on('click', function () {

    $(this).parent().find('.list').toggle();
    $(this).find('img').toggleClass('arrow-top');

    /*$('.select_filtros .list').toggle();
    $('.select_filtros .selected img').toggleClass('arrow-top');*/
})

$('.select_filtros_nochange .selected').on('click', function () {

    $(this).parent().find('.list').toggle();
    $(this).find('img').toggleClass('arrow-top');

})

var min_valor_filtros = parseInt($('.min-filtro').attr('data-info'));
var max_valor_filtros = $('.precio_filtro').attr('data-max');
var max_filtro_actual = $('.precio_filtro').attr('data-max-actual');

$("#slider-range").slider({
    range: true,
    min: 0,
    max: max_valor_filtros,
    step: 1000,
    values: [min_valor_filtros, max_filtro_actual],
    slide: function (event, ui) {
        $('.min-filtro').html('$' + ui.values[0]);
        $('.max-filtro').html('$' + ui.values[1]);

        $('.min-filtro').attr('data-valor', ui.values[0]);
        $('.max-filtro').attr('data-valor', ui.values[1]);
    },
    change: function (event, ui) {
        var chk_cat = $('input[name="chk_cat"]:checked'),
            chk_art = $('input[name="chk_art"]:checked'),
            chk_tam = $('input[name="chk_tam"]:checked'),
            chk_color = $('input[name="chk_color"]:checked'),
            min_price = $('.min-filtro').attr('data-valor'),
            max_price = $('.max-filtro').attr('data-valor'),
            ref = $('.filtros_col').attr('data-ref'),
            orden = $('.select_filtros').attr('data-order'),
            ofertas = $('.btn-ofertas').attr('data-off');

        var str_cat = '?cat=',
            str_min = '&desde=',
            str_max = '&hasta=',
            str_color = '&color=',
            str_tam = '&tam=';

        if (ref == 'lineas') {
            str_art = '&art=';

            chk_art.each(function (index, el) {
                if (str_art == '&art=') {
                    str_art += $(this).val();
                } else {
                    str_art += '-' + $(this).val();
                }
            });
        } else if (ref == 'cat') {
            str_art = '?art=';

            chk_art.each(function (index, el) {
                if (str_art == '?art=') {
                    str_art += $(this).val();
                } else {
                    str_art += '-' + $(this).val();
                }
            });
        } else if (ref == 'busquedas') {
            var valorBusqueda = $("#parametroBusqueda").html();
            str_art = '?busqueda=' + valorBusqueda;
            str_art += '&art=';

            chk_art.each(function (index, el) {
                if (str_art == '&art=') {
                    str_art += $(this).val();
                } else {
                    str_art += '-' + $(this).val();
                }
            });
        }

        chk_cat.each(function (index, el) {
            if (str_cat == '?cat=') {
                str_cat += $(this).val();
            } else {
                str_cat += '-' + $(this).val();
            }
        });

        chk_tam.each(function (index, el) {
            if (str_tam == '&tam=') {
                str_tam += $(this).val();
            } else {
                str_tam += '-' + $(this).val();
            }
        });

        chk_color.each(function (index, el) {
            if (str_color == '&color=') {
                str_color += $(this).val();
            } else {
                str_color += '-' + $(this).val();
            }
        });
        console.log("smdhfjdshbf");
        str_min += min_price;
        str_max += max_price;

        if (ref == 'lineas') {
            link = str_cat + str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
        } else if (ref == 'cat') {
            link = str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
        } else if (ref == 'busquedas') {
            link = str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
        }
        console.log(link);

        var url_actual = window.location.pathname;

        location.href = url_actual + link;
    }
});

$('.min-filtro').html('$' + $("#slider-range").slider("values", 0));
$('.max-filtro').html('$' + $("#slider-range").slider("values", 1));

$('.min-filtro').attr('data-valor', $("#slider-range").slider("values", 0));
$('.max-filtro').attr('data-valor', $("#slider-range").slider("values", 1));

$('.edicion-tab .tab').on('click', function () {

    var data_action = $(this).attr('data-action');
    var idTab = $(this).attr('data-id');
    //stock = $(this).attr('data-st');
    $('.edicion-tab .tab').removeClass("activado");
    $(this).addClass("activado");

    $(".content_tab").removeClass("activado");
    $("#tab-" + idTab).addClass("activado");
    /*$('.content_tab .list-radio .row input[type="radio"]').removeAttr('checked');

    if (data_action == "e1") {
    	$('.content_tab[data-action="e2"]').fadeOut(200, function(){
    		$('.content_tab[data-action="e1"]').fadeIn(200);
    		$('.edicion-tab .tab[data-action="e1"]').addClass('activado');
    		$('.edicion-tab .tab[data-action="e2"]').removeClass('activado');

    		$('.content_tab[data-action="e1"] .list-radio .row:first-child input[type="radio"]').attr('checked', 'checked');

    	})
    }else{
    	$('.content_tab[data-action="e1"]').fadeOut(200, function(){
    		$('.content_tab[data-action="e2"]').fadeIn(200);
    		$('.edicion-tab .tab[data-action="e1"]').removeClass('activado');
    		$('.edicion-tab .tab[data-action="e2"]').addClass('activado');

    		$('.content_tab[data-action="e2"] .list-radio .row:first-child input[type="radio"]').attr('checked', 'checked');

    	})
    }

    $('.box_select_stock').attr('data-visible', '0');
    $('.box_select_stock').html('');
    for (var i = 0; i < stock; i++) {
    	contador = i+1;
    	$('.box_select_stock').append('<div class="row_stock_ficha" data-val="'+contador+'">'+contador+'</div>');
    }*/

    //$('.whishlist_ficha').load('ajax/guardadoFicha.php?id='+productoHijoId);

    //resetSelect();

    //$('.cantidad_ficha').val(1);
    //$('.btn-compra').attr('data-id', productoHijoId);
    //$('.btn-compra').attr('data-st', stock);

})

$('.btn-compra').on('click', function () {
    var productoSeleccionado = parseInt($("input:radio[name=radio_ediciones_a]:checked").val());
    var cantidad = parseInt($('.cantidad_ficha').val());
    var marco = $('input:radio[name="radio_marcos"]:checked').val();

    // console.log(productoSeleccionado + " ----- " + cantidad);
    $.ajax({
            url: 'ajax/addToCart.php',
            type: 'POST',
            dataType: 'json',
            data: {
                producto: productoSeleccionado,
                cantidad: cantidad,
                marco: marco
            },
        })
        .done(function (res) {
            if (res['status'] == 'success') {
                $('.grid_pop').load('ajax/loadPopUp.php?id=' + productoSeleccionado + '&cantidad=' + cantidad);
                $('.bgpopcart').fadeIn();
                $('.pop-cart').fadeIn();

                $('.carro_i').load(' .cantidad_carro');

                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            } else {
                swal('', res['message'], 'error');
            }
        })
        .fail(function (res) {
            console.log(res);
        });

})

// CANTIDAD FICHA
$('.box_main_qty .cantidad_ficha').on('click', function () {
    var visible = $(this).parent().parent().find('.box_select_stock').attr('data-visible');

    if (visible == '0') {
        $(this).parent().parent().find('.box_select_stock').css('display', 'block');
        $(this).parent().parent().find('.box_select_stock').attr('data-visible', 1);
    } else {
        $(this).parent().parent().find('.box_select_stock').css('display', 'none');
        $(this).parent().parent().find('.box_select_stock').attr('data-visible', 0);
    }

})

$('.box_main_qty .row_stock_ficha').on('click', function () {

    var data_value = $(this).attr('data-val'),
        id = $('.box_qty').attr('data-id');

    if (data_value != 'add') {
        $('.cantidad_ficha').val(data_value);
        $('.box_price_ajx').load('ajax/precio_ficha.php?cant=' + data_value + '&id=' + id);
    } else {
        // Más de 10 productos
        $('.cantidad_ficha').val('');
        $('.cantidad_ficha').removeAttr('readonly');
        $('.cantidad_ficha').focus();
        $('.cantidad_ficha').attr('onblur', 'blurSelectFicha(this)');
    }

    $('.box_select_stock').toggle();

})

// CANTIDAD CARRO
function openListQty(obj) {

    var visible = $(obj).parent().parent().find('.box_select_stock').attr('data-visible');

    if (visible == '0') {
        $(obj).parent().parent().find('.box_select_stock').css('display', 'block');
        $(obj).parent().parent().find('.box_select_stock').attr('data-visible', 1);
    } else {
        $(obj).parent().parent().find('.box_select_stock').css('display', 'none');
        $(obj).parent().parent().find('.box_select_stock').attr('data-visible', 0);
    }

}

function changeQtyCart(obj) {
    var data_value = $(obj).attr('data-val'),
        id = $(obj).parent().parent().find('.box_qty').attr('data-id'),
        lista = $(obj).parent().attr('data-list');

    if (data_value != 'add') {
        $(obj).parent().parent().find('.cantidad_ficha').val(data_value);
        $.ajax({
            url: 'ajax/cartActions.php',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                cantidad: data_value,
                lista: lista,
                action: 'change'
            }
        }).done(function (res) {

            console.log(res);

            if (res['status'] == 'success') {
                $('.ajx_carro').load(' #contFilasCarro');
                $('#contValoresResumen').load(' .contentRes');
            }

        }).fail(function (res) {
            console.log(res);
        })
    } else {
        // Más de 10 productos
        $('.cantidad_ficha').val('');
        $('.cantidad_ficha').removeAttr('readonly');
        $('.cantidad_ficha').focus();
        $('.cantidad_ficha').attr('onblur', 'blurSelectFicha(this)');
    }

    $(obj).parent().toggle();
}

function resetSelect() {
    // Reset select
    $('.cantidad_ficha').on('click', function () {
        var visible = $(this).parent().parent().find('.box_select_stock').attr('data-visible');
        // console.log(visible);
        if (visible == '1') {
            $(this).parent().parent().find('.box_select_stock').css('display', 'block');
            $(this).parent().parent().find('.box_select_stock').attr('data-visible', 1);
        } else {
            $(this).parent().parent().find('.box_select_stock').css('display', 'none');
            $(this).parent().parent().find('.box_select_stock').attr('data-visible', 0);
        }

    })

    $('.box_main_qty .row_stock_ficha').on('click', function () {

        var data_value = $(this).attr('data-val'),
            id = $('.box_qty').attr('data-id');

        if (data_value != 'add') {
            $('.cantidad_ficha').val(data_value);
            $('.box_price_ajx').load('ajax/precio_ficha.php?cant=' + data_value + '&id=' + id);
        } else {
            // Más de 10 productos
            $('.cantidad_ficha').val('');
            $('.cantidad_ficha').removeAttr('readonly');
            $('.cantidad_ficha').focus();
            $('.cantidad_ficha').attr('onblur', 'blurSelectFicha(this)');
        }

        $('.box_select_stock').toggle();

    })
}

function delCart(obj) {
    var producto_id = $(obj).attr('data-rel');

    $.ajax({
        url: 'ajax/cartActions.php',
        type: 'post',
        dataType: 'json',
        data: {
            id: producto_id,
            action: 'delete'
        }
    }).done(function (res) {

        if (res['status'] == 'success') {
            $('.ajx_carro').load(' #contFilasCarro');
            $('#contValoresResumen').load(' .contentRes');
            swal('', 'Producto eliminado', 'success');
        }

    }).fail(function (res) {
        console.log(res);
    })

}

function blurSelectFicha(obj) {
    var valor = $(obj).val(),
        id = $(obj).parent().attr('data-id');

    if (valor.trim() != '') {
        $.ajax({
            url: 'ajax/ajax_validar_stock.php',
            type: 'post',
            data: {
                id: id,
                qty: valor
            }
        }).done(function (res) {
            if (res > 0) {
                $('.cantidad_ficha').val(valor);
                $('.box_price_ajx').load('ajax/precio_ficha.php?cant=' + valor + '&id=' + id);
            } else {
                swal('', 'Stock insuficiente', 'error');
            }
        }).fail(function (res) {
            console.log(res);
        })

        $(obj).removeAttr('onblur');
    }

}

$('.slider-ficha .item').on('click', function () {

    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
    $('.slider-zoom').addClass('fadeZoom');
    $('.slider-zoom').fadeIn();
    $('.bg_slider-zoom').fadeIn();

})

$('.close-zoom, .bg_slider-zoom').on('click', function () {
    $('.slider-zoom').removeClass('fadeZoom');
    $('.slider-zoom').fadeOut();
    $('.bg_slider-zoom').fadeOut();
})

$('.chk_filtro .row').on('click', function () {

    var chk_cat = $('input[name="chk_cat"]:checked'),
        chk_art = $('input[name="chk_art"]:checked'),
        chk_tam = $('input[name="chk_tam"]:checked'),
        chk_color = $('input[name="chk_color"]:checked'),
        min_price = $('.min-filtro').attr('data-valor'),
        max_price = $('.max-filtro').attr('data-valor'),
        ref = $('.filtros_col').attr('data-ref'),
        orden = $('.select_filtros').attr('data-order'),
        ofertas = $('.btn-ofertas').attr('data-off');

    var str_cat = '?cat=',
        str_min = '&desde=',
        str_max = '&hasta=',
        str_color = '&color=',
        str_tam = '&tam=';

    if (ref == 'lineas') {
        str_art = '&art=';

        chk_art.each(function (index, el) {
            if (str_art == '&art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    } else if (ref == 'cat') {
        str_art = '?art=';

        chk_art.each(function (index, el) {
            if (str_art == '?art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    } else if (ref == 'busquedas') {
        var valorBusqueda = $("#parametroBusqueda").html();
        str_bus = '?busqueda=' + valorBusqueda;
        str_art = '&art=';

        chk_art.each(function (index, el) {
            if (str_art == '&art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    }

    chk_cat.each(function (index, el) {
        if (str_cat == '?cat=') {
            str_cat += $(this).val();
        } else {
            str_cat += '-' + $(this).val();
        }
    });

    chk_tam.each(function (index, el) {
        if (str_tam == '&tam=') {
            str_tam += $(this).val();
        } else {
            str_tam += '-' + $(this).val();
        }
    });

    chk_color.each(function (index, el) {
        if (str_color == '&color=') {
            str_color += $(this).val();
        } else {
            str_color += '-' + $(this).val();
        }
    });

    var check_desde = getParameterByName('desde'),
        check_hasta = getParameterByName('hasta');

    if (check_desde) {
        if (check_desde != '') {
            if (parseInt(check_hasta) > 0) {

                str_min += min_price;
                str_max += max_price;

            } else {
                str_min = '&desde=0';
                str_max = '&hasta=0';
            }
        } else {
            str_min = '&desde=0';
            str_max = '&hasta=0';
        }
    } else {
        str_min = '&desde=0';
        str_max = '&hasta=0';
    }

    if (ref == 'lineas') {
        link = str_cat + str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    } else if (ref == 'cat') {
        link = str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    } else if (ref == 'busquedas') {
        link = str_bus + str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    }

    var url_actual = window.location.pathname;

    location.href = url_actual + link;

})

$('.aplicado').on('click', function () {
    var action = $(this).attr('data-action');

    var chk_cat = $('input[name="chk_cat"]:checked'),
        chk_art = $('input[name="chk_art"]:checked'),
        chk_tam = $('input[name="chk_tam"]:checked'),
        chk_color = $('input[name="chk_color"]:checked'),
        min_price = $('.min-filtro').attr('data-valor'),
        max_price = $('.max-filtro').attr('data-valor'),
        ref = $('.filtros_col').attr('data-ref'),
        orden = $('.select_filtros').attr('data-order'),
        ofertas = $('.btn-ofertas').attr('data-off');

    var str_cat = '?cat=',
        str_min = '&desde=',
        str_max = '&hasta=',
        str_color = '&color=',
        str_tam = '&tam=';

    if (ref == 'lineas') {
        str_art = '&art=';

        chk_art.each(function (index, el) {
            if (str_art == '&art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    } else if (ref == 'cat') {
        str_art = '?art=';

        chk_art.each(function (index, el) {
            if (str_art == '?art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    } else if (ref == 'busquedas') {
        var valorBusqueda = $("#parametroBusqueda").html();
        str_bus = '?busqueda=' + valorBusqueda;
        str_art += '&art=';

        chk_art.each(function (index, el) {
            if (str_art == '&art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    }

    chk_cat.each(function (index, el) {
        if (str_cat == '?cat=') {
            str_cat += $(this).val();
        } else {
            str_cat += '-' + $(this).val();
        }
    });

    chk_tam.each(function (index, el) {
        if (str_tam == '&tam=') {
            str_tam += $(this).val();
        } else {
            str_tam += '-' + $(this).val();
        }
    });

    chk_color.each(function (index, el) {
        if (str_color == '&color=') {
            str_color += $(this).val();
        } else {
            str_color += '-' + $(this).val();
        }
    });

    var check_desde = getParameterByName('desde'),
        check_hasta = getParameterByName('hasta');

    if (check_desde) {
        if (check_desde != '') {
            if (parseInt(check_hasta) > 0) {

                str_min += min_price;
                str_max += max_price;

            } else {
                str_min = '&desde=0';
                str_max = '&hasta=0';
            }
        } else {
            str_min = '&desde=0';
            str_max = '&hasta=0';
        }
    } else {
        str_min = '&desde=0';
        str_max = '&hasta=0';
    }

    if (action != 'precio') {
        if (action == 'ofertas') {

            ofertas = 0;

        } else {

            var id = $(this).attr('data-id');

            var params = getParameterByName(action),
                split = params.split('-');

            console.log(params);

            for (var i = 0; i < split.length; i++) {
                if (id == split[i]) {
                    split.splice(i, 1);
                }
            }

            if (action == 'cat') {

                str_cat = '?cat=' + split.join('-');

            } else if (action == 'art') {

                if (ref == 'lineas') {
                    str_art = '&art=' + split.join('-');
                } else {
                    str_art = '?art=' + split.join('-');
                }

            } else if (action == 'color') {

                str_color = '&color=' + split.join('-');

            } else if (action == 'tam') {

                str_tam = '&tam=' + split.join('-');

            }

        }
    } else {

        str_min = 0;
        str_max = 0;

    }

    if (ref == 'lineas') {
        link = str_cat + str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    } else if (ref == 'cat') {
        link = str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    } else if (ref == 'busquedas') {
        link = str_bus + str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    }

    var url_actual = window.location.pathname;

    location.href = url_actual + link;

})

$('.btn-ofertas').on('click', function () {

    var chk_cat = $('input[name="chk_cat"]:checked'),
        chk_art = $('input[name="chk_art"]:checked'),
        chk_tam = $('input[name="chk_tam"]:checked'),
        chk_color = $('input[name="chk_color"]:checked'),
        min_price = $('.min-filtro').attr('data-valor'),
        max_price = $('.max-filtro').attr('data-valor'),
        ref = $('.filtros_col').attr('data-ref'),
        orden = $('.select_filtros').attr('data-order'),
        ofertas = (parseInt($(this).attr('data-off')) == 1) ? 0 : 1;

    var str_cat = '?cat=',
        str_min = '&desde=',
        str_max = '&hasta=',
        str_color = '&color=',
        str_tam = '&tam=';

    if (ref == 'lineas') {
        str_art = '&art=';

        chk_art.each(function (index, el) {
            if (str_art == '&art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    } else if (ref == 'cat') {
        str_art = '?art=';

        chk_art.each(function (index, el) {
            if (str_art == '?art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    } else if (ref == 'busquedas') {
        var valorBusqueda = $("#parametroBusqueda").html();
        str_bus = '?busqueda=' + valorBusqueda;
        str_art = '&art=';

        chk_art.each(function (index, el) {
            if (str_art == '&art=') {
                str_art += $(this).val();
            } else {
                str_art += '-' + $(this).val();
            }
        });
    }

    chk_cat.each(function (index, el) {
        if (str_cat == '?cat=') {
            str_cat += $(this).val();
        } else {
            str_cat += '-' + $(this).val();
        }
    });

    chk_tam.each(function (index, el) {
        if (str_tam == '&tam=') {
            str_tam += $(this).val();
        } else {
            str_tam += '-' + $(this).val();
        }
    });

    chk_color.each(function (index, el) {
        if (str_color == '&color=') {
            str_color += $(this).val();
        } else {
            str_color += '-' + $(this).val();
        }
    });

    var check_desde = getParameterByName('desde'),
        check_hasta = getParameterByName('hasta');

    if (check_desde) {
        if (check_desde != '') {
            if (parseInt(check_hasta) > 0) {

                str_min += min_price;
                str_max += max_price;

            } else {
                str_min = '&desde=0';
                str_max = '&hasta=0';
            }
        } else {
            str_min = '&desde=0';
            str_max = '&hasta=0';
        }
    } else {
        str_min = '&desde=0';
        str_max = '&hasta=0';
    }

    if (ref == 'lineas') {
        link = str_cat + str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    } else if (ref == 'cat') {
        link = str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    } else if (ref == 'busquedas') {
        link = str_bus + str_art + str_tam + str_color + str_min + str_max + '&orden=' + orden + '&ofertas=' + ofertas + '&page=1';
    }




    var url_actual = window.location.pathname;

    location.href = url_actual + link;

})

$('#chk_filtro_grillas').on('change', function () {
    var checked = $(this).is(':checked');

    if (!checked) {
        $('.grid-page').addClass('animacion-filtros');
    } else {
        $('.grid-page').removeClass('animacion-filtros');
    }

})

$('#chk_filtro_grillas_xs').on('change', function () {
    var checked = $(this).is(':checked');
    console.log(checked);
    if (!checked) {
        $(".grid-page>.col:first-child").removeAttr("style");
    } else {
        $(".grid-page>.col:first-child").attr("style", "display:block!important");
    }

});

$('.close_pop, .seguirComprando, .bgpopcart').on('click', function () {
    $('.bgpopcart').fadeOut();
    $('.pop-cart').fadeOut();
})

$('.optionGroup .groupDocumento').on('click', function () {
    var valor = parseInt($(this).find('input[name="tipoDocumento"]').val());

    if (valor == 1) {

        $('#radioBoleta').attr('checked', 'checked');
        $('#radioFactura').removeAttr('checked');

        $('.contenedor-factura').fadeOut();

        $('input[name="rut_factura"]').removeAttr('data-valid');
        $('input[name="razon_social"]').removeAttr('data-valid');
        $('input[name="rut_factura"]').removeAttr('data-valid');
        $('select[name="region_factura"]').removeAttr('data-valid');
        $('select[name="comuna_factura"]').removeAttr('data-valid');
        $('input[name="direccion_factura"]').removeAttr('data-valid');
        $('input[name="giro"]').removeAttr('data-valid');

    } else if (valor == 2) {

        $('#radioFactura').attr('checked', 'checked');
        $('#radioBoleta').removeAttr('checked');

        $('.contenedor-factura').fadeIn();

        $('input[name="rut_factura"]').attr('data-valid', 'true');
        $('input[name="razon_social"]').attr('data-valid', 'true');
        $('input[name="rut_factura"]').attr('data-valid', 'true');
        $('select[name="region_factura"]').attr('data-valid', 'true');
        $('select[name="comuna_factura"]').attr('data-valid', 'true');
        $('input[name="direccion_factura"]').attr('data-valid', 'true');
        $('input[name="giro"]').attr('data-valid', 'true');

    }
})

$('#formCrearCuenta').on('submit', function () {

    var nombre = $(this).find('input[name="nombre"]').val(),
        email = $(this).find('input[name="email"]').val(),
        telefono = $(this).find('input[name="telefono"]').val(),
        password = $(this).find('input[name="password"]').val(),
        repassword = $(this).find('input[name="re-password"]').val();

    var terminos = $(this).find('#chk_terminos');

    if (nombre.trim() != '' && email.trim() != '' && telefono.trim() != '' && password.trim() != '' && repassword.trim() != '') {

        var re = /^([0-9]){8,12}$/;

        if (validarEmail(email)) {
            if (re.test(telefono)) {

                if (terminos.is(':checked')) {
                    if (password.trim() === repassword.trim()) {
                        return true;
                    } else {
                        swal('', 'Contraseñas ingresadas no son iguales', 'error');
                    }
                } else {
                    swal('', 'Debes aceptar los términos y condiciones', 'error');
                }

            } else {
                swal('', 'Teléfono ingresado incorrecto', 'error');
            }
        } else {
            swal('', 'Correo ingresado incorrecto', 'error');
        }

    } else {
        swal('', 'Debes completar todos los campos obligatorios', 'error');
    }

    return false;
})

$('.change_password').on('click', function () {
    $('.bg_password').fadeIn();
    $('.content_password').fadeIn();
})

$('.bg_password, .close_password, .btnAceptarPopup, .bg_dir').on('click', function () {
    $('.bg_password').fadeOut();
    $('.bg_dir').fadeOut();
    $('.content_password').fadeOut();
    $('.pop_success').fadeOut();
})

$('.btnUpdatePasswordPost').on('click', function () {

    var actual = $('input[name="actual_password"]').val(),
        nueva = $('input[name="nueva_password"]').val(),
        nueva_re = $('input[name="r_nueva_password"]').val();

    if (actual.trim() != '' && nueva.trim() != '' && nueva_re.trim() != '') {

        if (nueva.trim() == nueva_re.trim()) {

            $.ajax({
                url: 'ajax/cambiarPassword.php',
                type: 'post',
                data: {
                    actual: actual,
                    nueva: nueva,
                    nueva_re: nueva_re
                }
            }).done(function (res) {

                if (res == 1) {

                    // $('.bg_password').fadeOut('fast');
                    $('.content_password').fadeOut('fast', function () {
                        $('.pop_success').fadeIn();
                        $('.content_password input').val('');
                    })

                } else if (res == 2) { // Clave actual mala
                    swal('', 'Constraseña actual ingresada no coincide', 'error');
                } else if (res == 3) { // Nuevas no coinciden
                    swal('', 'Las nuevas contraseñas no coinciden', 'error');
                } else if (res == 4) { // No logueado
                    swal('', 'Usuario no logueado', 'error');
                } else if (res == 0) { // Error update
                    swal('', 'Hubo un error al actualizar la contraseña', 'error');
                }

            }).fail(function (res) {
                console.log(res);
            })

        } else {
            swal('', 'Las contraseñas no coinciden', 'error');
        }

    } else {
        swal('', 'Debes completar todos los campos', 'error');
    }

})

$('.ver_detalles').on('click', function () {
    var ts = $(this);
    $(this).parent().find('.content-detalles').slideToggle(400, function () {
        ts.toggleClass('ver_detalles_open');
    });
})

$('.tipo_moneda .moneda').on('click', function () {
    var action = $(this).attr('data-action');

    $.ajax({
            url: 'ajax/cambiarMoneda.php',
            type: 'post',
            data: {
                action: action
            },
        })
        .done(function (res) {
            location.reload();
        })
        .fail(function (res) {
            console.log(res);
        });

})

$('.notloginFav').on('click', function () {
    swal('', 'Debes iniciar sesión para ver tus productos favoritos', 'error');
})

$('.egDash').on('click', function () {
    var producto_id = $(this).attr('data-id');

    $.ajax({
        url: 'ajax/eliminarGuardado.php',
        type: 'post',
        data: {
            producto_id: producto_id
        },
        beforeSend: function () {
            $(".cont_loading").fadeIn(200);
        }
    }).done(function (res) {
        res = parseInt(res);

        if (res == 1) {
            location.reload();
        } else {
            swal('', 'Error al eliminar el producto de tus favoritos.', 'error');
        }

        $(".cont_loading").fadeOut(200);
    }).fail(function (res) {
        $(".cont_loading").fadeOut(200);
    })

})

$('.egDash').on('click', function () {
    var producto_id = $(this).attr('data-id');

    $.ajax({
        url: 'ajax/eliminarGuardado.php',
        type: 'post',
        data: {
            producto_id: producto_id
        },
        beforeSend: function () {
            $(".cont_loading").fadeIn(200);
        }
    }).done(function (res) {
        res = parseInt(res);

        if (res == 1) {
            location.reload();
        } else {
            swal('', 'Error al eliminar el producto de tus favoritos.', 'error');
        }

        $(".cont_loading").fadeOut(200);
    }).fail(function (res) {
        $(".cont_loading").fadeOut(200);
    })

})

function egCart(ts) {

    var producto_id = $(ts).attr('data-id');

    $.ajax({
        url: 'ajax/eliminarGuardado.php',
        type: 'post',
        data: {
            producto_id: producto_id
        },
        beforeSend: function () {
            $(".cont_loading").fadeIn(200);
        }
    }).done(function (res) {
        res = parseInt(res);

        if (res == 1) {
            $('#contProductosGuardados').load(' .load_ajx_fav');
        } else {
            swal('', 'Error al eliminar el producto de tus favoritos.', 'error');
        }

        $(".cont_loading").fadeOut(200);
    }).fail(function (res) {
        $(".cont_loading").fadeOut(200);
    })

}

// $('.btnMoverAlCarro').on('click', function() {

//     var id = $(this).attr('data-id');

//     $('.content_popup_prices').fadeIn();
//     $('.bg_prices').fadeIn();

//     $('.content_popup_prices .load_prices').load('ajax/listasDePrecio.php?producto_id=' + id + '&where=dashboard');
// })

$('.edicion-tab .tab').on('click', function () {

    var data_action = $(this).attr('data-action');
    var idTab = $(this).attr('data-id');
    //stock = $(this).attr('data-st');
    $('.edicion-tab .tab').removeClass("activado");
    $(this).addClass("activado");

    $(".content_tab").removeClass("activado");
    $("#tab-" + idTab).addClass("activado");

})

$('.bg_prices').on('click', function () {
    $('.content_popup_prices').fadeOut();
    $('.bg_prices').fadeOut();
})

$('#formContacto').on('submit', function () {

    var nombre = $(this).find('input[name="nombre"]').val(),
        email = $(this).find('input[name="email"]').val(),
        mensaje = $(this).find('textarea[name="mensaje"]').val();

    if (nombre.trim() != '' && email.trim() != '' && mensaje.trim() != '') {

        if (validarEmail(email.trim())) {
            $(".cont_loading").fadeIn(200);
            return true;
        } else {
            swal('', 'Correo ingresado no es válido', 'error');
        }

    } else {
        swal('', 'Debes completar todos los campos', 'error');
    }

    return false;

})

$('.crearCuentaCarro').on('click', function () {
    var content = $('.content_popup_carro .center'),
        nombre = $(content).find('input[name="nombre"]').val()
    email = $(content).find('input[name="email"]').val(),
        telefono = $(content).find('input[name="telefono"]').val(),
        password = $(content).find('input[name="password"]').val(),
        repassword = $(content).find('input[name="re-password"]').val(),
        terminos = $(content).find('input[name="terminos"]');


    if (nombre.trim() != '' && email.trim() != '' && telefono.trim() != '' && password.trim() != '' && repassword.trim() != '') {

        var re = /^([0-9]){8,12}$/;

        if (validarEmail(email)) {
            if (re.test(telefono)) {

                if (terminos.is(':checked')) {
                    if (password.trim() === repassword.trim()) {
                        $.ajax({
                            url: 'ajax/cuentaDesdeCarro.php',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                nombre: nombre,
                                email: email,
                                telefono: telefono,
                                password: password,
                                repassword: repassword
                            },
                            beforeSend: function () {
                                $(".cont_loading").fadeIn(200);
                            }
                        }).done(function (res) {

                            var status = res['status'],
                                message = res['message'];

                            if (res['status'] == 'success') {
                                $('.bg_popup_carro').fadeOut();
                                $('.content_popup_carro').fadeOut();

                                $(content).find('input[type="text"]').val('');
                                $(content).find('input[type="email"]').val('');
                                $(content).find('input[type="password"]').val('');

                                $(content).find('input[type="checkbox"]').removeAttr('checked');

                                swal('', message, status);

                            } else {
                                swal('', message, status);
                            }

                            $(".cont_loading").fadeOut(200);

                        }).fail(function (res) {
                            $(".cont_loading").fadeOut(200);
                            console.log(res);
                        })
                    } else {
                        swal('', 'Contraseñas ingresadas no son iguales', 'error');
                    }
                } else {
                    swal('', 'Debes aceptar los términos y condiciones', 'error');
                }

            } else {
                swal('', 'Teléfono ingresado incorrecto', 'error');
            }
        } else {
            swal('', 'Correo ingresado incorrecto', 'error');
        }

    } else {
        swal('', 'Debes completar todos los campos', 'error');
    }

})

$('.fadeCreate').on('click', function () {
    $('.bg_popup_carro').fadeIn();
    $('.content_popup_carro').fadeIn();
})

$('.bg_popup_carro, .closePopCarro').on('click', function () {
    $('.bg_popup_carro').fadeOut();
    $('.content_popup_carro').fadeOut();
    $('.contentPopPassword').fadeOut();
})

$('.fadeOutPop a, .closePopCarroPass').on('click', function () {
    $('.bg_popup_carro').fadeOut();
    $('.contentPopPassword').fadeOut();
})

$('.recuperar_password').on('click', function () {
    $('.bg_popup_carro').fadeIn();
    $('.contentPopPassword').fadeIn();
})

$('.btnRestablecer').on('click', function () {
    var content = $('.contentPopPassword'),
        email = $(content).find('input[name="email"]').val();

    if (email.trim() != '') {
        if (validarEmail(email.trim())) {
            $.ajax({
                url: 'ajax/restablecerPassword.php',
                type: 'post',
                dataType: 'json',
                data: {
                    email: email
                },
                beforeSend: function () {
                    $(".cont_loading").fadeIn(200);
                }
            }).done(function (res) {
                var status = res['status'],
                    message = res['message'];

                console.log(status);
                console.log(message);
                if (status == 'success') {
                    swal('', message, status);
                    $(content).find('input[name="email"]').val('');
                    $('.bg_popup_carro').fadeOut();
                    $('.contentPopPassword').fadeOut();

                } else {
                    swal('', message, status);
                }

                $(".cont_loading").fadeOut(200);
            }).fail(function (res) {
                console.log(res);
                $(".cont_loading").fadeOut(200);
            })
        }
    } else {
        swal('', 'Debes ingresar un correo', 'error');
    }
})

$('select[name="pais"]').on('change', function () {
    var valor = $(this).val();

    if (valor == 1) {
        $('.texto_aviso').fadeOut(200, function () {
            $('.content_opdespacho').fadeIn();
        });

        $.ajax({
            url: 'ajax/llenarSelect.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'regiones'
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var select = $('.content_opdespacho').find('select[name="region"]');

            select.html('<option value="0">Región</option>');
            for (var i = 0; i < res.length; i++) {
                select.append('<option value="' + res[i]['id'] + '">' + res[i]['nombre'] + '</option>');
            }

            $.ajax({
                url: 'ajax/sesionPais.php',
                type: 'post',
                data: {
                    action: 'delete'
                }
            }).done(function (res) {
                $('.ajx_metodos').load(" .grid-fp");
            }).fail(function (res) {
                console.log(res);
            })

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })

    } else if (valor > 1) {
        $('.content_opdespacho').fadeOut(200, function () {
            $('.texto_aviso').fadeIn();
        });

        $.ajax({
            url: 'ajax/sesionPais.php',
            type: 'post',
            data: {
                action: 'create'
            }
        }).done(function (res) {
            $('.ajx_metodos').load(" .grid-fp");
        }).fail(function (res) {
            console.log(res);
        })

    } else if (valor == 0) {
        $('.texto_aviso').fadeOut(200, function () {
            $('.content_opdespacho').fadeIn();
        });

        $('.regionesDir').html('<option value="0">Región</option>');
        $('.comunasDir').html('<option value="0">Comuna</option>');

        $.ajax({
            url: 'ajax/sesionPais.php',
            type: 'post',
            data: {
                action: 'delete'
            }
        }).done(function (res) {
            $('.ajx_metodos').load(" .grid-fp");
        }).fail(function (res) {
            console.log(res);
        })
    }
})

$('.regionesDir').on('change', function () {
    var id = $(this).val();

    if (id > 0) {
        $.ajax({
            url: 'ajax/llenarSelect.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'comunas',
                region_id: id
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var comunas = $('.comunasDir');

            comunas.html('<option value="0">Comuna</option>');
            for (var i = 0; i < res.length; i++) {
                comunas.append('<option value="' + res[i]['id'] + '">' + res[i]['nombre'] + '</option>');
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })
    } else {
        $('.comunasDir').html('<option value="0">Comuna</option>')
    }

})

$('.comunasDir').on('change', function () {

    var id = $(this).val();
    console.log(id);
    $('.cajaTotalesCarro').load('ajax/cambiarValoresCarro.php?comuna_id=' + id);

})

$('#formCompra').on('submit', function () {

    var nombre = $(this).find('input[name="nombre"]').val(),
        email = $(this).find('input[name="email"]').val(),
        telefono = $(this).find('input[name="telefono"]').val();

    var documento = parseInt($(this).find('input[name="tipoDocumento"]:checked').val()),
        envio = parseInt($(this).find('input[name="tipoEnvio"]:checked').val());

    var terminos = $('#chk_terminos');

    if (nombre.trim() != '' && email.trim() != '' && telefono.trim() != '') {

        if (documento == 1) {

            /* CONDICIONES BOLETA */
            if (validarEmail(email.trim())) {

                var re = /^([0-9]){8,12}$/;

                if (re.test(telefono)) {

                    if (envio == 2) {
                        /* CONDICIONES DESPACHO */
                        var pais = parseInt($(this).find('select[name="pais"]').val()),
                            region = parseInt($(this).find('select[name="region"]').val()),
                            comuna = parseInt($(this).find('select[name="comuna"]').val()),
                            direccion = $(this).find('input[name="direccion"]').val();

                        if (pais != 0 && pais == 1) {

                            /* CONDICIONES PARA CHILE */
                            if (region != 0 && comuna != 0 && direccion.trim() != '') {
                                if (terminos.is(':checked')) {

                                    return true;

                                } else {
                                    swal('', 'Debes aceptar los términos y condiciones.', 'error');
                                }
                            } else {
                                swal('', 'Debes completar todos los campos requeridos.', 'error');
                            }
                            /* FIN CONDICIONES PARA CHILE */

                        } else if (pais != 0 && pais > 1) {

                            /* CUALQUIER PAIS MENOS CHILE */
                            if (direccion.trim() != '') {
                                if (terminos.is(':checked')) {

                                    return true;

                                } else {
                                    swal('', 'Debes aceptar los términos y condiciones.', 'error');
                                }
                            } else {
                                swal('', 'Debes completar todos los campos requeridos.', 'error');
                            }
                            /* FIN CONDICIONES PAISES */

                        } else {
                            swal('', 'Debes completar todos los campos requeridos.', 'error');
                        }
                        /* FIN CONDICIONES DESPACHO */
                    } else {
                        /* CONDICIONES RETIRO EN TIENDA */
                        var sucursal = parseInt($(this).find('select[name="sucursal"]').val()),
                            nombreRetiro = $(this).find('input[name="nombre_retiro"]').val(),
                            rutRetiro = $(this).find('input[name="rut_retiro"]').val();

                        if (sucursal != 0 && nombreRetiro.trim() != '' && rutRetiro.trim() != '') {

                            if (terminos.is(':checked')) {

                                return true;

                            } else {
                                swal('', 'Debes aceptar los términos y condiciones.', 'error');
                            }

                        } else {
                            swal('', 'Debes completar todos los campos requeridos.', 'error');
                        }

                        /* FIN CONDICIONES RETIRO EN TIENDA */
                    }

                } else {
                    swal('', 'Teléfono ingresado no es válido', 'error');
                }

            } else {
                swal('', 'Email ingresado no es válido', 'error');
            }
            /* FIN CONDICIONES COMPRA CON BOLETA */

        } else {

            /* CONDICIONES FACTURA */
            var rutFactura = $(this).find('input[name="rut_factura"]').val(),
                razonSocial = $(this).find('input[name="razon_social"]').val(),
                regionFactura = parseInt($(this).find('select[name="region_factura"]').val()),
                comunaFactura = parseInt($(this).find('select[name="comuna_factura"]').val()),
                direccionFactura = $(this).find('input[name="direccion_factura"]').val(),
                giroFactura = $(this).find('input[name="giro"]').val();

            if (rutFactura.trim() != '' && razonSocial.trim() != '' && regionFactura != 0 && comunaFactura != 0 && direccionFactura.trim() != '' && giroFactura.trim()) {

                if (validarEmail(email.trim())) {
                    var re = /^([0-9]){8,12}$/;

                    if (re.test(telefono)) {

                        if (envio == 2) {
                            /* CONDICIONES DESPACHO */
                            var pais = parseInt($(this).find('select[name="pais"]').val()),
                                region = parseInt($(this).find('select[name="region"]').val()),
                                comuna = parseInt($(this).find('select[name="comuna"]').val()),
                                direccion = $(this).find('input[name="direccion"]').val();

                            if (pais != 0 && pais == 1) {

                                /* CONDICIONES PARA CHILE */
                                if (region != 0 && comuna != 0 && direccion.trim() != '') {
                                    if (terminos.is(':checked')) {

                                        return true;

                                    } else {
                                        swal('', 'Debes aceptar los términos y condiciones.', 'error');
                                    }
                                } else {
                                    swal('', 'Debes completar todos los campos requeridos.', 'error');
                                }
                                /* FIN CONDICIONES PARA CHILE */

                            } else if (pais != 0 && pais > 1) {

                                /* CUALQUIER PAIS MENOS CHILE */
                                if (direccion.trim() != '') {
                                    if (terminos.is(':checked')) {

                                        return true;

                                    } else {
                                        swal('', 'Debes aceptar los términos y condiciones.', 'error');
                                    }
                                } else {
                                    swal('', 'Debes completar todos los campos requeridos.', 'error');
                                }
                                /* FIN CONDICIONES PAISES */

                            } else {
                                swal('', 'Debes completar todos los campos requeridos.', 'error');
                            }
                            /* FIN CONDICIONES DESPACHO */
                        } else {
                            /* CONDICIONES RETIRO EN TIENDA */
                            var sucursal = parseInt($(this).find('select[name="sucursal"]').val()),
                                nombreRetiro = $(this).find('input[name="nombre_retiro"]').val(),
                                rutRetiro = $(this).find('input[name="rut_retiro"]').val();

                            if (sucursal != 0 && nombreRetiro.trim() != '' && rutRetiro.trim() != '') {

                                if (terminos.is(':checked')) {

                                    return true;

                                } else {
                                    swal('', 'Debes aceptar los términos y condiciones.', 'error');
                                }

                            } else {
                                swal('', 'Debes completar todos los campos requeridos.', 'error');
                            }

                            /* FIN CONDICIONES RETIRO EN TIENDA */
                        }

                    } else {
                        swal('', 'Teléfono ingresado no es válido', 'error');
                    }

                } else {
                    swal('', 'Email ingresado no es válido', 'error');
                }

            } else {

                swal('', 'Debes completar todos los campos requeridos.', 'error');

            }
            /* FIN CONDICIONES FACTURA */

        }

    } else {
        swal('', 'Debes completar todos los campos requeridos.', 'error');
    }

    $('.input-text[data-valid="true"]').each(function (i, x) {
        var valorInput = $(this).val();

        if (valorInput.trim() == '') {
            $(this).addClass('redBorder');
        }

    })

    $('.select-form[data-valid="true"]').each(function (i, x) {
        var valorInputSelect = $(this).val();

        if (valorInputSelect == 0) {
            $(this).addClass('redBorder');
        }

    })

    return false;

})

$('input[name="tipoEnvio"]').on('change', function () {
    var valor = parseInt($(this).val());

    if (valor == 1) {
        $('.content_despacho').fadeOut(200, function () {
            $('.content_retiro').fadeIn(200);
            $('.cajaTotalesCarro').load('ajax/cambiarValoresCarro.php?comuna_id=0');
        })

        $('select[name="sucursal"]').attr('data-valid', 'true');
        $('input[name="nombre_retiro"]').attr('data-valid', 'true');
        $('input[name="rut_retiro"]').attr('data-valid', 'true');

        $('select[name="pais"]').removeAttr('data-valid', 'true');
        $('select[name="region"]').removeAttr('data-valid', 'true');
        $('select[name="comuna"]').removeAttr('data-valid', 'true');
        $('input[name="direccion"]').removeAttr('data-valid', 'true');

    } else {
        var comuna = $('.comunasDir').val();
        $('.content_retiro').fadeOut(200, function () {
            $('.content_despacho').fadeIn(200);
            $('.cajaTotalesCarro').load('ajax/cambiarValoresCarro.php?comuna_id=' + comuna);
        })

        $('select[name="sucursal"]').removeAttr('data-valid', 'true');
        $('input[name="nombre_retiro"]').removeAttr('data-valid', 'true');
        $('input[name="rut_retiro"]').removeAttr('data-valid', 'true');

        $('select[name="pais"]').attr('data-valid', 'true');
        $('select[name="region"]').attr('data-valid', 'true');
        $('select[name="comuna"]').attr('data-valid', 'true');
        $('input[name="direccion"]').attr('data-valid', 'true');

    }

})

$('.slPaisDash').on('change', function () {
    var valor = $(this).val();

    if (valor == 1) {
        $('.content_dirDash').fadeIn();

        $.ajax({
            url: 'ajax/llenarSelect.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'regiones'
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var select = $('.content_dirDash').find('select[name="region"]');

            select.html('<option value="0">Región</option>');
            for (var i = 0; i < res.length; i++) {
                select.append('<option value="' + res[i]['id'] + '">' + res[i]['nombre'] + '</option>');
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })

    } else if (valor > 1) {
        $('.content_dirDash').fadeOut();
        $('.regionesDash').html('<option value="0">Región</option>');
        $('.comunasDash').html('<option value="0">Comuna</option>');
    } else if (valor == 0) {
        $('.content_opdespacho').fadeIn();

        $('.regionesDash').html('<option value="0">Región</option>');
        $('.comunasDash').html('<option value="0">Comuna</option>');
    }
})

$('.paisDirPop').on('change', function () {
    var valor = $(this).val();

    if (valor == 1) {
        $('.pop_content').fadeIn();

        $.ajax({
            url: 'ajax/llenarSelect.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'regiones'
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var select = $('.pop_content').find('select[name="region_direccion"]');

            select.html('<option value="0">Región</option>');
            for (var i = 0; i < res.length; i++) {
                select.append('<option value="' + res[i]['id'] + '">' + res[i]['nombre'] + '</option>');
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })

    } else if (valor > 1) {
        $('.pop_content').fadeOut();
        $('.pop_content').find('select[name="region_direccion"]').html('<option value="0">Región</option>');
        $('.pop_content').find('select[name="comuna_direccion"]').html('<option value="0">Comuna</option>');
    } else if (valor == 0) {
        $('.content_opdespacho').fadeIn();

        $('.pop_content').find('select[name="region_direccion"]').html('<option value="0">Región</option>');
        $('.pop_content').find('select[name="comuna_direccion"]').html('<option value="0">Comuna</option>');
    }
})

$('.regionesDash').on('change', function () {
    var id = $(this).val();

    if (id > 0) {
        $.ajax({
            url: 'ajax/llenarSelect.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'comunas',
                region_id: id
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var comunas = $('.comunasDash');

            comunas.html('<option value="0">Comuna</option>');
            for (var i = 0; i < res.length; i++) {
                comunas.append('<option value="' + res[i]['id'] + '">' + res[i]['nombre'] + '</option>');
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })
    } else {
        $('.comunasDash').html('<option value="0">Comuna</option>');
    }
})

$('select[name="region_factura"]').on('change', function () {
    var id = $(this).val();

    if (id > 0) {
        $.ajax({
            url: 'ajax/llenarSelect.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'comunas',
                region_id: id
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var comunas = $('select[name="comuna_factura"]');

            comunas.html('<option value="0">Comuna</option>');
            for (var i = 0; i < res.length; i++) {
                comunas.append('<option value="' + res[i]['id'] + '">' + res[i]['nombre'] + '</option>');
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })
    } else {
        $('select[name="comuna_factura"]').html('<option value="0">Comuna</option>');
    }
})

$('.content_dirCarro select[name="region_direccion"]').on('change', function () {
    var id = $(this).val();

    if (id > 0) {
        $.ajax({
            url: 'ajax/llenarSelect.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'comunas',
                region_id: id
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var comunas = $('.pop_content').find('select[name="comuna_direccion"]');

            comunas.html('<option value="0">Comuna</option>');
            for (var i = 0; i < res.length; i++) {
                comunas.append('<option value="' + res[i]['id'] + '">' + res[i]['nombre'] + '</option>');
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })
    } else {
        $('.pop_content').find('select[name="comuna_direccion"]').html('<option value="0">Comuna</option>');
    }
})

$('.agregarDireccionRe').on('click', function () {

    var nombre = $('.nombreDir').val(),
        pais = $('.slPaisDash').val(),
        region = $('.regionesDash').val(),
        comuna = $('.comunasDash').val(),
        direccion = $('.dirDash').val(),
        action = $('.actionDir').val();

    var link = (action == 'add') ? 'ajax/agregarDireccion.php' : 'ajax/editarDireccion.php';

    var dirId = $('.dirId').val();

    var error = false;

    if (pais > 1) {
        if (nombre.trim() == '' || direccion.trim() == '') {
            error = true;
        }
    } else if (pais == 1) {
        if (nombre.trim() == '' || pais == 0 || region == 0 || comuna == 0 || direccion.trim() == '') {
            error = true
        }
    } else {
        error = true;
    }

    if (!error) {
        $.ajax({
            url: link,
            type: 'post',
            dataType: 'json',
            data: {
                nombre: nombre,
                pais: pais,
                region: region,
                comuna: comuna,
                direccion: direccion,
                dirId: dirId
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var status = res['status'],
                message = res['message'];

            if (status == 'success') {
                $('.bg_dir').fadeIn();
                $('.pop_success').fadeIn();

                if (action == 'add') {
                    $('.nombreDir').val('');
                    $('.dirDash').val('');
                    $('.slPaisDash').val(0);
                    $('.regionesDash').html('<option value="0">Región</option>');
                    $('.comunasDash').html('<option value="0">Comuna</option>');
                }

            } else {
                swal('', message, status);
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })
    } else {

        swal('', 'Debes completar todos los campos', 'error');

    }

})

$('.eliminarDireccionDir').on('click', function () {

    var id = $(this).attr('data-id');

    $.ajax({
        url: 'ajax/eliminaDireccion.php',
        type: 'post',
        data: {
            id: id
        },
        beforeSend: function () {
            $(".cont_loading").fadeIn(200);
        }
    }).done(function (res) {

        if (res == 1) {
            location.reload();
        } else {
            swal('', 'Error al eliminar la dirección', 'error');
        }
        $(".cont_loading").fadeOut(200);

    }).fail(function (res) {

        $(".cont_loading").fadeOut(200);
        console.log(res);

    })

})

function addDirCarro() {
    $('.bg_dirCarro').fadeIn();
    $('.content_dirCarro').fadeIn();
}

$('.bg_dirCarro, .content_dirCarro .close_password').on('click', function () {
    $('.bg_dirCarro').fadeOut();
    $('.content_dirCarro').fadeOut();
})

$('.btnCrearDirCarro').on('click', function () {

    var nombre = $('.content_dirCarro input[name="nombre_direccion"]').val(),
        pais = $('.content_dirCarro select[name="pais_direccion"]').val(),
        region = $('.content_dirCarro select[name="region_direccion"]').val(),
        comuna = $('.content_dirCarro select[name="comuna_direccion"]').val(),
        direccion = $('.content_dirCarro input[name="dir_direccion"]').val();

    var error = false;

    if (pais > 1) {
        if (nombre.trim() == '' || direccion.trim() == '') {
            error = true;
        }
    } else if (pais == 1) {
        if (nombre.trim() == '' || pais == 0 || region == 0 || comuna == 0 || direccion.trim() == '') {
            error = true
        }
    } else {
        error = true;
    }

    if (!error) {
        $.ajax({
            url: 'ajax/agregarDireccion.php',
            type: 'post',
            dataType: 'json',
            data: {
                nombre: nombre,
                pais: pais,
                region: region,
                comuna: comuna,
                direccion: direccion
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var status = res['status'],
                message = res['message'];

            if (status == 'success') {
                $('.bg_dir').fadeIn();
                $('.pop_success').fadeIn();

                $('.content_dirCarro').fadeOut();
                $('.bg_dirCarro').fadeOut();

                $('.ajx_direcciones').load(' .content_direccionesCarro');

                $('.content_dirCarro input[name="nombre_direccion"]').val('');
                $('.content_dirCarro input[name="dir_direccion"]').val('');
                $('.content_dirCarro select[name="pais_direccion"]').val(0);
                $('.content_dirCarro select[name="region_direccion"]').html('<option value="0">Región</option>');
                $('.content_dirCarro select[name="comuna_direccion"]').html('<option value="0">Comuna</option>');

            } else {
                swal('', message, status);
            }

            $(".cont_loading").fadeOut(200);

        }).fail(function (res) {
            $(".cont_loading").fadeOut(200);
            console.log(res);
        })
    } else {

        swal('', 'Debes completar todos los campos', 'error');

    }

})

function moveCart(ts) {
    var id = $(ts).attr('data-id');

    $('.content_popup_prices').fadeIn();
    $('.bg_prices').fadeIn();

    $('.content_popup_prices .load_prices').load('ajax/listasDePrecio.php?producto_id=' + id + '&where=carro');
}

function moveToCart(ts) {
    var productoSeleccionado = $(ts).attr('data-id'),
        listaSeleccionada = $('.radio_ediciones:checked').val(),
        cantidad = 1;

    $.ajax({
            url: 'ajax/addToCart.php',
            type: 'post',
            dataType: 'json',
            data: {
                producto: productoSeleccionado,
                lista: listaSeleccionada,
                cantidad: cantidad
            },
        })
        .done(function (res) {
            if (res['status'] == 'success') {

                $.ajax({
                    url: 'ajax/eliminarGuardado.php',
                    type: 'post',
                    data: {
                        producto_id: productoSeleccionado
                    }
                }).done(function (res) {
                    res = parseInt(res);

                    if (res == 1) {
                        $('.grid_pop').load('ajax/loadPopUp.php?id=' + productoSeleccionado + '&lista=' + listaSeleccionada + '&cantidad=' + cantidad);
                        $('.bgpopcart').fadeIn();
                        $('.pop-cart').fadeIn();

                        $('.carro_i').load(' .cantidad_carro');

                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);

                        $('.content_popup_prices').fadeOut();
                        $('.bg_prices').fadeOut();
                    }

                }).fail(function (res) {
                    console.log();
                })

            } else {
                swal('', res['message'], 'error');
            }
        })
        .fail(function (res) {
            console.log(res);
        });
}

function moveToCartLista(ts) {
    var productoSeleccionado = $(ts).attr('data-id'),
        listaSeleccionada = $('.radio_ediciones:checked').val(),
        cantidad = 1;

    $.ajax({
            url: 'ajax/addToCart.php',
            type: 'post',
            dataType: 'json',
            data: {
                producto: productoSeleccionado,
                lista: listaSeleccionada,
                cantidad: cantidad
            },
        })
        .done(function (res) {
            if (res['status'] == 'success') {

                $.ajax({
                    url: 'ajax/eliminarGuardado.php',
                    type: 'post',
                    data: {
                        producto_id: productoSeleccionado
                    }
                }).done(function (res) {
                    res = parseInt(res);
                    console.log(res);
                    if (res == 1) {
                        $('.content_popup_prices').fadeOut();
                        $('.bg_prices').fadeOut();

                        $('#contProductosGuardados').load(' .load_ajx_fav');
                        $('.ajx_carro').load(' #contFilasCarro');
                        $('#contValoresResumen').load(' .contentRes');
                    }

                }).fail(function (res) {
                    console.log();
                })

            } else {
                swal('', res['message'], 'error');
            }
        })
        .fail(function (res) {
            console.log(res);
        });
}

function addToFav(dataId, e, ts) {
    e.preventDefault();

    $.ajax({
        url: 'ajax/agregarFavorito.php',
        type: 'post',
        data: {
            producto_id: dataId
        },
        beforeSend: function () {
            $(".cont_loading").fadeIn(200);
        }
    }).done(function (res) {
        res = parseInt(res);

        if (res == 2) { // Usuario no logueado
            swal('', 'Debes iniciar sesión para agregar el producto a favoritos.', 'error');
        } else if (res == 1) {
            swal('', 'Producto agregado a favoritos.', 'success');
            $(ts).html('<i class="material-icons">favorite</i>');
        } else if (res == 0) {
            swal('', 'Error al agregar el producto a favoritos.', 'error');
        } else if (res == 3) {
            swal('', 'El producto ya está en tus favoritos.', 'warning');
        }


        $(".cont_loading").fadeOut(200);

    }).fail(function (res) {
        $(".cont_loading").fadeOut(200);
    })
}

function addToFavFicha(dataId, e, ts) {
    e.preventDefault();

    $.ajax({
        url: 'ajax/agregarFavorito.php',
        type: 'post',
        data: {
            producto_id: dataId
        },
        beforeSend: function () {
            $(".cont_loading").fadeIn(200);
        }
    }).done(function (res) {
        res = parseInt(res);

        if (res == 2) { // Usuario no logueado
            swal('', 'Debes iniciar sesión para agregar el producto a favoritos.', 'error');
        } else if (res == 1) {
            swal('', 'Producto agregado a favoritos.', 'success');
            $(ts).html('<img src="img/heartcheck.png">');
        } else if (res == 0) {
            swal('', 'Error al agregar el producto a favoritos.', 'error');
        } else if (res == 3) {
            swal('', 'El producto ya está en tus favoritos.', 'warning');
        }


        $(".cont_loading").fadeOut(200);

    }).fail(function (res) {
        $(".cont_loading").fadeOut(200);
    })
}

function moveProdFav(ts) {
    var dataId = $(ts).attr('data-id');

    $.ajax({
        url: 'ajax/agregarFavorito.php',
        type: 'post',
        data: {
            producto_id: dataId
        },
        beforeSend: function () {
            $(".cont_loading").fadeIn(200);
        }
    }).done(function (res) {
        res = parseInt(res);

        if (res == 2) { // Usuario no logueado
            swal('', 'Debes iniciar sesión para agregar el producto a favoritos.', 'error');
        } else if (res == 1) {

            var lista = $(ts).attr('data-list');

            $.ajax({
                url: 'ajax/cartActions.php',
                type: 'post',
                dataType: 'json',
                data: {
                    id: dataId,
                    lista: lista,
                    action: 'delete'
                }
            }).done(function (res) {

                console.log(res);

                if (res['status'] == 'success') {
                    $('.ajx_carro').load(' #contFilasCarro');
                    $('#contValoresResumen').load(' .contentRes');
                    $('#contProductosGuardados').load(' .load_ajx_fav');

                    swal('', 'Producto agregado a favoritos.', 'success');
                }

            }).fail(function (res) {
                console.log(res);
            })

        } else if (res == 0) {
            swal('', 'Error al agregar el producto a favoritos.', 'error');
        } else if (res == 3) {
            swal('', 'El producto ya está en tus favoritos.', 'warning');
        }


        $(".cont_loading").fadeOut(200);

    }).fail(function (res) {
        $(".cont_loading").fadeOut(200);
    })
}

function justNumbers(e) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 8) || (keynum == 46))
        return true;

    return /\d/.test(String.fromCharCode(keynum));
}

function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

    return amount_parts.join('.');
}

function validarEmail(valor) {
    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (emailRegex.test(valor)) {
        return true;
    } else {
        return false;
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function selectDirecciones(ts) {

    var id = parseInt($(ts).val());

    if (id > 0) {

        $.ajax({
            url: 'ajax/consultarDireccion.php',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            beforeSend: function () {
                $(".cont_loading").fadeIn(200);
            }
        }).done(function (res) {
            var data = res['data'];

            $('input[name="direccion"]').val(data['direccion']);
            $('select[name="pais"]').val(data['pais']);

            if (data['pais'] == 1) {
                var regiones = res['regiones'],
                    comunas = res['comunas'];

                $('.texto_aviso').fadeOut(200, function () {
                    $('.content_opdespacho').fadeIn();
                });

                $('.regionesDir').html('<option value="0">Region</option>');
                for (var i = 0; i < regiones.length; i++) {
                    $('.regionesDir').append('<option value="' + regiones[i]['id'] + '" ' + regiones[i]['selected'] + '>' + regiones[i]['nombre'] + '</option>');
                }

                $('.comunasDir').html('<option value="0">Comuna</option>');
                for (var i = 0; i < comunas.length; i++) {
                    $('.comunasDir').append('<option value="' + comunas[i]['id'] + '" ' + comunas[i]['selected'] + '>' + comunas[i]['nombre'] + '</option>');

                    if (comunas[i]['selected'] != '') {
                        console.log(comunas[i]['id']);
                        $('.cajaTotalesCarro').load('ajax/cambiarValoresCarro.php?comuna_id=' + comunas[i]['id']);
                    }
                }

                $.ajax({
                    url: 'ajax/sesionPais.php',
                    type: 'post',
                    data: {
                        action: 'delete'
                    }
                }).done(function (res) {
                    $('.ajx_metodos').load(" .grid-fp");
                }).fail(function (res) {
                    console.log(res);
                })

            } else if (data['pais'] > 1) {
                $('.content_opdespacho').fadeOut(200, function () {
                    $('.texto_aviso').fadeIn();
                });
                $('.regionesDir').html('<option value="0">Región</option>');
                $('.comunasDir').html('<option value="0">Comuna</option>');

                $('.cajaTotalesCarro').load('ajax/cambiarValoresCarro.php?comuna_id=0');

                $.ajax({
                    url: 'ajax/sesionPais.php',
                    type: 'post',
                    data: {
                        action: 'create'
                    }
                }).done(function (res) {
                    $('.ajx_metodos').load(" .grid-fp");
                }).fail(function (res) {
                    console.log(res);
                })
            }

            $(".cont_loading").fadeOut(200);
        }).fail(function (res) {
            console.log(res);
            $(".cont_loading").fadeOut(200);
        })

    } else {

        $('input[name="direccion"]').val('');
        $('.regionesDir').html('<option value="0">Región</option>');
        $('.comunasDir').html('<option value="0">Comuna</option>');

        $('select[name="pais"]').val(0);

        $('.texto_aviso').fadeOut(200, function () {
            $('.content_opdespacho').fadeIn();
        });

        $('.cajaTotalesCarro').load('ajax/cambiarValoresCarro.php?comuna_id=0');

        $.ajax({
            url: 'ajax/sesionPais.php',
            type: 'post',
            data: {
                action: 'delete'
            }
        }).done(function (res) {
            $('.ajx_metodos').load(" .grid-fp");
        }).fail(function (res) {
            console.log(res);
        })

    }

}

$('.volverAComprar').on('click', function () {
    var productoSeleccionado = parseInt($(this).attr('data-id'));
    var cantidad = parseInt($(this).attr('data-qty'));

    console.log(cantidad);

    // console.log(productoSeleccionado + " ----- " + cantidad);
    $.ajax({
            url: 'ajax/addToCart.php',
            type: 'POST',
            dataType: 'json',
            data: {
                producto: productoSeleccionado,
                cantidad: cantidad
            },
        })
        .done(function (res) {
            if (res['status'] == 'success') {
                $('.grid_pop').load('ajax/loadPopUp.php?id=' + productoSeleccionado + '&cantidad=' + cantidad);
                $('.bgpopcart').fadeIn();
                $('.pop-cart').fadeIn();

                $('.carro_i').load(' .cantidad_carro');

                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            } else {
                swal('', res['message'], 'error');
            }
        })
        .fail(function (res) {
            console.log(res);
        });
})

$('#btnNewsletterHome').on('click', function () {
    var email = $('.email-news').val();

    if (email.trim() != '') {
        if (validarEmail(email)) {
            $.ajax({
                    url: 'ajax/agregarNews.php',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        email: email
                    },
                })
                .done(function (res) {
                    var status = res['status'],
                        message = res['message'],
                        link = location.href + "home?msje=" + message + "&a=1";

                    if (status == 'success') {
                        location.href = link;
                    } else {
                        swal('', message, 'error');
                    }

                })
                .fail(function (res) {
                    console.log(res);
                });

        } else {
            swal('', 'Email ingresado es incorrecto', 'error');
        }
    } else {
        swal('', 'Debes ingresar un correo', 'error');
    }

})

$(".filter-home-xs").on('click', function () {
    $(".cerrar-filtros-home").fadeIn();
    $(".content_nav").fadeIn();
})

$(".cerrar-filtros-home").on("click", function () {
    $(".cerrar-filtros-home").fadeOut();
    $(".content_nav").fadeOut();
});

$(".arrow-footer").on('click', function () {
    $(this).parent().parent().find(".open-list-footer").slideToggle();
})

$(".arrow-nav-xs").on('click', function () {
    let dataOpen = parseInt($(this).attr("data-open"));

    if (!dataOpen) {
        $(this).parent().find(".submenu").slideDown();
        $(this).attr("data-open", 1);
    } else {
        $(this).parent().find(".submenu").slideUp();
        $(this).attr("data-open", 0);
    }

})

$('.button-menu-dash-xs').on('click', function () {
    $('.menuMiCuenta').slideToggle();
})

$('.radio_ediciones').on('change', function () {
    let id = $(this).val();

    $.ajax({
        url: 'ajax/revisionMarco.php',
        type: 'post',
        data: {
            id
        }
    }).done((res) => {
        if (res) {
            $('.content-marcos').removeClass('disnone');
        } else {
            $('.content-marcos').addClass('disnone');
        }
    }).fail((err) => {
        console.error(err);
    })
})