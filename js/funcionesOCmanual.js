// JavaScript Document

$(function(){
	$("#vendedoresInicio").click(function(){
		$("#contPopLoginVendedores").fadeIn(100);
		});
	
	$(".cerrarVendedores").click(function(){
		$("#contPopLoginVendedores").fadeOut(100);
		});
	
	
	
	$("#inicioSesionVendedor").click(function() {
        var usuario = $("#emailVendedores").val();
        var pass = $("#claveVendedor").val();
        console.log(usuario + " - " + pass);
        if (pass != '') {
            console.log("tomo los datos y los envio por ajax");
            $.ajax({
                type: 'POST',
                url: 'tienda/loginVendedor.php',
                data: {
                    correo: usuario,
                    clave: pass
                },
                dataType: 'JSON',
                success: function(data) {
                    if (data == 1) {
                        alertify.success("inicio de sesión exitoso.");
						$("#contPopLoginVendedores").fadeOut(100);
                    } else {
                        alertify.error("usuario o contraseña no coinciden, si no conoce su clave puede solicitar una.");
                    };
                }
            });
        } else {
            alertify.error("Debe ingresar una contraseña válida.");
        };
    });
    
	
	$("a.btnRecuperarClaveVendedor").click(function() {
		console.log("entro en click");
        var usuario = $("#emailVendedores").val();
        $.ajax({
            type: 'POST',
            url: 'tienda/recuperarClaveCompraRapida.php',
            data: {
                correo: usuario
            },
            dataType: 'JSON',
            success: function(data) {
                if (data == 1) {
                    alertify.success("Revisa tu correo, te enviamos una nueva clave.");
                } else if (data == 2) {
                    alertify.error("No fue posible enviarte una nueva clave en estos momentos, intenta más tarde.");
                } else if (data == 3) {
                    alertify.error("El correo ingresado no se encuentra en nuestros registros.")
                };
            }
        });
    });
	
	
	
	$(".btnAceptarVendedor").click(function(){
		var codigo = $(".campoCodigoVendedor").val();
		console.log(codigo);
		if(codigo != ""){
			$("#cerrarCompraVendedor").submit();
			} else {
				alertify.error("Debe llenar el campo código.");
				};
		});
	
	
	
	
	
	
	
	
	
});