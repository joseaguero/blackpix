<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo">tu carro</div>
        <div class="list"><?= qty_pro() ?> productos</div>
    </div>
</div>

<div class="container_carro grid-carro-resumen">
    <?php if (isset($_COOKIE['cart_alfa_cm'])): ?>          
        <div class="contCarro">

            <div class="grid-title_carro mt-30">
                <div class="col"></div>
                <div class="col">Detalles</div>
                <div class="col">Precio unitario</div>
                <div class="col">Cantidad</div>
                <div class="col">Total item</div>
            </div>
            
            <div class="ajx_carro">
                <div id="contFilasCarro">
                    <?= ShowCart2(); ?>
                </div>
            </div>
            
            <?php 

            $cliente_id = (int)$_COOKIE['usuario_id'];

            $productos_guardados = consulta_bd('pd.id, p.id, p.nombre, a.nombre, t.nombre, pd.stock, p.thumbs', 'productos p JOIN productos_detalles pd ON pd.producto_id = p.id JOIN artistas a ON a.id = p.artista_id JOIN tamaños t ON t.id = pd.tamaño_id JOIN productos_guardados pg ON pg.productos_detalle_id = pd.id', "pg.cliente_id = $cliente_id GROUP BY p.id","");

            ?>
            
            <?php if (is_array($productos_guardados)): ?>
                <div id="contProductosGuardados">

                    <div class="load_ajx_fav">

                        <div class="mensajeGuardados">
                            Tienes productos guardados para comprar más tarde. Para comprar haga clic en <span>mover al carro</span>
                        </div>
                        <div class="tituloGuardados">Productos guardados</div>
                        <div class="clearfix"></div>

                        <?php foreach ($productos_guardados as $pg):
                            
                            $thumbProdGuardado = ($pg[6] != null AND $pg[6] != '' AND is_file("imagenes/productos/{$pg[6]}")) ? imagen("imagenes/productos/",$pg[6]) : imagen("img/", "sinFotoGrid.jpg"); ?>

                            <div class="row-productos-guardados">
                                <div class="col">
                                    <a href="ficha/<?=$pg[1]?>/<?=url_amigables($pg[2])?>">
                                        <img src="<?= $thumbProdGuardado ?>">
                                    </a>

                                    <a href="javascript:void(0)" class="eliminarGuardado" onclick="egCart(this)" data-id="<?= $pg[0] ?>">Eliminar</a>
                                </div>
                                <div class="col">
                                    <div class="nombre"><?= $pg[2] ?></div>
                                    <div class="artista"><?= $pg[3] ?></div>
                                    <!-- <div class="medida mt-20"><?= $pg[4] ?></div> -->
                                    
                                    <?php if ($pg[5] > 0): ?>
                                        <!-- <div class="stock">Stock disponible</div> -->
                                    <?php else: ?>
                                        <!-- <div class="stock">Stock no disponible</div> -->
                                    <?php endif ?>
                                    
                                </div>
                                <div class="col">
                                    <a href="javascript:void(0)" class="btnMoverCart" onclick="moveToCartLista(this)"  data-id="<?= $pg[0] ?>">Mover al carro</a>
                                </div>
                            </div>
                            
                        <?php endforeach ?>
                        
                    </div>

                </div>
            <?php endif ?>
            
        </div><!-- fin contCarro-->
        
        <div class="contTotales mt-30">
            <div id="contValoresResumen">
                <?php echo resumenCompra(); ?>
            </div>
            
            
            <!-- <a href="home" class="btnAgregarMasProductos">agregar mas productos</a> -->
            
            <div class="contUltimosVistos">
                <?php echo vistosRecientemente("lista","4"); ?>
            </div>
            
        </div><!-- Fin contTotales-->
    <?php else: ?>
        <div class="nocarro">
            Tu carro está vacío
        </div>
    <?php endif ?>
    
</div>


<?php if($_GET['stock']){ ?>
    <script type="text/javascript">
        swal("","Uno de los productos en el carro, ya no cuenta con stock","warning");
    </script>
<?php } ?>
            


<?php if(qty_pro() > 0){ ?>
<script type="text/javascript">
<?php
    if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}  
	$cart = cartPrepare();

    foreach ($cart as $item) {
        $qty = $item['cantidad'];
        $prd_id = $item['id'];
        
		$filas = consulta_bd('pd.id, pd.sku, pd.nombre, p.nombre, pd.precio, pd.descuento', 'productos p, productos_detalles pd', "pd.producto_id = p.id and pd.id = $item[id]", '');
		
	
		$idProductoInterior = $filas[0][0];
		if($filas[0][5] != '' and $filas[0][5] > 0){
			$valor = $filas[0][5];
		} else {
			$valor = $filas[0][4];
		}
        ?>
        ga('ec:addProduct', {'id': '<?= $filas[0][0]; ?>','name': '<?= $filas[0][2]; ?>','brand': 'no informada','price': '<?= $valor; ?>','quantity': <?= $qty; ?>});
		<?php
    }
?>
</script>
<script type="text/javascript">ga('ec:setAction','checkout', {'step': 1});</script>
<?php } //valido que el carro tenga productos para mostrar el analytics ?>


<div class="fixedResponsive">
	<?php // echo resumenCompra(); ?>
</div>

<div class="clearfix"></div>

<div class="bg_prices"></div>
<div class="content_popup_prices">
    <div class="head_prices">
        <div class="title">Listas de precio</div>
        <div class="subtitle">Debes escoger la opción que deseas <br> agregar al carro</div>
    </div>

    <div class="edicion_bg">
        <div class="edicion-tab">
            <?php 
                $subproductos = consulta_bd("s.id, s.nombre, t.nombre","subproductos s JOIN tamaños t ON t.id = s.tamaño_id","","s.id asc"); 
                for($i=0; $i<sizeof($subproductos ); $i++) {
            ?>
            <div class="tab <?= ($i == 0) ? 'activado' : '' ?>" data-id="<?= $subproductos[$i][0] ?>" data-action="e<?=$num?>" data-st="<?= $edicion[3] ?>"><?= $subproductos[$i][1] ?></div>
            <?php } ?>
        </div>
    </div>
    
    <div class="load_prices"></div>

</div>