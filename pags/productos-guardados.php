<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "inicio-sesion";</script>';
    }

    $cliente_id = (int)$_COOKIE['usuario_id'];

    $productos_guardados = consulta_bd('pd.id, p.id, p.nombre, a.nombre, t.nombre, pd.stock, p.thumbs', 'productos p JOIN productos_detalles pd ON pd.producto_id = p.id JOIN artistas a ON a.id = p.artista_id JOIN tamaños t ON t.id = pd.tamaño_id JOIN productos_guardados pg ON pg.productos_detalle_id = pd.id', "pg.cliente_id = $cliente_id GROUP BY p.id", "");

?>

<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">mi cuenta</div>
    </div>
</div>

<div class="gray-body">
    <div class="container">
        <div class="text_ident">podrás ver tus pedidos, historial de compra y <br>
        editar tus datos personales y de envío
        </div>

        <?php include("pags/menuMiCuenta.php"); ?>

        <div class="cont_bodydash">
            <h3 class="subtitulo">Productos guardados</h3>
            
            <?php if (is_array($productos_guardados)): ?>
                <?php foreach ($productos_guardados as $pg):
                    $thumbProdGuardado = ($pg[6] != null AND $pg[6] != '' AND is_file("imagenes/productos/{$pg[6]}")) ? imagen("imagenes/productos/",$pg[6]) : imagen("img/", "sinFotoGrid.jpg"); ?>

                    <div class="row-productos-guardados">
                        <div class="col">
                            <a href="ficha/<?=$pg[1]?>/<?=url_amigables($pg[2])?>">
                                <img src="<?= $thumbProdGuardado ?>">
                            </a>

                            <a href="javascript:void(0)" class="eliminarGuardado egDash" data-id="<?= $pg[1] ?>">Eliminar</a>
                        </div>
                        <div class="col">
                            <div class="nombre"><?= $pg[2] ?></div>
                            <div class="artista"><?= $pg[3] ?></div>
                            <!-- <div class="medida mt-20"><?= $pg[4] ?></div> -->
                            
                            <?php if ($pg[5] > 0): ?>
                                <!-- <div class="stock">Stock disponible</div> -->
                            <?php else: ?>
                                <!-- <div class="stock">Stock no disponible</div> -->
                            <?php endif ?>
                            
                        </div>
                        <div class="col">
                            <a href="javascript:void(0)" class="btnMoverAlCarro" onclick="moveToCart(this)" data-id="<?= $pg[0] ?>">Mover al carro</a>
                        </div>
                    </div>
                    
                <?php endforeach ?>
            <?php else: ?>
                <p>No tienes productos guardados.</p>
            <?php endif ?>
            
        </div>
        <div class="clearfix"></div>
        <div class="mb-30"></div>
    </div>
</div>

<div class="bg_prices"></div>
<div class="content_popup_prices">
    <div class="head_prices">
        <div class="title">Listas de precio</div>
        <div class="subtitle">Debes escoger la opción que deseas <br> agregar al carro</div>
    </div>
    
    <div class="edicion_bg">
        <div class="edicion-tab">
            <?php 
                $subproductos = consulta_bd("s.id, s.nombre, t.nombre","subproductos s JOIN tamaños t ON t.id = s.tamaño_id","","s.id asc"); 
                for($i=0; $i<sizeof($subproductos ); $i++) {
            ?>
            <div class="tab <?= ($i == 0) ? 'activado' : '' ?>" data-id="<?= $subproductos[$i][0] ?>" data-action="e<?=$num?>" data-st="<?= $edicion[3] ?>"><?= $subproductos[$i][1] ?></div>
            <?php } ?>
        </div>
    </div>
    
    <div class="load_prices"></div>

</div>

<div class="bgpopcart"></div>
<div class="pop-cart">
    <a href="javascript:void(0)" class="close_pop"><img src="img/close_pop.png"></a>
    <div class="title_pop">
        ¡añadido al carrito correctamente!
    </div>

    <div class="grid_pop">
        
    </div>

    <div class="buttons_pop">
        <a href="javascript:void(0)" class="seguirComprando">Seguir comprando</a>
        <a href="mi-carro">Finalizar compra</a>
    </div>
</div>
