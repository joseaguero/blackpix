<?php 
	$categoria = (isset($_GET[categoria])) ? mysql_real_escape_string($_GET[categoria]) : 0;
	$marca = (isset($_GET[marca])) ? mysql_real_escape_string($_GET[marca]) : 0;
	
	if(isset($_GET[marca]) and $marca != 0){
		$sqlMarca = " and p.marca_id = $marca";
	} else {
		$sqlMarca = "";
	}
	
	if(isset($_GET[categoria]) and $categoria != 0){
		$sqlCategoria = "cp.categoria_id = $categoria and";
	} else {
		$sqlCategoria = "";
	}
	
	
	$productos = consulta_bd("distinct(p.id), lower(p.nombre), p.thumbs, p.marca_id, pd.id, pd.precio, pd.descuento","productos p, categorias_productos cp, productos_detalles pd","$sqlCategoria cp.producto_id = p.id and p.id=pd.producto_id  $sqlMarca and p.publicado=1 GROUP BY(p.id)","p.nombre"); 
	
	
	$cantResultados = mysqli_affected_rows($conexion);
	
?>

<section>

    <div class="row">
        <div class="col-md-12">
            <h1>Resultados de Búsqueda</h1>
        </div>
    </div>

    <div class="col-md-12 alert alert-info">
        <div class="col-md-7">
        	<p>Se han encontrado <span class="orange"><?php echo $cantResultados; ?> productos</span> con el filtro de busqueda</p>   		</div>
        <!--<div class="col-md-5 buscador-resultado">
            <div class="input-group">
              <input type="text" class="form-control buscador" placeholder="Cortacésped">
              <span class="input-group-btn">
                <button class="btn btn-default search" type="button"><img src="images/lupa.png" alt="Buscar Producto"></button>
              </span>
            </div>
    
        </div>-->
    </div>

    <div class="col-md-12 col-lg-10 col-lg-push-1">
        <div class="row">
		<?php if($cantResultados = 0) { ?>
        <div class="col-md-12 text-center"><br><br><br>
            <p class="lead-resultados">¡Lo sentimos no se encuentra lo que buscas!</p>
            <p class="lead-resultados"><small>El producto no existe o prueba con buscarlo con otro nombre</small></p><br><br>
            <p class="lead-populares">Productos MÁS BUSCADOS</p>
        </div>
        <?php } ?>
        <div class="clearfix"></div>
            
            <?php for($i=0; $i<sizeof($productos); $i++){ 
			$marcaP = consulta_bd("nombre, imagen","marcas","id=".$productos[$i][3],"");
			?>
            <!--Fila resultado -->
            <article class="box-resultado">
                <div class="img-resultado">
                    <img src="imagenes/productos/<?php echo $productos[$i][2]; ?>" class="img-responsive center-block">
                </div>
                <div class="pull-left col-xs-6 col-sm-7">
                    <div class="col-sm-7 col-xs-12 titulos-resultados">
                        <h3><a href="ficha/<?php echo $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>"><?php echo ucfirst(strtolower($productos[$i][1])); ?></a></h3>
                    </div>
                    <div class="col-sm-3 col-xs-7 marca-resultados">
                        <img src="imagenes/marcas/<?php echo $marcaP[0][1]; ?>" class="img-responsive center-block" alt="<?php echo $marcaP[0][0]; ?>" />
                    </div>
    
                    <div class="col-sm-2 col-xs-12 precio-resultados">
                        <span class="precio">$<?php echo number_format($productos[$i][5],0,",","."); ?></span>
                    </div>
                </div>
                <div class="col-md-3 hidden-xs">
                    <div class="pull-right botones-resultado">
                        <div class="pull-left box-carro">
                           <a href="javascript:void(0)" rel="<?php echo $productos[$i][4]; ?>" class="agregaCarroDesdeLista">    
                           		<img src="images/carro.png" alt="Mi carro"> <span class="badge">+</span>
                           </a>
                        </div>
                        <a class="btn btn-success pull-left btn-ver-mas-listado" href="ficha/<?php echo $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>">VER MÁS</a>
                    </div>
                </div>
                <div class="box-btn visible-xs">
                        <div class="btn-carro-movil">
                            <a href="javascript:void(0)" rel="<?php echo $productos[$i][4]; ?>" class="agregaCarroDesdeLista">
                            	<img src="images/carro-movil.jpg" class="img-responsive pull-left">
                            </a>
                            <span>
                            	<a href="ficha/<?php echo $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>"> Ver más</a>
                            </span>
                        </div>
                    </div>
            </article>
			<!--fin Fila resultado -->
        	<?php } ?>

        </div>
        </div>

        <div class="clearfix"></div>
        <!--<a href="javascript:void(0)" class="btn btn-ver-mas-internas center-block">
            Ver más <span>+</span>
        </a>-->

    </section>


<div class="clearfix"></div>
<div class="col-xs-12 footer-top"></div>

<script type="text/javascript">
    $(function(){
        var i=0;
        $('.box-popular').each(function (index) { 
            i=i+1;
            if(i==5){
                $(this).addClass("sinMargen");
                i=0;
            } else {

            }
          console.log(i); 
        });
    });
</script>