<div class="bread_carro">
	<div class="container_carro">
		<div class="list activo center-text">contacto</div>
	</div>
</div>

<div class="gray-body">
	<div class="center_ident">
		<div class="text_ident">ingresa tus datos para poder <br>
			contactarte con nosotros</div>


		<div class="grid_ident">
			<div class="col">
				<form action="correoContacto" method="post" id="formContacto">
					<div class="st_ident">Nombre y apellido</div>
					<input type="text" name="nombre" class="input_ident" placeholder="Nombre y apellido...">

					<div class="st_ident mt-20">Email</div>
					<input type="email" name="email" class="input_ident" placeholder="Email...">

					<div class="st_ident mt-20">Mensaje</div>
					<div class="form-group mt-10">
						<textarea name="mensaje" class="input-area" placeholder="Mensaje..." rows="4"></textarea>
					</div>

					<div class="center-button">
						<button class="btnContacto">Enviar</button>
					</div>
				</form>
			</div>
			<div class="col">
				<div class="t_ident">Teléfonos</div>
				<div><a href="tel:+56991628747" class="st_ident">(+56) 9 9162 8747</a></div>
				<div><a href="tel:+56227800999" class="st_ident">(2) 2 2780 0999</a></div>

				<div class="t_ident mt-20">Email</div>
				<div><a href="mailto:leon.blackpix@gmail.com" class="st_ident">leon.blackpix@gmail.com</a></div>

				<div class="t_ident mt-20">Dirección</div>
				<div class="st_ident">Cousin 0227 Providencia, Santiago, Chile</div>

				<div class="map mt-20">
					<a href="https://www.google.com/maps/place/Cousin+227,+Providencia,+Regi%C3%B3n+Metropolitana/@-33.4439618,-70.6311192,17z/data=!3m1!4b1!4m5!3m4!1s0x9662c578c702cf17:0x921e68284e2ac73e!8m2!3d-33.4439618!4d-70.6289305" target="_blank">
						<img src="img/map.jpg">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>