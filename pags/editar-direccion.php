<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "inicio-sesion";</script>';
    }
$id         = mysqli_real_escape_string($conexion, $_GET['id']);
$direccion  = consulta_bd('cliente_id, nombre, pais_id, region_id, comuna_id, calle', 'clientes_direcciones', "id = $id", '');
$clienteLog = (int)$_COOKIE['usuario_id'];
?>

<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">mi cuenta</div>
    </div>
</div>

<div class="gray-body">
    <div class="container">
        <div class="text_ident">podrás ver tus pedidos, historial de compra y <br>
        editar tus datos personales y de envío
        </div>

        <?php include("pags/menuMiCuenta.php"); ?>

        <div class="cont_bodydash max-dash">
        	<h3 class="subtitulo">Editar dirección

                <a href="mis-direcciones" class="btnVolverDir">Volver</a></h3>
            
            <?php if ($clienteLog == $direccion[0][0]): ?>
                <div class="form-group">
                    <label>Nombre de la dirección <small class="require">*</small></label>
                    <input type="text" name="nombre" class="input-text nombreDir" placeholder="Nombre..." value="<?= $direccion[0][1] ?>">
                </div>

                <div class="form-group">
                    <label>País <small class="require">*</small></label>
                    <?php $paises = consulta_bd('id, nombre', 'paises', "", 'id asc'); ?>
                    <select class="select-form slPaisDash" name="paisDash">
                        <option value="0">País</option>
                        <?php foreach ($paises as $pais): 
                            $selected = ($pais[0] == $direccion[0][2]) ? 'selected' : ''; ?>
                            <option value="<?= $pais[0] ?>" <?= $selected ?>><?= $pais[1] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                
                <?php 

                $displayNone = ((int)$direccion[0][2] > 1) ? 'style="display:none;"' : '';

                ?>
                <div class="content_dirDash" <?= $displayNone ?>>
                    <div class="form-group">
                        <?php $regiones = consulta_bd('id, nombre', 'regiones', '', 'posicion asc'); ?>
                        <label>Región <small class="require">*</small></label>
                        <select class="select-form regionesDash" name="region">
                            <option value="0">Región</option>
                            <?php foreach ($regiones as $reg):
                            $selected = ($reg[0] == $direccion[0][3]) ? 'selected' : ''; ?>
                                <option value="<?= $reg[0] ?>" <?= $selected ?>><?= $reg[1] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <?php $comunas = consulta_bd('id, nombre', 'comunas', '', 'nombre asc'); ?>
                        <label>Comuna <small class="require">*</small></label>
                        <select class="select-form comunasDash" name="comuna">
                            <option value="0">Comuna</option>
                            <?php foreach ($comunas as $comuna): 
                                $selected = ($comuna[0] == $direccion[0][4]) ? 'selected' : ''; ?>
                                <option value="<?= $comuna[0] ?>" <?= $selected ?>><?= $comuna[1] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div> 

                <div class="form-group">
                    <label>Dirección <small class="require">*</small></label>
                    <input type="text" name="direccion" class="input-text dirDash" placeholder="Dirección..." value="<?= $direccion[0][5] ?>">
                </div>

                <input type="hidden" name="action" class="actionDir" value="update">
                <input type="hidden" name="dirId" class="dirId" value="<?= $id ?>">

                <a href="javascript:void(0)" class="agregarDireccionRe">Agregar</a>
            <?php else: ?>
                <p>No tienes permisos para editar esta dirección</p>
            <?php endif ?>

        	
        </div>

        <div class="clearfix"></div>
      	<div class="mb-30"></div>
    </div>
</div>

<div class="bg_dir"></div>
<div class="pop_success">
  <div class="close_password"><img src="img/close_pop.png"></div>
  <p>Direccion editada con éxito</p>

  <a href="javascript:void(0)" class="btnAceptarPopup">Aceptar</a>
</div>