<?php 

if (isset($_COOKIE['usuario_id'])) {
  echo '<script>location.href="home";</script>';
}

$id = (isset($_POST['cliente_id'])) ? mysqli_real_escape_string($conexion, $_POST['cliente_id']) : 0;
$cliente = consulta_bd("id, nombre, telefono, email","clientes","id = $id and activo = 0",""); 


?>
<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">cuenta</div>
    </div>
</div>

<div class="gray-body">
  <div class="center_ident">
    <div class="text_ident">Crea una cuenta en blackpix para comprar con <br>mayor facilidad o inicia sesión</div> 

  <?php 
        if(isset($_COOKIE[nombreUsuario]) and isset($_COOKIE[claveUsuario])){
          $checked = 'checked="checked"';
          $usuarioCookie = $_COOKIE[nombreUsuario];
          $claveCookie = base64_decode($_COOKIE[claveUsuario]);
          } else {
            $checked = '';
            }
          ?>

    <div class="grid_ident">
      <div class="col">
        <form action="registroExito" method="post" id="formCrearCuenta">
          <div class="t_ident">Crea tu cuenta blackpix</div>
          <div class="form-group">
              <label>Nombre y apellido <small class="require">*</small></label>
              <input type="text" name="nombre" class="input-text" placeholder="Nombre y Apellido..." value="<?= $cliente[0][1] ?>">
          </div>

          <div class="form-group">
              <label>Email <small class="require">*</small></label>
              <input type="email" name="email" class="input-text" placeholder="Email..." value="<?= $cliente[0][3] ?>" readonly>
          </div>

          <div class="form-group">
              <label>Teléfono <small class="require">*</small></label>
              <input type="text" name="telefono" class="input-text" placeholder="Teléfono..." value="<?= $cliente[0][2] ?>">
          </div>

          <div class="form-group">
              <label>Contraseña <small class="require">*</small></label>
              <input type="password" name="password" class="input-text" placeholder="Contraseña...">
          </div>

          <div class="form-group">
              <label>Repetir Contraseña <small class="require">*</small></label>
              <input type="password" name="re-password" class="input-text" placeholder="Repetir contraseña...">
          </div>

          <div class="content-chk_terminos_r">
              <input type="checkbox" name="terminos" id="chk_terminos" class="chk">
              <label id="chk_terminos">Acepto los <a href="terminos-y-condiciones">términos y condiciones.</a></label>
          </div>

          <input type="hidden" name="cliente" value="<?= $id ?>">

          <div class="clearfix"></div>
          <button class="btnCrearCuenta" id="btnCrearCuenta">Crear cuenta</button>
        </form>
      </div>
      <div class="col">

        <div class="t_ident">Inicia sesión</div>
        
        <form action="ajax/login.php" method="post" class="formularioLogin" id="formularioLogin">

          <div class="form-group">
              <label>Ingresa tu correo</label>
              <input type="email" name="email" class="input-text" placeholder="Email..." value="<?= $usuarioCookie; ?>">
          </div>

          <div class="form-group">
              <label>Contraseña</label>
              <input type="password" name="clave" class="input-text" placeholder="Contraseña..." value="<?= $claveCookie; ?>">
          </div>

          <input type="hidden" name="origen" value="login">

          <div class="content_chk_password">
            <input type="checkbox" name="recordarCuenta" id="chk_mantener" class="chk" <?= $checked ?>>
            <label id="chk_mantener">Recordar mis datos</label>
          </div>

          <a href="javascript:void(0)" class="recuperar_password">¿olvidaste tu contraseña?</a>

          <div class="clearfix mb-20"></div>

          <button class="btnFormCompraRapida" id="inicioSesion">Iniciar sesión</button>
           
           <!-- <a href="javascript:void(0)" id="inicioSesion" class="btnFormCompraRapida">Iniciar sesión</a> -->
           <div style="clear:both"></div>
       </form>

      </div>
    </div>
    </div>
</div>

<div class="contentPopPassword">
  <div class="closePopCarroPass"><img src="img/close_pop.png"></div>

  <div class="title">restablecer contraseña</div>
  <div class="subtitle">Ingresa tu dirección de correo electrónico y te enviaremos
instrucciones para restablecer la contraseña.</div>

  <div class="center">
    
    <div class="form-group">
      <input type="email" name="email" class="input-text input-white" placeholder="Email...">
    </div>

    <a href="javascript:void(0)" class="btnRestablecer">Enviar</a>
    <div class="fadeOutPop">o vuelve a <a href="javascript:void(0)">iniciar sesión</a></div>

  </div>

</div>


