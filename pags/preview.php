<?php
if (!$_SESSION[Blackpix_admin]) {
    echo '<script>location.href ="404";</script>';
}


$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
$nombre = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;

$producto = consulta_bd('p.nombre, a.nombre, p.año, p.descripcion, a.id', 'productos p JOIN artistas a ON a.id = p.artista_id', "p.id = $id", '');

$categoria_producto = consulta_bd('c.id', 'lineas_productos lp JOIN categorias c ON c.id = lp.categoria_id', "lp.producto_id = $id", '');

foreach ($categoria_producto as $cp) {
    if ($explode_categorias == '') {
        $explode_categorias = $cp[0];
    } else {
        $explode_categorias .= '-' . $cp[0];
    }
}

$ediciones = consulta_bd('pd.id, pd.nombre, t.nombre, pd.stock', 'productos_detalles pd JOIN tamaños t ON t.id = pd.tamaño_id', "pd.producto_id = $id and pd.publicado = 1", '');

$imagenes_ficha = consulta_bd('archivo', 'img_productos', "producto_id = $id", 'posicion asc');

if (!is_array($producto) and !is_array($ediciones)) {
    echo '<script>location.href="404";</script>';
}

?>

<div class="breadcrumbs">
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="lineas/1/ediciones-limitadas" itemprop="item" class="active">
                <span itemprop="name">Ediciones limitadas</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="ficha/<?= $id ?>/<?= $nombre ?>" itemprop="item" class="active">
                <span itemprop="name"><?= $producto[0][0] ?></span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
    </ul>
</div>

<div class="bgpopcart"></div>
<div class="pop-cart">
    <a href="javascript:void(0)" class="close_pop"><img src="img/close_pop.png"></a>
    <div class="title_pop">
        ¡añadido al carrito correctamente!
    </div>

    <div class="grid_pop">

    </div>

    <div class="buttons_pop">
        <a href="javascript:void(0)" class="seguirComprando">Seguir comprando</a>
        <a href="mi-carro">Finalizar compra</a>
    </div>
</div>




<div class="container">
    <div class="grid-ficha">
        <div class="col">
            <div class="slider-ficha">
                <?php if (is_array($imagenes_ficha)) : ?>
                    <?php foreach ($imagenes_ficha as $imgf) : ?>
                        <div class="item">
                            <img src="<?= imagen("imagenes/productos/", $imgf[0]); ?>">
                            <i class="fas fa-search-plus zoom_ficha"></i>
                        </div>
                    <?php endforeach ?>
                <?php else : ?>
                    <div class="item">
                        <img src="<?= imagen("img/", "sinfotoficha.jpg"); ?>">
                        <i class="fas fa-search-plus zoom_ficha"></i>
                    </div>
                <?php endif ?>
            </div>

            <div class="slider-zoom">
                <div class="slider-ficha-zoom">
                    <?php if (is_array($imagenes_ficha)) : ?>
                        <?php foreach ($imagenes_ficha as $imgz) : ?>
                            <div class="item">
                                <img src="<?= imagen("imagenes/productos/", $imgz[0]); ?>">
                            </div>
                        <?php endforeach ?>
                    <?php else : ?>
                        <div class="item">
                            <img src="<?= imagen("img/", "sinfotoficha.jpg"); ?>">
                        </div>
                    <?php endif ?>
                </div>

                <div class="close-zoom"><img src="img/closezoom.png"></div>
            </div>
            <div class="bg_slider-zoom"></div>
        </div>
        <div class="col col-info">

            <div class="titulo"><?= $producto[0][0] ?></div>
            <div class="artista"><a href="artista/<?= $producto[0][4] . "/" . url_amigables($producto[0][1]) ?>"><?= $producto[0][1] ?></a></div>
            <div class="anio"><?= $producto[0][2] ?></div>

            <div class="short-description"><?= strip_tags($producto[0][3]) ?></div>

            <div class="edicion-tab">
                <?php
                $subproductos = consulta_bd("id, nombre, medidas", "subproductos", "", "id asc");
                for ($i = 0; $i < sizeof($subproductos); $i++) {
                ?>
                    <div class="tab <?= ($i == 0) ? 'activado' : '' ?>" data-id="<?= $subproductos[$i][0] ?>" data-action="e<?= $num ?>" data-st="<?= $edicion[3] ?>"><?= $subproductos[$i][1] ?></div>
                <?php } ?>
            </div>


            <?php //$contador_ed = 0; 
            //foreach ($ediciones as $ed):
            //$num_ed = $contador_ed+1;
            for ($i = 0; $i < sizeof($subproductos); $i++) {
                $pd = consulta_bd("id, nombre, bajada, precio, descuento, principal", "productos_detalles", "producto_id = $id and publicado = 1 and subproducto_id = " . $subproductos[$i][0], "posicion asc");
                $cantSub = mysqli_affected_rows($conexion);

                //if($cantSub > 0){
            ?>

                <div class="content_tab <?= ($i == 0) ? 'activado' : '' ?>" id="tab-<?= $subproductos[$i][0] ?>" data-rel="<?= $subproductos[$i][0] ?>" data-action="">

                    <div class="grid-tt">
                        <div class="tamanio">
                            <span>tamaño</span>
                            <span><?= $subproductos[$i][2]; ?></span>
                        </div>
                        <div class="tiraje"><span>tiraje:</span> 1/20</div>
                    </div>

                    <?php //$lista_precio = consulta_bd('lp.id, lp.nombre, lpp.precio, lpp.descuento, lp.descripcion', 'lista_precio_producto lpp JOIN lista_precios lp ON lp.id = lpp.lista_precio_id', "lpp.productos_detalle_id = $ed[0]", ''); 
                    ?>

                    <div class="list-radio">
                        <?php
                        //$contador_sub = 0; 
                        //foreach ($lista_precio as $lp):
                        for ($j = 0; $j < sizeof($pd); $j++) {
                            $checked_c = ($pd[$j][5] == 1) ? 'checked="checked"' : ''; ?>
                            <div class="row">
                                <input type="radio" name="radio_ediciones_a" class="radio_ediciones" value="<?= $pd[$j][0]; ?>" <?= $checked_c ?>>
                                <label><?= $pd[$j][1]; ?>
                                    <?php if ($pd[$j][2] != null and trim($pd[$j][2]) != '') : ?>
                                        <span><?= strip_tags($pd[$j][2]) ?></span>
                                    <?php endif ?>
                                </label>
                                <div class="precio_ficha">
                                    <?php if ($pd[$j][4] > 0) : ?>
                                        <span class="descuento">$<?= number_format($pd[$j][3], 0, ',', '.') ?></span>
                                        <span class="precio_final">$<?= number_format($pd[$j][4], 0, ',', '.') ?></span>
                                    <?php else : ?>
                                        <span class="precio_final">$<?= number_format($pd[$j][3], 0, ',', '.') ?></span>
                                    <?php endif ?>

                                </div>
                            </div>
                            <!--FIn ROW-->
                        <?php } ?>

                        <div class="clearfix"></div>
                    </div>

                </div>
                <!--/* aca termina el tab*/-->
                <?php // } 
                ?>
            <?php }  ?>
        </div>
    </div>

    <div class="grid-ficha-bottom">
        <div class="col">
            <div class="nav-slider-ficha">
                <?php if (is_array($imagenes_ficha)) : ?>
                    <?php foreach ($imagenes_ficha as $imgn) : ?>
                        <div class="item" style="background: url(<?= imagen("imagenes/productos/", $imgn[0]); ?>);"></div>
                    <?php endforeach ?>
                <?php else : ?>
                    <div class="item" style="background: url(<?= imagen("img/", "sinfotoficha.jpg"); ?>);"></div>
                <?php endif ?>
            </div>

            <div class="artista-link">
                <a href="artista/<?= $producto[0][4] . "/" . url_amigables($producto[0][1]) ?>"><?= $producto[0][1] ?></a>
            </div>
        </div>


    </div>

    <div class="border_title mt-50 mb-50">fotografías relacionadas</div>

    <?php
    $filtros_rel['categorias'] = $explode_categorias;
    $filtros_rel['limite'] = 4;
    $filtros_rel['id_ficha'] = $id;
    $filtros_rel['random'] = true;

    $productosRelacionados = get_products($filtros_rel);
    ?>

    <div class="grid-products mt-30 grid-relacionados">
        <?php foreach ($productosRelacionados['productos']['producto'] as $prd) : ?>
            <a href="ficha/<?= $prd['id_producto'] ?>/<?= $prd['nombre_seteado'] ?>" class="col">
                <div class="thumbg">
                    <img src="<?= imagen($prd['ruta_imagen'], $prd['nombre_imagen']); ?>" />
                </div>
                <div class="add_fav" onclick="addToFav(<?= $prd['id_producto'] ?>, event, this)">
                    <?php if (productoGuardado($prd['id_producto'])) : ?>
                        <i class="material-icons">favorite</i>
                    <?php else : ?>
                        <i class="material-icons">favorite_border</i>
                    <?php endif ?>
                </div>
                <?php if ($prd['descuento'] > 0) : ?>
                    <span class="descuentoGrilla"><?= round(100 - ($prd['descuento'] * 100) / $prd['precio']) ?>% DCTO</span>
                <?php endif ?>
                <div class="data">
                    <div class="titles">
                        <span class="artista"><?= $prd['artista'] ?></span>
                        <span class="nombre"><?= $prd['nombre'] ?></span>
                    </div>
                    <div class="precio">
                        <?php if ($prd['descuento'] > 0) : ?>
                            <span class="oferta"><?= cambiarMoneda($prd['precio']) ?></span>
                            <div class="precio_final">
                                <span>Desde</span>
                                <span><?= cambiarMoneda($prd['descuento']) ?></span>
                            </div>
                        <?php else : ?>
                            <span class="oferta"></span>
                            <div class="precio_final">
                                <span>Desde</span>
                                <span><?= cambiarMoneda($prd['precio']) ?></span>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </a>
        <?php endforeach ?>
    </div>

    <?php $artistas = consulta_bd('DISTINCT(id), nombre, thumb', 'artistas', '', "RAND() LIMIT 3"); ?>

    <div class="border_title mt-50 mb-50">artistas</div>

    <div class="grilla-artistas mb-30">
        <?php foreach ($artistas as $art) : ?>
            <div class="col">
                <a href="artista/<?= $art[0] ?>/<?= url_amigables($art[1]) ?>">
                    <img src="imagenes/artistas/<?= $art[2] ?>">

                    <span class="nombre"><?= $art[1] ?></span>
                </a>
            </div>
        <?php endforeach ?>
    </div>

</div>


<!-- <div>
    <div itemtype="http://schema.org/Product" itemscope>
        <meta itemprop="mpn" content="<?= $producto[0][8] ?>" />
        <meta itemprop="name" content="<?= $producto[0][7] ?>" />
        <?php if (is_array($galeria)) : ?>
            <?php for ($i = 0; $i < sizeof($galeria); $i++) { ?>
                <link itemprop="image" href="imagenes/productos/<?= $galeria[$i][0] ?>" />
            <?php } ?>
        <?php endif ?>
        <meta itemprop="description" content="<?= strip_tags($producto[0][17]) ?>" />
        
        <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
            <link itemprop="url" href="<?= $url_base ?>ficha/<?= $id ?>/<?= $nombre ?>" />
            <?php if ($producto[0][14] > 0) : ?>
            <meta itemprop="availability" content="https://schema.org/InStock" />
            <?php else : ?>
            <meta itemprop="availability" content="https://schema.org/OutOfStock" />
            <?php endif ?>
            <meta itemprop="priceCurrency" content="CLP" />
            <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
            <meta itemprop="price" content="<?= $precio_final ?>" />
        </div>
        <meta itemprop="sku" content="<?= $producto[0][8] ?>" />
        <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
            <meta itemprop="name" content="<?= $producto[0][18] ?>" />
        </div>
    </div> 
</div> -->