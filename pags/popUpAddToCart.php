<?php 
	include("../admin/conf.php");
	include("../funciones.php");
	require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

	$id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
	$producto = consulta_bd("p.nombre, p.thumbs, pd.precio, pd.descuento, p.id, pd.precio_cyber","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
	
	if ($is_cyber AND is_cyber_product($producto[0][4])) {
        $precios = get_cyber_price($id);
        var_dump($precios);
        $precio = $precios['precio'];
        $descuento = $precios['precio_cyber'];

        $valores = '<div class="valorAntFilaArticulo floatLeft ancho100">$'.number_format($precio,0,",",".").'</div>
                        <div class="valorAhoraFilaArticulo floatLeft ancho100">$'.number_format($descuento,0,",",".").'</div>';
        $total_pedido = "";
    }else{
        $precio = $producto[0][2];
        $descuento = $producto[0][3];
        if($descuento > 0){
            $valores = '<div class="valorAntFilaArticulo floatLeft ancho100">$'.number_format($precio,0,",",".").'</div>
                        <div class="valorAhoraFilaArticulo floatLeft ancho100">$'.number_format($descuento,0,",",".").'</div>';
            $total_pedido = "";
        } else {
            $valores = '<div class="valorAhoraFilaArticulo floatLeft ancho100">$'.number_format($precio,0,",",".").'</div>';
            $total_pedido = "";
        }
    }
?>
<div class="fondoPopUp">
    <div class="contPop">
        <a href="javascript:cerrar()" class="cerrar"><i class="fas fa-times"></i></a>
        <h3>Has añadido <?= $_GET[qty]; ?> artículo al carro </h3>
        <div class="ancho50 floatLeft">
            <div class="contFilasArticulos floatLeft ancho100">
                <div class="filaArtPopUp floatLeft ancho100">
                    <div class="contImgFilaArticulo">
                    	<img src="imagenes/productos/<?= $producto[0][1]; ?>" width="100%" />
                    </div>
                    <div class="contDatosFilaArt">
                        <div class="NomFilaArticulo floatLeft ancho100"><?= $producto[0][0]; ?></div>
                        <?= $valores; ?>
                    </div>
                </div>
            </div><!--fin contFilasArticulos -->
        </div><!--Fin ancho50-->
        
        <div class="ancho50 floatLeft">
            <div class="contDatosCompra">
            	<div class="cantArticulos floatLeft ancho100"><?= qty_pro(); ?> artículos</div>
                <div class="filaTotalesPopUp floatLeft ancho100">
                	<div class="ancho60 floatLeft">Coste total de los productos:</div>
                    <div class="ancho40 floatLeft"><span class="valoresPopUp floatLeft ancho100">$<?= number_format(totalCart(),0,",","."); ?></span></div>
                </div>
                <div class="filaTotalesPopUp lineaAbajo floatLeft ancho100">
                	<div class="ancho60 floatLeft">Envío estándar:</div>
                    <div class="ancho40 floatLeft"><span class="valoresPopUp floatLeft ancho100">$0</span></div>
                </div>
                
                <div class="filaTotalesPopUp floatLeft ancho100">
                	<div class="ancho60 floatLeft"><span class="verde"><strong>TOTAL</strong></span></div>
                    <div class="ancho40 floatLeft">
                        <span class="naranjo floatLeft ancho100 valoresPopUp">
                        	<strong>$<?= number_format(totalCart(),0,",","."); ?></strong>
                        </span>
                    </div>
                </div>
                <a href="mi-carro" class="btnPopUpComprar">Comprar</a>
                <a href="javascript:cerrar()" class="btnPopUpSeguirComprando">seguir comprando</a>
            </div><!--fin contDatosCompra -->
        </div><!--fin  ancho50 -->
    </div>

</div>
<?php mysqli_close($conexion);?>