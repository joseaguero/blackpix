<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">Error 404</div>
    </div>
</div>

<div class="gray-body contFracaso">
    <div class="container mt-40 mb-40">
		<div class="top-identificacion" style="margin-bottom:20px;">
         <div>La pagina que buscas no existe </div>
        </div>
        
        <div class="cont100 contBtnFracaso">
            <a href="home">IR AL HOME</a> 
        </div>
    </div>
</div>