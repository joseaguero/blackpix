<?php
	$clientes = consulta_bd("id, apellido, nombre, rut, telefono, email","clientes","id=".$_COOKIE[usuario_id],"");
	$cant = mysqli_affected_rows($conexion);
	
	if($cant == 0){
	   echo '<script>location.href="identificacion?msje=Usuario no encontrado.";</script>';
	}
	
	$nombre =  $clientes[0][2];
	$apellido =  $clientes[0][1];
	$email =  $clientes[0][5];
	$rut = $clientes[0][3];
	$telefono = $clientes[0][4];
	$idCliente = $_COOKIE[usuario_id];
	
	$direccion = consulta_bd("id, region_id, comuna_id, calle, numero, localidad_id","clientes_direcciones","cliente_id=".$clientes[0][0],"");
	$cantDirecciones = mysqli_affected_rows($conexion);
	if($cantDirecciones > 0){
		$region_id = $direccion[0][1];
		$comuna_id = $direccion[0][2];
		$localidad_id = $direccion[0][5];
		$calle = $direccion[0][3];
		$numero = $direccion[0][4];
	}

    $_SESSION["correo"] = $email;
	
	?>
    
    
<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li><a href="mi-carro">Mi carro</a></li>
          <li class=""><a href="identificacion">identificación</a></li>
          <li class="active">Envio y pago</li>
        </ul>
    </div>
</div>

<div class="cont100">
    <div class="cont100Centro">
        <div class="filaPasos cont100">
        	<a class="" href="mi-carro">
            	<span class="paso">1</span>
                <span class="nomPaso">Mi Carro</span>
            </a>
            <a class="" href="javascript:void(0)">
            	<span class="paso">2</span>
                <span class="nomPaso">Identifícate</span>
            </a>
            <a class="activo" href="javascript:void(0)">
            	<span class="paso">3</span>
                <span class="nomPaso">Envío y Pago</span>
            </a>
        </div><!--Fin filaPasos -->
    </div>
</div>



<div class="contBotoneraEnvioYPago">
	<a  href="javascript:void(0)" rel="misDatos">Mis Datos</a>
    <a class="activo" href="javascript:void(0)" rel="retiroDespacho">Retiro / Despacho</a>
    <a href="javascript:void(0)" rel="medioPago">Medios de pago</a>
</div>


<form method="post" action="proceso-pago" id="formCompra">

<div class="cont100">
    <div class="cont100Centro">
        
        <div class="contDatosCarro">
        	<div class="colForm1 contPaso3Movil" id="misDatos">
                <div class="tituloColumnas">Tus datos</div>
                    <div class="contCampos">
                        <label for="nombre">Nombre y apellidos</label>
                        <input type="text" name="nombre" id="nombre" value="<?php echo $nombre." ".$apellido; ?>" class="campoPasoFinal" disabled="disabled" />
                    </div>
                    
                    <div class="contCampos">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" value="<?php echo $email; ?>" class="campoPasoFinal" disabled="disabled" />
                    </div>
                    
                    <div class="contCampos">
                        <label for="telefono">Teléfono</label>
                        <input type="text" name="telefono" id="telefono" value="<?php echo $telefono; ?>" class="campoPasoFinal" disabled="disabled" />
                    </div>
                    
                    <div class="contRut">
                    	<label for="rut">Rut</label>
                        <input type="text" name="rut" id="Rut" value="<?php echo $rut; ?>" class="campoPasoFinal" disabled="disabled" />
                    </div>
                    
                    <div class="mensajeCaluga">
                    	<i class="fas fa-lock"></i>
                        <span>Tus datos seran encriptados en la siguiente compra, para una mejor seguridad.</span>
                    </div>
            </div><!--Fin colForm1 -->
            
            <div class="colForm1 contPaso3Movil" id="retiroDespacho" style="display:block;" >
                	<div class="selectorTabs">
                        <div class="direccionDespachoTabs">
                            <input type="radio" name="envio" id="tienda2" value="direccionCliente"  />
                            <span class="tituloTabsDirecciones">Direccion de envío</span>
                        </div>
                    	<div class="direccionDespachoTabs">
                            <input type="radio" name="envio" id="tienda1" value="retiroEnTienda" checked="checked" />
                            <span class="tituloTabsDirecciones">Retiro en tienda</span>
                        </div>
                        
                    </div>
                	<div class="tabsCompraRapida" id="tabsCompra2">	
						<div class="caluga-retiro">
                        	
                            <div class="cont100 filaSucursalCarro">Puedes retirar tus productos en:</div>
                           <?php $sucursales = consulta_bd("s.id, s.nombre, s.direccion, s.comuna_id, s.latitud, s.longitud, c.nombre, r.nombre, s.link_mapa","sucursales s, comunas c, ciudades ciu, regiones r","s.publicado = 1 and s.retiro_en_tienda=1 and s.comuna_id = c.id and c.ciudad_id = ciu.id and ciu.region_id = r.id","s.posicion asc");
						   for($i=0; $i<sizeof($sucursales); $i++) {
						   ?> 
                            <div class="cont100 filaSucursalCarro">
                            	<div class="contRadioDireccion">
                                	<input type="radio" name="retiro" id="sucursal<?= $sucursales[$i][0]; ?>" value="<?= $sucursales[$i][0]; ?>" <?php if($i == 0){echo 'checked="checked"';}?> />
                                </div>
                                 <div class="contNombreSucursal">
                                 	<span class="nombreSucursal"><?= $sucursales[$i][1]; ?></span>
                                    <span><?= $sucursales[$i][2]; ?>, <?= $sucursales[$i][6]; ?>, <?= $sucursales[$i][7]; ?></span>
                                   
                                    <?php if($sucursales[$i][8] != ""){?>
                                    <a class="mapaSucursal" data-fancybox data-type="iframe"  data-src="pags/mapaSucursales.php?id=<?= $sucursales[$i][0]; ?>" href="javascript:void(0)">Ver mapa</a>
                                    <?php } ?>
                                 </div>
                            </div>
                            <?php } ?>
                            
                             <br><br>
							<i class="fas fa-calendar-alt"></i> <span>Desde  el <?= sumaDiasHabiles() ?></span>                 
                        </div>
                        <div class="contCamposRetiro">
                            <h3>Quien retira</h3>
                            <input type="text" class="campoPasoFinal" name="nombre_retiro" value="" placeholder="Nombre completo" id="nombre_retiro">
                            <input type="text" class="campoPasoFinal" name="rut_retiro" value="" placeholder="Rut" id="rut_retiro">
                            
                        </div>
                    </div>
					<div class="tabsCompraRapida" id="tabsCompra1">	
						
						<div class="direccionesGuardadas">
                        	<?php $direcciones = consulta_bd("id, nombre","clientes_direcciones","cliente_id = ".$_COOKIE[usuario_id],"id desc");?>
                        	<label for="direcciones">Mis direcciones <a class="masDir" href="mis-direcciones"><i class="fas fa-plus-circle"></i> Agregar dirección</a></label>
                            <select name="direcciones" id="direcciones">
                                <option>Seleccione Dirección</option>
                                <?php for($i=0; $i<sizeof($direcciones); $i++){ ?>
                                <option value="<?= $direcciones[$i][0]; ?>"><?= $direcciones[$i][1]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
						
                        
						<?php $regiones = consulta_bd("id, nombre","regiones","","id desc");?>
                        <label for="región">Región</label>
                        <select name="region" id="region">
                            <option>Seleccione region</option>
                            <?php for($i=0; $i<sizeof($regiones); $i++){ ?>
                            <option <?php if($regiones[$i][0] == $region_id){echo 'selected="selected"';} ?> value="<?php echo $regiones[$i][0]; ?>"><?php echo $regiones[$i][1]; ?></option>
                            <?php } ?>
                        </select>
                        
                        
                        <label for="comuna">Comuna</label>
                        <select name="comuna" id="comuna">
                            <option>Seleccione Comuna</option>
                        </select>
                        
                        
                        <label for="localidades">Localidades</label>
                        <select name="localidades" id="localidad">
                            <option>Seleccione Localidad</option>
                        </select>
                        
                        
                        <label for="direccion">Dirección</label>
                        <input type="text" name="direccion" id="direccion" value="<?php if($calle != ''){ echo $calle.','.$numero;}; ?>" class="campoPasoFinal" />
                        
                        
                        <label for="comentarios_envio">Comentarios</label>
                        <textarea name="comentarios_envio" class="form-control comentarios-envio" rows="3" placeholder="Ej: Dejar envío con el conserje."></textarea>

                	</div><!--fin tabsCompraRapida-->
            </div> <!--fin colForm1-->
            
            
            <?php 
                $datosFactura = consulta_bd("razon_social, direccion_factura, rut_factura, email_factura, giro","pedidos","email='$email' and factura=1","id desc");
                $cf = mysqli_affected_rows($conexion);
            ?>             
             
           <div class="contPaso3Movil" id="facturaMovil">
             	<div class="contFactura">
                    <h2>Cómo pagas</h2>
                    <div class="opcionFactura">
                        <input type="radio" name="factura" value="no" id="facturaNo" checked="checked" />
                        <span class="tituloRadio">Boleta</span>
                        
                        <input type="radio" name="factura" value="si" id="facturaSi" />
                        <span class="tituloRadio">Factura</span>
                    </div>     
                </div><!--fin contFactura --> 
                <div class="contDatosFactura">
                    <div class="ancho50Centro">
                        <label for="rut_factura">Rut</label>
                        <input type="text" name="rut_factura" value="<?= $datosFactura[0][2]; ?>" id="RutFactura" class="campoPasoFinal" />
                    </div>
    
                    <div class="ancho50Centro">
                        <label for="giro_factura">Giro</label>
                        <input type="text" name="giro_factura" value="<?= $datosFactura[0][4]; ?>" id="giro" class="campoPasoFinal" />
                    </div>
    
                    <div class="ancho50Centro">
                        <label for="razon_social">Razón social</label>
                        <input type="text" name="razon_social" value="<?= $datosFactura[0][0]; ?>" id="razonSocial" class="campoPasoFinal" />
                    </div>
                    <div class="ancho50Centro">
                        <label for="direccion_factura">Dirección</label>
                        <input type="text" name="direccion_factura" id="direccionFactura" value="<?= $datosFactura[0][1]; ?>" class="campoPasoFinal" />
                    </div>
                                
                                
                                
                                
                </div><!--Fin contDatosFactura -->
           </div><!--factura movil -->             
          
            
        </div><!--fin contDatosCarro -->
        
        
        <div class="contCarroPaso3 contPaso3Movil" id="medioPago">
        	<div class="col2">
            	<div class="tituloColumnas">Resumen de compra</div>
				<div class="contResumenCompra">
                	<div id="contDatosCompra">
                    <label for="codigo_descuento">¿Tienes un código de descuento?</label>
                    <?php if($_SESSION["descuento"]){
							$codigo = $_SESSION["descuento"];
							descuentoBy($codigo);
							$estadoInput = 'disabled="disabled"';
							$icono = '<i class="fas fa-check"></i>';
					} else {
						$estadoInput = '';
					} ?>
                    <div class="codigoOK"><?php echo $icono; ?></div>
                    <input type="text" name="codigo_descuento" class="descuento campoPasoFinal" value="<?php echo $codigo; ?>" placeholder="Ingresa el código" <?php echo $estadoInput ?>>
                    <!-- Fila 1 -->
                    	<div id="contDatosCartVariables">
                            <div class="contCartCompraRapida">
                              <?php echo resumenCompraShort(0, 1); ?>
                            </div>
                            <!--<div class="mensajesDespacho"></div>-->
                            
                            
        				</div>
                        
                        <div class="datosFinalizacionPago"> 
            					<div class="formasDePago">
                                    <h3>Forma de Pago</h3>
                                        <div class="contFormasPago cont100">
                                            <div class="cont50">
                                                <input type="radio" name="medioPago" id="webpay" value="webpay" checked="checked" />
                                                <span class="imagenMedioPago">
                                                	<img src="tienda/webpay.jpg" width="80%"/>
                                                </span>
                                            </div>
                                            
                                            
                                            <!--<div class="cont50">
                                                <input type="radio" name="medioPago" id="mercadopago" value="mercadopago" />
                                                <span class="imagenMedioPago">
                                                	<img src="tienda/mercadopago.jpg" width="80%"/>
                                                </span>
                                            </div>-->
                                            
                                        </div>
                                    </div>
                        
                                    
                        
                                </div><!--fin datosFinalizacionPago -->
                    </div>
				</div><!-- Fin contResumenCompra -->

            </div><!--Fin col2 -->
        </div><!--Fin contCarroPaso3-->
        
        
    </div><!--Fin cont100Centro -->
</div><!--FIn cont100-->



<div class="compraYAceptaTerminos">
    <div class="txtTerminosAcepto">
        <input type="checkbox" name="terminos" id="terminos" value="1" />
        <a href="<?php echo $url_base; ?>terminos-y-condiciones">
             Al comprar acepto los Términos y condiciones
        </a>
    </div>
    
    <a href="javascript:void(0)" class="btnTerminarCarro" id="comprar" />Comprar productos</a>
</div>





</form>     
            