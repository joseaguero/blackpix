<?php 
    require_once 'paginador/paginator.class.php';

    $page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 0;
    $ipp = (isset($_GET[ipp])) ? mysqli_real_escape_string($conexion, $_GET[ipp]) : 4;
    
    $id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
    $busqueda = (isset($_GET[buscar])) ? mysqli_real_escape_string($conexion, $_GET[buscar]) : 0;
    
    $orden = (isset($_GET[orden])) ? mysqli_real_escape_string($conexion, $_GET[orden]) : 0;


    $whereSql = "AND (p.nombre like '%$busqueda%' OR pd.nombre like '%$busqueda%' OR pd.sku like '%$busqueda%')";

    if($orden === "valor-desc"){
        $orderSql = ' valorMenor desc';
        $nombreOrden = "Mayor precio";
    } else if($orden === "valor-asc"){
        $orderSql = ' valorMenor asc';
        $nombreOrden = "Menor precio";
    } else {
        $orderSql = "pp.posicion asc";
        $nombreOrden = "Más relevantes";
    }


$productosPaginador = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, MIN(if(pd.descuento > 0, pd.descuento,pd.precio)) as valorMenor","productos p, subcategorias sc, lineas_productos lp, productos_detalles pd, posicion_productos pp","sc.id = lp.subcategoria_id and lp.producto_id = p.id and p.id = pp.producto_id and p.id = pd.producto_id and p.publicado=1 $whereSql group by(p.id)","$orderSql");
$total = mysqli_affected_rows($conexion);

    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 8; 
    $rutaRetorno = "busquedas/$busqueda/$orden";
    $pages->paginate($rutaRetorno);

    $productos = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, MIN(if(pd.descuento > 0, pd.descuento,pd.precio)) as valorMenor","productos p, subcategorias sc, lineas_productos lp, productos_detalles pd, posicion_productos pp","sc.id = lp.subcategoria_id and lp.producto_id = p.id and p.id = pp.producto_id and p.id = pd.producto_id and p.publicado=1 $whereSql group by(p.id)","$orderSql $pages->limit");
    $cant_productos = mysqli_affected_rows($conexion);

?>


        
        <div class="bannerGrillas cont100">
            <div class="cont100Centro">
                <h1><?php echo $busqueda; ?></h1>
            </div>
        </div>

    
    
    
    <div class="cont100">
        <div class="cont100Centro">
            <ul class="breadcrumb">
              <li><a href="home">Home</a></li>
              <li class="active"> Busqueda: <?= $busqueda; ?></li>
            </ul>
        </div>
    </div><!--Fin breadcrumbs -->
    
    <?php if($total){ ?>
    
        <div class="cont100">
            <div class="cont100Centro">
                
                <!--contenedorFiltro orden -->
                <div class="filtros">
                    <div class="contenedorFiltro">
                        <span class="filtroActual"><?= $nombreOrden ?></span>
                        <ul>
                            <li class="<?php if($orden === 0){ echo 'filtroSeleccionado';} ?>">
                                <a href="subcategorias/<?php echo $id."/".url_amigables($subcategorias[0][4]); ?>">Ordenar por relevancia</a>
                            </li>
                            <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                                <a href="subcategorias/<?= $id."/".url_amigables($subcategorias[0][4])."/".$id_marca."/".$marca."/valor-desc"; ?>">Precio Mayor a Menor</a>
                            </li>
                            <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                                <a href="subcategorias/<?= $id."/".url_amigables($subcategorias[0][4])."/".$id_marca."/".$marca."/valor-asc"; ?>">Precio Menor a Mayor</a>
                            </li>
                        </ul>
                    </div><!--Fin contenedorFiltro orden -->
                 </div>   
                    
                
            </div>
        </div><!--Fin contenido del sitio -->
    
        <div class="cont100">
            <div class="cont100Centro contGrillasProductos">
                
                <?php for($i=0; $i<sizeof($productos); $i++){ ?>
                    <div class="grilla">
                        <?php if(ultimasUnidades($productos[$i][0])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla"></a>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                            <?php if(tieneDescuento($productos[$i][6])){ ?>
                                <span class="antes">$<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>
                                <span> - </span>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                            <?php }else{ ?>
                                $<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?>
                            <?php } ?>
                        </a>
                    </div>
                <?php } ?>
                
            </div>
        </div><!--Fin contenedor grillas -->
    <?php }else{ ?>

        <div class="cont100">
            <div class="cont100Centro contGrillasProductos">

                <h2>NO SE HA ENCONTRADO LO QUE BUSCAS</h2>

                <div class="buscador">
                    <form method="get" action="busquedas" style="float:none;">
                      <input type="text" name="buscar" class="campo_buscador" placeholder="<?php if(isset($_GET[buscar])){echo $_GET[buscar];} else {echo 'Buscar producto';}?>">
                       <input class="btn_search" type="submit" value="buscar">
                    </form>
                </div>

            </div>
        </div>

    <?php } ?>
    
    
    
    <div class="cont100">
        <div class="cont100Centro paginador">
            <?= $pages->display_pages(); ?>
        </div>
    </div><!--Fin paginador -->

