<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "inicio-sesion";</script>';
    }

$direcciones = consulta_bd('d.id, d.nombre, p.id, p.nombre, r.nombre, c.nombre, d.calle', 'clientes_direcciones d LEFT JOIN comunas c ON d.comuna_id = c.id LEFT JOIN regiones r ON r.id = d.region_id JOIN paises p ON p.id = d.pais_id', "d.cliente_id = {$_COOKIE['usuario_id']}", 'd.id desc');

?>

<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">mi cuenta</div>
    </div>
</div>

<div class="gray-body">
    <div class="container">
        <div class="text_ident">podrás ver tus pedidos, historial de compra y <br>
        editar tus datos personales y de envío
        </div>

        <?php include("pags/menuMiCuenta.php"); ?>

        <?php 
        // $direcciones = consulta_bd("cd.id, cd.nombre, cd.region_id, cd.ciudad_id, cd.comuna_id, cd.localidad_id, cd.calle, cd.numero, c.nombre, r.nombre","clientes_direcciones cd, regiones r, ciudades c, comunas com, localidades l","r.id = c.region_id and c.id = com.ciudad_id and com.id = l.comuna_id and l.id = cd.localidad_id and cd.cliente_id=".$_COOKIE['usuario_id'],"cd.id asc"); 
        
        ?>

        <div class="cont_bodydash">
            <h3 class="subtitulo">Mis direcciones <a href="agregar-direccion" class="agregarDireccion">Agregar nueva dirección</a></h3>
            
            <?php if (is_array($direcciones)): ?>
                <div class="grid-direcciones">
                <?php foreach ($direcciones as $dir): ?>

                    <div class="col">
                        <div class="title"><?= $dir[1] ?></div>
                        <?php if ($dir[2] == 1): ?>
                            <div class="dir"><?= $dir[6] ?>, <?= $dir[5] ?>, <?= $dir[4] ?></div>
                        <?php else: ?>
                            <div class="dir"><?= $dir[3] ?>, <?= $dir[6] ?></div>
                        <?php endif ?>

                        <div class="btnDir">
                            <a href="editar-direccion/<?= $dir[0] ?>" class="editarDireccionDir">Editar</a> / 
                            <a href="javascript:void(0)" class="eliminarDireccionDir" data-id="<?= $dir[0] ?>">Eliminar</a>
                        </div>
                    </div>
                    
                <?php endforeach ?>
                </div>

            <?php else: ?>

                <p>Aún no tienes direcciones guardadas</p>

            <?php endif ?>
        </div>
        <div class="clearfix"></div>
        <div class="mb-30"></div>
    </div>
</div>
