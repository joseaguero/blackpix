<div class="lateralServicio">
	<a href="despacho" <?= ($op2 == 'despachos') ? 'class="current"' : '';?>>Despacho <img src="img/arrow-select.png" class="arrow-serv <?= ($op2 == 'despachos') ? 'rotate-180' : '' ?>" alt="Abrir despachos"></a>
	<div class="body-serv-xs <?= ($op2 == 'despachos') ? 'dis-serv-xs' : '' ?>">
		<?php include("servicio_al_cliente/despachos.php"); ?>
	</div>
	<a href="cambios-y-devoluciones" <?= ($op2 == 'cambios-y-devoluciones') ? 'class="current"' : '';?>>Cambios y devoluciones <img src="img/arrow-select.png" class="arrow-serv <?= ($op2 == 'cambios-y-devoluciones') ? 'rotate-180' : '' ?>" alt="Abrir Cambios y devoluciones"></a>
	<div class="body-serv-xs <?= ($op2 == 'cambios-y-devoluciones') ? 'dis-serv-xs' : '' ?>">
		<?php include("servicio_al_cliente/cambios-y-devoluciones.php"); ?>
	</div>
	<a href="terminos-y-condiciones" <?= ($op2 == 'terminos-y-condiciones') ? 'class="current"' : '';?>>Términos y condiciones <img src="img/arrow-select.png" class="arrow-serv <?= ($op2 == 'terminos-y-condiciones') ? 'rotate-180' : '' ?>" alt="Abrir Terminos y condiciones"></a>
	<div class="body-serv-xs <?= ($op2 == 'terminos-y-condiciones') ? 'dis-serv-xs' : '' ?>">
		<?php include("servicio_al_cliente/terminos-y-condiciones.php"); ?>
	</div>
	<a href="politicas-de-privacidad" <?= ($op2 == 'politicas-de-privacidad') ? 'class="current"' : '';?>>Políticas de privacidad <img src="img/arrow-select.png" class="arrow-serv <?= ($op2 == 'politicas-de-privacidad') ? 'rotate-180' : '' ?>" alt="Abrir Politicas de privacidad"></a>
	<div class="body-serv-xs <?= ($op2 == 'politicas-de-privacidad') ? 'dis-serv-xs' : '' ?>">
		<?php include("servicio_al_cliente/politicas-de-privacidad.php"); ?>
	</div>
</div>