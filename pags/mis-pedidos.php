<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "login";</script>';
    }
    $pedidos = consulta_bd("id, oc, fecha, total_pagado","pedidos","estado_id = 2 and cliente_id = {$_COOKIE['usuario_id']}","fecha desc");
?>

<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">mi cuenta</div>
    </div>
</div>

<div class="gray-body">
    <div class="container">
        <div class="text_ident">podrás ver tus pedidos, historial de compra y <br>
        editar tus datos personales y de envío
        </div>

        <?php include("pags/menuMiCuenta.php"); ?>

        <div class="cont_bodydash">
            <h3 class="subtitulo">Mis pedidos</h3>

            <?php if (is_array($pedidos)):
                foreach ($pedidos as $pedido):
                    $fecha_seteada      = setFechaPedido($pedido[2]);
                    $productos_pedido   = consulta_bd('p.id, p.nombre, p.thumbs, a.nombre, IF(pd.descuento > 0, pd.descuento, pd.precio), pp.precio_total, pp.cantidad, pd.id', 'productos p JOIN artistas a ON a.id = p.artista_id JOIN productos_detalles pd ON pd.producto_id = p.id JOIN productos_pedidos pp ON pp.productos_detalle_id = pd.id', "pp.pedido_id = {$pedido[0]}", ''); 
                    $info_pedido        = infoPedido($pedido[0]);
                    $method             = ($info_pedido['medio_de_pago'] == 'webpay') ? 'tbk' : 'mpago';
                    if ($method == 'tbk' OR $method == 'mpago') {
                        $tipo_pago          = tipo_pago($info_pedido['tipo_pago'], $info_pedido['cuotas'], $method);
                    } ?>
                    <div class="row-pedidos">
                        <a href="javascript:void(0)" class="ver_detalles"></a>
                        <div class="header-ped">
                            <div class="col">
                                <span class="title">Fecha pedido</span>
                                <span class="subtitle"><?= $fecha_seteada ?></span>
                            </div>
                            <div class="col">
                                <span class="title">Total pagado</span>
                                <span class="subtitle">$<?= number_format($pedido[3], 0, ',', '.') ?></span>
                            </div>
                            <div class="col">
                                <span class="title">Nº de orden</span>
                                <span class="subtitle"><?= $pedido[1] ?></span>
                            </div>
                        </div>
                        
                        <?php 
                        if (is_array($productos_pedido)):
                            foreach($productos_pedido as $producto):
                            $thumProdPedido = ($producto[2] != null AND $producto[2] != '' AND is_file("imagenes/productos/{$producto[2]}")) ? imagen("imagenes/productos/",$producto[2]) : imagen("img/", "sinFotoGrid.jpg"); ?>
                            <div class="row-producto">
                                <div class="col">
                                    <a href="productos/<?= $producto[0] ?>/<?= url_amigables($producto[1]) ?>">
                                        <img src="<?= $thumProdPedido ?>">
                                    </a>
                                </div>
                                <div class="col">
                                    <div class="nombre"><?= $producto[1] ?></div>
                                    <div class="artista"><?= $producto[3] ?></div>

                                    <div class="cantidad">Cantidad: <?= $producto[6] ?></div>
                                </div>
                                <div class="col">
                                    <div class="precio">
                                        <span class="txt_p">Comprado a:</span>
                                        <span class="valor">$<?= number_format($producto[5], 0, ',', '.') ?></span>

                                        <span class="txt_p">Valor actual:</span>
                                        <span class="valor">$<?= number_format($producto[4], 0, ',', '.') ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="title">Valor pagado:</div>
                                    <div class="subtitle">$<?= number_format($producto[5], 0, ',', '.') ?></div>
                                </div>
                                <div class="col">
                                    <a href="javascript:void(0)" class="volverAComprar" data-id="<?= $producto[7] ?>" data-qty="<?= $producto[6] ?>">Volver a comprar</a>
                                </div>
                            </div>
                        <?php 
                            endforeach;
                        endif ?>

                        <!-- Detalles -->
                        <div class="content-detalles">
                            <div class="grid-detalles">
                                <div class="col">
                                    <?php if ($info_pedido['retiro_en_tienda'] == 1): ?>
                                        <div class="title">Retiro en tienda</div>
                                    <?php else: ?>
                                        <div class="title">Dirección despacho</div>
                                    <?php endif ?>

                                    <div class="dbody">
                                        <?= $info_pedido['direccion'] ?>, <?= $info_pedido['comuna'] ?>, <?= $info_pedido['region'] ?>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="title">Pago</div>
                                    <div class="dbody">
                                        <span class="row-data">Medio de pago: <?= ucwords($info_pedido['medio_de_pago']) ?></span>
                                        <span class="row-data">Tipo de transacción: venta</span>
                                        <?php if ($info_pedido['medio_de_pago'] != 'transferencia'): ?>
                                            <span class="row-data">Nº de cuotas: <?= $tipo_pago['cuota'] ?></span>
                                            <span class="row-data">Tipo de cuotas: <?= $tipo_pago['tipo_cuota'] ?></span>
                                            <span class="row-data">Tarjeta: **** **** **** <?= $info_pedido['card_number'] ?></span>
                                            <span class="row-data">Tipo de pago: <?= $tipo_pago['tipo_pago']; ?></span>
                                            <span class="row-data">Fecha y hora de transacción: <?= date("m/d/Y", strtotime($info_pedido['fecha_transaccion'])) ?></span>
                                        <?php endif ?>
                                        <span class="row-data">Valor pagado: $ <?= number_format($info_pedido['total_pagado'], 0, ',', '.') ?></span>
                                        <?php if ($info_pedido['medio_de_pago'] != 'transferencia'): ?>
                                            <span class="row-data">Código de autorización: <?= $info_pedido['auth_code'] ?></span>
                                        <?php endif ?>
                                        <span class="row-data">Tipo de moneda: CLP / Peso Chileno</span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="title">Totales</div>
                                    <div class="dbody">
                                        <span class="row-data">Subtotal <small class="numero">$<?= number_format($info_pedido['subtotal'], 0, ',', '.') ?></small></span>
                                        <span class="row-data">Descuentos <small class="numero">$<?= number_format($info_pedido['descuento'], 0, ',', '.') ?></small></span>
                                        <span class="row-data">Envío <small class="numero">$<?= number_format($info_pedido['despacho'], 0, ',', '.') ?></small></span>

                                        <span class="row-data mt-10">Total cancelado <small class="numero">$<?= number_format($info_pedido['total_pagado'], 0, ',', '.') ?></small></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php endforeach;
            else: ?>
                <p>Aún no tienes pedidos</p>
            <?php endif ?>    
            
        </div>
        <div class="clearfix"></div>
        <div class="mb-30"></div>
    </div>
</div>

<div class="bgpopcart"></div>
<div class="pop-cart">
    <a href="javascript:void(0)" class="close_pop"><img src="img/close_pop.png"></a>
    <div class="title_pop">
        ¡añadido al carrito correctamente!
    </div>

    <div class="grid_pop">
        
    </div>

    <div class="buttons_pop">
        <a href="javascript:void(0)" class="seguirComprando">Seguir comprando</a>
        <a href="mi-carro">Finalizar compra</a>
    </div>
</div>