<?php
$popup = consulta_bd('activo', 'active_popup', '', '');
$nombre_cliente = opciones('nombre_cliente');
$nombre_popup = 'POPUP' . strtoupper(str_replace(' ', '', $nombre_cliente));
if ($popup[0][0] == 1 and !isset($_COOKIE[$nombre_popup])) { ?>
    <script>
        setTimeout(function() {
            $('.bg-popup').fadeIn();
            $('.popup-news').fadeIn();
        }, 2000)
    </script>
<?php }

?>
<div class="bg-popup" data-cliente="<?= $nombre_popup ?>">
    <div class="popup-news">
        <div class="contenido">
            <div class="close"></div>
            <h3 style="text-align: center;">ACÁ EL LOGO</h3>
            <p>¡Suscríbete a nuestro newsletter y <br>entérate primero de nuestras ofertas, novedades y más!</p>
            <div class="datos">
                <input type="text" name="nombre" placeholder="Nombre" id="nombre-suscribe">
                <input type="email" name="email" placeholder="Email" id="email-suscribe">
                <a href="#" id="suscribe-newsletter">SUSCRIBIRME</a>
                <h3 style="text-align: center;">LOGO PEQUEÑO</h3>
            </div>
        </div>
    </div>
</div>

<!-- <div class="barra_promocion">
    <div class="texto">Artista del mes: Cómpralo con 10% de descuento | CÓDIGO</div>
</div> -->

<?php $slider = consulta_bd('link, imagen, imagen_tablet, imagen_movil', 'slider', 'publicado = 1', 'posicion asc'); ?>
<section id="slider">
    <div class="slider-home">
        <?php foreach ($slider as $sl) : ?>
            <div class="item">
                <a href="<?= $sl[0] ?>" class="s-desktop">
                    <img src="<?= imagen("imagenes/slider/", $sl[1]); ?>" class="desktop-slider">
                    <img src="<?= imagen("imagenes/slider/", $sl[2]); ?>" class="tablet-slider">
                    <img src="<?= imagen("imagenes/slider/", $sl[3]); ?>" class="movil-slider">
                </a>
            </div>
        <?php endforeach ?>
    </div>
    <?php if (sizeof($slider) > 1) : ?>
        <div class="slider-progress">
            <div class="progress"></div>
        </div>
    <?php endif; ?>
</section>

<div class="bg-clean pb-40 xs_pb-20">
    <div class="title-fine">THE FINE ART CONNECTION</div>

    <?php $banners_primarios = consulta_bd('bp.imagen, bp.categoria_id, c.nombre', 'banner_primarios bp JOIN categorias c ON c.id = bp.categoria_id', '', 'bp.posicion asc'); ?>

    <div class="container">
        <div class="titulo-home">tendencias</div>

        <div class="grid-banner">
            <?php foreach ($banners_primarios as $bp) : ?>
                <div class="col">
                    <a href="categorias/<?= $bp[1] ?>/<?= url_amigables($bp[2]) ?>">
                        <img src="<?= imagen("imagenes/banner_primarios/", $bp[0]); ?>">
                    </a>

                    <a href="categorias/<?= $bp[1] ?>/<?= url_amigables($bp[2]) ?>" class="textos">
                        <span class="title"><?= $bp[2] ?></span>
                        <span class="ver_mas">Ver más</span>
                    </a>
                </div>
            <?php endforeach ?>
        </div>

    </div>

</div>

<div class="bg-dark pb-40 pt-40">
    <div class="container relativo">
        <div class="titulo-home f-left">temas</div>

        <div class="filter-home-xs"><span>Filtros</span> <img src="img/icons/arrow-bottom.png" alt="Filtros"></div>
        <div class="cerrar-filtros-home"><img src="img/icons/arrow-bottom.png" alt="Cerrar Filtros"></div>
        <div class="content_nav">
            <div class="nav-slider-home-2">
                <?php $categoriasDestacadas = consulta_bd('id, nombre, linea_id', 'categorias', "recomendada = 1", 'posicion asc');

                $linea_home = consulta_bd('id, nombre', 'lineas', "id = {$categoriasDestacadas[0][2]}", '');

                foreach ($categoriasDestacadas as $catdes) { ?>
                    <li><a href="categorias/<?= $catdes[0] ?>/<?= url_amigables($catdes[1]) ?>"><?= $catdes[1] ?></a></li>
                <?php } ?>
            </div>

            <a href="lineas/<?= $linea_home[0][0] ?>/<?= url_amigables($linea_home[0][1]) ?>" class="btn-ver-temas">ver más</a>
        </div>

        <div class="clearfix"></div>
        <?php $banner_secundario = consulta_bd('bs.imagen, bs.categoria_id, c.nombre', 'banner_secundarios bs JOIN categorias c ON bs.categoria_id = c.id', '', 'bs.posicion asc'); ?>
        <div class="content_display">
            <div class="box-slider">
                <div class="content_box">
                    <?php foreach ($banner_secundario as $bs) :
                        $url_bs = 'categorias/' . $bs[1] . '/' . url_amigables($bs[2]); ?>
                        <div class="col">
                            <a href="<?= $url_bs ?>">
                                <img src="<?= imagen("imagenes/banner_secundarios/", $bs[0]); ?>" />
                            </a>

                            <a href="<?= $url_bs ?>" class="textos">
                                <span class="title"><?= $bs[2] ?></span>
                                <span class="ver_mas">Ver más</span>
                            </a>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php

$artistas = consulta_bd('id, nombre, banner_secundario', 'artistas', 'recomendado = 1', '');

?>

<div class="bg3 pb-40 pt-40">

    <div class="container">
        <div class="titulo-home">artistas</div>
    </div>

    <div class="content_slider">
        <div class="owl-carousel">
            <?php foreach ($artistas as $art) :
                $url = 'artista/' . $art[0] . '/' . url_amigables($art[1]);
                $imgArtista = ($art[2] != null and $art[2] != '' and is_file("imagenes/artistas/{$art[2]}")) ? imagen("imagenes/artistas/", $art[2]) : imagen("img/", "sinFotoPrincipalArtista.jpg"); ?>
                <div class="item">
                    <a href="<?= $url ?>">
                        <img src="<?= $imgArtista ?>">

                        <div class="nombre-artista"><?= $art[1] ?></div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    </div>

    <div class="container">
        <a href="artistas" class="btn-artistas">Ver todos</a>
    </div>

</div>

<?php

$filtros_home['destacados'] = true;
$filtros_home['limite'] = 6;
$destacados = get_products($filtros_home);

?>

<div class="bg-dark pb-40 pt-40">
    <div class="container">
        <div class="titulo-home">Recomendados</div>

        <div class="grid-products grid-home mt-30">
            <?php foreach ($destacados['productos']['producto'] as $pr) :
                $url = "ficha/{$pr['id_producto']}/" . url_amigables($pr['nombre']); ?>
                <a href="<?= $url ?>" class="col">
                    <div class="thumbg">
                        <img src="<?= $pr['imagen_grilla'] ?>">
                    </div>
                    <div class="add_fav" onclick="addToFav(<?= $pr['id_hijo'] ?>, event, this)">
                        <?php if (productoGuardado($pr['id_hijo'])) : ?>
                            <i class="material-icons">favorite</i>
                        <?php else : ?>
                            <i class="material-icons">favorite_border</i>
                        <?php endif ?>
                    </div>
                    <div class="data">
                        <div class="titles">
                            <span class="nombre"><?= $pr['nombre'] ?></span>
                            <span class="artista"><?= $pr['artista'] ?></span>
                        </div>
                        <div class="precio">
                            <span>Desde</span>
                            <?php if ($pr['descuento'] > 0) : ?>
                                <span><?= cambiarMoneda($pr['descuento']) ?></span>
                            <?php else : ?>
                                <span><?= cambiarMoneda($pr['precio']) ?></span>
                            <?php endif ?>

                        </div>
                    </div>

                    <div class="box_vacio">

                    </div>
                </a>
            <?php endforeach; ?>
        </div>

    </div>
</div>

<div class="bg-clean pb-40 pt-40">
    <div class="container content-news">
        <div class="center-news">
            <div class="title">Newsletter</div>
            <input type="text" name="email" class="email-news" placeholder="Ingresa tu email...">
            <a class="btn" id="btnNewsletterHome">suscríbete</a>
        </div>
    </div>
</div>