<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">Servicio al cliente</div>
    </div>
</div>

<?php $op2 = mysqli_real_escape_string($conexion, $_GET['op2']); ?>

<div class="gray-body">
	<div class="subtitulo-sac">
		<?php switch ($op2) {
			case 'despachos':
				echo 'Despacho a domicilio de blackpix';
			break;
			case 'cambios-y-devoluciones':
				echo 'Cambios y devoluciones de blackpix';
			break;
			case 'terminos-y-condiciones':
				echo 'Términos y condiciones de blackpix';
			break;
			case 'politicas-de-privacidad':
				echo 'Políticas de privacidad de blackpix';
			break;
		} ?>
	</div>
	<div class="container grid-servicios mt-30">
		<div class="col">
			<?php include('menuServicioCliente.php'); ?>
		</div>

		<div class="content-body">
			<?php include("servicio_al_cliente/{$op2}.php"); ?>
		</div>
	</div>
</div>