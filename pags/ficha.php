<?php
$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
$nombre = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;

$producto = consulta_bd('p.nombre, a.nombre, p.año, p.descripcion, a.id, p.descripcion2', 'productos p JOIN artistas a ON a.id = p.artista_id', "p.id = $id and p.publicado = 1", '');
$minProduct = consulta_bd("id, IF(descuento > 0, descuento, precio) as orden", "productos_detalles", "producto_id = $id", "orden asc");
$categoria_producto = consulta_bd('c.id', 'lineas_productos lp JOIN categorias c ON c.id = lp.categoria_id', "lp.producto_id = $id", '');

foreach ($categoria_producto as $cp) {
    if ($explode_categorias == '') {
        $explode_categorias = $cp[0];
    } else {
        $explode_categorias .= '-' . $cp[0];
    }
}

$ediciones = consulta_bd('pd.id, pd.nombre, t.nombre, pd.stock', 'productos_detalles pd JOIN tamaños t ON t.id = pd.tamaño_id', "pd.producto_id = $id and pd.publicado = 1", '');

$imagenes_ficha = consulta_bd('archivo', 'img_productos', "producto_id = $id", 'posicion asc');

if (!is_array($producto) and !is_array($ediciones)) {
    echo '<script>location.href="404";</script>';
}

$descripcion_2 = $producto[0][5];
?>

<div class="breadcrumbs">
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="lineas/1/ediciones-limitadas" itemprop="item" class="active">
                <span itemprop="name">Ediciones limitadas</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="ficha/<?= $id ?>/<?= $nombre ?>" itemprop="item" class="active">
                <span itemprop="name"><?= $producto[0][0] ?></span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
    </ul>
</div>

<div class="bgpopcart"></div>
<div class="pop-cart">
    <a href="javascript:void(0)" class="close_pop"><img src="img/close_pop.png"></a>
    <div class="title_pop">
        ¡añadido al carrito correctamente!
    </div>

    <div class="grid_pop">

    </div>

    <div class="buttons_pop">
        <a href="javascript:void(0)" class="seguirComprando">Seguir comprando</a>
        <a href="mi-carro">Finalizar compra</a>
    </div>
</div>




<div class="container">
    <div class="grid-ficha">
        <div class="col">
            <div class="slider-ficha">
                <?php if (is_array($imagenes_ficha)) : ?>
                    <?php foreach ($imagenes_ficha as $imgf) :
                        if (is_file("imagenes/productos/{$imgf[0]}")) : ?>
                            <div class="item">
                                <img src="<?= imagen("imagenes/productos/", $imgf[0]); ?>">
                                <i class="fas fa-search-plus zoom_ficha"></i>
                            </div>
                        <?php else : ?>
                            <img src="<?= imagen("img/", "sinfotoficha.jpg"); ?>">
                            <i class="fas fa-search-plus zoom_ficha"></i>
                        <?php endif; ?>
                    <?php endforeach ?>
                <?php else : ?>
                    <div class="item">
                        <img src="<?= imagen("img/", "sinfotoficha.jpg"); ?>">
                        <i class="fas fa-search-plus zoom_ficha"></i>
                    </div>
                <?php endif ?>
            </div>

            <div class="slider-zoom">
                <div class="slider-ficha-zoom">
                    <?php if (is_array($imagenes_ficha)) : ?>
                        <?php foreach ($imagenes_ficha as $imgz) : ?>
                            <div class="item">
                                <img src="<?= imagen("imagenes/productos/", $imgz[0]); ?>">
                            </div>
                        <?php endforeach ?>
                    <?php else : ?>
                        <div class="item">
                            <img src="<?= imagen("img/", "sinfotoficha.jpg"); ?>">
                        </div>
                    <?php endif ?>
                </div>

                <div class="close-zoom"><img src="img/closezoom.png"></div>
            </div>
            <div class="bg_slider-zoom"></div>

            <div class="xs-display mb-10">
                <div class="nav-slider-ficha">
                    <?php if (is_array($imagenes_ficha)) : ?>
                        <?php foreach ($imagenes_ficha as $imgn) : ?>
                            <div class="item" style="background: url(<?= imagen("imagenes/productos/", $imgn[0]); ?>);"></div>
                        <?php endforeach ?>
                    <?php else : ?>
                        <div class="item" style="background: url(<?= imagen("img/", "sinfotoficha.jpg"); ?>);"></div>
                    <?php endif ?>
                </div>

                <div class="artista-link">
                    <a href="artista/<?= $producto[0][4] . "/" . url_amigables($producto[0][1]) ?>"><?= $producto[0][1] ?></a>
                </div>
            </div>
        </div>
        <div class="col col-info">

            <div class="titulo"><?= $producto[0][0] ?></div>
            <div class="artista"><a href="artista/<?= $producto[0][4] . "/" . url_amigables($producto[0][1]) ?>"><?= $producto[0][1] ?></a></div>
            <div class="anio"><?= $producto[0][2] ?></div>

            <div class="short-description"><?= strip_tags($producto[0][3]) ?></div>

            <?php
            // Subproductos asociados a producto
            $subproductos = consulta_bd(
                "sp.id, sp.nombre, t.nombre, t.id, pd.stock, pd.comprados",
                "productos_detalles pd 
                            JOIN subproductos sp ON sp.id = pd.subproducto_id
                            JOIN tamaños t ON t.id = pd.tamaño_id",
                "pd.producto_id = {$id} 
                        GROUP BY sp.id",
                "sp.id asc"
            );
            ?>
            <div class="edicion-tab">
                <?php for ($i = 0; $i < sizeof($subproductos); $i++) { ?>
                    <div class="tab <?= ($i == 0) ? 'activado' : '' ?>" data-id="<?= $subproductos[$i][0] ?>" data-action="e<?= $num ?>" data-st="<?= $edicion[3] ?>"><?= $subproductos[$i][1] ?></div>
                <?php } ?>
            </div>

            <?php for ($i = 0; $i < sizeof($subproductos); $i++) : ?>
                <div class="content_tab <?= ($i == 0) ? 'activado' : '' ?>" id="tab-<?= $subproductos[$i][0] ?>" data-rel="<?= $subproductos[$i][0] ?>" data-action="">
                    <div class="grid-tt">
                        <div class="tamanio">
                            <span>tamaño imagen</span>
                            <span><?= $subproductos[$i][2]; ?></span>
                        </div>
                        <div class="tiraje"><span>tiraje:</span> <?= $subproductos[$i][5] ?>/<?= $subproductos[$i][4] ?></div>
                    </div>

                    <!-- Lista productos -->
                    <?php $productosHijoSub = consulta_bd(
                        "id, nombre, bajada, precio, descuento",
                        "productos_detalles",
                        "producto_id = {$id} and subproducto_id = {$subproductos[$i][0]} and tamaño_id = {$subproductos[$i][3]}",
                        'id asc'
                    ); ?>
                    <div class="list-radio">
                        <?php for ($j = 0; $j < sizeof($productosHijoSub); $j++) {
                            $checked_c = ($i == 0 and $j == 0) ? 'checked="checked"' : ''; ?>
                            <div class="row">
                                <input type="radio" name="radio_ediciones_a" class="radio_ediciones" id="ps-<?= $productosHijoSub[$j][0]; ?>" value="<?= $productosHijoSub[$j][0]; ?>" <?= $checked_c ?>>
                                <label for="ps-<?= $productosHijoSub[$j][0]; ?>"><?= $productosHijoSub[$j][1]; ?>
                                    <?php if ($productosHijoSub[$j][2] != null and trim($productosHijoSub[$j][2]) != '') : ?>
                                        <span><?= strip_tags($productosHijoSub[$j][2]) ?></span>
                                    <?php endif ?>
                                </label>
                                <div class="precio_ficha">
                                    <?php if ($productosHijoSub[$j][4] > 0) : ?>
                                        <span class="descuento"><?= cambiarMoneda($productosHijoSub[$j][3]) ?></span>
                                        <span class="precio_final"><?= cambiarMoneda($productosHijoSub[$j][4]) ?></span>
                                    <?php else : ?>
                                        <span class="precio_final"><?= cambiarMoneda($productosHijoSub[$j][3]) ?></span>
                                    <?php endif ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php endfor; ?>

            <div class="content-marcos <?= (strtolower($ediciones[0][1]) == 'obra impresa') ? 'disnone' : '' ?>">
                <div class="titulo-marcos ">Tipo de marco:</div>
                <div class="opciones-marco">
                    <div class="opcion">
                        <input type="radio" name="radio_marcos" class="radio-marcos" value="negro" id="r-negro" checked>
                        <label for="r-negro">Negro</label>
                    </div>
                    <div class="opcion">
                        <input type="radio" name="radio_marcos" class="radio-marcos" value="madera" id="r-madera">
                        <label for="r-madera">Madera</label>
                    </div>
                    <div class="opcion">
                        <input type="radio" name="radio_marcos" class="radio-marcos" value="blanco" id="r-blanco">
                        <label for="r-blanco">Blanco</label>
                    </div>
                </div>
            </div>

            <div class="short-description"><?= strip_tags($descripcion_2) ?></div>
        </div>
    </div>

    <div class="grid-ficha-bottom">
        <div class="col">
            <div class="desk-display">
                <div class="nav-slider-ficha">
                    <?php if (is_array($imagenes_ficha)) : ?>
                        <?php foreach ($imagenes_ficha as $imgn) : ?>
                            <div class="item" style="background: url(<?= imagen("imagenes/productos/", $imgn[0]); ?>);"></div>
                        <?php endforeach ?>
                    <?php else : ?>
                        <div class="item" style="background: url(<?= imagen("img/", "sinfotoficha.jpg"); ?>);"></div>
                    <?php endif ?>
                </div>

                <div class="artista-link">
                    <a href="artista/<?= $producto[0][4] . "/" . url_amigables($producto[0][1]) ?>"><?= $producto[0][1] ?></a>
                </div>
            </div>
        </div>

        <div class="col grid-options">
            <div class="whishlist_ficha" onclick="addToFavFicha(<?= $minProduct[0][0] ?>, event, this)">
                <?php if (productoGuardado($minProduct[0][0])) : ?>
                    <img src="img/heartcheck.png">
                <?php else : ?>
                    <img src="img/heartficha.png" />
                <?php endif ?>
            </div>

            <a href="javascript:void(0)" class="btn-compra" data-st="<?= $ediciones[0][3] ?>" data-id="<?= $ediciones[0][0] ?>">agregar al carro</a>

            <div class="box_main_qty">
                <div class="box_qty" data-id="1">

                    <input type="text" class="cantidad_ficha" value="1" readonly>

                </div>

                <div class="box_select_stock" data-visible="0">
                    <?php if ($ediciones[0][3] > 10) : ?>
                        <?php for ($i = 0; $i < 10; $i++) { ?>
                            <div class="row_stock_ficha" data-val="<?= $i + 1 ?>"><?= $i + 1 ?></div>
                        <?php } ?>

                        <div class="row_stock_ficha" data-val="add">+ más</div>
                    <?php else : ?>
                        <?php for ($i = 0; $i < $ediciones[0][3]; $i++) { ?>
                            <div class="row_stock_ficha" data-val="<?= $i + 1 ?>"><?= $i + 1 ?></div>
                        <?php } ?>
                    <?php endif ?>


                </div>
            </div>
        </div>
    </div>

    <div class="border_title mt-50 mb-50 xs840-none">fotografías relacionadas</div>

    <?php
    $filtros_rel['categorias'] = $explode_categorias;
    $filtros_rel['limite'] = 4;
    $filtros_rel['id_ficha'] = $id;
    $filtros_rel['random'] = true;

    $productosRelacionados = get_products($filtros_rel);
    ?>

    <div class="grid-products mt-30 grid-relacionados xs840-none">
        <?php foreach ($productosRelacionados['productos']['producto'] as $prd) : ?>
            <a href="ficha/<?= $prd['id_producto'] ?>/<?= $prd['nombre_seteado'] ?>" class="col">
                <div class="thumbg">
                    <img src="<?= $prd['imagen_grilla'] ?>" />
                </div>
                <div class="add_fav" onclick="addToFav(<?= $prd['id_hijo'] ?>, event, this)">
                    <?php if (productoGuardado($prd['id_hijo'])) : ?>
                        <i class="material-icons">favorite</i>
                    <?php else : ?>
                        <i class="material-icons">favorite_border</i>
                    <?php endif ?>
                </div>
                <?php if ($prd['descuento'] > 0) : ?>
                    <span class="descuentoGrilla"><?= round(100 - ($prd['descuento'] * 100) / $prd['precio']) ?>% DCTO</span>
                <?php endif ?>
                <div class="data">
                    <div class="titles">
                        <span class="artista"><?= $prd['artista'] ?></span>
                        <span class="nombre"><?= $prd['nombre'] ?></span>
                    </div>
                    <div class="precio">
                        <?php if ($prd['descuento'] > 0) : ?>
                            <span class="oferta"><?= cambiarMoneda($prd['precio']) ?></span>
                            <div class="precio_final">
                                <span>Desde</span>
                                <span><?= cambiarMoneda($prd['descuento']) ?></span>
                            </div>
                        <?php else : ?>
                            <span class="oferta"></span>
                            <div class="precio_final">
                                <span>Desde</span>
                                <span><?= cambiarMoneda($prd['precio']) ?></span>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </a>
        <?php endforeach ?>
    </div>

    <?php $artistas = consulta_bd('DISTINCT(id), nombre, thumb', 'artistas', '', "RAND() LIMIT 3"); ?>

    <div class="border_title mt-50 mb-50 xs840-none">artistas</div>

    <div class="grilla-artistas mb-30 xs840-none">
        <?php foreach ($artistas as $art) :
            $imagenArtista = ($art[2] != null  and trim($art[2]) != '' and is_file("imagenes/artistas/{$art[2]}")) ? imagen('imagenes/artistas/', $aux[2]) : imagen('img/', "sinFotoArtista.jpg"); ?>
            <div class="col">
                <a href="artista/<?= $art[0] ?>/<?= url_amigables($art[1]) ?>">
                    <img src="<?= $imagenArtista ?>">

                    <span class="nombre"><?= $art[1] ?></span>
                </a>
            </div>
        <?php endforeach ?>
    </div>

</div>


<!-- <div>
    <div itemtype="http://schema.org/Product" itemscope>
        <meta itemprop="mpn" content="<?= $producto[0][8] ?>" />
        <meta itemprop="name" content="<?= $producto[0][7] ?>" />
        <?php if (is_array($galeria)) : ?>
            <?php for ($i = 0; $i < sizeof($galeria); $i++) { ?>
                <link itemprop="image" href="imagenes/productos/<?= $galeria[$i][0] ?>" />
            <?php } ?>
        <?php endif ?>
        <meta itemprop="description" content="<?= strip_tags($producto[0][17]) ?>" />
        
        <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
            <link itemprop="url" href="<?= $url_base ?>ficha/<?= $id ?>/<?= $nombre ?>" />
            <?php if ($producto[0][14] > 0) : ?>
            <meta itemprop="availability" content="https://schema.org/InStock" />
            <?php else : ?>
            <meta itemprop="availability" content="https://schema.org/OutOfStock" />
            <?php endif ?>
            <meta itemprop="priceCurrency" content="CLP" />
            <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
            <meta itemprop="price" content="<?= $precio_final ?>" />
        </div>
        <meta itemprop="sku" content="<?= $producto[0][8] ?>" />
        <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
            <meta itemprop="name" content="<?= $producto[0][18] ?>" />
        </div>
    </div> 
</div> -->