<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo '<script>location.href = "inicio-sesion";</script>';
	}
?>

<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">mi cuenta</div>
    </div>
</div>

<div class="gray-body">
  <div class="container">
    <div class="text_ident">podrás ver tus pedidos, historial de compra y <br>
editar tus datos personales y de envío</div>
      <?php include("pags/menuMiCuenta.php"); ?>

      <?php $cliente = consulta_bd("id, nombre, telefono, email","clientes","id=".$_COOKIE['usuario_id'],""); ?>    

      <div class="cont_bodydash max-dash">
        <h3 class="subtitulo">Datos personales</h3>
        <form action="ajax/actualizarDatos.php" method="post" id="formularioActualizar">

            <div class="form-group">
                <label>Nombre y apellido <small class="require">*</small></label>
                <input type="text" name="nombre" class="input-text" placeholder="Nombre y Apellido..." value="<?= $cliente[0][1] ?>">
            </div>

            <div class="double-form">
              <div class="form-group">
                <label>Email <small class="require">*</small></label>
                <input type="email" name="email" class="input-text" placeholder="Email..." value="<?= $cliente[0][3] ?>">
              </div>

              <div class="form-group">
                <label>Teléfono <small class="require">*</small></label>
                <input type="text" name="telefono" class="input-text" placeholder="Teléfono..." onkeypress="return justNumbers(event);" maxlength="9" value="<?= $cliente[0][2] ?>">
              </div>
            </div>

            <div class="contBtn">
              <a href="javascript:void(0)" class="change_password">Cambiar contraseña</a>
              <button class="btnFormRegistro">Actualizar datos</button>
            </div>
            
        </form>
      </div>
      
      <div class="clearfix"></div>
      <div class="mb-30"></div>
  </div>
</div>

<div class="bg_password"></div>
<div class="content_password">
  <div class="close_password"><img src="img/close_pop.png"></div>
  <h2>Cambiar contraseña</h2>

  <p>Evita usar contraseñas que hayas utilizado en otros sitios web o que se puedan adivinar fácilmente.</p>
  
  <div class="form-group">
    <label class="dark-label">Contraseña actual</label>
    <input type="password" name="actual_password" class="input-text input-white">
  </div>

  <div class="form-group">
    <label class="dark-label">Contraseña nueva</label>
    <input type="password" name="nueva_password" class="input-text input-white">
  </div>

  <div class="form-group">
    <label class="dark-label">Repetir contraseña nueva</label>
    <input type="password" name="r_nueva_password" class="input-text input-white">
  </div>

  <a href="javascript:void(0)" class="btn btnUpdatePasswordPost">Cambiar</a>

</div>

<div class="pop_success">
  <div class="close_password"><img src="img/close_pop.png"></div>
  <p>Contraseña cambiada con éxito</p>

  <a href="javascript:void(0)" class="btnAceptarPopup">Aceptar</a>
</div>