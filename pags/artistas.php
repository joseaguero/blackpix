<?php

require_once 'paginador/paginator.class.php';

$artistas = consulta_bd('DISTINCT(id), nombre, thumb', 'artistas', 'publicado=1', '');
$total = count($artistas);

$pages = new Paginator;
$pages->items_total = $total;
$pages->mid_range = 8;
$rutaRetorno = "artistas";
$pages->paginateArtistas($rutaRetorno);

$artistas = consulta_bd('DISTINCT(id), nombre, thumb', 'artistas', 'publicado=1', "posicion asc $pages->limit");

$slider = consulta_bd('id, nombre, banner_principal', 'artistas', 'recomendado = 1', '');

?>

<h2 class="tituloArtistas">Artistas</h2>

<div class="container mb-40">

    <div id="slider2">
        <div class="slider-artistas">
            <?php foreach ($slider as $sl) : ?>
                <div class="item">
                    <a href="artista/<?= $sl[0] ?>/<?= url_amigables($sl[1]) ?>" class="s-desktop">
                        <?php if ($sl[2] != null and $sl[2] != '' and is_file("imagenes/artistas/{$sl[2]}")) : ?>
                            <img src="<?= imagen("imagenes/artistas/", $sl[2]); ?>">
                        <?php else : ?>
                            <img src="<?= imagen("img/", "sinBannerArtista.jpg"); ?>">
                        <?php endif; ?>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    </div>

</div>

<div class="gray-body">
    <div class="container mt-40 mb-40">

        <div class="grilla-artistas">
            <?php foreach ($artistas as $art) :
                $imagenArtista = ($art[2] != null  and trim($art[2]) != '' and is_file("imagenes/artistas/{$art[2]}")) ? imagen('imagenes/artistas/', $art[2]) : imagen('img/', "sinFotoArtista.jpg"); ?>
                <div class="col">
                    <a href="artista/<?= $art[0] ?>/<?= url_amigables($art[1]) ?>">
                        <img src="<?= $imagenArtista ?>">
                        <span class="nombre"><?= $art[1] ?></span>
                    </a>
                </div>
            <?php endforeach ?>
        </div>

        <div class="paginador-productos paginador-artistas">

            <div class="paginas">
                <?= $pages->display_pages(); ?>
            </div>
        </div>

    </div>

</div>