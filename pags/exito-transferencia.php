<?php
$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
$PA = consulta_bd("id, estado_id, oc, medio_de_pago", "pedidos", "oc = '$oc'", "");
$cant = mysqli_affected_rows($conexion);

$campos = "id,nombre,direccion,email,telefono,"; //0-4
$campos .= "regalo,total,valor_despacho,"; //5-8
$campos .= "mp_transaction_date, mp_paid_amount, mp_card_number, mp_cuotas, mp_auth_code, mp_payment_type,cliente_id,";
$campos .= "comuna,region,ciudad,estado_id,"; //15-18
$campos .= "factura, direccion_factura, giro, email_factura, rut_factura, fecha_modificacion, region,ciudad,estado_id, rut,retiro_en_tienda, retiro_en_tienda_id, region_factura, comuna_factura,fecha_creacion"; //19-30

$pedido = consulta_bd($campos, "pedidos", "oc='$oc'", "");

//detalle comprador
$direccion_cliente = $pedido[0][2];
$comuna_cliente    = $pedido[0][15];
$region_cliente    = $pedido[0][16];

//detalle pedido
$pedido_id      = $pedido[0][0];
$total_pedido      = $pedido[0][6];
$total_despacho   = $pedido[0][7];
$fecha        = date('d/m/Y | H:i:s', time($pedido[0][8]));
$hora_pago       = date('H:s:i', time($pedido[0][8]));

//USER
$id_usuario     = $pedido[0][14];
//$info = info_user($id_usuario);

$esFactura = $pedido[0][19];
$direccionFactura = $pedido[0][20];
$giro = $pedido[0][21];
$rutFactura = $pedido[0][23];
$regionFactura = $pedido[0][31];
$comunaFactura = $pedido[0][32];

//total
$total_productos = $pedido[0][6];
$total_pagado = number_format($pedido[0][9], 0, ",", ".");

$retiroEnTienda = $pedido[0][29];
$sucursal = $pedido[0][30];

//detalle productos
$detalle_productos = consulta_bd("productos_detalle_id, cantidad, precio_unitario", "productos_pedidos", "pedido_id=" . $pedido[0][0], "");
?>


<div class="bread_carro">
  <div class="container_carro">
    <div class="list activo center-text">transacción exitosa</div>
  </div>
</div>

<div class="texto-exito">
  <div class="center-content">
    <div class="title">muchas gracias por comprar en blackpix</div>
    <div class="oc">
      su número de orden de compra es <br>
      <?= $_GET['oc'] ?>
    </div>

    <div class="botonesExito">
      <a href="tienda/voucher/voucherCompra.php?oc=<?php echo $oc; ?>" target="_blank">Ver voucher de compra</a>
      <a href="home">Seguir comprando</a>
    </div>
  </div>
  <div style="text-align: center; padding: 20px 0; margin: auto; width: 80%; font-size: 14px;">
    A continuación encontrarás los datos para que realices la transferencia antes de 48 horas, después de ese periodo se anulara el pedido. <br> <br>

    En el caso que el producto incluya despacho pagado o por pagar el o los productos serán enviados 24 horas después de haber realizado el pago, previa confirmación de la transacción. En el caso que la transacción se realice un día viernes, fin de semana o feriado el despacho se realizara en los primeros días hábiles siguientes. <br> <br>

    Media & Pix Spa<br>
    Banco Security<br>
    Tipo de cuenta: Cuenta Corriente <br>
    Cuenta: 919138002 <br>
    RUT: 76.699.358-3 <br>
    Correo: mediaypix@gmail.com <br>
  </div>

  <div class="clearfix"></div>
</div>

<div class="gray-body pd-30up">
  <?php if (!$_COOKIE['usuario_id']) { ?>
    <div class="center-registroExito">

      <?php $email = $pedido[0][3];
      $cliente = consulta_bd("id", "clientes", "email = '$email' and activo = 0", "");
      if (is_array($cliente)) { ?>
        <h2>¿Aún no tienes tu cuenta?</h2>
        <p>puedes crear una cuenta con nosotros para una futura compra, revisar el estado de tus despachos y revisar tus boletas.</p>
        <form method="post" id="crearCuentaDesdeExito" action="crear-cuenta-exito">
          <input type="hidden" name="cliente_id" value="<?= $cliente[0][0]; ?>" />
          <input class="btnCuentaExito" type="submit" name="crearCuenta" value="CREAR CUENTA" />
        </form>
      <?php } ?>
    </div>
  <?php } ?>

  <div class="container">

    <div class="three-gridExito">

      <div class="col">
        <div class="title">Información Personal</div>

        <div class="data"><span>Nombre:</span> <?= $pedido[0][1] ?></div>
        <div class="data"><span>Teléfono:</span> <?= $pedido[0][4] ?></div>
        <div class="data"><span>Email:</span> <?= $pedido[0][3] ?></div>

      </div>

      <div class="col">
        <?php if ($retiroEnTienda == 0) : ?>
          <div class="title">Información de envío</div>

          <div class="data"><span>Dirección:</span> <?= $direccion_cliente ?>, <?= ucwords(strtolower($comuna_cliente)) ?>, <?= $region_cliente ?></div>
        <?php else : ?>
          <div class="title">Información de retiro</div>
          <?php $datosSucursal = consulta_bd('s.nombre, s.direccion, c.nombre, r.nombre', 'sucursales s JOIN comunas c ON c.id = s.comuna_id JOIN regiones r ON r.id = c.region_id', ''); ?>
          <div class="data"><span>Sucursal:</span> <?= $datosSucursal[0][0] ?></div>
          <div class="data"><span>Dirección:</span> <?= $datosSucursal[0][1] ?>, <?= ucwords(strtolower($datosSucursal[0][2])) ?>, <?= $datosSucursal[0][3] ?></div>
        <?php endif ?>


      </div>

      <div class="col">
        <?php if ($esFactura == 1) : ?>
          <div class="title">Información de facturación</div>

          <div class="data"><span>Nombre:</span> <?= $pedido[0][1] ?></div>
          <div class="data"><span>Dirección:</span> <?= $direccionFactura ?>, <?= $comunaFactura ?>, <?= $regionFactura ?></div>
          <div class="data"><span>Rut:</span> <?= $rutFactura ?></div>
          <div class="data"><span>Giro:</span> <?= $giro ?></div>
        <?php endif ?>


      </div>
    </div>
  </div>

</div>

<!-- <script type="text/javascript">  
<?php
/*for ($i=0; $i <sizeof($detalle_productos) ; $i++) {
		$pro_id = $detalle_productos[$i][0]; 
		$details_pro = consulta_bd("nombre,producto_id,id","productos_detalles","id=$pro_id","");
		$total_item = round($detalle_productos[$i][2])*$detalle_productos[$i][1];
		
		$marca = consulta_bd("m.nombre","productos p, marcas m","p.id=".$details_pro[0][1]." and p.marca_id = m.id","");
		$categoria = consulta_bd("sc.nombre","lineas_productos cp, categorias c, subcategorias sc","cp.producto_id=".$details_pro[0][1]." and cp.categoria_id = c.id and sc.id = cp.subcategoria_id and cp.subcategoria_id <> ''","");*/

?>							
    ga('ec:addProduct', {
      'id': '<?= $details_pro[0][2]; ?>',
      'name': '<?= $details_pro[0][0]; ?>',
      'category': '<?= $categoria[0][0] ?>',
      'brand': '<?= $marca[0][0] ?>',
      'variant': '<?= $details_pro[$i][0]; ?>',
      'price': <?= round($detalle_productos[$i][2]); ?>,
      'quantity': <?= $detalle_productos[$i][1]; ?>
    });
<?php // } 
?>

// Transaction level information is provided via an actionFieldObject.
ga('ec:setAction', 'purchase', {
  'id': '<?= $oc; ?>',
  'affiliation': '<?= opciones("nombre_cliente"); ?>',
  'revenue': <?= round($pedido[0][6]); ?>,
  'tax': <?= round(($pedido[0][6] / 1.19) * 0.19); ?>,
  'shipping': <?= $total_despacho; ?>
});


//ga('ec:setAction','checkout', {'step': 4});
</script> -->