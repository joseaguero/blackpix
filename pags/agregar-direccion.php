<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "inicio-sesion";</script>';
    }
?>

<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">mi cuenta</div>
    </div>
</div>

<div class="gray-body">
    <div class="container">
        <div class="text_ident">podrás ver tus pedidos, historial de compra y <br>
        editar tus datos personales y de envío
        </div>

        <?php include("pags/menuMiCuenta.php"); ?>

        <div class="cont_bodydash max-dash">
        	<h3 class="subtitulo">Agregar nueva dirección

                <a href="mis-direcciones" class="btnVolverDir">Volver</a></h3>

        	<div class="form-group">
                <label>Nombre de la dirección <small class="require">*</small></label>
                <input type="text" name="nombre" class="input-text nombreDir" placeholder="Nombre...">
            </div>

            <div class="form-group">
                <label>País <small class="require">*</small></label>
                <?php $paises = consulta_bd('id, nombre', 'paises', "", 'id asc'); ?>
                <select class="select-form slPaisDash" name="paisDash">
                    <option value="0">País</option>
                    <?php foreach ($paises as $pais): ?>
                        <option value="<?= $pais[0] ?>"><?= $pais[1] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            
            <div class="content_dirDash">
                <div class="form-group">
                    <label>Región <small class="require">*</small></label>
                    <select class="select-form regionesDash" name="region">
                        <option value="0">Región</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Comuna <small class="require">*</small></label>
                    <select class="select-form comunasDash" name="comuna">
                        <option value="0">Comuna</option>
                    </select>
                </div>
            </div> 

            <div class="form-group">
                <label>Dirección <small class="require">*</small></label>
                <input type="text" name="direccion" class="input-text dirDash" placeholder="Dirección...">
            </div>

            <input type="hidden" name="action" class="actionDir" value="add">
            <input type="hidden" name="dirId" class="dirId" value="0">

            <a href="javascript:void(0)" class="agregarDireccionRe">Agregar</a>
        </div>

        <div class="clearfix"></div>
      	<div class="mb-30"></div>
    </div>
</div>

<div class="bg_dir"></div>
<div class="pop_success">
  <div class="close_password"><img src="img/close_pop.png"></div>
  <p>Direccion agregada con éxito</p>

  <a href="javascript:void(0)" class="btnAceptarPopup">Aceptar</a>
</div>