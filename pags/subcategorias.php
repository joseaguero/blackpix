<?php 
require_once 'paginador/paginator.class.php';

    $page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 0;
	$ipp = (isset($_GET[ipp])) ? mysqli_real_escape_string($conexion, $_GET[ipp]) : 4;
	
	$id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
	$nombreSubcategoria = (isset($_GET[nombre])) ? mysqli_real_escape_string($conexion, $_GET[nombre]) : 0;
	
	$id_marca = (isset($_GET[id_marca])) ? mysqli_real_escape_string($conexion, $_GET[id_marca]) : 0;
	$marca = (isset($_GET[marca])) ? mysqli_real_escape_string($conexion, $_GET[marca]) : 0;
	$orden = (isset($_GET[orden])) ? mysqli_real_escape_string($conexion, $_GET[orden]) : 0;
	$subcategorias = consulta_bd("c.id, c.nombre, c.linea_id, l.nombre, sc.nombre","categorias c, lineas l, subcategorias sc","l.id = c.linea_id and c.id=sc.categoria_id and sc.id = $id","");


	if($id_marca != 0){
		$whereSql = " and marca_id = $id_marca";
	} else {
		$whereSql;
	}
	if($orden === "valor-desc"){
		$orderSql = ' valorMenor desc';
		$nombreOrden = "Mayor precio";
	} else if($orden === "valor-asc"){
		$orderSql = ' valorMenor asc';
		$nombreOrden = "Menor precio";
	} else {
		$orderSql = "pp.posicion asc";
		$nombreOrden = "Más relevantes";
	}

$productosPaginador = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor", "productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN lineas_productos lp ON lp.producto_id = p.id JOIN subcategorias s ON s.id = lp.subcategoria_id LEFT JOIN posicion_productos pp ON p.id = pp.producto_id", "s.id = $id AND p.publicado = 1 $whereSql group by p.id", "$orderSql");
$total = mysqli_affected_rows($conexion);

$pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 8; 
	$rutaRetorno = "subcategorias/$id/$nombreSubcategoria/$id_marca/$marca/$orden";
    $pages->paginate($rutaRetorno);

    $productos = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor", "productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN lineas_productos lp ON lp.producto_id = p.id JOIN subcategorias s ON s.id = lp.subcategoria_id LEFT JOIN posicion_productos pp ON p.id = pp.producto_id", "s.id = $id AND p.publicado = 1 $whereSql group by p.id", "$orderSql $pages->limit");

    $cant_productos = mysqli_affected_rows($conexion);

?>


        
        <div class="bannerGrillas cont100">
            <div class="cont100Centro">
                <h1><?php echo $subcategorias[0][4]; ?></h1>
            </div>
        </div>

    
    
    
    <div class="cont100">
        <div class="cont100Centro">
        	<ul class="breadcrumb">
              <li><a href="home">Home</a></li>
              <li><a href="lineas/<?= $subcategorias[0][2]."/".url_amigables($subcategorias[0][3]); ?>"><?= $subcategorias[0][3]; ?></a></li>
              <li><a href="categorias/<?= $subcategorias[0][0]."/".url_amigables($subcategorias[0][1]); ?>"><?= $subcategorias[0][1]; ?></a></li>
              <li class="active"><?= $subcategorias[0][4]; ?></li>
            </ul>
        </div>
    </div><!--Fin breadcrumbs -->
    
    
    <div class="cont100">
        <div class="cont100Centro">
        	
            <!--contenedorFiltro orden -->
            <div class="filtros">
            	<div class="contenedorFiltro">
                	<span class="filtroActual"><?= $nombreOrden ?></span>
                    <ul>
                        <li class="<?php if($orden === 0){ echo 'filtroSeleccionado';} ?>">
                        	<a href="subcategorias/<?php echo $id."/".url_amigables($subcategorias[0][4]); ?>">Ordenar por relevancia</a>
                        </li>
                        <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                        	<a href="subcategorias/<?= $id."/".url_amigables($subcategorias[0][4])."/".$id_marca."/".$marca."/valor-desc"; ?>">Precio Mayor a Menor</a>
                        </li>
                        <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                        	<a href="subcategorias/<?= $id."/".url_amigables($subcategorias[0][4])."/".$id_marca."/".$marca."/valor-asc"; ?>">Precio Menor a Mayor</a>
                        </li>
                    </ul>
                </div><!--Fin contenedorFiltro orden -->
             </div>   
                
             
             <?php 
				$marcas = consulta_bd("id,nombre","marcas","","nombre asc");
				$cantMarcas = mysqli_affected_rows($conexion);
				
				$marca_actual = consulta_bd("id,nombre","marcas","id = $id_marca","nombre asc"); 
				$cantMarca = mysqli_affected_rows($conexion);
				if($cantMarca >  0){
					$marcaSeleccionada = $marca_actual[0][1];
					} 
					else {
						$marcaSeleccionada = "Filtrar por marca";
						}
					if($cantMarcas > 0){	
					?>   
             <div class="filtros">   
                <div class="contenedorFiltro"><!-- Filtro marcas -->
                	
                	<span class="filtroActual"><?= $marcaSeleccionada; ?></span>
                    <ul>
                    	<?php for($i=0; $i<sizeof($marcas); $i++){ ?>
                        
                        <li class="<?php if($id_marca == $marcas[$i][0]){ echo 'filtroSeleccionado';} ?>">
                        	<a href="subcategorias/<?= $id."/".url_amigables($subcategorias[0][4])."/".$marcas[$i][0]."/".url_amigables($marcas[$i][1])."/".$orden; ?>"><?php echo $marcas[$i][1]; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div><!-- Fin Filtro marcas -->
            </div><!--fin filtros-->
            <?php } ?>
            
        </div>
    </div><!--Fin contenido del sitio -->
    





	<div class="cont100">
        <div class="cont100Centro contGrillasProductos">
        	
            <?php for($i=0; $i<sizeof($productos); $i++){ ?>
                <div class="grilla">
                    <?php if(ultimasUnidades($productos[$i][6])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>

                    <?php if(ofertaTiempo($productos[$i][6])){ ?><div class="countdown" rel="<?= ofertaTiempoHasta($productos[$i][6]); ?>"></div><?php } ?>

                    <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                    <?php endif ?>

                	<a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla"></a>
                    <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                    <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">

                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                            $precios = get_cyber_price($productos[$i][6]); ?>
                            <span class="antes">$<?= number_format($precios['precio'],0,",",".") ?></span>
                            <span> - </span>
                            <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                        <?php else: ?>
                            <?php if(tieneDescuento($productos[$i][6])){ ?>
                                <span class="antes">$<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>
                                <span> - </span>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                            <?php }else{ ?>
                                $<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?>
                            <?php } ?>
                        <?php endif ?>

                    </a>
                </div>
            <?php } ?>
            
        </div>
    </div><!--Fin contenedor grillas -->
    
    
    
    <div class="cont100">
        <div class="cont100Centro paginador">
        	<?= $pages->display_pages(); ?>
        </div>
    </div><!--Fin paginador -->

