<h3>Condiciones Generales de Despacho</h3>

<p>• Al momento de realizar tu compra es importante que revises bien los datos de nombre, dirección y teléfonos de contacto. Esta información es fundamental para una correcta y oportuna entrega de tus productos en tu domicilio y/o lugar indicado.</p>
<p>• Si tienes consultas respecto de tu orden, puedes  enviarnos un correo a mediaypix@gmail.com. Para ello, debes indicarnos los antecedentes de la compra (boleta, factura, orden de compra internet o venta telefónica)
Entrega del producto</p>

<p>• Tus productos podrán ser despachados de Lunes a Viernes de 09:00 a 19:00 , de acuerdo a las condiciones pactadas al momento de la compra.</p>
<p>• Verifica que el producto corresponda a lo que compraste, y que se encuentra en perfectas condiciones antes de firmar la recepción.</p>
<p>• El personal de transporte no está autorizado ni capacitado para instalar, armar, intervenir o alterar los productos en el domicilio.</p>
<p>• En caso de disconformidad con el producto al momento de la entrega, recházalo, en este caso el transportista se llevará el producto para realizar el respectivo cambio. Envíanos un correo a mediaypix@gmail.com indicando razones del rechazo y datos de la compra.</p>
<p>• La reposición del producto se hará dentro de las próximas 5 días hábiles, en la misma dirección de despacho.</p>


<h3>Condiciones de la entrega</h3>

<p>• El lugar físico donde se realiza la entrega del despacho, debe tener acceso del medio de transporte o disponibilidad de estacionamiento.</p>
<p>• El servicio de despacho no considera instalación de productos.</p>
<p>• Tratándose de despachos en pisos superiores, solo se entregarán productos que cumplan con las siguientes condiciones: .- Deben asegurarse las condiciones físicas mínimas, que permitan desplazar los productos con embalaje, esto es escala adecuada para maniobrar según las dimensiones del producto, disponibilidad de ascensores para carga que disponga de las dimensiones para el traslado del producto. </p>