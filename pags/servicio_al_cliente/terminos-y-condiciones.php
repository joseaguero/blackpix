<p>Lamentablemente, tampoco podemos prescindir de una letra pequeña. Después de todo, estamos tratando con obras de arte sofisticadas y sensibles.</p>


<p>Comenzaríamos señalando que las fotografías siempre dependen de las influencias ambientales; su apariencia y color pueden cambiar y desvanecerse. Para asegurarse de seguir disfrutando de nuestras obras de arte, no debe exponer fotografías, laminaciones o marcos, ya sea en parte o en su totalidad, a la luz solar directa, colgarlos directamente sobre los aparatos de calefacción o exponerlos a un nivel de humedad más alto de lo esperado en Sala de estar normal. Por lo tanto, las fotografías, las láminas y los marcos no son adecuados para áreas externas, bodegas, baños o cocinas. Las laminaciones con película protectora UV o vidrio acrílico y marcos de madera son muy sensibles a los arañazos y la suciedad y, por lo tanto, solo deben manipularse con guantes o protección equivalente.</p>


<h3>PRIMERO: GENERALIDADES</h3>

<p>Este documento regula los términos y condiciones bajo los cuales Ud. tiene derecho a acceder y usar los servicios del sitio Web www.blackpix.cl y de cualquier información, texto, video u otro material comunicado en el sitio web. La edad mínima para la contratación del servicio es de 18 años cumplidos.</p>

<p>En este sitio Web podrá usar, sin costo, el software y las aplicaciones para equipos móviles que le permitan navegar, visitar, comparar y si lo desea, adquirir los bienes o servicios que se exhiben aquí.</p>

<p>Le recomendamos que lea detenidamente estas Condiciones e imprima o guarde una copia de las mismas en la unidad de disco local para su información.</p>

<p>Estos Términos y Condiciones serán aplicados y se entenderán incorporados en cada uno de los contratos que celebre con Media & Pix SPA por medio de este sitio web.</p>

<p>El uso de este sitio web, la aplicación de estos Términos y Condiciones, los actos que ejecute y los contratos que celebre por medio de este sitio web, se encuentran sujetos y sometidos a las leyes de la República de Chile y en especial a la ley 19.496 de protección de los derechos de los consumidores.</p>
	
<p>Media & Pix  SPA, por lo tanto, aplicará estrictamente todos los beneficios, garantías y derechos reconocidos en favor de los consumidores en la ley 19.496. Además, Media & Pix  SPA adhiere en todas sus partes al Código de Buenas Prácticas para el Comercio Electrónico de la Cámara de Comercio de Santiago, el cual se encuentra disponible en el siguiente link <a href="https://www.ccs.cl/prensa/publicaciones/CodigoBuenasPracticas_02.pdf">https://www.ccs.cl/prensa/publicaciones/CodigoBuenasPracticas_02.pdf</a></p>


<h3>SEGUNDO: COMUNICACIONES</h3>

<p>Para la comunicación de cualquier presentación, consulta o reclamo a propósito del uso de este sitio, o los contratos que en él se lleven a cabo, Media & Pix  SPA designa como representante especial a don León Errázuriz, Gerente Servicio al Cliente, disponible en el correo león.blackpix@gmail.com y en el teléfono de Servicio al Cliente +56 991628747 domiciliado para estos efectos en Cousin 0227, comuna de Providencia, ciudad de Santiago.</p>

<p>Media & Pix  SPA se obliga a que, en caso de enviarle información publicitaria o promocional, por correo electrónico, ésta contendrá al menos la siguiente información:</p>


<p>a) Identificación del mensaje, que lo haga reconocible como publicidad o comunicación promocional, en el campo de asunto del mensaje.</p>
<p>b) Existencia del derecho del consumidor o usuario a solicitar el cese de envíos de publicidad de ese tipo por Media & Pix  SPA y la obligación de Media & Pix  SPA de suspender esos envíos.</p>
<p>c) Un procedimiento simple y eficaz para que el consumidor pueda hacer esa solicitud, indicando una dirección electrónica para estos efectos.</p>
<p>d) La Identificación del anunciante con su denominación comercial.</p>


<p>Tratándose de publicidad teaser, se cumplirá con las obligaciones señaladas precedentemente en la medida que se identifique la agencia que realiza la misma.</p>
<p>Media & Pix  SPA cesará el envío de los mensajes publicitarios o promocionales por correo electrónico u otros medios de comunicación individual equivalentes, a toda persona que hubiere solicitado expresamente la suspensión de esos envíos.</p>
<p>Los precios publicados en Redes Sociales de Blackpix serán válidos solamente por 24 horas desde su publicación en redes sociales. Los precios publicados en la página web oficial de Blackpix siempre serán respetados según la normativa vigente.</p>


<h3>TERCERO: LIBERTAD DE NAVEGACIÓN y ACEPTACIÓN DE LOS TÉRMINOS Y CONDICIONES</h3>

<p>La mera visita de este sitio no impone ningún tipo de obligación para el usuario, a menos que éste exprese de forma inequívoca, por medio de actos positivos, su voluntad de contratar con la empresa para adquirir bienes o servicios, en la forma indicada en estos términos y condiciones.</p>
<p>Para aceptar estos Términos y Condiciones, el usuario deberá hacer click donde el sitio web de Media & Pix  SPA ofrezca esta opción en la interfaz del usuario con la frase “he leído y aceptado” u otra equivalente que permita dar su consentimiento inequívoco respecto de la aceptación.</p>


<h3>CUARTO: REGISTRO</h3>

<p>El registro en el sitio y el uso de contraseña es requisito obligatorio para contratar, facilita el acceso personalizado, confidencial y seguro al sitio.</p>
<p>En caso de registrarse en el sitio, el usuario contará con una contraseña secreta de la cual podrá disponer e incluso modificar si así lo requiriera el usuario. Para activar la contraseña deberá completar el formulario de registro disponible en el sitio y enviarlo a Media & Pix  SPA haciendo click en el campo respectivo.</p>
<p>Respecto de la confidencialidad de la contraseña serán responsables el usuario y el administrador dentro del ámbito que a cada uno corresponda.</p>


<h3>QUINTO: CÓMO CONTRATAR</h3>

<p>Para hacer compras o contratar servicios en este sitio deberá seguir los siguientes pasos haciendo click en el campo correspondiente:</p>


<p>1. Para iniciar el proceso de contratación es necesario que confirme haber leído y aceptado estos Términos y Condiciones.</p>
<p>2. Seleccione el producto o servicio que le interesa y agréguelo a su “carro de compra”.</p>
<p>3. Inicie sesión en el sitio o Ingresa tu correo electrónico y tu contraseña. Y, en caso de no estar registrado y desea hacerlo, utilice el ícono “registra tu contraseña”.</p>
<p>4. Seleccione el tipo de despacho y entrega entre las alternativas disponibles en el sitio en caso de no haber alternativas disponibles siga las instrucciones para la entrega de acuerdo a lo señalado en el sitio.</p>
<p>5. Seleccione el medio de pago.</p>
<p>6. Una vez hecha la orden, se desplegará en la pantalla: Esta información debe aparecer antes de la selección del medio de pago y es cómo funciona en la mayoría de los sitios.</p>
<p>a. una descripción del producto o servicio contratado.</p>
<p>b. Precio</p>
<p>c. indicación de costo de envío si corresponde.</p>
<p>d. d. fecha de entrega según el tipo de despacho escogido.</p>
<p>e. medio de pago.</p>
<p>f. valor total de la operación.</p>
<p>g. demás condiciones de la orden.</p>
<p>h. Posibilidad de imprimir y almacenar la orden.</p>
<p>i. también un número único de la orden, con la cual podrá hacer seguimiento en
	línea de ella.</p>
<p>7. Envío de la información al correo electrónico registrado.</p>
<p>8. La orden luego pasará automáticamente a un proceso de confirmación de identidad, resguardándose siempre la seguridad y privacidad del usuario y del proceso de contratación, disponibilidad, vigencia y cupo del medio de pago que se haya seleccionado.</p>
<p>9. Cumplido con lo anterior se perfecciona el contrato haciéndose el cargo en el medio de pago seleccionado, se enviará el comprobante de compra con la boleta o factura que corresponda en formato electrónico y será despachado el producto, de acuerdo al modo de entrega que se hubiera seleccionado.</p>
<p>10. No se verá afectado el comprador en sus derechos ni tampoco se le efectuarán cargos, sin que sea confirmada su identidad.</p>

 


<h3>SEXTO: MEDIOS DE PAGO</h3>

<p>Salvo que se señale un medio diferente para casos u ofertas específicos, los productos y servicios que se informan en este sitio sólo podrán ser pagados por medio de:</p>
<p>1. Tarjeta de crédito bancarias Visa, Mastercard, emitidas en Chile o en el extranjero, y tarjetas Dinners Club International o American Express, emitidas en Chile. Siempre que mantengan un contacto vigente para este efecto con Media & Pix SPA.</p>
<p>2. Tarjetas de débito bancarias acogidas al sistema Redcompra, emitidas en Chile por bancos nacionales, que mantengan un contrato vigente para tales efectos con Media & Pix SPA.</p>
<p>3. Transferencia bancaria. Esta debe realizarse a la cuenta corriente de la empresa en el Banco de Chile y su comprobante debe ser enviado al correo mediaypix@gmail.com Efectivo. Esta modalidad se realizará en las dependencias de Media & Pix SPA, bajo la modalidad de Pago contra entrega.</p>
<p>5. Otros medios de pago electrónicos que pueda tener contratada la empresa (Paypal).</p>


<p>El pago con tarjetas de débito se realizará a través de WebPay y MercadoPago, que son sistemas de pago electrónico que se encargan de hacer el cargo automático a la cuenta bancaria del usuario.</p>
<p>Los usuarios declaran que entienden que estos medios de pago o portales de pago pertenecen a terceras empresas proveedoras de estos servicios, independientes y no vinculadas a Media & Pix SPA, por lo que la continuidad de su prestación de servicios en el tiempo, así como el correcto funcionamiento de sus herramientas y botones de pago en línea, será de exclusiva responsabilidad de las empresas proveedoras de estos servicios y en ningún caso de Media & Pix SPA.</p>

<h3>SEPTIMO: DERECHOS Y GARANTÍAS</h3>

<p>Cambios y devoluciones</p>
<p>Tenga en cuenta que las obras de arte con montaje / encuadre personalizado , así como impresiones fotográficas sin montar / sin marco están excluidas del derecho de cancelación / retirada, ya que se producen a medida según sus especificaciones individuales. Los productos personalizados están excluidos del derecho de cancelación.</p>


<p>Si el producto comprado presenta fallas o no cuenta con las características técnicas informadas este puede ser cambiado hasta 10 días después de la fecha en que este haya sido recibido, en el taller de la empresa, o si fue despachado, la empresa puede ir a buscarlo al mismo lugar, pagando sólo el valor del transporte que será el mismo valor que el despacho.</p>
<p>Para poder hacer cambio o devolución es necesario que el producto esté sin uso, con todos sus accesorios y embalajes originales.</p>


<p>Para cambio o devolución deberá presentarse la boleta, guía de despacho, ticket de cambio, u otro comprobante de compra.</p>
<p>En caso de devolución de dinero la empresa realizará un abono en el medio de pago que haya utilizado en un período no superior a 72 Horas de haberse aceptado la devolución, cuestión que será informada a través del correo electrónico que se hubiere registrado.</p>


<p>Garantías legales</p>
<p>En caso que el producto no cuente con las características técnicas informadas, si estuviera dañado o incompleto, podrá ser cambiado previa evaluación en el taller de Blackpix. </p>


<p>Si el producto cuenta con una garantía del fabricante, se aplicará el plazo de esta garantía, si este plazo fuera mayor.</p>
<p>Se considerará como falla o defecto:</p>


<p>1. Productos sujetos a normas de seguridad o calidad de cumplimiento obligatorio que no cumplan con las especificaciones</p>
<p>2. Si los materiales, partes, piezas, elementos, sustancias o ingredientes que constituyan o integren los productos no correspondan a las especificaciones que ostenten o a las menciones del rotulado.</p>
<p>3. Productos que por deficiencias de fabricación, elaboración, materiales, partes, piezas, elementos, sustancias, ingredientes, estructura, calidad o condiciones sanitarias, en su caso, no sea enteramente apto para el uso o consumo al que está destinado o al que el proveedor hubiese señalado en su publicidad.</p>
<p>4. Si el proveedor y consumidor hubieren convenido que los productos objeto del contrato deban reunir determinadas especificaciones y esto no ocurra.</p>
<p>5. Si después de la primera vez de haberse hecho efectiva la garantía y prestado el servicio técnico correspondiente, subsistieren las deficiencias que hagan al bien inapto para el uso o consumo a que se refiere el numeral 3. Este derecho subsistirá para el evento de presentarse una deficiencia a la que fue objeto del servicio técnico, o volviere a presentarse la misma.</p>
<p>6. Si la cosa objeto del contrato tiene efectos o vicios ocultos que imposibiliten el uso a que habitualmente se destine.</p>


<p>Derecho de Retracto</p>
<p>La empresa Media & Pix SPA no otorga el derecho a retracto a los consumidores.</p>


<h3>OCTAVO: ENTREGA DE PRODUCTOS</h3>

<p>Despacho de Productos</p>
<p>Las condiciones de despacho y entrega de los productos adquiridos podrán ser escogidas de entre las opciones ofrecidas en el sitio.</p>
<p>Compra Online retira en tienda</p>
<p>Si elige retirar el producto en una tienda, podrá hacerlo en el horario en que ésta se encuentre abierta al público.</p>
<p>Los retiros deberán ser hechos dentro de un plazo de 10 días. Por medio del correo electrónico registrado se informará que el producto está a disposición para ser retirado. Los comprobantes de esta gestión quedarán disponibles durante 30 días. Si el producto no fuera retirado dentro del plazo especificado, la transacción quedará sin efecto, haciéndose la devolución de la cantidad pagada.</p>
<p>Retiro en tienda: Luego de la confirmación de pago el plazo de entrega será de 3 a 15 días hábiles. Previa confirmación del Asesor Web y será reservado por 20 días hábiles en nuestra tienda.</p>
<p>Despacho en Santiago: El plazo máximo de entrega será de hasta 15 días hábiles luego de confirmado los datos del pago. El cliente recibirá el producto después de este plazo considerando el tiempo que la empresa de Courier tarda en llegar a su domicilio.</p>
<p>Despacho a otras regiones: Los despachos fuera de la Ciudad de Santiago, se harán en un plazo máximo de 20 días hábiles luego confirmado los datos del pago. El cliente recibirá el producto después de este plazo considerando el tiempo que la empresa de Courier tarda en llegar a su domicilio.</p>


<h3>NOVENO: RESPONSABILIDAD</h3>

<p>En ningún caso Media & Pix SPA responderá por:</p>


<p>1. La utilización indebida que Usuarios o visitantes de El Sitio puedan hacer de los materiales exhibidos, de los derechos de propiedad industrial y de los derechos de propiedad intelectual.</p>
<p>2. Daños o eventuales daños y perjuicios que se le puedan causar a los Compradores y/o Usuarios por el funcionamiento de las herramientas de búsqueda y de los errores que se generen por los elementos técnicos de El Sitio o motor de búsqueda.</p>
<p>3. Contenidos de las páginas a las que los Compradores o Usuarios puedan acceder con o sin autorización de Media & Pix SPA.</p>
<p>4. El acceso de menores de edad o personas sin capacidad, bajo los términos de la legislación correspondiente, a los contenidos adherentes a la relación contractual que surja de El Sitio.</p>
<p>5. Pérdida, mal uso o uso no autorizado de su código de validación, ya sea por parte del Usuario y/ o comprador Compradores, o de terceros, luego de realizada la compra en la forma expresada en los Términos y Condiciones. Asimismo, las partes reconocen y dejan constancia que la plataforma computacional proporcionada por Media & Pix SPA no es infalible, y por tanto, durante la vigencia del presente Contrato pueden verificarse circunstancias ajenas a la voluntad de Media & Pix SPA, que impliquen que El Sitio o la plataforma computacional no se encuentren operativos durante un determinado periodo de tiempo.</p>
<p>6. Información de Media & Pix SPA o sus servicios que se encuentre en sitios distintos a www.blackpix.cl</p>


<p>En tales casos, Media & Pix SPA procurará restablecer El Sitio y el sistema computacional con la mayor celeridad posible, sin que por ello pueda imputársele algún tipo de responsabilidad.</p>
<p>Media & Pix SPA no garantiza la disponibilidad y continuidad del funcionamiento de El Sitio y tampoco que en cualquier momento y tiempo, los Usuarios puedan acceder a las promociones y Ofertas del El Sitio.</p>
<p>Media & Pix SPA no se hace responsable por los virus ni otros elementos en los documentos electrónicos almacenados en los sistemas informáticos de los Usuarios. Media & Pix SPA no responderá de los perjuicios ocasionados al Cliente, provenientes del uso inadecuado de las tecnologías puestas a disposición de éste, cualquiera sea la forma en la cual se utilicen inadecuadamente estas tecnologías. Media & Pix SPA no responderá de los daños producidos a El Sitio por el uso indebido y de mala fe de los Usuarios y/o Compradores. No obstante, en el evento de realizarse un doble pago por un Usuario o Comprador en El Sitio, Media & Pix SPA devolverá la suma del sobrepago, dentro de los 3 días siguientes a la recepción del respectivo reclamo escrito del Usuario o Comprador, en el que se anexen los originales de los comprobantes del pago adicional a lo adquirido.</p>
<p>En Login, registro y comunicación con empresas de medios de pago, Media & Pix SPA usa certificados digitales de seguridad (SSL), con el fin de encriptar comunicación. Media & Pix SPA no manipula ni almacena datos financieros de sus clientes.</p>
<p>En todo caso, la responsabilidad de Ferretería Industrial Ferretek SPA, contractual, extracontractual o legal, con los Usuarios, Compradores o visitantes de El Sitio no excederá del precio efectivamente pagado por el Comprador en contraprestación por el producto o servicio, sin perjuicio de lo que determinen los Tribunales de Justicia.</p>


<h3>DÉCIMO: SEGURIDAD DE DATOS Y CLAVE SECRETA</h3>

<p>Media & Pix SPA adoptará las medidas necesarias y prudentes para resguardar la seguridad de los datos y clave secreta, como sistemas de encriptación de información, certificados de seguridad u otros que la empresa estime pertinente. En caso de detectarse cambios en la información que hayas registrado en el sitio, o bien, ante cualquier irregularidad en las transacciones relacionadas con su identificación o la del medio de pago, o simplemente como medida de protección a su identidad, nuestros ejecutivos podrán contactarlo por vía telefónica o correo electrónico, a fin de corroborar sus datos e intentar evitar posibles fraudes.</p>
<p>En caso de no poder establecer el contacto en un plazo de 72 hrs, por su propia seguridad, su orden de compra efectuada en nuestro sitio no podrá ser confirmada. Le informaremos vía telefónica o por correo electrónico que su orden ha quedado sin efecto por no poder confirmar su identidad o el medio de pago ofrecido. Además, los comprobantes de las gestiones realizadas para contactarte y poder confirmar la operación, estarán disponibles en nuestras oficinas durante 30 días, para que puedas confirmar la orden de compra. Cualquier consulta puede ser efectuada a leon.blackpix@gmail.com</p>
<p>Sin embargo, los Usuarios y/o Compradores son exclusivamente responsables por la pérdida, mal uso o uso no autorizado del código de validación, ya sea por parte de los mismos o de terceros, luego de realizada la compra en la forma expresada en los Términos y Condiciones.</p>
<p>Datos personales:</p>
<p>Los Usuarios y/o Compradores garantizan que la información que suministran para la celebración del contrato es veraz, completa, exacta y actualizada.</p>
<p>De conformidad con la Ley 19.628 los datos personales que suministren en el Sitio Web pasarán a formar parte de una base de datos de Media & Pix SPA y serán destinados única y exclusivamente en para ser utilizados en los fines que motivaron su entrega y especialmente para la comunicación en general entre la empresa y sus clientes, validar los datos de la compra, concretar el despacho y responder sus consultas. Los datos no serán comunicados a otras empresas sin la expresa autorización de su titular ni serán transferidos internacionalmente.</p>
<p>Media & Pix SPA jamás solicita datos personales o financieros a través de correo electrónico</p>
<p>Media & Pix SPA presume que los datos han sido incorporados por su titular o por persona autorizada por éste, así como que son correctos y exactos. Los Usuarios y/o Compradores con la aceptación de los presentes Términos y Condiciones manifiestan que los datos de carácter personal que aporte a través de los formularios online en la página web de Media & Pix SPA pueden ser utilizados para Ofertas posteriores y distintas a las ofrecidas en El Sitio.</p>
<p>Sin perjuicio de lo anterior, Media & Pix SPA garantiza a los usuarios el libre ejercicio de sus derechos de información, modificación, cancelación y bloqueo de sus datos personales establecidos en la Ley 19.628. Por consiguiente, los compradores podrán realizar requerimientos que digan relación con dichos derechos, y en un plazo máximo de dos días corridos, Media & Pix SPA deberá dar respuesta e implementar efectivamente esta solicitud.</p>
<p>Documentos electrónicos:</p>
<p>El usuario en su calidad de receptor manual de documentos electrónicos, de conformidad con la Resolución Exenta N° 11 del 14 de febrero de 2003 del Servicio de Impuestos Internos (que estableció el procedimiento para que contribuyentes autorizados para emitir documentos electrónicos puedan también enviarlos por estos medios a receptores manuales), declara y acepta lo siguiente:</p>
<p>Al aprobar estos términos y condiciones, el usuario autoriza a la empresa Media & Pix SPA RUT: 76.699.358-3, para que el documento tributario correspondiente de esta transacción, le sea entregada solamente por un medio electrónico. De igual forma, autoriza que el aviso de publicación del documento tributario me enviado mediante correo electrónico.</p>
<p>De conformidad con la normativa indicada, y en caso que el usuario lo requiera para respaldar la información contable, asume en relación a dichos documentos tributarios, las siguientes obligaciones:</p>


<p>1. Imprimir los documentos recibidos en forma electrónica, para cada período tributario, en forma inmediata a su recepción desde el emisor.;</p>
<p>2. Imprimir el documento en el tamaño y forma que fue generado;</p>
<p>3. Utilizar papel blanco tipo original de tamaño mínimo 21,5 cm x 14 cm (1/2 carta) y de tamaño máximo 21,5 x 33 cm (oficio);</p>
<p>4. Imprimir en una calidad que asegure la permanencia de la legibilidad del documento durante un periodo mínimo de seis años, conforme lo establece la legislación vigente sobre la materia. Esta impresión se hará hecha usando impresión láser o de inyección de tinta, excepto que se establezca una autorización o norma distinta al respecto.</p>


<h3>DÉCIMO PRIMERO: ALCANCE DE LAS CONDICIONES INFORMADAS EN EL SITIO</h3>

<p>Media & Pix SPA no modificará las condiciones bajo las cuales haya contratado con los consumidores en este sitio. Mientras aparezcan en este sitio, los precios informados estarán a disposición del usuario, aunque no sean los mismos que se ofrezcan en otros canales de venta de Media & Pix SPA, como tiendas físicas, catálogos, televisión, radio, u otros.</p>
<p>Con todo, los precios son aplicables para la ciudad de entrega o despacho. Si una vez ingresado un producto al carro de compras, es cambiada la dirección de entrega o despacho a una ciudad diferente, cambiará el precio total del producto de acuerdo a los costos de envío a la nueva ciudad registrada.</p>
<p>Cualquier cambio en las informaciones publicadas en este sitio, incluyendo las referidas a mercaderías, servicios, precios, existencias y condiciones, promociones y ofertas, tendrá lugar antes de recibir una orden de compra y solo se referirá a operaciones futuras, sin afectar, en caso alguno, derechos adquiridos por los consumidores.</p>
<p>Las promociones que ofrecidas en el sitio no necesariamente serán las mismas que Media & Pix SPA ofrezca por otros canales de venta. En las promociones que consistan en la entrega gratuita o rebajada de un producto por la compra de otro, el despacho del bien que se entregue gratuitamente o a precio rebajado se hará en el mismo lugar al cual se despacha el producto comprado, salvo que el adquirente solicite, al aceptar la oferta, que los productos se remitan a direcciones distintas, en cuyo caso deberá pagar el valor del despacho de ambos productos. No se podrá participar en estas promociones sin adquirir conjuntamente todos los productos comprendidos en ellas.</p>


<h3>DÉCIMO SEGUNDO: PROPIEDAD INTELECTUAL</h3>

 

<p>Al comprar una impresión fotográfica, solo está adquiriendo la propiedad del material de la impresión. No se otorgan ni implican otros derechos. No se permite ninguna reproducción (copia), difusión, arrendamiento, exhibición pública u otro uso analógico o digital a menos que lo autorice la ley. Sin embargo, puede revender la impresión.</p>


<p>Todos los contenidos incluidos en este sitio, como textos, material gráfico, logotipos, íconos de botones, códigos fuente, imágenes, audio clips, descargas digitales y compilaciones de datos, son propiedad de Media & Pix SPA o de sus proveedores de contenidos, y están protegidos por las leyes chilenas e internacionales sobre propiedad intelectual. Los materiales gráficos, logotipos, encabezados de páginas, frases publicitarias, iconos de botones, textos escritos y nombres de servicios incluidos en este sitio son marcas comerciales, creaciones o imágenes comerciales de propiedad de Media & Pix SPA en Chile y en otros países. Dichas marcas, creaciones e imágenes comerciales no se pueden usar en relación a ningún producto o servicio que pueda causar confusión entre los clientes y en ninguna forma que desprestigie o desacredite a Media & Pix SPA. Las demás marcas comerciales que no sean de propiedad de Media & Pix SPA y que aparezcan en este sitio pertenecen a sus respectivos dueños.</p>


<p>Todos los derechos no expresamente otorgados en estos Términos y Condiciones son reservados por Media & Pix SPA o sus cesionarios, proveedores, editores, titulares de derechos u otros proveedores de contenidos. Ningún producto, imagen o sonido pueden ser reproducidos, duplicados, copiados, vendidos, revendidos, visitados o explotados para ningún fin, en todo o en parte, sin el consentimiento escrito previo de Media & Pix SPA. No se puede enmarcar o utilizar técnicas de enmarcación para encerrar alguna marca comercial, logotipo u otra información registrada o patentada (incluyendo imágenes, texto, disposición de páginas, o formulario) de Media & Pix SPA, sin nuestro consentimiento escrito previo. Tampoco se puede usar meta etiquetas ni ningún otro “texto oculto” que use el nombre o marcas comerciales de Media & Pix SPA, sin autorización escrita previa de esta empresa. Se prohíbe hacer un uso indebido de este sitio o de estas marcas, licencias o patentes. Lo anterior, sin perjuicio de las excepciones expresamente señaladas en la ley.</p>


<p>Media & Pix SPA respeta la propiedad intelectual de otros. Si cree que su trabajo ha sido copiado en forma tal que constituye una violación del derecho de propiedad intelectual, contáctate con nosotros a leon.blackpix@gmail.com</p>


<h3>DÉCIMO TERCERO: LEGISLACIÓN APLICABLE y COMPETENCIA</h3>

<p>Los presentes términos y condiciones se rigen por las leyes de la República de Chile. Cualquier controversia o conflicto derivado de la utilización del sitio web de Media & Pix SPA, sus Términos y Condiciones y las Políticas de Privacidad, su validez, interpretación, alcance o cumplimiento, será sometida a las leyes aplicables de la República de Chile.</p>


<h3>REPRESENTANTE LEGAL</h3>

El representante legal de Media & Pix SPA es el Señor Hernán Eduardo Fernández León., domiciliado en Avenida Kennedy 5770 oficina 1215, Vitacura, Chile. Su teléfono es el +56 991628747. Su correo electrónico es mediaypix@gmail.com