<p>Tenga en cuenta que las obras de arte con montaje / encuadre personalizado , así como impresiones fotográficas sin montar / sin marco están excluidas del derecho de cancelación / retirada, ya que se producen a medida según sus especificaciones individuales. Los productos personalizados están excluidos del derecho de cancelación.</p>


<p>Si el producto comprado presenta fallas o no cuenta con las características técnicas informadas este puede ser cambiado hasta 10 días después de la fecha en que este haya sido recibido, en el taller de la empresa, o si fue despachado, la empresa puede ir a buscarlo al mismo lugar, pagando sólo el valor del transporte que será el mismo valor que el despacho.
Para poder hacer cambio o devolución es necesario que el producto esté sin uso, con todos sus accesorios y embalajes originales.</p>


<p>Para cambio o devolución deberá presentarse la boleta, guía de despacho, ticket de cambio, u otro comprobante de compra.</p>
<p>En caso de devolución de dinero la empresa realizará un abono en el medio de pago que haya utilizado en un período no superior a 72 Horas de haberse aceptado la devolución, cuestión que será informada a través del correo electrónico que se hubiere registrado.</p>


<h3>Garantías legales</h3>
<p>En caso que el producto no cuente con las características técnicas informadas, si estuviera dañado o incompleto, podrá ser cambiado previa evaluación en el taller de Blackpix. </p>


<p>Si el producto cuenta con una garantía del fabricante, se aplicará el plazo de esta garantía, si este plazo fuera mayor.</p>
<p>Se considerará como falla o defecto:</p>


<p>1. Productos sujetos a normas de seguridad o calidad de cumplimiento obligatorio que no cumplan con las especificaciones </p>
<p>2. Si los materiales, partes, piezas, elementos, sustancias o ingredientes que constituyan o integren los productos no correspondan a las especificaciones que ostenten o a las menciones del rotulado. </p>
<p>3. Productos que por deficiencias de fabricación, elaboración, materiales, partes, piezas, elementos, sustancias, ingredientes, estructura, calidad o condiciones sanitarias, en su caso, no sea enteramente apto para el uso o consumo al que está destinado o al que el proveedor hubiese señalado en su publicidad. </p>
<p>4. Si el proveedor y consumidor hubieren convenido que los productos objeto del contrato deban reunir determinadas especificaciones y esto no ocurra. </p>
<p>5. Si después de la primera vez de haberse hecho efectiva la garantía y prestado el servicio técnico correspondiente, subsistieren las deficiencias que hagan al bien inapto para el uso o consumo a que se refiere el numeral 3. Este derecho subsistirá para el evento de presentarse una deficiencia a la que fue objeto del servicio técnico, o volviere a presentarse la misma. </p>
<p>6. Si la cosa objeto del contrato tiene efectos o vicios ocultos que imposibiliten el uso a que habitualmente se destine.</p>


<h3>Derecho de Retracto</h3>
<p>La empresa Media & Pix SPA no otorga el derecho a retracto a los consumidores.</p>