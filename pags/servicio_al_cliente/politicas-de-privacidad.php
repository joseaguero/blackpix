<h3>1. CUERPO RESPONSABLE</h3>


<p>El organismo responsable de la recopilación, el procesamiento y el uso de datos junto con el uso de nuestro sitio web es Media & Pix SPA, Cousin 0227, comuna de Providencia, ciudad de Santiago, Chile.</p>


<h3>2. RECOPILACIÓN DE DATOS PERSONALES</h3>


<p>Utilizamos sus datos personales (título, nombre y apellido, dirección postal, dirección de correo electrónico, número de teléfono e información de pago) de conformidad con la Ley Alemana de Protección de Datos para cumplir y procesar su pedido y enviar mensajes por correo electrónico para , por ejemplo, para informarle sobre el estado de los pedidos. También almacenaremos la redacción del contrato y le enviaremos los datos de su pedido por correo electrónico. Puede ver nuestros Términos y condiciones aquí en cualquier momento.</p>
<p>Sus datos personales, que ingresa durante el pedido, solo se utilizan para procesar el contrato. Algunos de sus datos (título, nombre y apellido, datos de dirección, número de teléfono) se pondrán a disposición de nuestros socios logísticos para facilitar la entrega de su pedido. Este reenvío solo se realiza en cumplimiento de los estrictos requisitos de la Ley Alemana de Protección de Datos. No habrá más reenvío de su información para fines que no sean cumplir, realizar y terminar la relación contractual entre usted y nosotros, a menos que haya alguna orden legal u oficial para hacerlo. Lo mismo se aplicará a los datos personales que haya proporcionado al contactarnos en forma electrónica o impresa.</p>


<h3>3. NEWSLETTER</h3>


<p>Puede solicitar un boletín a través de nuestro sitio web. Al suscribirse a nuestro boletín, su dirección de correo electrónico también se guardará para nuestros propios fines de marketing y para enviar el boletín, hasta que se dé de baja del boletín. Usted ha aceptado específicamente lo siguiente ("Solicitar boletín informativo (puede darse de baja en cualquier momento)"). Tenga en cuenta que puede retirar su acuerdo en cualquier momento con efecto para el futuro haciendo clic en el enlace para cancelar la suscripción en un boletín al final de la página. Para este fin, o si tiene alguna pregunta sobre la protección de datos, contáctenos por correo electrónico a mediaypix@gmail.com.</p>


<h3>4. SEGURIDAD DE DATOS, MEDIOS DE PAGO, INFORMACIÓN GENERAL DE SEGURIDAD</h3>


<p>Tomamos todas las medidas técnicas y organizativas para almacenar sus datos personales de tal manera que terceros no puedan acceder a ellos. Cuando nos comunicamos por correo electrónico, no podemos garantizar la seguridad completa de los datos, por lo que recomendamos enviar información confidencial por correo tradicional.</p>
<p>Para ofrecerle la mayor seguridad posible, toda la información de pago (por ejemplo, datos bancarios o datos de la tarjeta de crédito) la enviamos exclusivamente al proveedor de pagos. Debe volver a ingresar estos detalles para cada pedido.</p>


<h3>5. DERECHO A LA INFORMACIÓN Y ELIMINACIÓN DE SUS DATOS PERSONALES</h3>


<p>Usted tiene derecho a solicitar información sobre los datos almacenados sobre usted de forma gratuita y tiene derecho a corregir, bloquear o eliminar estos datos. Para hacer esto, puede contactar a nuestro equipo de servicio al cliente en la dirección de correo electrónico mediaypix@gmail.com.</p>


<h3>6. RECOPILACIÓN Y PROCESAMIENTO DE DATOS ANÓNIMOS</h3>


<p>Cuando accede a una página, se guarda información de carácter general. Esto incluye, entre otras cosas, el tipo y la versión del navegador web utilizado, el sitio web desde el que estaba vinculado nuestro sitio, nuestros sitios web visitados y la fecha de la visita. Todo esto es información que no permite identificar a la persona. BLACKPIX solo utiliza estos datos anónimos con fines estadísticos. Esta información no está disponible para terceros.</p>


<h3>7. RECOPILACIÓN DE DATOS ESTADÍSTICOS, USO DE COOKIES</h3>


<p>Utilizamos las llamadas cookies para que su visita a nuestro sitio web sea aún más atractiva y para apoyar el uso de ciertas funciones. La mayoría de las cookies utilizadas en www.blackpix.cl se eliminan automáticamente después de la sesión de su navegador. Otras cookies nos permiten reconocer su computadora en su próxima visita a nuestro sitio web y brindarle el mejor servicio posible.</p>
<p>Para diseñar y estructurar nuestro sitio web de acuerdo con las necesidades de las personas, los datos de uso se recopilan, agregan, guardan y evalúan como perfiles de uso, utilizando seudónimos. Los anuncios refinados no pueden asignar estos datos a personas reales y estos datos definitivamente no se combinan con otras fuentes de datos. Para rechazar y evitar que sus datos se guarden, use la cookie de "exclusión voluntaria" de los anuncios refinados.</p>


<h3>8. USO DE GOOGLE ANALYTICS</h3>


<p>Nuestro sitio web utiliza Google Analytics, un servicio de análisis web de Google Inc. ("Google"). Google Analytics utiliza las llamadas "cookies". Estos son archivos de texto, guardados en su computadora, que permiten un análisis de su uso del sitio web. El análisis elaborado por Google nos proporciona información sobre las actividades del sitio web y sobre el uso del sitio web y de Internet Sus datos técnicos se evalúan de forma anónima y solo con fines estadísticos para optimizar constantemente nuestro sitio web y hacer que nuestras ofertas en línea sean aún más atractivas. Su privacidad permanece protegida; los datos anónimos no pueden dar lugar a la identificación de una persona individual. La información generada es normalmente se transmite a un servidor que pertenece a Google en EE. UU. y se guarda allí. Además, en casos excepcionales en los que Google solo anonimiza la dirección IP en EE. UU., su dirección IP se abrevia directamente por Google en los estados miembros de la Unión Europea o en otros países signatarios del Acuerdo sobre el Espacio Económico Europeo. Google no combina la dirección IP transmitida con otros datos. Google puede evitar el almacenamiento de cookies y el procesamiento de datos mediante la configuración correspondiente en el software de su navegador. Este es el enlace actual para descargar e instalar el complemento del navegador correspondiente. Sin embargo, debemos señalar que, en este caso, es posible que no pueda utilizar todas las funciones de nuestro sitio web en toda su extensión.</p>
<p>Además de la opción de instalar el complemento del navegador, existe otra alternativa para evitar que Google Analytics recopile datos. Es particularmente interesante para los usuarios de dispositivos móviles. Para ver esta alternativa, haga clic en (javascript: gaOptout ()). Esto instala una llamada cookie de exclusión voluntaria, que impide el seguimiento de datos en este sitio web. La función permanece hasta que se elimine la cookie. Cuando la cookie ha sido eliminada, se trata simplemente de llamar el enlace una vez más.</p>
<p>Para obtener más información sobre las condiciones de uso y la protección de datos, consulte los siguientes sitios: <br>
	https://www.google.com/analytics/terms/gb.html <br>
	http://www.google.de/intl/en/policies/</p>


<h3>9. PLUGINS SOCIALES</h3>


<p>Nuestro sitio web utiliza complementos sociales ("complementos") de la red social facebook.com, operada por Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, EE. UU. ("Facebook"). Los complementos pueden reconocerse mediante uno de los logotipos de Facebook ("f" blanca en un mosaico azul o un signo de "pulgar hacia arriba") o están marcados con el sufijo "Complemento social de Facebook". Puede encontrar más información sobre los complementos sociales de Facebook aquí: https://developers.facebook.com/plugins. Si accede a una página de nuestro sitio web que contiene uno de estos complementos, su navegador abre automáticamente un enlace directo a los servidores de Facebook. Facebook transmite el contenido del complemento directamente a su navegador y, por lo tanto, se integra en el sitio web. Por lo tanto, no tenemos influencia en el alcance de los datos recopilados por Facebook usando este complemento, pero esto es lo que sabemos: Al integrar los complementos, Facebook obtiene información de que accedió a la página correspondiente de nuestro sitio web. Si ha iniciado sesión en Facebook, Facebook puede vincular su visita a su cuenta de Facebook. Si interactúa con los complementos, por ejemplo, utilizando el "Botón Me gusta" o dejando un comentario, la información correspondiente se transmite desde su navegador directamente a Facebook y se guarda allí. Si no es miembro de Facebook, es posible que Facebook descubra y guarde su dirección IP. El propósito y el alcance de la recopilación de datos y el posterior procesamiento y uso de los datos por parte de Facebook, así como sus derechos a este respecto y las posibles configuraciones para proteger su privacidad, se pueden ver en la información de protección de datos de Facebook en: https: //www.facebook.com/policy.php. Si, como miembro de Facebook, no desea que Facebook recopile datos a través de nuestro sitio web y los vincule a su cuenta de Facebook, debe cerrar sesión en Facebook antes de visitar nuestro sitio web.</p>

 

 


<p>Información legal</p>


<p>Este sitio web está dirigido por: <br>


Media & Pix Spa.
	Cousin 0227, Providencia.
	SantiagoChile</p>


<p>Tel: + 56991628747  <br>
	Correo electrónico: mediaypix@gmail.com <br>
	RUT: 76.699.358-3 <br>
	Gerente general: León Errázuriz. <br>
	Este sitio web es administrado por Media & Pix Spa.</p>


<p>Derechos de autor <br>
	© Copyright 2020-2030 Media & Pix Spa. Todos los derechos reservados. © para todas las imágenes de los artistas nombrados, úselas solo mediante un acuerdo contractual con BLACKPIX. © Todos los textos para los artistas nombrados. Ninguna parte del sitio web de BLACKPIX puede copiarse, reutilizarse, reproducirse, distribuirse, publicarse o utilizarse de ninguna otra manera en forma analógica o digital sin previo acuerdo por escrito.</p>

 


<p>Marcas registradas <br>
	BLACKPIX es una marca de Media & Pix Spa .. Otras marcas y marcas registradas que aparecen en este sitio web pero que no pertenecen a Media & Pix Spa son propiedad de sus respectivos titulares de derechos de autor.</p>


<p>Descargo de responsabilidad <br>
	Media & Pix Spa. no se hace responsable de las fotos y otros materiales no solicitados. Media & Pix Spa. no asume ninguna responsabilidad por el contenido de los enlaces externos. Nos reservamos el derecho de aumentar los precios, ofrecer ventas, corregir errores y erratas.</p>


<p>Puede encontrar nuestros Términos y condiciones generales <a href="terminos-y-condiciones">aquí.</a></p>