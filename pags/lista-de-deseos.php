<script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' debe contener una dirección de correo.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' debe contener numeros.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' debe contener números entre '+min+' y '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es obligatorio.\n'; }
    } if (errors) alert('Han ocurrido los siguientes errores:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>
<section class="bg-lista-deseos contGeneral">
<div class="row">
    <div class="col-md-12 hidden-xs"><br>
        <ol class="breadcrumb padding-left0 border0">
            <li><a href="javascript:void(0)">Home</a></li>
            <li class="active">Lista de deseos</li>
        </ol>
        <br>
    </div>

    <div class="col-md-12">
    <div class="col-md-12 bg-box-lista">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-push-1">
                <div class="row">
                    <div class="col-md-12 text-center"><br class="hidden-xs"><br class="hidden-xs"><br class="hidden-xs">
                        <p class="lead-populares">
                        <span class="glyphicon glyphicon-tags"></span> Mi Lista de Deseos
                        </p><br class="hidden-xs">
                    </div>
                    <div class="clearfix"></div>
					<?php echo listaDeseos(); ?>
				</div>
            </div>

        
        </div> 
        <br><br> 
    </div>

    <div class="clearfix"></div>
    <br>
    <div class="well col-md-12 col-lg-10 col-lg-push-1">
            <div class="row">
                <div class="col-md-12 col-lg-push-1 col-lg-10">
                <div class="row">
                <div class="col-md-6 col-md-push-0 col-sm-10 col-sm-push-1">
                     <p>Quiero envíar mi lista de deseos al correo</p>
                </div>
                    <div class="col-md-6 col-md-pull-0 col-sm-10 col-sm-pull-1 col-xs-12 pull-right">
                        <div class="input-group news">
                            <form action="envioListaFavoritos.php" method="post" onsubmit="MM_validateForm('email','','RisEmail');return document.MM_returnValue">
                                <input name="email" type="text" class="form-control ingresa-tu-mail ingresa-mail2 mail-news" id="email" placeholder="Ingresa tu mail">
                                <input type="hidden" name="lista" value="<?php echo $_COOKIE['listaDeseos']; ?>" />
                                <input type="hidden" name="validacion" value="" />
                                <span class="input-group-btn btn-news">
                                  <input type="submit" name="enviar" class="btn btn-lista-deseos" value="Enviar">
                                </span>
                          </form>
                        </div>
                    </div>
                </div>
                   
                </div>
                
            </div>
    </div>
    </div>

    

</div>
    </section>


<script type="text/javascript">
    $(function(){
        var i=0;
        $('.box-popular').each(function (index) { 
            i=i+1;
            if(i==5){
                $(this).addClass("sinMargen");
                i=0;
            } else {

            }
          console.log(i); 
        });

$("")
    });
</script>