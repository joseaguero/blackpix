<?php 

$sesionIniciada = (isset($_COOKIE['usuario_id']) AND $_COOKIE['usuario_id'] != NULL) ? true : false;

if ($sesionIniciada) {
	echo '<script>location.href="envio-y-pago";</script>';
}

?>

<div class="bread_carro">
    <div class="container_carro">
        <div class="list activo center-text">identifícate</div>
    </div>
</div>

<div class="center_ident">
	<div class="text_ident">ingresa tu correo para realizar tu compra <br>
de manera rápida, fácil y segura.</div>

	<?php 

	if(isset($_COOKIE[nombreUsuario]) and isset($_COOKIE[claveUsuario])){
		$checked = 'checked="checked"';
		$usuarioCookie = $_COOKIE[nombreUsuario];
		$claveCookie = base64_decode($_COOKIE[claveUsuario]);
	} else {
		$checked = '';
	}

	?>

	<div class="grid_ident">
		<div class="col">
			<form action="envio-y-pago" method="post" class="formCompraRapida1">
				<div class="t_ident">Compra rápida</div>
				<div class="st_ident">Ingresa tu correo</div>
				<input type="email" name="email" class="input_ident" placeholder="Email..." id="emailRapido">
				<div class="crear-cuenta">Si quieres crear un cuenta haz clic en <a href="javascript:void(0)" class="fadeCreate">crear cuenta</a></div>
				<div class="clearfix"></div>
				<a href="javascript:void(0)" id="validarCorreoCompraRapida" class="btnFormCompraRapida">Siguiente</a>
			</form>
		</div>
		<div class="col">
			<form action="ajax/login.php" method="post" class="formInicioSesion formularioLogin" id="formularioLogin">
				<div class="t_ident">Inicia sesión</div>
				<div class="st_ident">Ingresa tu correo</div>
				<input type="email" name="email" class="input_ident" placeholder="Email...">
				<div class="st_ident mt-15">Contraseña</div>
				<input type="password" name="clave" class="input_ident" placeholder="Contraseña...">
				
				<div class="content_chk_password">
					<input type="checkbox" name="chk_cat" id="chk_mantener" class="chk" <?= $checked ?>>
					<label id="chk_mantener">Recordar mis datos</label>
				</div>

				<input type="hidden" name="origen" value="carro">

				<a href="javascript:void(0)" class="recuperar_password">¿olvidaste tu contraseña?</a>

				<div class="clearfix mb-20"></div>

				<button class="btnFormCompraRapida" id="inicioSesion">Iniciar sesión</button>
			</form>

			<div class="crear-cuenta">Si quieres crear un cuenta haz clic en <a href="javascript:void(0)" class="fadeCreate">crear cuenta</a></div>
		</div>
	</div>
</div>

<div class="bg_popup_carro"></div>
<div class="content_popup_carro">
	<div class="closePopCarro"><img src="img/close_pop.png"></div>

	<div class="title mb-20">Crea tu cuenta blackpix</div>

	<div class="center">
		<div class="form-group">
              <label class="gray-label">Nombre y apellido <small class="require">*</small></label>
              <input type="text" name="nombre" class="input-text input-white" placeholder="Nombre y Apellido...">
          </div>

          <div class="form-group">
              <label class="gray-label">Email <small class="require">*</small></label>
              <input type="email" name="email" class="input-text input-white" placeholder="Email...">
          </div>

          <div class="form-group">
              <label class="gray-label">Teléfono <small class="require">*</small></label>
              <input type="text" name="telefono" class="input-text input-white" placeholder="Teléfono...">
          </div>

          <div class="form-group">
              <label class="gray-label">Contraseña <small class="require">*</small></label>
              <input type="password" name="password" class="input-text input-white" placeholder="Contraseña...">
          </div>

          <div class="form-group">
              <label class="gray-label">Repetir Contraseña <small class="require">*</small></label>
              <input type="password" name="re-password" class="input-text input-white" placeholder="Repetir contraseña...">
          </div>

          <div class="content-chk_terminos_r">
              <input type="checkbox" name="terminos" id="chk_terminos" class="chk chk_white">
              <label id="chk_terminos">Acepto los <a href="terminos-y-condiciones">términos y condiciones.</a></label>
          </div>

          <div class="clearfix"></div>
          <a href="javascript:void(0)" class="btnCrearCuenta crearCuentaCarro">Crear cuenta</a>
	</div>
</div>

<div class="contentPopPassword">
	<div class="closePopCarroPass"><img src="img/close_pop.png"></div>

	<div class="title">restablecer contraseña</div>
	<div class="subtitle">Ingresa tu dirección de correo electrónico y te enviaremos
instrucciones para restablecer la contraseña.</div>

	<div class="center">
		
		<div class="form-group">
			<input type="email" name="email" class="input-text input-white" placeholder="Email...">
		</div>

		<a href="javascript:void(0)" class="btnRestablecer">Enviar</a>
		<div class="fadeOutPop">o vuelve a <a href="javascript:void(0)">iniciar sesión</a></div>

	</div>

</div>