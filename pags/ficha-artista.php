<?php

require_once 'paginador/paginator.class.php';

/*$id = mysqli_real_escape_string($conexion, $_GET['id']);
$nombre = mysqli_real_escape_string($conexion, $_GET['nombre']);*/


$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
$nombre = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;

$infoArtista = consulta_bd('nombre, banner_principal, banner_secundario, biografia', 'artistas', "id = $id", '');



$filtros_grilla['artista'] = $id;
$productosArtista = get_products($filtros_grilla);
$total = $productosArtista['productos']['total'][0];

$pages = new Paginator;
$pages->items_total = $total;
$pages->mid_range = 8;
$rutaRetorno = "artista/$id/$nombre";
$pages->paginateArtistas($rutaRetorno);

$filtros_grilla['limit'] = $pages->limit;

$productosArtista = get_products($filtros_grilla);


?>

<h2 class="tituloArtistas"><?= $infoArtista[0][0] ?></h2>

<div class="container mb-40">

    <div id="slider2">
        <div class="slider-artistas">
            <div class="item">
                <a href="artista/<?= $id . "/" . $nombre ?>" class="s-desktop">
                    <?php if ($infoArtista[0][1] != null and $infoArtista[0][1] != '' and is_file("imagenes/artistas/{$infoArtista[0][1]}")) : ?>
                        <img src="<?= imagen("imagenes/artistas/", $infoArtista[0][1]); ?>">
                    <?php else : ?>
                        <img src="<?= imagen("img/", "sinBannerArtista.jpg"); ?>">
                    <?php endif; ?>
                </a>
            </div>
        </div>
    </div>

</div>

<div class="gray-body">

    <div class="container mt-40 mb-40">
        <div class="titulo-home mb-20">Biografía</div>
        <div class="contentBiografia">
            <div class="col">
                <?php if ($infoArtista[0][2] != null and $infoArtista[0][2] != '' and is_file("imagenes/artistas/{$infoArtista[0][2]}")) : ?>
                    <img src="<?= imagen("imagenes/artistas/", $infoArtista[0][2]); ?>" alt="">
                <?php else : ?>
                    <img src="<?= imagen("img/", "sinBannerSecundario.jpg"); ?>">
                <?php endif; ?>
            </div>
            <div class="col">
                <div class="biografia">
                    <?= $infoArtista[0][3] ?>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="container mt-40 mb-40">


    <div class="grid-products mt-30">
        <?php foreach ($productosArtista['productos']['producto'] as $prd) : ?>
            <a href="ficha/<?= $prd['id_producto'] ?>/<?= $prd['nombre_seteado'] ?>" class="col">
                <div class="thumbg">
                    <img src="<?= $prd['imagen_grilla'] ?>" />
                </div>
                <div class="add_fav" onclick="addToFav(<?= $prd['id_hijo'] ?>, event, this)">
                    <?php if (productoGuardado($prd['id_hijo'])) : ?>
                        <i class="material-icons">favorite</i>
                    <?php else : ?>
                        <i class="material-icons">favorite_border</i>
                    <?php endif ?>
                </div>

                <?php if ($prd['descuento'] > 0) : ?>
                    <span class="descuentoGrilla"><?= round(100 - ($prd['descuento'] * 100) / $prd['precio']) ?>% DCTO</span>
                <?php endif ?>

                <div class="data">
                    <div class="titles">
                        <span class="artista"><?= $prd['artista'] ?></span>
                        <span class="nombre"><?= $prd['nombre'] ?></span>
                    </div>
                    <div class="precio">
                        <?php if ($prd['descuento'] > 0) : ?>
                            <span class="oferta"><?= cambiarMoneda($prd['precio']) ?></span>
                            <div class="precio_final">
                                <span>Desde</span>
                                <span><?= cambiarMoneda($prd['descuento']) ?></span>
                            </div>
                        <?php else : ?>
                            <span class="oferta"></span>
                            <div class="precio_final">
                                <span>Desde</span>
                                <span><?= cambiarMoneda($prd['precio']) ?></span>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </a>
        <?php endforeach ?>
    </div>

    <div class="paginador-productos paginador-artistas">

        <div class="paginas">
            <?= $pages->display_pages(); ?>
        </div>
    </div>

</div>