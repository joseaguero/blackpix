<div class="gray-body">
    <div class="subtitulo-sac">
        ¿Qué es BlackPix?
    </div>
    <div class="container body-somos mt-30">
        <p>Blackpix nace como un estudio editorial de producción, difusión y venta de obra fine art.</p>

        <p>Nuestro propósito es convertirnos en un motor de difusión y promoción del hacer fotográfico y artístico contemporáneo, cuyos trabajos visuales enriquecen y aportan a la construcción de una identidad colectiva y cultural.</p>

        <p>Toda nuestra obra es impresa en el laboratorio de Blackpix y se produce con estándares de calidad museo, en papeles de conservación, libre de ácidos, impresos con tintas de pigmentos naturales (Giclée) que permiten duraciones de mas de 100 años en estado de conservación optima y se vende en diferentes ediciones. </p>

        <p>En BLACKPIX somos un grupo de amantes a la Fotografía y al Arte. Nuestra misión es democratizar el arte, para que este pueda llegar a todos los segmentos de la sociedad a valores asequibles.</p>

        <p>Todas nuestras obras son piezas originales de serie limitada y certificadas mediante certificados de autenticidad.</p>

        <p>Somos una alternativa diferente a las galerías de arte tradicional, contamos con un amplio catálogo de obras cuidadosamente seleccionadas que pueden ser vistas y compradas online.</p>


    </div>
</div>