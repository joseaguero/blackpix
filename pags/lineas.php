<?php
require_once 'paginador/paginator.class.php';

$page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 1;

$id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
$nombreLinea = (isset($_GET[nombre])) ? mysqli_real_escape_string($conexion, $_GET[nombre]) : 0;

$id_marca = (is_numeric($_GET[id_marca])) ? mysqli_real_escape_string($conexion, $_GET[id_marca]) : 0;
$marca = (isset($_GET[marca])) ? mysqli_real_escape_string($conexion, $_GET[marca]) : 0;
$orden = (isset($_GET['orden'])) ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;

$datos_grilla = consulta_bd("id,nombre", "lineas", "id=$id", "");

$categorias = (isset($_GET['cat']) and $_GET['cat'] != '') ? mysqli_real_escape_string($conexion, $_GET['cat']) : 0;
$artistas = (isset($_GET['art']) and $_GET['art'] != '') ? mysqli_real_escape_string($conexion, $_GET['art']) : 0;
$tamaños = (isset($_GET['tam']) and $_GET['tam'] != '') ? mysqli_real_escape_string($conexion, $_GET['tam']) : 0;
$color = (isset($_GET['color']) and $_GET['color'] != '') ? mysqli_real_escape_string($conexion, $_GET['color']) : 0;
$filtro_desde = (isset($_GET['desde']) and $_GET['desde'] != '') ? mysqli_real_escape_string($conexion, $_GET['desde']) : 0;
$filtro_hasta = (isset($_GET['hasta']) and $_GET['hasta'] != '') ? mysqli_real_escape_string($conexion, $_GET['hasta']) : 0;
$ofertas = (isset($_GET['ofertas']) and $_GET['ofertas'] != '') ? mysqli_real_escape_string($conexion, $_GET['ofertas']) : 0;

if ($orden != '' or $artistas != '' or $categorias != '' or $filtro_desde != '' or $filtro_hasta != '' or $color != '' or $tamaños != '') {
    $conteo_de_filtros = 1;
}

$explode_categorias = explode('-', $categorias);
$explode_artistas = explode('-', $artistas);
$explode_colores = explode('-', $color);
$explode_tamaños = explode('-', $tamaños);

if (!is_numeric($orden)) {
    if ($orden == "desc") {
        $orderSql = ' valorMenor desc';
        $titulo_orden = 'Precio de mayor a menor';
    } else if ($orden == "asc") {
        $orderSql = ' valorMenor asc';
        $titulo_orden = 'Precio de menor a mayor';
    } else if ($orden == 'rel') {
        $orderSql = "posicion asc";
        $titulo_orden = 'Recomendados';
    }
} else {
    $orderSql = "posicion asc";
    $titulo_orden = 'Recomendados';
}

if ($artistas != 0) {
    $filtros_grilla['artistas'] = $artistas;
    $filtro_max['artistas'] = $artistas;
    $artistas_display = true;
}

if ($categorias != 0) {
    $filtros_grilla['categorias'] = $categorias;
    $filtro_max['categorias'] = $categorias;
    $categorias_display = true;
}

if ($filtro_desde != 0) {
    $filtros_grilla['desde'] = $filtro_desde;
    $precios_display = true;
}

if ($filtro_hasta != 0) {
    $filtros_grilla['hasta'] = $filtro_hasta;
    $precios_display = true;
}

if ($color != 0) {
    $filtros_grilla['colores'] = $color;
    $filtro_max['colores'] = $color;
    $colores_display = true;
}

if ($tamaños != 0) {
    $filtros_grilla['tamaños'] = $tamaños;
    $filtro_max['tamaños'] = $tamaños;
    $tamaños_display = true;
}

if ($ofertas != 0) {
    $filtros_grilla['ofertas'] = true;
    $filtro_max['ofertas'] = true;
    $ofertas_display = true;
}

$filtros_grilla['linea'] = $id;

$productos = get_products($filtros_grilla);

$total = $productos['productos']['total'][0];
$total_paginas = ceil($total / 18);

$pages = new Paginator;
$pages->items_total = $total;
$pages->mid_range = 8;
$rutaRetorno = "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&ofertas=$ofertas&orden=$orden";
$pages->paginate($rutaRetorno);

$filtros_grilla['limit'] = $pages->limit;
$filtros_grilla['orden'] = $orderSql;
$productos = get_products($filtros_grilla);

$filtro_max['linea'] = $id;
$filtro_max['max'] = true;
$maximo_valor = get_products($filtro_max);

if ($maximo_valor['valor_max'] != '') {
    $maximo_actual = ($filtro_hasta != 0) ? $filtro_hasta : $maximo_valor['valor_max'] + 1000;
} else {
    $maximo_valor['valor_max'] = $filtro_hasta;
    $maximo_actual = $filtro_hasta;
}

/*echo '<pre>';
    print_r($productos);
    echo '</pre>';

    die();*/

?>

<div class="breadcrumbs">
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="lineas/<?= $id ?>/<?= $nombreLinea ?>" itemprop="item" class="active">
                <span itemprop="name"><?= ucwords($datos_grilla[0][1]) ?></span>
                <meta itemprop="position" content="2" />
            </a>
        </li>
    </ul>
</div>

<div class="grid-page container mb-35">
    <div class="col filtros_col" data-ref="lineas">
        <div class="titulo_filtros">Filtros</div>

        <div class="content_filtros">
            <?php if ($conteo_de_filtros == 1) :
                $filtros_aplicados = filtros_aplicados($artistas, $filtro_desde, $filtro_hasta, $categorias, $color, $tamaños); ?>
                <div class="subtitle">Filtros aplicados:</div>
                <div class="content_filtros_aplicados mt-15 mb-20">
                    <?php foreach ($filtros_aplicados['filtro_categorias'] as $item) : ?>
                        <a href="javascript:void(0)" class="aplicado cat_aplicadas" data-id="<?= $item['id'] ?>" data-action="cat"><span><?= $item['nombre'] ?></span> X</a>
                    <?php endforeach ?>

                    <?php foreach ($filtros_aplicados['filtro_artistas'] as $item) : ?>
                        <a href="javascript:void(0)" class="aplicado art_aplicadas" data-id="<?= $item['id'] ?>" data-action="art"><span><?= $item['nombre'] ?></span> X</a>
                    <?php endforeach ?>

                    <?php foreach ($filtros_aplicados['filtro_colores'] as $item) : ?>
                        <a href="javascript:void(0)" class="aplicado color_aplicadas" data-id="<?= $item['id'] ?>" data-action="color"><span><?= $item['nombre'] ?></span> X</a>
                    <?php endforeach ?>

                    <?php foreach ($filtros_aplicados['filtro_tamaños'] as $item) : ?>
                        <a href="javascript:void(0)" class="aplicado tam_aplicadas" data-id="<?= $item['id'] ?>" data-action="tam"><span><?= $item['nombre'] ?></span> X</a>
                    <?php endforeach ?>

                    <?php if ($ofertas_display) : ?>
                        <a href="javascript:void(0)" class="aplicado off_aplicadas" data-id="0" data-action="ofertas">OFERTAS</span> X</a>
                    <?php endif ?>

                    <?php foreach ($filtros_aplicados['filtro_precios'] as $item) : ?>
                        <a href="javascript:void(0)" class="aplicado precio_aplicadas" data-id="<?= $item['id'] ?>" data-action="precio"><span><?= $item ?></span> X</a>
                    <?php endforeach ?>
                </div>

                <div class="clearfix mb-15"></div>
            <?php endif ?>

            <div class="box_filtro">
                <div class="subtitle">ordenar por:</div>

                <div class="select_filtros mt-20" data-order="<?= $orden ?>">
                    <div class="selected">
                        <span><?= $titulo_orden ?></span>
                        <img src="img/icons/arrow-bottom.png" class="arrow-bt" />
                    </div>
                    <div class="list">
                        <?php if (!is_numeric($orden)) : ?>
                            <?php if ($orden == 'desc') : ?>

                                <li>
                                    <a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=rel&ofertas=$ofertas&page=1" ?>">
                                        Recomendados</a>
                                </li>
                                <li><a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=asc&ofertas=$ofertas&page=1" ?>">
                                        Precio de menor a mayor
                                    </a>
                                </li>

                            <?php elseif ($orden == 'asc') : ?>
                                <li>
                                    <a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=rel&ofertas=$ofertas&page=1" ?>">
                                        Recomendados
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=desc&ofertas=$ofertas&page=1" ?>">
                                        Precio de mayor a menor
                                    </a>
                                </li>
                            <?php elseif ($orden == 'rel') : ?>
                                <li>
                                    <a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=asc&ofertas=$ofertas&page=1" ?>">
                                        Precio de menor a mayor
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=desc&ofertas=$ofertas&page=1" ?>">
                                        Precio de mayor a menor
                                    </a>
                                </li>
                            <?php endif ?>
                        <?php else : ?>
                            <li>
                                <a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=asc&ofertas=$ofertas&page=1" ?>">
                                    Precio de menor a mayor
                                </a>
                            </li>
                            <li>
                                <a href="<?= "lineas/$id/$nombreLinea?cat=$categorias&art=$artistas&tam=$tamaños&color=$color&desde=$filtro_desde&hasta=$filtro_hasta&orden=desc&ofertas=$ofertas&page=1" ?>">
                                    Precio de mayor a menor
                                </a>
                            </li>
                        <?php endif ?>


                    </div>
                </div>
            </div>

            <div class="box_filtro mt-25">
                <div class="subtitle">Categorías</div>

                <?php
                $categorias = consulta_bd('c.id, c.nombre', 'categorias c JOIN lineas_productos lp ON lp.categoria_id = c.id JOIN productos p ON lp.producto_id = p.id JOIN productos_detalles pd ON pd.producto_id = p.id', "c.publicada = 1 AND p.publicado = 1 AND pd.publicado = 1 AND pd.principal = 1 AND c.linea_id = $id GROUP BY c.id", 'c.posicion asc');
                ?>

                <div class="chk_filtro mt-10">
                    <?php foreach ($categorias as $catl) :
                        $checked_c = (in_array($catl[0], $explode_categorias)) ? "checked" : ''; ?>
                        <div class="row">
                            <input type="checkbox" name="chk_cat" class="chk" value="<?= $catl[0] ?>" id="cat_<?= $catl[0] ?>" <?= $checked_c ?>>
                            <label for="cat_<?= $catl[0] ?>"><?= $catl[1] ?></label>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="box_filtro mt-25">
                <div class="subtitle">Artistas</div>
                <?php $artistas = consulta_bd('a.id, a.nombre', 'artistas a join productos p on p.artista_id = a.id join lineas_productos lp on lp.producto_id = p.id join productos_detalles pd ON pd.producto_id = p.id', "lp.linea_id = $id and p.publicado = 1 and pd.publicado = 1 and pd.principal = 1 group by a.id", 'a.nombre asc');
                ?>

                <div class="chk_filtro mt-10">
                    <?php foreach ($artistas as $artl) :
                        $checked_a = (in_array($artl[0], $explode_artistas)) ? "checked" : ''; ?>
                        <div class="row">
                            <input type="checkbox" name="chk_art" class="chk" value="<?= $artl[0] ?>" id="art_<?= $artl[0] ?>" <?= $checked_a ?>>
                            <label for="art_<?= $artl[0] ?>"><?= $artl[1] ?></label>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="box_filtro mt-25">
                <div class="subtitle">Tamaños</div>
                <?php $tamanios = consulta_bd('id, nombre', 'grupo_tamanios', '', 'id asc'); ?>

                <div class="chk_filtro mt-10">
                    <?php foreach ($tamanios as $tl) :
                        $checked_t = (in_array($tl[0], $explode_tamaños)) ? "checked" : ''; ?>
                        <div class="row">
                            <input type="checkbox" name="chk_tam" class="chk" value="<?= $tl[0] ?>" id="tam_<?= $tl[0] ?>" <?= $checked_t ?>>
                            <label for="tam_<?= $tl[0] ?>>"><?= $tl[1] ?></label>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="box_filtro mt-25">
                <div class="subtitle">Color</div>
                <?php $colores_fil = consulta_bd('c.id, c.nombre', 'colores c JOIN colores_productos cp ON cp.color_id = c.id JOIN productos p ON cp.producto_id = p.id JOIN productos_detalles pd ON pd.producto_id = p.id join lineas_productos lp on lp.producto_id = p.id', "p.publicado = 1 and pd.publicado = 1 and pd.principal = 1 and lp.linea_id = $id  GROUP BY c.id", 'c.nombre asc'); ?>

                <div class="select_filtros_nochange mt-20">
                    <div class="selected">
                        <span>Elegir color</span>
                        <img src="img/icons/arrow-bottom.png" class="arrow-bt" />
                    </div>
                    <div class="list">
                        <div class="content_flinside">
                            <div class="chk_filtro">
                                <?php foreach ($colores_fil as $c) :
                                    $checked_color = (in_array($c[0], $explode_colores)) ? "checked" : ''; ?>
                                    <div class="row">
                                        <input type="checkbox" name="chk_color" class="chk" value="<?= $c[0] ?>" id="tam_<?= $c[0] ?>" <?= $checked_color ?>>
                                        <label for="tam_<?= $c[0] ?>>"><?= ucwords($c[1]) ?></label>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- FIN colores -->

            <?php if (!isset($_SESSION['usd'])) : ?>
                <div class="box_filtro mt-25 precio_filtro" data-max="<?= $maximo_valor['valor_max'] + 1000 ?>" data-max-actual=<?= $maximo_actual ?>>
                    <div class="subtitle">Precio</div>

                    <div class="box_price mt-10" data-ref="prices">
                        <div class="center_price">
                            <span class="min-filtro" data-info="<?= $filtro_desde ?>" data-valor="0"></span> -
                            <span class="max-filtro" data-info="<?= $maximo_valor['valor_max'] + 1000 ?>" data-valor="<?= $maximo_actual ?>"></span>
                        </div>

                        <div class="clearfix"></div>
                        <div id="slider-range"></div>
                    </div>
                </div>
            <?php endif ?>

            <a href="javascript:void(0)" class="btn-ofertas" data-off="<?= $ofertas ?>">Ofertas</a>
        </div>

    </div>
    <div class="col">

        <div class="gr-titulo-grillas">
            <div class="resultados">
                <div><span><?= $total ?></span> resultados</div>
                <div>página <span><?= $page ?></span> de <span><?= $total_paginas ?></span></div>
            </div>
            <div class="titulo_grilla">
                <span><?= $datos_grilla[0][1] ?></span>
            </div>
            <div class="nonfilter">
                <input type="checkbox" name="chk_filtro_mostrar" id="chk_filtro_grillas" checked="checked">
                <span></span>
            </div>
            <div class="nonfilter-xs">
                <input type="checkbox" name="chk_filtro_mostrar" id="chk_filtro_grillas_xs">
                <span></span>
            </div>
        </div>

        <div class="grid-products mt-30">
            <?php foreach ($productos['productos']['producto'] as $prd) : ?>
                <a href="ficha/<?= $prd['id_producto'] ?>/<?= $prd['nombre_seteado'] ?>" class="col">
                    <div class="thumbg">
                        <img src="<?= $prd['imagen_grilla'] ?>" />
                    </div>
                    <div class="add_fav" onclick="addToFav(<?= $prd['id_hijo'] ?>, event, this)">
                        <?php if (productoGuardado($prd['id_hijo'])) : ?>
                            <i class="material-icons">favorite</i>
                        <?php else : ?>
                            <i class="material-icons">favorite_border</i>
                        <?php endif ?>
                    </div>

                    <?php if ($prd['descuento'] > 0) : ?>
                        <span class="descuentoGrilla"><?= round(100 - ($prd['descuento'] * 100) / $prd['precio']) ?>% DCTO</span>
                    <?php endif ?>

                    <div class="data">
                        <div class="titles">
                            <span class="artista"><?= $prd['artista'] ?></span>
                            <span class="nombre"><?= $prd['nombre'] ?></span>
                        </div>
                        <div class="precio">
                            <?php if ($prd['descuento'] > 0) : ?>
                                <span class="oferta"><?= cambiarMoneda($prd['precio']) ?></span>
                                <div class="precio_final">
                                    <span>Desde</span>
                                    <span><?= cambiarMoneda($prd['descuento']) ?></span>
                                </div>
                            <?php else : ?>
                                <span class="oferta"></span>
                                <div class="precio_final">
                                    <span>Desde</span>
                                    <span><?= cambiarMoneda($prd['precio']) ?></span>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </a>
            <?php endforeach ?>
        </div>

        <div class="paginador-productos">
            <div class="totales_page">
                <div>página <span><?= $page ?></span> de <span><?= $total_paginas ?></span></div>
            </div>
            <div class="paginas">
                <span class="title_paginador">Páginas:</span>
                <?= $pages->display_pages(); ?>
            </div>
        </div>

    </div>
</div>