<?php
include("../admin/conf.php");
$id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
$direccion = consulta_bd("nombre, region_id, ciudad_id, comuna_id, localidad_id, calle, numero","clientes_direcciones","id=$id","");

$regiones = consulta_bd("id, nombre","regiones","","posicion asc");
 
$comuna = $direccion[0][3];
$comuna_id = consulta_bd("ciudade_id","comunas","id = $comuna","nombre asc"); 
$ciudad_id = $comuna_id[0][0]; 

$comunas = consulta_bd("id, nombre","comunas","ciudade_id = $ciudad_id","id asc"); 

$localidades = consulta_bd("id, nombre","localidades","comuna_id = $comuna","id asc"); 
$localidad_id = $direccion[0][4];
?>
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.uniform.standalone.js"></script>
<script type="text/javascript">
	$(function() {
		$("select").uniform();
		
		$("#region2").change(function() {
			var region_id = $(this).val();
			$("#comuna2").load("tienda/comunas.php?region_id=" + region_id);
			$("#uniform-comuna2 span").html("Seleccione comuna");
			$("#uniform-localidad2 span").html("Localidades");
		});
		$("#comuna2").change(function() {
			var comuna_id = $(this).val();
			console.log(comuna_id);
			$("#localidad2").load("tienda/localidades.php?comuna_id=" + comuna_id);
			$("#uniform-localidad2 span").html("Seleccione Localidad.");
		});
		
		$("#actualizarDireccion").click(function(){
			var nombre = $("#nombre2").val();
			var region = $("#region2").val();
			var comuna = $("#comuna2").val();
			var localidad = $("#localidad2").val();
			var calle = $("#calle2").val();
			var numero = $("#numero2").val();
			if(nombre != "" && region != "" && comuna != "" && localidad != "" && calle != "" && numero != ""){
				$("#formActualizaDireccion").submit();
				} else {
					console.log("Faltan campos por completar.");
					swal({
					  type: 'error',
					  text: 'Faltan campos por completar.'
					});
				}
			});
			
			
	}); 
</script>

<div class="contNuevaDireccion" id="contNuevaDireccion">

    <div class="cont100 direcciones">
        <form method="post" action="ajax/actualizarDireccion.php" id="formActualizaDireccion">
            <input type="hidden" name="id" value="<?= $id; ?>">
            <h2>Editar dirección</h2>
            <div class="cont100">
                <div class="ancho50Centro">
                    <label for="">Nombre asignado</label>
                    <input class="campoPasoFinal" type="text" name="nombre" id="nombre2" value="<?= $direccion[0][0];?>" />
                </div>
                <div class="ancho50Centro">
                    <label>Región</label>
                    <select name="region_id" id="region2">
                        <?php for($i=0; $i<sizeof($regiones); $i++) {?>
                        <option <?php if($regiones[$i][0] == $direccion[0][1]){echo "selected";}?> value="<?= $regiones[$i][0];?>"><?= $regiones[$i][1];?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="cont100">
                <div class="ancho50Centro">
                    <label>Comuna</label>
                    <select name="comuna_id" id="comuna2">
                        <?php for($i=0; $i<sizeof($comunas); $i++){ ?>
                        <option <?php if($comunas[$i][0] == $comuna){echo "selected";} ?> value="<?= $comunas[$i][0]; ?>"><?= $comunas[$i][1]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="ancho50Centro">
                    <label>Localidad</label>
                    <select name="localidad_id" id="localidad2">
                        <?php for($i=0; $i<sizeof($localidades); $i++){ ?>
                        <option <?php if($localidades[$i][0] == $localidad_id){echo "selected";} ?> value="<?= $localidades[$i][0]; ?>"><?= $localidades[$i][1]; ?></option>
                        <?php } ?>
                    </select>
                </div>
              </div> 
               
             
            <div class="cont100">
                <div class="ancho50Centro">
                    <label>Calle</label>
                    <input class="campoPasoFinal" type="text" name="calle" value="<?= $direccion[0][5];?>" id="calle2" />
                </div>
                <div class="ancho50Centro">
                    <label>Número</label>
                    <input class="campoPasoFinal" type="text" name="numero" value="<?= $direccion[0][6];?>" id="numero2" />
                </div>
            </div>
            <div class="cont100">
                <a class="guardarDireccion" id="actualizarDireccion" href="javascript:void(0)">Guardar cambios</a>
            </div>
            
        </form>
    </div>
</div>
            