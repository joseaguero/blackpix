<?php

$sesionIniciada = (isset($_COOKIE['usuario_id']) and $_COOKIE['usuario_id'] != NULL) ? true : false;

if ($sesionIniciada) :
    $datosUsuario = consulta_bd('id, nombre, email, telefono', 'clientes', "id = {$_COOKIE['usuario_id']}", '');

    $nombre     = $datosUsuario[0][1];
    $telefono   = $datosUsuario[0][3];
    $email      = $datosUsuario[0][2];

else :

    $nombre     = '';
    $telefono   = '';
    $email      = trim(strtolower($_SESSION["correo"]));

endif;

?>

<form method="post" action="proceso-pago" id="formCompra">
    <div class="body-ep">
        <div class="col">
            <div class="titulo_ep">Tus datos</div>

            <div class="optionGroup">
                <div class="group groupDocumento">
                    <input type="radio" name="tipoDocumento" value="1" class="radioCart" id="radioBoleta" checked>
                    <label class="lblTitle" for="radioBoleta">Boleta</label>
                </div>

                <div class="group groupDocumento">
                    <input type="radio" name="tipoDocumento" value="2" class="radioCart" id="radioFactura">
                    <label class="lblTitle" for="radioFactura">Factura</label>
                </div>
            </div>

            <div class="form-group">
                <label>Nombre y apellido <small class="require">*</small></label>
                <input type="text" name="nombre" class="input-text" data-valid="true" placeholder="Nombre y Apellido..." value="<?= $nombre ?>">
            </div>

            <div class="form-group">
                <label>Email <small class="require">*</small></label>
                <input type="email" name="email" class="input-text" data-valid="true" placeholder="Email..." value="<?= $email ?>">
            </div>

            <div class="form-group">
                <label>Teléfono <small class="require">*</small></label>
                <input type="text" name="telefono" class="input-text" data-valid="true" placeholder="Teléfono..." onkeypress="return justNumbers(event);" maxlength="9" value="<?= $telefono ?>">
            </div>

            <div class="contenedor-factura">
                <div class="sep_cart"></div>

                <div class="form-group">
                    <label>Rut empresa <small class="require">*</small></label>
                    <input type="text" name="rut_factura" class="input-text rutInput" placeholder="Rut empresa...">
                </div>

                <div class="form-group">
                    <label>Razón social <small class="require">*</small></label>
                    <input type="text" name="razon_social" class="input-text" placeholder="Razón social...">
                </div>

                <div class="form-group">
                    <?php $regionesFac = consulta_bd('id, nombre', 'regiones', '', 'posicion asc'); ?>
                    <label>Región <small class="require">*</small></label>
                    <select class="select-form" name="region_factura">
                        <option value="0">Región</option>
                        <?php foreach ($regionesFac as $regFac) : ?>
                            <option value="<?= $regFac[0] ?>"><?= $regFac[1] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Comuna <small class="require">*</small></label>
                    <select class="select-form" name="comuna_factura">
                        <option value="0">Comuna</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Dirección <small class="require">*</small></label>
                    <input type="text" name="direccion_factura" class="input-text" placeholder="Dirección...">
                </div>

                <!-- <div class="texto_factura">
                    <strong>Selecciona tu actividad económica:</strong>
                    Puedes ingresar tu actividad usando el giro correspondiente, o buscar en el campo actividad.
                </div> -->

                <div class="form-group">
                    <label>Giro <small class="require">*</small></label>
                    <input type="text" name="giro" class="input-text" placeholder="6 dígitos...">
                </div>

                <!-- <div class="form-group">
                    <label>Actividad económica <small class="require">*</small></label>
                    <input type="text" name="actividad_economica" class="input-text" placeholder="Ingresa tu actividad económica...">
                </div> -->

            </div>
        </div>
        <div class="col">
            <div class="titulo_ep">Dirección de envío</div>

            <div class="optionGroup">
                <div class="group">
                    <input type="radio" name="tipoEnvio" value="1" class="radioCart" id="radioRetiro">
                    <label class="lblTitle" for="radioRetiro">Retiro en tienda</label>
                </div>

                <div class="group">
                    <input type="radio" name="tipoEnvio" value="2" class="radioCart" id="radioDespacho" checked>
                    <label class="lblTitle" for="radioDespacho">Despacho</label>
                </div>
            </div>

            <div class="content_retiro">

                <div class="form-group">
                    <?php $sucursales = consulta_bd('id, nombre', 'sucursales', 'retiro_en_tienda = 1', 'posicion asc'); ?>
                    <label>Sucursal <small class="require">*</small></label>
                    <select class="select-form" name="sucursal">
                        <option value="0">Sucursal</option>
                        <?php foreach ($sucursales as $sucursal) : ?>
                            <option value="<?= $sucursal[0] ?>"><?= $sucursal[1] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Nombre y apellido <small class="require">*</small></label>
                    <input type="text" name="nombre_retiro" class="input-text" placeholder="Nombre y apellido...">
                </div>

                <div class="form-group">
                    <label>Rut <small class="require">*</small></label>
                    <input type="text" name="rut_retiro" class="input-text rutInput" placeholder="Rut...">
                </div>
            </div>

            <div class="content_despacho">

                <?php if ($sesionIniciada) : ?>
                    <div class="ajx_direcciones">
                        <div class="content_direccionesCarro">
                            <?php $clienteId = $_COOKIE['usuario_id']; ?>
                            <div class="form-group">
                                <?php $direcciones = consulta_bd('id, nombre', 'clientes_direcciones', "cliente_id = {$clienteId}", 'id desc'); ?>
                                <label>Mis direcciones <small class="require">*</small> <a href="javascript:void(0)" class="addDirCarro" onclick="addDirCarro()">agregar dirección</a></label>
                                <select class="select-form direccionesCarro" name="direcciones" onchange="selectDirecciones(this)">
                                    <option value="0">Mis direcciones</option>
                                    <?php foreach ($direcciones as $dir) : ?>
                                        <option value="<?= $dir[0] ?>"><?= $dir[1] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <div class="form-group">
                    <?php $paises = consulta_bd('id, nombre', 'paises', '', 'posicion asc'); ?>
                    <label>País <small class="require">*</small></label>
                    <select class="select-form" name="pais" data-valid="true">
                        <option value="0">País</option>
                        <?php foreach ($paises as $pais) : ?>
                            <option value="<?= $pais[0] ?>"><?= $pais[1] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="content_opdespacho">
                    <div class="form-group">
                        <label>Región <small class="require">*</small></label>
                        <select class="select-form regionesDir" data-valid="true" name="region">
                            <option value="0">Región</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Comuna <small class="require">*</small></label>
                        <select class="select-form comunasDir" data-valid="true" name="comuna">
                            <option value="0">Comuna</option>
                        </select>
                    </div>
                </div>

                <div class="texto_aviso">El despacho al extranjero será Por Pagar</div>

                <div class="form-group">
                    <label>Dirección <small class="require">*</small></label>
                    <input type="text" name="direccion" class="input-text" data-valid="true" placeholder="Dirección...">
                </div>

                <div class="form-group">
                    <label>Comentarios</label>
                    <textarea class="input-area" name="comentarios" rows="4" placeholder="Ejemplo: Dejar con el conserje..."></textarea>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="grid-vertical-resumen">
                <div class="col">
                    <div class="sep_cart sep-xs"></div>
                    <div class="titulo_ep">Resumen de compra</div>
                    <button class="btnComprar">comprar productos</button>

                    <div class="content-chk_terminos">
                        <input type="checkbox" name="terminos" id="chk_terminos" class="chk">
                        <label id="chk_terminos">Al comprar acepto los <a href="terminos-y-condiciones">términos y condiciones.</a></label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col">
                    <div class="sep_cart"></div>
                    <div class="title_fp">Forma de pago</div>

                    <div class="ajx_metodos">
                        <div class="grid-fp">
                            <?php if (isset($_SESSION['compra_otro_pais']) and $_SESSION['compra_otro_pais']) : ?>
                                <div class="metodo">
                                    <input type="radio" class="radioCart" name="metodoPago" value="transferencia" id="fpTrans">
                                    <label for="fpTrans">Transferencia<br>Bancaria</label>
                                </div>
                            <?php else : ?>
                                <div class="metodo">
                                    <input type="radio" class="radioCart" name="metodoPago" value="webpay" checked="checked" id="fpWebpay">
                                    <label for="fpWebpay"><img src="img/carroWebpay.png"></label>
                                </div>
                                <div class="metodo">
                                    <input type="radio" class="radioCart" name="metodoPago" value="mercadopago" id="fpMpago">
                                    <label for="fpMpago"><img src="img/carroMpago.png"></label>
                                </div>
                                <div class="metodo">
                                    <input type="radio" class="radioCart" name="metodoPago" value="transferencia" id="fpTrans">
                                    <label for="fpTrans">Transferencia<br>Bancaria</label>
                                </div>
                            <?php endif ?>

                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="sep_cart sep-desk"></div>
                    <div class="cajaTotalesCarro">
                        <?php $valoresTotales = valorTotalCarro(0);  ?>
                        <div class="row_total">
                            <span>Subtotal:</span>
                            <span class="number">$<?= number_format($valoresTotales['subtotal'], 0, ',', '.') ?>
                                <?php if (isset($_SESSION['usd']) and $valoresTotales['subtotal'] > 0) : ?>
                                    (<?= cambiarMoneda($valoresTotales['subtotal']) ?>)
                                <?php endif ?></span>
                        </div>

                        <div class="row_total">
                            <span>Iva:</span>
                            <span class="number">$<?= number_format($valoresTotales['iva'], 0, ',', '.') ?>
                                <?php if (isset($_SESSION['usd']) and $valoresTotales['iva'] > 0) : ?>
                                    (<?= cambiarMoneda($valoresTotales['iva']) ?>)</span>
                        <?php endif ?>
                        </div>

                        <div class="row_total">
                            <span>Envío:</span>
                            <span class="number">$<?= number_format($valoresTotales['despacho'], 0, ',', '.') ?>
                                <?php if (isset($_SESSION['usd']) and $valoresTotales['despacho'] > 0) : ?>
                                    (<?= cambiarMoneda($valoresTotales['despacho']) ?>)</span>
                        <?php endif ?>
                        </div>

                        <div class="row_total">
                            <span>Total:</span>
                            <span class="total_cart number">$<?= number_format($valoresTotales['total'], 0, ',', '.') ?>
                                <?php if (isset($_SESSION['usd']) and $valoresTotales['total'] > 0) : ?>
                                    (<?= cambiarMoneda($valoresTotales['total']) ?>)</span>
                        <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>



<!-- <div class="compraYAceptaTerminos">
    <div class="txtTerminosAcepto">
        <input type="checkbox" name="terminos" id="terminos" value="1" />
        <a href="<?php echo $url_base; ?>terminos-y-condiciones">
             Al comprar acepto los Términos y condiciones
        </a>
    </div>
    
    <a href="javascript:void(0)" class="btnTerminarCarro" id="comprar" />Comprar productos</a>
</div> -->

<?php if ($sesionIniciada) : ?>
    <div class="bg_dirCarro"></div>
    <div class="content_dirCarro">
        <div class="close_password"><img src="img/close_pop.png"></div>
        <h2>Agregar dirección</h2>

        <p>Ingresa tu dirección para registrarla en la compra rápida, y así poder guardarla para próximas compras.</p>

        <div class="form-group">
            <label class="dark-label">Nombre de la dirección</label>
            <input type="text" name="nombre_direccion" class="input-text input-white" placeholder="Nombre...">
        </div>

        <div class="form-group">
            <label>País <small class="require">*</small></label>
            <select class="select-form input-white paisDirPop" name="pais_direccion">
                <option value="0">País</option>
                <?php foreach ($paises as $pais) : ?>
                    <option value="<?= $pais[0] ?>"><?= $pais[1] ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="pop_content">

            <div class="form-group">
                <label>Región <small class="require">*</small></label>
                <select class="select-form input-white" name="region_direccion">
                    <option value="0">Región</option>
                </select>
            </div>

            <div class="form-group">
                <label>Comuna <small class="require">*</small></label>
                <select class="select-form input-white" name="comuna_direccion">
                    <option value="0">Comuna</option>
                </select>
            </div>

        </div>

        <div class="form-group">
            <label class="dark-label">Dirección</label>
            <input type="text" name="dir_direccion" class="input-text input-white" placeholder="Dirección...">
        </div>

        <a href="javascript:void(0)" class="btn btnCrearDirCarro">Agregar</a>

    </div>

    <div class="bg_dir"></div>
    <div class="pop_success">
        <div class="close_password"><img src="img/close_pop.png"></div>
        <p>Direccion agregada con éxito</p>

        <a href="javascript:void(0)" class="btnAceptarPopup">Aceptar</a>
    </div>
<?php endif ?>