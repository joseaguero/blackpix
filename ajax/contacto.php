<?php 

include('../admin/conf.php');
require_once('../PHPMailer/PHPMailerAutoload.php');
date_default_timezone_set('America/Santiago');

// Request
$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
$email = mysqli_real_escape_string($conexion, $_POST['email']);
$mensaje = mysqli_real_escape_string($conexion, $_POST['mensaje']);

if (trim($nombre) != '' AND trim($email) != '' AND trim($mensaje)) {
	$fecha_hoy = date("Y-m-d H:i:s", time());

	// Datos sitio
	$url_sitio = $url_base;
	$color_logo = opciones('color_logo');
	$logo = opciones('logo_mail');
	$asunto = 'Contacto desde BlackPix';
	$sitio = opciones('nombre_cliente');

	// Datos envío
	$host = opciones("Host");
	$port = opciones("Port");
	$smtp = opciones("SMTPSecure");
	$username = opciones("Username");
	$password = opciones("Password");

	$msje = '<html>
	<head><link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"></head>
	<body style="font-family: \'Open Sans\', sans-serif; color: #333;">
	<div style="background: #f9f9f9; width: 100%;">
		<div style="background: #fff; border-top: 2px solid '.$color_logo.'; border-bottom: 2px solid '.$color_logo.'; width: 90%;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$sitio.'">
			<p style="font-size: 14px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
			<p style="font-size: 13px; margin-top: 0;">Se ha enviado un nuevo mensaje desde la página de contacto <br>
			Los datos de la persona que contacta son:</p>
			<blockquote style="font-size: 13px;">
			<strong>Nombre:</strong> '.$nombre.' <br>
			<strong>E-mail:</strong> '.$email.' <br>
			<strong>Mensaje:</strong> '.$mensaje.' <br>
			</blockquote>

			<p style="font-size: 13px;">Rogamos contactarse lo antes posible con el cliente.<br><br>
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$sitio.'</b></p>
		</div>
	</div>
	</body>
	</html>';

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = $host;
	$mail->Port = $port;
	$mail->SMTPSecure = $smtp;
	$mail->SMTPAuth = true;
	$mail->Username = $username;
	$mail->Password = $password;

	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));

	$mail->addAddress($email, $nombre);
	$mail->Subject = $asunto;
	$mail->msgHTML($msje);
	$mail->AltBody = $msje;
	$mail->CharSet = 'UTF-8';

	if (!$mail->send()) {
		$tipo = 0;
		$msje = 'Error al enviar formulario de contacto';
	    // return "Mailer Error: " . $mail->ErrorInfo;
	} else {
	   	$tipo = 1;
		$msje = 'Mensaje enviado correctamente';

		$insert = insert_bd('contacto', 'nombre, email, mensaje, fecha_creacion', "'$nombre', '$email', '$mensaje', '$fecha_hoy'");
	}
}else{
	$tipo = 0;
	$msje = 'Debes completar todos los campos';
}

header("Location: contacto?a={$tipo}&msje={$msje}");

?>