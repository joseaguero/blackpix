<?php 

include('../admin/conf.php');

$producto_id = mysqli_real_escape_string($conexion, $_POST['producto_id']);

if (!isset($_COOKIE['usuario_id'])) {
	die("2"); // Usuario no logueado
}else{
	$cliente = (int)$_COOKIE['usuario_id'];

	$productoGuardado = consulta_bd('id', 'productos_guardados', "productos_detalle_id = $producto_id AND cliente_id = $cliente", '');

	if (!is_array($productoGuardado)) {
		$insert = insert_bd('productos_guardados', 'productos_detalle_id, cliente_id', "$producto_id, $cliente");
		if ($insert) {
			die("1"); // Guardado correctamente
		}else{
			die("0"); // Error al agregar el producto a guardados
		}
	}else{
		die("3"); // Producto ya está guardado
	}

}

?>