<?php 

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$comuna_id = (int)mysqli_real_escape_string($conexion, $_GET['comuna_id']);

$valoresTotales = valorTotalCarro($comuna_id); 

?>

<div class="row_total">
    <span>Subtotal:</span>
    <span class="number">$<?= number_format($valoresTotales['subtotal'], 0, ',', '.') ?></span>
</div>

<div class="row_total">
    <span>Iva:</span>
    <span class="number">$<?= number_format($valoresTotales['iva'], 0, ',', '.') ?></span>
</div>

<div class="row_total">
    <span>Envío:</span>
    <?php if(!is_numeric($valoresTotales['despacho'])): ?>
        <span class="number">Por pagar</span>
    <?php else: ?>
        <span class="number">$<?= number_format($valoresTotales['despacho'], 0, ',', '.') ?></span>
    <?php endif; ?>
</div>

<div class="row_total">
    <span>Total:</span>
    <span class="total_cart number">$<?= number_format($valoresTotales['total'], 0, ',', '.') ?></span>
</div>