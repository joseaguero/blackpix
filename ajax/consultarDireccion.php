<?php 

include('../admin/conf.php');

$id = mysqli_real_escape_string($conexion, $_POST['id']);

$query = consulta_bd('pais_id, region_id, comuna_id, calle, nombre', 'clientes_direcciones', "id = $id", '');

$out['data']['nombre'] 		= $query[0][4];
$out['data']['pais']		= $query[0][0];
$out['data']['region']		= $query[0][1];
$out['data']['comuna']		= $query[0][2];
$out['data']['direccion'] 	= $query[0][3];

if ($query[0][0] == 1) {

	$regiones = consulta_bd('id, nombre', 'regiones', '', 'posicion asc');
	$comunas = consulta_bd('id, nombre', 'comunas', "region_id = {$query[0][1]}", 'nombre asc');

	$contador_reg = 0;
	foreach ($regiones as $reg) {
		$out['regiones'][$contador_reg]['id'] 		= $reg[0];
		$out['regiones'][$contador_reg]['nombre'] 	= $reg[1];
		$out['regiones'][$contador_reg]['selected'] = ($reg[0] == $query[0][1]) ? 'selected' : '';

		$contador_reg++;
	}

	$contador_com = 0;
	foreach ($comunas as $com) {
		$out['comunas'][$contador_com]['id'] 		= $com[0];
		$out['comunas'][$contador_com]['nombre'] 	= $com[1];
		$out['comunas'][$contador_com]['selected'] 	= ($com[0] == $query[0][2]) ? 'selected' : '';

		$contador_com++;
	}
	
}

echo json_encode($out);

?>