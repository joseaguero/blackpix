<?php 

include('../admin/conf.php');

$producto_id = mysqli_real_escape_string($conexion, $_POST['id']);
$lista = mysqli_real_escape_string($conexion, $_POST['lista']); 

$action = mysqli_real_escape_string($conexion, $_POST['action']);

$carroActual = json_decode($_COOKIE['cart_alfa_cm'], true);

switch ($action) {
	case 'change':
		$cantidad = mysqli_real_escape_string($conexion, $_POST['cantidad']); 

		// Se cambia la cantidad en el carro
		$valida = false;
		$indexValidado = 0;
		foreach ($carroActual as $index => $carro) {
			if ($carro['id'] == $producto_id) {
				$indexValidado = $index;
				$valida = true;
				break;
			}
		}

		if ($valida) {
			$carroActual[$indexValidado]['cantidad'] = $cantidad;
			$out['status'] = 'success';
			$out['message'] = 'Carro actualizado';
		}else{
			$out['status'] = 'error';
			$out['message'] = 'Error al actualizar el carro';
		}
	break;

	case 'delete':

		$valida = false;
		$indexValidado = 0;
		foreach ($carroActual as $index => $carro) {
			if ($carro['id'] == $producto_id) {
				$indexValidado = $index;
				$valida = true;
				break;
			}
		}

		if ($valida) {
			unset($carroActual[$indexValidado]);
			$out['status'] = 'success';
			$out['message'] = 'Carro actualizado';
		}else{
			$out['status'] = 'error';
			$out['message'] = 'Error al actualizar el carro';
		}

	break;
	
	default:
		# code...
		break;
}

$arr = json_encode($carroActual);

setcookie("cart_alfa_cm", "$arr", time() + (365 * 24 * 60 * 60), "/");

echo json_encode($out);

?>