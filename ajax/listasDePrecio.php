<?php 

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$producto_id = mysqli_real_escape_string($conexion, $_GET['producto_id']);
$where = mysqli_real_escape_string($conexion, $_GET['where']);

?>

<?php 
	$subproductos = consulta_bd("s.id, s.nombre, t.nombre","subproductos s JOIN tamaños t ON t.id = s.tamaño_id","","s.id asc"); 
?>

<?php //$contador_ed = 0; 
	//foreach ($ediciones as $ed):
    //$num_ed = $contador_ed+1;
	for($i=0; $i<sizeof($subproductos ); $i++) {
		$pd = consulta_bd("id, nombre, bajada, precio, descuento, principal","productos_detalles","producto_id = $producto_id and publicado = 1 and subproducto_id = ".$subproductos[$i][0],"posicion asc");
		$cantSub = mysqli_affected_rows($conexion);
		
		//if($cantSub > 0){
?>
   
<div class="content_tab <?= ($i == 0) ? 'activado' : '' ?>" id="tab-<?= $subproductos[$i][0] ?>" data-rel="<?= $subproductos[$i][0] ?>" data-action="">

        <div class="grid-tt">
            <div class="tamanio">
                <span>tamaño</span>
                <span><?= $subproductos[$i][2]; ?></span>
            </div> 
            <div class="tiraje"><span>tiraje:</span> 1/20</div> 
        </div>

        <?php //$lista_precio = consulta_bd('lp.id, lp.nombre, lpp.precio, lpp.descuento, lp.descripcion', 'lista_precio_producto lpp JOIN lista_precios lp ON lp.id = lpp.lista_precio_id', "lpp.productos_detalle_id = $ed[0]", ''); ?>

        <div class="list-radio">
            <?php 
			//$contador_sub = 0; 
			//foreach ($lista_precio as $lp):
                for($j=0; $j<sizeof($pd); $j++) {
				$checked_c = ($pd[$j][5] == 1) ? 'checked="checked"' : ''; ?>
                <div class="row">
                    <input type="radio" name="radio_ediciones_a" class="radio_ediciones" value="<?= $pd[$j][0]; ?>" <?= $checked_c ?>>
                    <label><?= $pd[$j][1]; ?>
                        <?php if ($pd[$j][2] != null and trim($pd[$j][2]) != ''): ?>
                            <span><?= strip_tags($pd[$j][2]) ?></span>
                        <?php endif ?>
                    </label>
                    <div class="precio_ficha">
                        <?php if ($pd[$j][4] > 0): ?>
                            <span class="descuento">$<?= cambiarMoneda($pd[$j][3]) ?></span>
                            <span class="precio_final">$<?= cambiarMoneda($pd[$j][4]) ?></span>
                        <?php else: ?>
                            <span class="precio_final">$<?= cambiarMoneda($pd[$j][3]) ?></span>
                        <?php endif ?>
                        
                    </div>
                </div><!--FIn ROW-->
            <?php } ?>

            <div class="clearfix"></div>

            <?php if ($where == 'carro'): ?>
				<a href="javascript:void(0)" onclick="moveToCartLista(this)" class="btnMoverAlCarroLista" data-id="<?= $producto_id ?>">Mover al carro</a>
			<?php else: ?>
				<a href="javascript:void(0)" onclick="moveToCart(this)" class="btnMoverAlCarroLista" data-id="<?= $producto_id ?>">Mover al carro</a>
			<?php endif ?>
        </div>
                      
    </div><!--/* aca termina el tab*/-->
	<?php // } ?>
<?php }  ?>