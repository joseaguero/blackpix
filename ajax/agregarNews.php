<?php 

include('../admin/conf.php');
include("../admin/includes/tienda/cart/inc/functions.inc.php");
require '../vendor/autoload.php';
include("../tienda/mailchimp.class.php");

$email = mysqli_real_escape_string($conexion, $_POST['email']);

$existe_correo = consulta_bd("email","newsletter","email ='$email'","id desc");

if (!is_array($existe_correo)) {
	$mailchimp_module = opciones('mailchimp');
	insert_bd("newsletter","email, fecha_creacion","'$email', now()");
	/*aca debo enviar el correo a mailchimp*/

	if ($mailchimp_module == '1') {
		$newsletter_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'newsletter'", '');
		$todos_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');

		$apikey = opciones('key_mailchimp');

		if ($apikey != null AND trim($apikey) != '') {
			$data['email'] = $correo;
			$data['listId'] = $newsletter_list[0][0];

			$mcc = new MailChimpClient($apikey);

			$mcc->subscribeNewsletter($data);
		}
	}

	$out['status'] = 'success';
	$out['message'] = 'Correo ingresado correctamente.';
	
}else{
	mysqli_close($conexion);
	$out['status'] = 'error';
	$out['message'] = 'Correo ingresado ya se encuentra en nuestros registros.';
}

echo json_encode($out);

?>