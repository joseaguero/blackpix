<?php 

include('../admin/conf.php');

$action = mysqli_real_escape_string($conexion, $_POST['action']);

switch ($action) {
	case 'regiones':
		$query = consulta_bd('id, nombre', 'regiones', '', 'posicion asc');

		$contador = 0;
		foreach ($query as $aux) {
			$out[$contador]['id'] = $aux[0];
			$out[$contador]['nombre'] = $aux[1];

			$contador++;
		}

		echo json_encode($out);
	break;

	case 'comunas':
		$region_id = (int)mysqli_real_escape_string($conexion, $_POST['region_id']);
		$query = consulta_bd('id, nombre', 'comunas', "region_id = {$region_id}", 'nombre asc');

		$contador = 0;
		foreach ($query as $aux) {
			$out[$contador]['id'] = $aux[0];
			$out[$contador]['nombre'] = $aux[1];

			$contador++;
		}

		echo json_encode($out);
	break;
}

?>