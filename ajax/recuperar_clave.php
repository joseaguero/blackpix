<?php 
	include("../admin/conf.php");
	require '../PHPMailer/PHPMailerAutoload.php';
	
	$correo = (isset($_POST[email])) ? mysqli_real_escape_string($conexion, $_POST[email]) : 0;
	$existe = consulta_bd("id, nombre", "clientes", "email = '$correo'", "");
	$cant2 = mysqli_affected_rows($conexion);
	//Consulto por el correo, si existe ejecuto la sentencia
	if ($cant2 > 0){
		$usuario = $existe[0][1];
		$id = $existe[0][0];
		 	$nombre_sitio = opciones("nombre_cliente");
			$url_sitio = opciones("url_sitio");;
			$logo = "https://moldeable.com/img/logo-moldeable.png";
			$asunto = "Nueva contraseña.";
			$color_logo = "#666";
			
			$rand = rand(1000, 999999);
			// si el correo no existe indico que no esta en los registros
			$pass_nueva = "NUEVACLAVE".$rand;
			$pass_encrypted =  md5($pass_nueva);
			$password_salt = md5($rand);
			$password = hash('md5',$pass_encrypted.$password_salt);
			
			$update = update_bd("clientes","clave = '$password', password_salt = '$password_salt'","id=$id");
		
		
			
			$msg2 = '
				<html>
					<head>
					<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
					<title>'.$nombre_sitio.'</title>
					<style type="text/css">
							p, ul, a { 
								color:#666; 
								font-family: "Open Sans", sans-serif;
								background-color: #ffffff; 
								font-weight:300;
							}
							strong{
								font-weight:600;
							}
							a {
								color:#666;
							}
						</style>
					</head>
					<body style="background:#f9f9f9;">
						<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
						
								<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
									<tr>
										<th align="left" width="50%">
											<p>
												<a href="'.$url_sitio.'" style="color:#8CC63F;">
													<img src="'.$logo.'" alt="'.$logo.'" border="0" width="214"/>
												</a>
											</p>
										</th>
									</tr>
								</table>
								<br/><br/>
									
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td valign="top">
												<p><strong>Estimado '.$usuario.':</strong></p>
												<p>Se ha solicitado recuperar clave a través de la página web '.$nombre_sitio.'. <br />
												se recomienda cambiarla en cuanto reciba este correo:<br /></p>
												
												<p>Nueva clave: '.$pass_nueva.'</p>
												
												<p>Muchas gracias<br /> Atte,</p>
												<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
											</td>
										</tr>
									</table>
						</div>
					</body>
				</html>';
	
			
			//Create a new PHPMailer instance
			$mail = new PHPMailer;
			$mail->setFrom('no-responder@moldeable.com', 'No responder');
			//$mail->addAddress('htorres@moldeable.com', 'Hernan');
			$mail->addAddress($correo, $usuario);
			$mail->CharSet = 'UTF-8';
			$mail->Subject = $asunto;
			$mail->msgHTML($msg2);
			$mail->AltBody = $msg2;

			//send the message, check for errors
			if (!$mail->send()) {
			    //echo "Mailer Error: " . $mail->ErrorInfo;
				header("Location: ../recuperar-clave?a=2&msje=No fue posible enviar una nueva clave..");
				die("2");
			} else {
			    //echo "Message sent!";
				header("Location: ../inicio-sesion?a=1&msje=te enviamos una nueva clave a tu email: $correo.");
				die("1");
			}
			//header("Location: ../login?msje=te enviamos una nueva clave a tu email: $correo.");
			
	} else {
		//si no existe retorno e indico que no esta en los registros
		header("Location: ../recuperar-clave?a=2&msje=Usuario no registrado.");
		die("3");
	}

?>