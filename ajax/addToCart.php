<?php

include('../admin/conf.php');

$producto = (isset($_POST['producto'])) ? mysqli_real_escape_string($conexion, $_POST['producto']) : 0; //mysqli_real_escape_string($conexion, $_POST['producto']);
//$lista = mysqli_real_escape_string($conexion, $_POST['lista']);
$cantidad = (isset($_POST['cantidad'])) ? mysqli_real_escape_string($conexion, $_POST['cantidad']) : 0;
$marco = (isset($_POST['marco'])) ? mysqli_real_escape_string($conexion, $_POST['marco']) : 0;
//mysqli_real_escape_string($conexion, $_POST['cantidad']);

$stockProducto = consulta_bd('stock, comprados, nombre', 'productos_detalles', "id = $producto", '');

$carroActual = json_decode($_COOKIE['cart_alfa_cm'], true);

$agregado = false;

$nuevoItem = array(
	'id' => (int)$producto,
	//'lista' => (int)$lista,
	'cantidad' => (int)$cantidad,
	'marco' => (strtolower($stockProducto[0][2]) != 'obra impresa') ? $marco : ''
);

if ($carroActual == null) {

	// Si el carro está vacío solo verifico el stock del producto y si está OK lo agrego al carro.
	if (!is_null($nuevoItem)) {

		if ($stockProducto[0][0] - $stockProducto[0][1] > 0 and $cantidad <= $stockProducto[0][0]) {
			$carroActual[] = $nuevoItem;
			$agregado = true;
		} else {
			$agregado = false;
		}
	}
} else {
	$stockReal = $stockProducto[0][0] - $stockProducto[0][1];
	$valida = false;
	$indexValidado = 0;
	// Si el carro no está vacío se validará si ya hay del mismo producto en el carro y se verifica el Stock.
	foreach ($carroActual as $index => $carro) {
		if ($carro['id'] == $producto) {
			$indexValidado = $index;
			$valida = true;
			break;
		}
	}

	if ($valida) {
		$cantidadActualCarro = $carroActual[$indexValidado]['cantidad'];
		$cantidadTotal = $cantidadActualCarro + $cantidad;

		if ($stockReal > 0 and $cantidadTotal <= $stockReal) {
			$carroActual[$indexValidado]['cantidad'] = $cantidadTotal;
			$agregado = true;
		} else {
			$agregado = false;
		}
	} else {
		if ($stockReal > 0 and $cantidad <= $stockReal) {
			$carroActual[] = $nuevoItem;
			$agregado = true;
		} else {
			$agregado = false;
		}
	}
}

if ($agregado) {
	$output = array('status' => 'success', 'message' => 'Agregado con exito al carro');
} else {
	$output = array('status' => 'error', 'message' => 'Stock insuficiente');
}

$arr = json_encode($carroActual);

setcookie("cart_alfa_cm", "$arr", time() + (365 * 24 * 60 * 60), "/");

echo json_encode($output);
