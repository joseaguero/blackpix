<?php 
	include("../admin/conf.php");

	$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
	$email = mysqli_real_escape_string($conexion, $_POST['email']);
	$telefono = mysqli_real_escape_string($conexion, $_POST['telefono']);
	$password = mysqli_real_escape_string($conexion, $_POST['password']);
	$repassword = mysqli_real_escape_string($conexion, $_POST['re-password']);

	if (trim($nombre) != '' AND trim($email) != '' AND trim($telefono) != '' AND trim($password) != '' AND trim($repassword) != '') {

		if ($password == $repassword) {

			$pass_encrypted = md5($password);
			$rand = rand(1000, 9999);
		    $password_salt = md5($rand);
		    $password = hash('md5',$pass_encrypted.$password_salt);

		    $existe = consulta_bd("id","clientes","email='$email'","");
		    if (!is_array($existe)) {

		    	$insert = insert_bd('clientes', 'nombre, email, telefono, clave, password_salt, activo', "'$nombre', '$email', '$telefono', '$password', '$password_salt', 1");
		    	if ($insert) {
		    		$error = "Cuenta creada";
		    		$tipo = 1;
		    	}else{
		    		$error = "Error al intentar crear la cuenta";
		    		$tipo = 2;
		    	}
		    	
		    }else{
				$error = "Correo ingresado ya se encuentra en uso.";
				$tipo = 2;
		    }
			
		}else{
			$error = "Contraseñas ingresadas no coinciden.";
			$tipo = 2;
		}
		
	}else{
		$error = "Debes completar todos los campos obligatorios.";
		$tipo = 2;
	}

	header("Location: {$url_base}login?a=$tipo&msje=$error");
	
?>