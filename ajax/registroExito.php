<?php 
	include("../admin/conf.php");

	$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
	$email = mysqli_real_escape_string($conexion, $_POST['email']);
	$telefono = mysqli_real_escape_string($conexion, $_POST['telefono']);
	$password = mysqli_real_escape_string($conexion, $_POST['password']);
	$repassword = mysqli_real_escape_string($conexion, $_POST['re-password']);

	$cliente_id = mysqli_real_escape_string($conexion, $_POST['cliente']);

	$existeClienteEmail = consulta_bd('id', 'clientes', "id = $cliente_id and email = '$email'", '');

	if (is_array($existeClienteEmail)) {

		if (trim($nombre) != '' AND trim($email) != '' AND trim($telefono) != '' AND trim($password) != '' AND trim($repassword) != '') {

			if ($password == $repassword) {

				$pass_encrypted = md5($password);
				$rand = rand(1000, 9999);
			    $password_salt = md5($rand);
			    $password = hash('md5',$pass_encrypted.$password_salt);

		    	$insert = update_bd('clientes', "nombre = '$nombre', email = '$email', telefono = '$telefono', clave = '$password', password_salt = '$password_salt', activo = 1", "id = $cliente_id and email = '$email'");
		    	if ($insert) {
		    		$error = "Cuenta creada";
		    		$tipo = 1;
		    	}else{
		    		$error = "Error al intentar crear la cuenta";
		    		$tipo = 2;
		    	}
				
			}else{
				$error = "Contraseñas ingresadas no coinciden.";
				$tipo = 2;
			}
			
		}else{
			$error = "Debes completar todos los campos obligatorios.";
			$tipo = 2;
		}
		
	}else{
		$error = "Datos ingresados no coinciden con el usuario.";
		$tipo = 2;
	}

	header("Location: {$url_base}login?a=$tipo&msje=$error");
	
?>