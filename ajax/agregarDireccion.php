<?php 

include('../admin/conf.php');

date_default_timezone_set('America/Santiago');

$nombre 	= mysqli_real_escape_string($conexion, $_POST['nombre']);
$pais 		= (int)mysqli_real_escape_string($conexion, $_POST['pais']);
$region 	= (int)mysqli_real_escape_string($conexion, $_POST['region']);
$comuna 	= (int)mysqli_real_escape_string($conexion, $_POST['comuna']);
$direccion 	= mysqli_real_escape_string($conexion, $_POST['direccion']);

$usuarioId 	= (int)$_COOKIE['usuario_id'];

$fechaHoy 	= date('Y-m-d H:i:s', time());

$error 		= false;

if ($pais > 1) {
	if (trim($nombre) == '' OR trim($direccion) == '') {
		$error = true;
	}
}else{
	if (trim($nombre) == '' OR $pais == 0 OR $region == 0 OR $comuna == 0 OR trim($direccion) == '') {
		$error = true;
	}
}

if (!$error) {
	if ($pais == 1) {
		$insertDireccion = insert_bd('clientes_direcciones', 'cliente_id, pais_id, region_id, comuna_id, calle, fecha_creacion, nombre', "$usuarioId, $pais, $region, $comuna, '$direccion', '$fechaHoy', '$nombre'");
	}else{
		$insertDireccion = insert_bd('clientes_direcciones', 'cliente_id, pais_id, calle, fecha_creacion, nombre', "$usuarioId, $pais, '$direccion', '$fechaHoy', '$nombre'");
	}
	

	if ($insertDireccion) {
		$out = array( 'status' => 'success', 'message' => 'Direccion agregada con éxito' );
	}else{
		$out = array( 'status' => 'error', 'message' => 'Error al agregar la dirección' );
	}
}else{
	$out = array( 'status' => 'error', 'message' => 'Debes completar todos los campos' );
}

echo json_encode($out);

?>