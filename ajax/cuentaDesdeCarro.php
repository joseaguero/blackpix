<?php 
	include("../admin/conf.php");

	$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
	$email = mysqli_real_escape_string($conexion, $_POST['email']);
	$telefono = mysqli_real_escape_string($conexion, $_POST['telefono']);
	$password = mysqli_real_escape_string($conexion, $_POST['password']);
	$repassword = mysqli_real_escape_string($conexion, $_POST['repassword']);

	if (trim($nombre) != '' AND trim($email) != '' AND trim($telefono) != '' AND trim($password) != '' AND trim($repassword) != '') {

		if ($password == $repassword) {

			$pass_encrypted = md5($password);
			$rand = rand(1000, 9999);
		    $password_salt = md5($rand);
		    $password = hash('md5',$pass_encrypted.$password_salt);

		    $existe = consulta_bd("id","clientes","email='$correo'","");
		    if (!is_array($existe)) {

		    	$insert = insert_bd('clientes', 'nombre, email, telefono, clave, password_salt, activo', "'$nombre', '$email', '$telefono', '$password', '$password_salt', 1");
		    	if ($insert) {
		    		$error = "Cuenta creada";
		    		$tipo = 'success';
		    	}else{
		    		$error = "Error al intentar crear la cuenta";
		    		$tipo = 'error';
		    	}
		    	
		    }else{
				$error = "Correo ingresado ya se encuentra en uso.";
				$tipo = 'error';
		    }
			
		}else{
			$error = "Contraseñas ingresadas no coinciden.";
			$tipo = 'error';
		}
		
	}else{
		$error = "Debes completar todos los campos obligatorios.";
		$tipo = 'error';
	}

	$out = array('status' => $tipo, 'message' => $error);

	echo json_encode($out);
	
?>