<?php 

include('../admin/conf.php');
require_once('../PHPMailer/PHPMailerAutoload.php');
date_default_timezone_set('America/Santiago');

$email = mysqli_real_escape_string($conexion, $_POST['email']);

$usuario = consulta_bd('id, nombre', 'clientes', "email = '$email'", '');

if (is_array($usuario)) {

	// Datos sitio
	$url_sitio = $url_base;
	$color_logo = opciones('color_logo');
	$logo = opciones('logo_mail');
	$asunto = 'Nueva contraseña.';
	$nombre_sitio = opciones('nombre_cliente');

	$rand = rand(1000, 999999);
	// si el correo no existe indico que no esta en los registros
	$pass_nueva = "BP".$rand;
	$pass_encrypted =  md5($pass_nueva);
	$password_salt = md5($rand);
	$password = hash('md5',$pass_encrypted.$password_salt);

	$msg2 = '
		<html>
			<head>
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
			<title>'.$nombre_sitio.'</title>
			<style type="text/css">
					p, ul, a { 
						color:#666; 
						font-family: "Open Sans", sans-serif;
						background-color: #ffffff; 
						font-weight:300;
					}
					strong{
						font-weight:600;
					}
					a {
						color:#666;
					}
				</style>
			</head>
			<body style="background:#f9f9f9;">
				<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
				
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
							<tr>
								<th align="left" width="50%">
									<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" />
										</a>
									</p>
								</th>
							</tr>
						</table>
						<br/><br/>
							
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td valign="top">
										<p><strong>Estimado '.$usuario[0][1].':</strong></p>
										<p>Se ha solicitado recuperar clave a través de la página web '.$nombre_sitio.'. <br />
										se recomienda cambiarla en cuanto reciba este correo:<br /></p>
										
										<p>Nueva clave: '.$pass_nueva.'</p>
										
										<p>Muchas gracias<br /> Atte,</p>
										<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
									</td>
								</tr>
							</table>
				</div>
			</body>
		</html>';

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");

	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	//Set an alternative reply-to address

	$mail->addAddress($email, $usuario[0][1]);
	$mail->Subject = $asunto;
	$mail->msgHTML($msg2);
	$mail->AltBody = $msg2;
	$mail->CharSet = 'UTF-8';

	//send the message, check for errors
	if (!$mail->send()) {
		$message = 'Error al restablecer contraseña';
		$tipo = 'error';
	} else {
		$update = update_bd("clientes","clave = '$password', password_salt = '$password_salt'","email='$email'");
	    
	    $message = 'Nueva contraseña enviada con exito';
		$tipo = 'success';
	}
	
}else{
	$message = 'Email ingresado no se encuentra en nuestros registros';
	$tipo = 'error';
}

$out = array('status' => $tipo, 'message' => $message);

echo json_encode($out);

?>