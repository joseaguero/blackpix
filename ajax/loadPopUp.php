<?php

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$producto_id = (isset($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
//$lista = mysqli_real_escape_string($conexion, $_GET['lista']);
$cantidad = (isset($_GET['cantidad'])) ? mysqli_real_escape_string($conexion, $_GET['cantidad']) : 0;

//$query = consulta_bd('p.nombre, p.thumbs, a.nombre, t.nombre, pd.id, pd.precio, pd.descuento', 'productos p JOIN productos_detalles pd ON pd.producto_id = p.id JOIN artistas a ON a.id = p.artista_id JOIN tamaños t ON t.id = pd.tamaño_id', "pd.id = $producto_id", '');

$query = consulta_bd("p.nombre, p.thumbs, a.nombre, t.nombre, pd.precio, pd.descuento, pd.bajada", "productos p, productos_detalles pd, artistas a, tamaños t", "pd.producto_id = p.id and a.id = p.artista_id and t.id = pd.tamaño_id and  pd.id = $producto_id", "");


$thumb = ($query[0][1] != null and trim($query[0][1]) != '' and is_file("../imagenes/productos/{$query[0][1]}")) ? imagen("imagenes/productos/", $query[0][1]) : imagen("img/", "sinFotoGrid.jpg");

?>

<div class="cl">
	<img src="<?= $thumb ?>">
</div>
<div class="cl">
	<div class="txt_up"><?= $query[0][0] ?></div>
	<div class="txt_del"><?= $query[0][2] ?></div>
	<!--Nombre artista-->
	<div class="txt_up"><?= $query[0][3] ?></div>
	<!--medidas-->
	<div class="txt_del_2"><?= $query[0][6] ?></div>
</div>
<div class="cl">
	<?php if ($query[0][5] > 0) : ?>
		<div class="descuento"><?= cambiarMoneda($query[0][4]) ?></div>
		<div class="price"><?= cambiarMoneda($query[0][5]) ?></div>
	<?php else : ?>
		<div class="price"><?= cambiarMoneda($query[0][4]) ?></div>
	<?php endif ?>


</div>