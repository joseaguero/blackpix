<?php 
include("../admin/conf.php");
include("../admin/includes/tienda/cart/inc/functions.inc.php");
require '../vendor/autoload.php';
include("../tienda/mailchimp.class.php");

$correo = (isset($_POST[correo])) ? mysqli_real_escape_string($conexion, $_POST[correo]) : 0;

$existe_correo = consulta_bd("email","newsletter","email ='$correo'","id desc");
$cant = mysqli_affected_rows($conexion);

if($cant > 0){
	mysqli_close($conexion);
	die('2');
}else{
	insert_bd("newsletter","email, fecha_creacion","'$correo', now()");
	/*aca debo enviar el correo a mailchimp*/

	$newsletter_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'newsletter'", '');
	$todos_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');

	$apikey = opciones('key_mailchimp');

	$data['email'] = $correo;
	$data['listId'] = $newsletter_list[0][0];

	$data_all['email'] = $correo;
	$data_all['listId'] = $todos_list[0][0];

	$mcc = new MailChimpClient($apikey);

	$mcc->subscribeNewsletter($data);
	$mcc->subscribeNewsletter($data_all);
	
	/*aca debo enviar el correo a mailchimp*/
	mysqli_close($conexion);
	die('1');
}

?>
