<?php 

include('../admin/conf.php');

date_default_timezone_set('America/Santiago');

$nombre 	= mysqli_real_escape_string($conexion, $_POST['nombre']);
$pais 		= (int)mysqli_real_escape_string($conexion, $_POST['pais']);
$region 	= (int)mysqli_real_escape_string($conexion, $_POST['region']);
$comuna 	= (int)mysqli_real_escape_string($conexion, $_POST['comuna']);
$direccion 	= mysqli_real_escape_string($conexion, $_POST['direccion']);

$dirId 		= (int)mysqli_real_escape_string($conexion, $_POST['dirId']);

$usuarioId 	= (int)$_COOKIE['usuario_id'];

$fechaHoy 	= date('Y-m-d H:i:s', time());

$error 		= false;

if ($pais > 1) {
	if (trim($nombre) == '' OR trim($direccion) == '') {
		$error = true;
	}
}else{
	if (trim($nombre) == '' OR $pais == 0 OR $region == 0 OR $comuna == 0 OR trim($direccion) == '') {
		$error = true;
	}
}

if (!$error) {
	if ($pais == 1) {
		$updateDireccion = update_bd('clientes_direcciones', "cliente_id = $usuarioId, pais_id = $pais, region_id = $region, comuna_id = $comuna, calle = '$direccion', nombre = '$nombre', fecha_modificacion = '$fechaHoy'", "id = $dirId");
	}else{
		$updateDireccion = update_bd('clientes_direcciones', "cliente_id = $usuarioId, pais_id = $pais, region_id = NULL, comuna_id = NULL, calle = '$direccion', nombre = '$nombre', fecha_modificacion = '$fechaHoy'", "id = $dirId");
	}
	

	if ($updateDireccion) {
		$out = array( 'status' => 'success', 'message' => 'Direccion editada con éxito' );
	}else{
		$out = array( 'status' => 'error', 'message' => 'Error al editar la dirección' );
	}
}else{
	$out = array( 'status' => 'error', 'message' => 'Debes completar todos los campos' );
}

echo json_encode($out);

?>