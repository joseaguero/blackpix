<div class="parcheHeader"></div>
<header>
    <div class="top_gradient"></div>
	<div class="contenedor_header">
        <div class="btn_menu">
            <div class="hamburger">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <h1 class="logo">
            <a href="home">
                
            </a>
        </h1><!--Fin logo-->

        <div class="main_menu">
            <nav>
                <ul>
                    <li>
                        <a href="home" class="<?= ($op == 'home') ? 'active' : '' ?>">Home</a>
                    </li>
                    <?php
                    $lineas = consulta_bd('id, nombre', 'lineas', 'publicado = 1', 'posicion asc');
                    foreach ($lineas as $lh): 
                        $categorias = consulta_bd('id, nombre, banner', 'categorias', "publicada = 1 AND linea_id = {$lh[0]}", 'posicion asc'); 
                        $active = ($op == 'lineas' AND $lh[0] == (int)$_GET['id']) ? 'class="active"' : ''; ?>
                        <li><a href="lineas/<?= $lh[0] ?>/<?= url_amigables($lh[1]) ?>" <?= $active ?>><?= $lh[1] ?></a>
                            <span class="arrow-nav-xs" data-open="0"><img src="img/arrow-footer.png" /></span>
                            
                            <?php if (is_array($categorias)): ?>

                            <div class="submenu">
                                <ul class="content-submenu">
                                    <?php foreach ($categorias as $ch): ?>
                                        <li>
                                            <a href="categorias/<?= $ch[0] ?>/<?= url_amigables($ch[1]) ?>" class="item_sub">
                                                <?php if ($ch[2] != '' and $ch[2] != null): ?>
                                                    <img src="<?= imagen("imagenes/categorias/",$ch[2]);?>">
                                                <?php else: ?>
                                                    <img src="<?= imagen("img/","cat-notimage.jpg"); ?>">
                                                <?php endif ?>
                                                
                                                <span><?= $ch[1] ?></span>
                                            </a>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                                
                            <?php endif ?>

                        </li>
                    <?php endforeach ?>
                    <li><a href="artistas" class="<?= ($op == 'artistas') ? 'active' : '' ?>">Artistas</a></li>
                    <li><a href="contacto" class="<?= ($op == 'contacto') ? 'active' : '' ?>">Contacto</a></li>
                </ul>
            </nav>
        </div>

        
        <div class="i_carro">
            <div class="tipo_moneda">
                <a href="javascript:void(0)" data-action="pesos" class="moneda <?= (!isset($_SESSION[usd])) ? 'active' : '' ?>">CLP</a>
                /
                <a href="javascript:void(0)" data-action="usd" class="moneda <?= (isset($_SESSION[usd])) ? 'active' : '' ?>">USD</a>
            </div>


            <div class="carro_iconos">
                <a href="javascript:void(0)" class="ahead i-search"></a>
                <?php if (isset($_COOKIE['usuario_id'])): ?>
                    <a href="mi-cuenta" class="ahead i-profile"></a>
                    <a href="productos-guardados" class="ahead i-heart"></a>
                <?php else: ?>
                    <a href="login" class="ahead i-profile"></a>
                    <a href="javascript:void(0)" class="ahead i-heart notloginFav"></a>
                <?php endif ?>
                
                <a href="mi-carro" class="ahead i-cart carro_i">
                    <span class="cantidad_carro">
                        <?= qty_pro() ?>
                    </span>
                </a>
            </div>
        </div>

        <!-- Buscador -->
        <!-- <div class="buscador">
            <form method="get" action="busquedas" style="float:none;">
              <input type="text" name="buscar" class="campo_buscador" placeholder="<?php if(isset($_GET[buscar])){echo $_GET[buscar];} else {echo 'Buscar producto';}?>">
               <input class="btn_search" type="submit" value="buscar">
            </form>
        </div> -->
        <!-- -->
    
</div><!--fin contenedor header-->

</header>