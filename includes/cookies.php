<?php
//asigno valor a la cookie solo cuando estoy en la ficha de un producto
if($op == 'ficha'){
	if(!isset($_COOKIE[productosRecientes])){
		//creo la cookie y le asigno un valor
		$idProductoCookie = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
		if($idProductoCookie != ""){
			setcookie("productosRecientes", "$idProductoCookie", time() + (365 * 24 * 60 * 60), "/");
			}
		
	} else {
		$actuales = $_COOKIE[productosRecientes];
		$idProductoCookie = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
		$PR = explode(",", $actuales );
		$estaAgregado = 0;
		foreach($PR as $productoAgregado){
			if ($productoAgregado == $idProductoCookie){
				$estaAgregado = 1;
				}
		}//fin foreach
		if($idProductoCookie != "" and $estaAgregado == 0){
			setcookie("productosRecientes", "$actuales, $idProductoCookie", time() + (365 * 24 * 60 * 60), "/");
			}
		
	}
}
//Fin asigno valor a la cookie solo cuando estoy en la ficha de un producto


/*Cuando el usuario esta logeado pasa directo al envio y pago, se salta la identificacion*/
if($op == "identificacion"){
	if(isset($_COOKIE[usuario_id])){
		header('Location: envio-y-pago');
		}
	}
/*Cuando el usuario esta logeado pasa directo al envio y pago, se salta la identificacion*/


?>