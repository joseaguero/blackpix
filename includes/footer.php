<footer class="pt-40 xs_pt-0">
    <div class="container">
        <div class="col-footer">
            <div class="col">
                <img src="img/logo-footer.jpg" id="logo-footer" />
                <div class="title_ft noneDeskt">Blackpix <img src="img/arrow-footer.png" class="arrow-footer" /></div>
                <div class="list mt-25 xs_mt-0 open-list-footer">
                    <li><a href="somos">¿Quiénes somos?</a></li>
                    <!-- <li><a href="#">¿Qué vendemos?</a></li>
                    <li><a href="#">Nuestras tiendas</a></li> -->
                    <li><a href="https://drive.google.com/open?id=1iRRc1OMIJEvFPlU9MYYBL4EDBGVPo56y" target="_blank">Catálogo</a></li>
                </div>

            </div>
            <div class="col">
                <div class="title_ft">Servicio al cliente <img src="img/arrow-footer.png" class="arrow-footer" /></div>
                <div class="list mt-30 xs_mt-0 open-list-footer">
                    <li><a href="despacho">Despacho</a></li>
                    <li><a href="cambios-y-devoluciones">Cambios y devoluciones</a></li>
                    <li><a href="terminos-y-condiciones">Términos y condiciones</a></li>
                    <li><a href="politicas-de-privacidad">Políticas de privacidad</a></li>
                    <!-- <li><a href="#">Información legal</a></li> -->
                </div>
            </div>
            <div class="col">
                <div class="title_ft">Contacto <img src="img/arrow-footer.png" class="arrow-footer" /></div>
                <div class="lista-contacto open-list-footer">
                    <div class="row">
                        <img src="img/iphoneft.png">
                        <a href="tel:+56991628747">(+56) 9 9162 8747</a>
                        <a href="tel:+56222780099">(2) 22780099</a>
                    </div>

                    <div class="row">
                        <img src="img/imailft.png">
                        <a href="mailto:leon.blackpix@gmail.com">leon.blackpix@gmail.com</a>
                    </div>

                    <div class="row">
                        <img src="img/imapft.png">
                        <a href="https://www.google.cl/maps/place/Cousin+227,+Providencia,+Regi%C3%B3n+Metropolitana/@-33.4439412,-70.6310041,17z/data=!3m1!4b1!4m5!3m4!1s0x9662c578c702cf17:0x921e68284e2ac73e!8m2!3d-33.4439457!4d-70.6288154" target="_blank">Cousin 0227 Providencia, <br> Santiago, Chile</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="title_ft">Siguenos en: <img src="img/arrow-footer.png" class="arrow-footer" /></div>
                <div class="open-list-footer">
                    <div class="col-rrss">
                        <div class="item"><a href="https://www.facebook.com/blackpix.fineart.9" target="_blank"><img src="img/i-face.png"></a></div>
                        <div class="item"><a href="https://www.instagram.com/blackpix_fineart/" target="_blank"><img src="img/i-insta.png"></a></div>
                        <!-- <div class="item"><img src="img/i-twitter.png"></div>
                        <div class="item"><img src="img/i-youtube.png"></div> -->
                    </div>

                    <img src="img/webpayft.jpg" style="margin-right: 15px;" class="logowpay-footer">
                    <img src="img/mpagoft.jpg" class="logompago-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-footer">
        <div class="container">
            <span>Todos los derechos reservados. Blackpix <?= date("Y", time()) ?>.</span>
            <a class="firma" href="https://www.moldeable.com" target="_blank">
                <img src="img/firma.png" width="20">
            </a>
        </div>
    </div>
</footer>